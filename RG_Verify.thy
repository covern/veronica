theory RG_Verify
  imports Language TypeSystem RG
begin

context lang
begin

definition reachable_coms :: "('level, 'val) com \<Rightarrow> ('level, 'val) com set" where
  "reachable_coms c = com ` {l'. \<exists> l g g'. com l = c \<and> (l, g) \<leadsto>\<^sub>n\<^sup>* (l', g')}"

definition reachable_locs :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state set" where
  "reachable_locs l = {l'. \<exists> g g'. (l, g) \<leadsto>\<^sub>n\<^sup>* (l', g')}"

definition ann_step_implies :: "('level, 'val) com \<Rightarrow> ('level, 'val) ann \<Rightarrow> ('level, 'val) ann \<Rightarrow> bool" (infix "\<longrightarrow>\<^sup>A\<index>" 65) where
  "A \<longrightarrow>\<^sup>A\<^bsub>c\<^esub> B \<equiv> (\<forall> g l g'. g \<Turnstile>\<^sub>P A \<and> com l = c \<and> g' \<in> next_states l g \<longrightarrow> g' \<Turnstile>\<^sub>P B)"

definition ensures_guar :: "('level, 'val) com \<Rightarrow> ('level, 'val) guar \<Rightarrow> bool" (infix "\<Turnstile>\<^sub>G" 65) where
  "c \<Turnstile>\<^sub>G G \<equiv> \<forall> t. guar_guaranteed G t c (active_ann_com c)"

text {* Note that weird usage of sequential composition, for example @{term "(a ;; b) ;; c"} makes
reasoning about annotations annoying. We forbid this for now using the @{term com_wf} predicate.
Perhaps we should show that for every command there is a semantically equivalent well-formed one. *}


fun compute_reachable_coms :: "('level, 'val) com \<Rightarrow> ('level, 'val) com set" where
  "compute_reachable_coms Skip = {Skip}" |
  "compute_reachable_coms ({A} x ::= e) = {{A} x ::= e, Skip}" |
  "compute_reachable_coms ({A} x :D= e) = {{A} x :D= e, Skip}" |
  "compute_reachable_coms ({A} x <- l') = {{A} x <- l', Skip}" |
  "compute_reachable_coms (Out A l' e) = {Out A l' e, Skip}" |
  "compute_reachable_coms (DOut A l' e) = {DOut A l' e, Skip}" |
  "compute_reachable_coms (Acquire A loc) = {Acquire A loc, Skip}" |
  "compute_reachable_coms (Release A loc) = {Release A loc, Skip}" |
  "compute_reachable_coms (If A e c\<^sub>1 c\<^sub>2) = 
    {If A e c\<^sub>1 c\<^sub>2} \<union> compute_reachable_coms c\<^sub>1 \<union> compute_reachable_coms c\<^sub>2" |
  "compute_reachable_coms (While A e I c) =
    {Skip, While A e I c, While I e I c} \<union> (\<lambda> c'. c' ;; While I e I c) ` compute_reachable_coms c" |
  "compute_reachable_coms (c\<^sub>1 ;; c\<^sub>2) = {c\<^sub>1 ;; c\<^sub>2} \<union>
    ((\<lambda> c'. c' ;; c\<^sub>2) ` {c''. c'' \<in> compute_reachable_coms c\<^sub>1 \<and> c'' \<noteq> Skip}) \<union> compute_reachable_coms c\<^sub>2"

definition compute_reachable_locs :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state set" where
  "compute_reachable_locs l = (\<lambda> c'. l\<lparr>com := c'\<rparr>) ` compute_reachable_coms (com l)"

lemma compute_reachable_coms_self:
  "c \<in> compute_reachable_coms c"
  by (induction c, auto)

lemma skip_reachable:
  "Skip \<in> compute_reachable_coms c"
  by (induction c, auto)

lemma reachable_subset:
  assumes "(l, g) \<leadsto> (l', g')"
  shows "compute_reachable_coms (com l') \<subseteq> compute_reachable_coms (com l)"
  using assms
  apply (induction rule: eval.induct, auto)
  using compute_reachable_coms_self apply fastforce
  by (simp add: compute_reachable_coms_self)

lemma reachable_subset_multi:
  assumes "(l, g) \<leadsto>\<^sub>n\<^sup>* (l', g')"
  shows "compute_reachable_coms (com l') \<subseteq> compute_reachable_coms (com l)"
  using assms
proof (induction rule: rtrancl.induct[where r = eval_nondet, split_format(complete)])
  case (rtrancl_refl a b)
  then show ?case by auto
next
  case (rtrancl_into_rtrancl l g l' g' l'' g'')
  from `(l', g') \<leadsto>\<^sub>n (l'', g'')` show ?case
  proof (cases rule: eval_nondet.cases)
    case step
    then show ?thesis
      using rtrancl_into_rtrancl reachable_subset
      by force
  next
    case env
    then show ?thesis 
      using rtrancl_into_rtrancl by auto
  qed
qed

lemma compute_reachable_step:
  assumes "(l, g) \<leadsto> (l', g')"
  shows "com l' \<in> compute_reachable_coms (com l)"
  using assms
  by (induction rule: eval.induct, auto simp: compute_reachable_coms_self)

lemma compute_reachable_coms_correct: "reachable_coms c \<subseteq> compute_reachable_coms c"
  unfolding reachable_coms_def
proof (clarify)
  fix x 
  fix l' l  :: "('level, 'val) local_state"
  fix g g'
  assume c_eq: "c = com l"
  assume "(l, g) \<leadsto>\<^sub>n\<^sup>* (l', g')"
  then show "com l' \<in> compute_reachable_coms (com l)"
  proof (induction rule: rtrancl.induct[where r = eval_nondet, split_format(complete)])
    case (rtrancl_refl l g)
    then show ?case 
      using compute_reachable_coms_self by auto
  next
    case (rtrancl_into_rtrancl l\<^sub>1 g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3)
    then show ?case
      using reachable_subset reachable_subset_multi compute_reachable_step
      by (meson contra_subsetD compute_reachable_coms_self rtrancl.rtrancl_into_rtrancl)
  qed
qed

lemma reachable_loc_same_tid:
  "l' \<in> reachable_locs l \<Longrightarrow> tid l = tid l'"
  unfolding reachable_locs_def using eval_nondet_tid_const_multi
  by auto

lemma compute_reachable_loc_same_tid:
  "l' \<in> compute_reachable_locs l \<Longrightarrow> tid l = tid l'"
  unfolding compute_reachable_locs_def by auto

lemma compute_reachable_locs_correct: "reachable_locs l \<subseteq> compute_reachable_locs l"
proof
  fix l'
  assume "l' \<in> reachable_locs l"
  hence "com l' \<in> reachable_coms (com l)"
    unfolding reachable_locs_def reachable_coms_def
    using imageI by blast
  hence "com l' \<in> compute_reachable_coms (com l)"
    using compute_reachable_coms_correct
    by auto
  then show "l' \<in> compute_reachable_locs l"
    unfolding compute_reachable_locs_def
    using \<open>l' \<in> reachable_locs l\<close> image_iff reachable_loc_same_tid by fastforce
qed

definition next_anns :: 
  "('level, 'val) local_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) ann set" where
  "next_anns l g = active_ann ` {l'. \<exists> g'. (l, g) \<leadsto> (l', g')}"

definition step_property :: "(('level, 'val) local_state \<Rightarrow> bool) \<Rightarrow> bool" where
  "step_property P \<equiv> 
    \<forall> l l'. (\<forall> g. active_ann l = active_ann l' \<and> next_anns l g = next_anns l' g \<and> next_states l g = next_states l' g) \<longrightarrow> P l = P l'"

definition simplify_func :: "(('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state) \<Rightarrow> bool"
  where "simplify_func f \<equiv> \<forall> l g. (next_anns l g = next_anns (f l) g) \<and>
                                  (next_states l g = next_states (f l) g \<and>
                                  (active_ann l = active_ann (f l)))"

subsection "Computational version of next states and next annotations"

(* To satisfy the termination checker: *)
fun comp_next_states_com :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) com \<Rightarrow> 
  ('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state set" where
  "comp_next_states_com l Skip g = {}" |
  "comp_next_states_com l ({A} x ::= e) g = {g\<lparr>mem := (mem g)(x := eval_exp e (mem g))\<rparr>}" |
  "comp_next_states_com l ({A} x :D= e) g =
   (case \<L> x of
     Some lev \<Rightarrow>
       {g\<lparr>mem := (mem g)(x := eval_exp e (mem g)),
          trace := trace g @ [Declassification lev (eval_exp e (mem g)) e]\<rparr>}
    | None \<Rightarrow> {})" |
  "comp_next_states_com l ({A} x <- l') g = {g\<lparr>env := (env g)(l' := ltl (env g l')),
                          mem := (mem g)(x := lhd (env g l')),
                          trace := trace g @ [Input l' (lhd (env g l'))]\<rparr>}" |
  "comp_next_states_com l (Out A l' e ) g = {g\<lparr>trace := trace g @ [Output l' (eval_exp e (mem g)) e]\<rparr>}" |
  "comp_next_states_com l (DOut A l' e ) g = {g\<lparr>trace := trace g @ [Declassification l' (eval_exp e (mem g)) e]\<rparr>}" |
  "comp_next_states_com l (Acquire A loc) g = (if lock_state g loc = None
                          then {g\<lparr>lock_state := (lock_state g)(loc \<mapsto> tid l)\<rparr>}
                          else {})" |
  "comp_next_states_com l (Release A loc) g = (if lock_state g loc = Some (tid l)
                          then {g\<lparr>lock_state := (lock_state g)(loc := None)\<rparr>}
                          else {})" |
  "comp_next_states_com l (If A e c\<^sub>1 c\<^sub>2) g = {g}" |
  "comp_next_states_com l (While A e I c\<^sub>2) g = {g}" |
  "comp_next_states_com l (c\<^sub>1 ;; c\<^sub>2) g = comp_next_states_com l c\<^sub>1 g"

fun comp_next_states :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state set" where
  "comp_next_states l g = comp_next_states_com l (com l) g"

lemma comp_next_states_com_ignores_loc_com:
  "comp_next_states_com l c g = comp_next_states_com (l\<lparr>com := c'\<rparr>) c g"
  by (induction c, auto)

lemma comp_next_states_correct:
  "comp_next_states l g = next_states l g"
proof (induction "com l" arbitrary: l)
  case Skip
  then show ?case
    apply (auto simp: eval_skip_elim)[1]
    using eval_skip_elim by force
next
  case (Assign A x e)
  hence "com l = {A} x ::= e" by auto
  with Assign show ?case
    apply auto[1]
    using eval_assign by force
next
  case (DAssign A x e)
  hence "com l = {A} x :D= e" by auto
  with DAssign show ?case
    apply auto[1]
    using eval_dassign 
    apply (cases "\<L> x")
     apply auto
    by force
next
  case (Out A l' e)
  hence "com l = Out A l' e" by auto
  with Out show ?case 
    apply auto[1]
    using eval_out by force
next
  case (DOut A l' e)
  hence "com l = DOut A l' e" by auto
  with DOut show ?case 
    apply auto[1]
    using eval_dout by force
next
  case (In A x l')
  hence "com l = {A} x <- l'"
    by auto
  with In show ?case 
    apply auto[1]
    using eval_in by force
next
  case (If A e c c')

  from If(1-2) If(3)[symmetric] show ?case
    apply auto
    apply (cases "val_true (eval_exp e (mem g))")
    using eval_iftrue apply meson 
    using eval_iffalse by meson 
next
  case (While A e I c)
  from While(2)[symmetric] While(1) show ?case 
    apply auto
    apply (cases "val_true (eval_exp e (mem g))")
    using eval_whiletrue apply meson
    using eval_whilefalse by meson 
next
  case (Seq c\<^sub>1 c\<^sub>2)
  note Seq' = Seq(1-2) Seq(3)[symmetric]
  have "comp_next_states l g \<subseteq> next_states l g"
  proof
    fix g'
    assume "g' \<in> comp_next_states l g"
    with Seq' have "g' \<in> comp_next_states (l\<lparr>com := c\<^sub>1\<rparr>) g"
      using comp_next_states_com_ignores_loc_com
      by auto
    with Seq' obtain l' where "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g')"
      apply simp
      by (smt local_state.select_convs(1) local_state.surjective local_state.update_convs(1) mem_Collect_eq)
    with Seq' show "g' \<in> next_states l g" 
      using eval_seq eval_seqskip
      by (smt next_states.simps local_state.fold_congs(1) mem_Collect_eq)
      (* FIXME: a bit slow *)
  qed
  moreover have "next_states l g \<subseteq> comp_next_states l g"
  proof
    fix g'
    assume "g' \<in> next_states l g"
    with Seq' obtain l' where "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g')"
      by auto
    with Seq' have "g' \<in> comp_next_states (l\<lparr>com := c\<^sub>1\<rparr>) g"
      apply simp
      by (metis (no_types, lifting) local_state.ext_inject local_state.surjective local_state.update_convs(1) mem_Collect_eq)
    with Seq' show "g' \<in> comp_next_states l g"
      using comp_next_states_com_ignores_loc_com
      by auto
  qed
  ultimately show ?case by auto
next
  case (Acquire x1 x2)
  from Acquire(1)[symmetric] show ?case
    apply auto
    using eval_acquire by force
next
  case (Release x1 x2)
  from Release(1)[symmetric] show ?case
    apply auto
    using eval_release by force
qed

fun to_skip_com :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) com \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool" where 
  "to_skip_com l Skip g = False" |
  "to_skip_com l ({A} x ::= e) g = True" |
  "to_skip_com l ({A} x :D= e) g =
    (case \<L> x of
       Some lev \<Rightarrow> True
     | None \<Rightarrow> False)" |
  "to_skip_com l ({A} x <- l') g = True" |
  "to_skip_com l (Out A l' e ) g = True" |
  "to_skip_com l (DOut A l' e ) g = True" |
  "to_skip_com l (Acquire A loc) g = 
    (if lock_state g loc = None then True else False)" |
  "to_skip_com l (Release A loc) g = 
    (if lock_state g loc = Some (tid l) then True else False)" |
  "to_skip_com l (If A e c\<^sub>1 c\<^sub>2) g = 
    (if val_true (eval_exp e (mem g)) then c\<^sub>1 = Skip else c\<^sub>2 = Skip)" |
  "to_skip_com l (While A e I c\<^sub>2) g =
    (\<not> val_true (eval_exp e (mem g)))" |
  "to_skip_com l (c\<^sub>1 ;; c\<^sub>2) g = (to_skip_com l c\<^sub>1 g \<and> c\<^sub>2 = Skip)"

fun comp_next_anns_com :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) com \<Rightarrow> 
  ('level, 'val) global_state \<Rightarrow> ('level, 'val) ann set" where
  "comp_next_anns_com l Skip g = {}" |
  "comp_next_anns_com l ({A} x ::= e) g = {ann\<^sub>0}" |
  "comp_next_anns_com l ({A} x :D= e) g =
    (case \<L> x of Some lev \<Rightarrow> {ann\<^sub>0} | None \<Rightarrow> {})" |
  "comp_next_anns_com l ({A} x <- l') g = {ann\<^sub>0}" |
  "comp_next_anns_com l (Out A l' e ) g = {ann\<^sub>0}" |
  "comp_next_anns_com l (DOut A l' e ) g = {ann\<^sub>0}" |
  "comp_next_anns_com l (Acquire A loc) g =
    (if lock_state g loc = None then {ann\<^sub>0} else {})" |
  "comp_next_anns_com l (Release A loc) g = 
    (if lock_state g loc = Some (tid l) then {ann\<^sub>0} else {})" |
  "comp_next_anns_com l (If A e c\<^sub>1 c\<^sub>2) g = 
    (if val_true (eval_exp e (mem g)) then {active_ann_com c\<^sub>1} else {active_ann_com c\<^sub>2})" |
  "comp_next_anns_com l (While A e I c) g =
    (if val_true (eval_exp e (mem g)) then {active_ann_com c} else {ann\<^sub>0})" |
  "comp_next_anns_com l (c\<^sub>1 ;; c\<^sub>2) g =
    (if to_skip_com l c\<^sub>1 g then {active_ann_com c\<^sub>2} else comp_next_anns_com l c\<^sub>1 g)"

fun comp_next_anns :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) ann set"
  where "comp_next_anns l g = comp_next_anns_com l (com l) g"

lemma to_skip_com_ignores_loc_com:
  "to_skip_com l c g = to_skip_com (l\<lparr>com := c'\<rparr>) c g"
  by (induction c, auto)

lemma to_skip_correct:
  "(\<exists> l' g'. (l, g) \<leadsto> (l', g') \<and> com l' = Skip) \<longleftrightarrow> to_skip_com l (com l) g"
proof (induction "com l" arbitrary: l)
  case Skip
  from Skip(1)[symmetric] show ?case
    apply auto
    using eval_skip_elim by force
next
  case (Assign x1 x2 x3)
  from Assign(1)[symmetric] show ?case
    apply clarsimp
    using eval_assign by force
next
  case (DAssign x1 x2 x3)
  from DAssign(1)[symmetric] show ?case
    apply clarsimp
    apply (cases "\<L> x2")
    using eval_dassign
    by (auto, force)
next
  case (Out x1 x2 x3)
  from Out(1)[symmetric] show ?case
    apply clarsimp
    using eval_out by force
next
  case (DOut x1 x2 x3)
  from DOut(1)[symmetric] show ?case
    apply clarsimp
    using eval_dout by force
next
  case (In x1 x2 x3)
  from In(1)[symmetric] show ?case 
    apply clarsimp
    using eval_in by force
next
  case (If A e c\<^sub>1 c\<^sub>2)
  from If(3)[symmetric] If(1-2) show ?case 
    apply auto[1]
    apply (metis eval.eval_iftrue local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
    apply (metis eval.eval_iffalse local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
    done
next
  case (While A e I c)
  from While(1) While(2)[symmetric] show ?case
    apply auto[1]
    by (metis (no_types, lifting) eval.eval_whilefalse local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (Seq c\<^sub>1 c\<^sub>2)
  note Seq' = Seq(1-2) Seq(3)[symmetric]
  show ?case
  proof
    assume A: "\<exists>l' g'. (l, g) \<leadsto> (l', g') \<and> com l' = Skip"
    then obtain l' g' where ev': "(l, g) \<leadsto> (l', g')" and s: "com l' = Skip" by blast
    from ev' Seq'(3) show "to_skip_com l (com l) g" 
      apply (rule eval_seq_elim)
      apply (metis (no_types, lifting) Seq'(3) Seq.hyps(1) local_state.select_convs(1) local_state.surjective local_state.update_convs(1) s to_skip_com.simps(11) to_skip_com_ignores_loc_com)
      using s by auto
  next
    assume "to_skip_com l (com l) g"
    with Seq' have "c\<^sub>2 = Skip" "to_skip_com l c\<^sub>1 g"
      by auto
    moreover with Seq' obtain l' g' where "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g')" "com l' = Skip"
      using to_skip_com_ignores_loc_com 
      apply auto
      by (metis local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
    with Seq' show "\<exists>l' g'. (l, g) \<leadsto> (l', g') \<and> com l' = Skip"
      using eval_seqskip `c\<^sub>2 = Skip`
      apply clarsimp
      apply (rule exI[where x = "l'\<lparr>com := Skip\<rparr>"])
      apply auto
      apply (rule exI[where x = "g'"])
      using eval_seqskip by force
  qed
next
  case (Acquire x1 x2)
  from Acquire(1)[symmetric] show ?case
    apply auto[1]
    using eval_acquire by force
next
  case (Release x1 x2)
  from Release(1)[symmetric] show ?case 
    apply auto[1]
    using eval_release by force
qed

lemma comp_next_anns_com_ignores_loc_com:
  "comp_next_anns_com l c g = comp_next_anns_com (l\<lparr>com := c'\<rparr>) c g"
proof (induction c)
  case Skip
  then show ?case by auto
next
  case (Assign x1 x2 x3)
then show ?case by auto
next
  case (DAssign x1 x2 x3)
  then show ?case by auto
next
  case (Out x1 x2 x3)
  then show ?case by auto
next
  case (DOut x1 x2 x3)
  then show ?case by auto
next
  case (In x1 x2 x3)
  then show ?case by auto
next
  case (If x1 x2 c1 c2)
  then show ?case by auto
next
  case (While x1 x2 x3 c)
  then show ?case by auto
next
  case (Seq c\<^sub>1 c\<^sub>2)
  have "to_skip_com l c\<^sub>1 g = to_skip_com (l\<lparr>com := c'\<rparr>) c\<^sub>1 g"
    using to_skip_com_ignores_loc_com
    by auto
  then show ?case 
    apply clarsimp
    apply (cases "to_skip_com l c\<^sub>1 g")
    apply simp
    using Seq by auto
next
  case (Acquire x1 x2)
  then show ?case by auto
next
  case (Release x1 x2)
  then show ?case by auto
qed

lemma set_prop_singleton:
  "(\<exists> z. P z) \<Longrightarrow> (\<And> z. P z \<Longrightarrow> f z = x) \<Longrightarrow> {x} = f ` {y. P y}"
  by auto

lemma next_at_most_one_succ:
  "{(l', g'). (l, g) \<leadsto> (l', g')} = {} \<or> (\<exists> l' g'. {(l', g')} = {(l', g'). (l, g) \<leadsto> (l', g')})" 
  (is "?none \<or> (\<exists> cnf'. ?one cnf')")
  using eval_deterministic
  apply (cases "\<exists> l' g'. (l, g) \<leadsto> (l', g')")
   apply (rule disjI2)
   apply (erule exE)+
  apply (rename_tac l' g')
  apply (rule_tac x = l' in "exI")
  apply (rule_tac x = g' in "exI")
  by auto

lemma next_anns_at_most_singleton:
  "next_anns l g = {} \<or> (\<exists> A. next_anns l g = {A})"
  unfolding next_anns_def
  apply (insert next_at_most_one_succ[where l = l and g = g])
  apply (erule disjE)
   apply (rule disjI1)
   apply auto[1]
  apply (rule disjI2)
  apply clarsimp
  apply (rename_tac l' g')
  apply (rule_tac x = "active_ann l'" in exI)
  apply auto[1]
  apply (metis eval_deterministic mem_Collect_eq old.prod.case singletonI)
  by blast

lemma next_anns_eval:
  "(l, g) \<leadsto> (l', g') \<Longrightarrow> next_anns l g = {active_ann l'}"
  using next_anns_at_most_singleton
  unfolding next_anns_def
  by (smt eval_deterministic set_prop_singleton)

lemma comp_next_anns_correct:
  assumes "com l = c"
  shows "comp_next_anns l g = next_anns l g"
  using assms unfolding next_anns_def
proof (induction "c" arbitrary: l)
  case Skip
  then show ?case 
    apply auto
    using eval_skip_elim by force
next
  case (Assign A x e)
  then have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
    using eval_assign by force
  moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0"
    by (auto simp: active_ann_def)
  ultimately show ?case 
    using Assign
    by (auto intro!: set_prop_singleton)
next
  case (DAssign x1 x2 x3)
  show ?case
  proof (cases "\<L> x2")
    case None
    then show ?thesis 
      using DAssign
      by (auto intro!: set_prop_singleton)
  next
    case (Some a)
    with DAssign have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
      using eval_dassign
      by force
    moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0"
      by (auto simp: active_ann_def)
    ultimately show ?thesis 
      using Some DAssign
      by (auto intro!: set_prop_singleton)
  qed

next
  case (DOut x1 x2 x3)
  then have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
    using eval_dout by force
  moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0"
    by (auto simp: active_ann_def)
  ultimately show ?case 
    using DOut
    by (auto intro!: set_prop_singleton)
next
  case (Out x1 x2 x3)
  then have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
    using eval_out by force
  moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0"
    by (auto simp: active_ann_def)
  ultimately show ?case 
    using Out
    by (auto intro!: set_prop_singleton)
next
  case (In x1 x2 x3)
  then have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
    using eval_in by force
  moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0"
    by (auto simp: active_ann_def)
  ultimately show ?case 
    using In
    by (auto intro!: set_prop_singleton)
next
  case (If x1 x2 x1 x2)
  then show ?case
    apply auto
    apply (smt CollectI active_ann_def eval.eval_iftrue local_state.select_convs(1) local_state.surjective local_state.update_convs(1) setcompr_eq_image)
    apply (auto simp: active_ann_def)[1]
      apply (smt active_ann_def eval.eval_iffalse local_state.select_convs(1) local_state.surjective local_state.update_convs(1) mem_Collect_eq setcompr_eq_image)
    by (auto simp: active_ann_def)
next
  case (While A e I c)
  show ?case
  proof (cases "val_true (eval_exp e (mem g))")
    case False
    with While.prems have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
      using eval_whilefalse by force
    moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0"
      by (auto simp: active_ann_def)
    ultimately show ?thesis 
      using While False
      by (auto intro!: set_prop_singleton)
  next
    case True
    hence "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := c ;; While I e I c\<rparr>, g')"
      using eval_whiletrue While.prems by force
    moreover have "active_ann (l\<lparr>com := c ;; While I e I c\<rparr>) = active_ann_com c"
      unfolding active_ann_def by auto
    ultimately show ?thesis 
      using True While
      by (auto intro!: set_prop_singleton)
  qed
next
  case (Seq c\<^sub>1 c\<^sub>2)
  then show ?case 
  proof (cases "to_skip_com l c\<^sub>1 g")
    case True
    hence "to_skip_com (l\<lparr>com := c\<^sub>1\<rparr>) c\<^sub>1 g"
      using to_skip_com_ignores_loc_com by auto
    then obtain l' g' where "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g')" "com l' = Skip"
      using Seq to_skip_correct[where l = "l\<lparr>com := c\<^sub>1\<rparr>"]
      by (auto, meson)
    with Seq have "(l, g) \<leadsto> (l'\<lparr>com := c\<^sub>2\<rparr>, g')"
      using eval_seqskip by force
    hence "next_anns l g = {active_ann_com c\<^sub>2}"
      using next_anns_eval
      unfolding active_ann_def by auto
    moreover from Seq True have "comp_next_anns l g = {active_ann_com c\<^sub>2}"
      by auto
    ultimately show ?thesis 
      using Seq
      unfolding next_anns_def
      by auto
  next
    case False
    note F = False
    hence ns: "\<not> (to_skip_com (l\<lparr>com := c\<^sub>1\<rparr>) c\<^sub>1 g)"
      using to_skip_com_ignores_loc_com by auto
    show ?thesis
    proof (cases "\<exists> l' g'. (l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g')")
      case True
      then obtain l' g' where ev': "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g')"
        by blast
      with ns have "com l' \<noteq> Skip"
        using to_skip_correct eval_deterministic
        apply auto
        by (metis local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
      with Seq ev' have "(l, g) \<leadsto> (l'\<lparr>com := com l' ;; c\<^sub>2\<rparr>, g')"
        using eval_seq by force
      hence "next_anns l g = {active_ann (l'\<lparr>com := com l' ;; c\<^sub>2\<rparr>)}"
        using next_anns_eval
        by auto
      then also have "... = {active_ann l'}" unfolding active_ann_def by auto
      finally have "next_anns l g = {active_ann l'}" by simp
      moreover then have "next_anns l g = next_anns (l\<lparr>com := c\<^sub>1\<rparr>) g"
        using next_anns_eval ev'
        by force
      moreover have "comp_next_anns (l\<lparr>com := c\<^sub>1\<rparr>) g = next_anns (l\<lparr>com := c\<^sub>1\<rparr>) g"
        using Seq(1)[where l1 = "l\<lparr>com := c\<^sub>1\<rparr>"]
        unfolding next_anns_def 
        by auto
      moreover from False Seq(3) have "comp_next_anns l g = comp_next_anns (l\<lparr>com := c\<^sub>1\<rparr>) g"
        using comp_next_anns_com_ignores_loc_com
        by auto
      ultimately show ?thesis
        unfolding next_anns_def by auto
    next
      case False
      hence empt: "next_anns (l\<lparr>com := c\<^sub>1\<rparr>) g = {}"
        unfolding next_anns_def
        by auto
      hence "next_anns l g = {}"
        using Seq
        unfolding next_anns_def
        by auto
      moreover from Seq F False have "comp_next_anns l g = comp_next_anns (l\<lparr>com := c\<^sub>1\<rparr>) g"
        using comp_next_anns_com_ignores_loc_com
        by auto
      moreover then have "... = {}"
        using Seq empt
        unfolding next_anns_def by auto
      ultimately show ?thesis using False unfolding next_anns_def
        by auto
    qed
  qed
next
  case (Acquire A loc)
  show ?case
  proof (cases "lock_state g loc = None")
    case True
    with Acquire have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
      using eval_acquire by force
    moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0" unfolding active_ann_def by auto
    ultimately show ?thesis using Acquire True
      by (auto intro!: set_prop_singleton)
  next
    case False
    with Acquire.prems have "\<nexists> l' g'. (l, g) \<leadsto> (l', g')"
      by force
    with Acquire have "next_anns l g = {}"
      unfolding next_anns_def by auto
    with Acquire False show ?thesis 
      by auto
  qed
next
  case (Release A loc)
  show ?case
  proof (cases "lock_state g loc = Some (tid l)")
    case True
    with Release have "\<exists> g'. (l, g) \<leadsto> (l\<lparr>com := Skip\<rparr>, g')"
      using eval_release by force
    moreover have "active_ann (l\<lparr>com := Skip\<rparr>) = ann\<^sub>0" unfolding active_ann_def by auto
    ultimately show ?thesis using Release True
      by (auto intro!: set_prop_singleton)
  next
    case False
    with Release.prems have "\<nexists> l' g'. (l, g) \<leadsto> (l', g')"
      by force
    with Release have "next_anns l g = {}"
      unfolding next_anns_def by auto
    with Release False show ?thesis 
      by auto
  qed
qed

subsection "Verification condition generation"

definition next_step_ensures_guar :: "('level, 'val) guar \<Rightarrow> ('level, 'val) local_state \<Rightarrow> bool" where
  "next_step_ensures_guar G l \<equiv>
    (\<forall> g g'. g' \<in> comp_next_states l g \<and> g \<Turnstile>\<^sub>P active_ann l \<longrightarrow> G g g')"

definition next_step_preserves_ann :: "('level, 'val) local_state \<Rightarrow> bool" where
  "next_step_preserves_ann l \<equiv>
     (\<forall> g g' A'. g \<Turnstile>\<^sub>P active_ann l \<and> g' \<in> comp_next_states l g \<and> A' \<in> comp_next_anns l g \<longrightarrow>
         g' \<Turnstile>\<^sub>P A')"

lemma anns_step_property:
  "step_property next_step_preserves_ann"
  unfolding step_property_def next_step_preserves_ann_def
  apply (simp only: comp_next_anns_correct comp_next_states_correct)
  by auto

lemma sound_anns:
  assumes pres: "\<forall> l' \<in> f ` compute_reachable_locs l. next_step_preserves_ann l'"
      and "simplify_func f"
    shows "anns_preserved l"
  unfolding anns_preserved_def
proof (clarify)
  fix g l' g' l'' g''
  assume ev: "(l, g) \<leadsto>\<^sub>n\<^sup>* (l', g')"
  assume ev': "(l', g') \<leadsto> (l'', g'')"
  assume sat: "g' \<Turnstile>\<^sub>P active_ann l'"
  from ev pres have "l' \<in> reachable_locs l"
    using assms(2) reachable_locs_def by auto
  then have "l' \<in> compute_reachable_locs l"
    using compute_reachable_locs_correct
    by blast
  moreover
  then have "next_step_preserves_ann (f l')"
    using pres
    by blast
  with anns_step_property have "next_step_preserves_ann l'"
    using `simplify_func f`
    unfolding step_property_def simplify_func_def
    apply clarsimp
    by meson
  ultimately show "g'' \<Turnstile>\<^sub>P active_ann l''"
    using ev' 
    unfolding next_step_preserves_ann_def
    using sat comp_next_anns_correct next_anns_def comp_next_states_correct next_states.simps
    apply clarsimp
    by blast
qed

lemma next_step_ensures_guar_step_prop:
  "step_property (next_step_ensures_guar G)"
  unfolding step_property_def next_step_ensures_guar_def next_anns_def
  apply (simp only: comp_next_states_correct)
  by auto

lemma sound_guars:
  assumes pres: "\<forall> l' \<in> f ` compute_reachable_locs l. next_step_ensures_guar G l'"
      and "simplify_func f"
    shows "preserves_guars G l"
  unfolding preserves_guars_def
proof (clarify)
  fix g l' g' l'' g''
  assume ev: "(l, g) \<leadsto>\<^sub>n\<^sup>* (l', g')"
  assume ev': "(l', g') \<leadsto> (l'', g'')"
  assume sat: "g' \<Turnstile>\<^sub>P active_ann l'"
  from ev pres have "l' \<in> reachable_locs l"
    using assms(2) reachable_locs_def by auto
  then have "l' \<in> compute_reachable_locs l"
    using compute_reachable_locs_correct
    by blast
  moreover
  then have "next_step_ensures_guar G (f l')"
    using pres
    by blast
  with next_step_ensures_guar_step_prop have "next_step_ensures_guar G l'"
    using `simplify_func f`
    unfolding step_property_def simplify_func_def
    apply clarsimp
    by meson
  ultimately show "G g' g''"
    using ev' 
    unfolding next_step_ensures_guar_def
    using sat
    using comp_next_states_correct next_states.simps
    by force
qed

lemma ann_stable_split:
  assumes "\<forall> P \<in> preds A. \<forall> g g'. (P g \<and> R g g') \<longrightarrow> P g'"
  shows "ann_stable R A"
  unfolding ann_stable_def
  using assms apply (simp add: satisfies_ann_preds_def)
  by force

definition verif_conds :: "(('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state) \<Rightarrow> ('level, 'val) local_state \<Rightarrow> ('level, 'val) rely \<Rightarrow> ('level, 'val) guar \<Rightarrow> bool" where
  "verif_conds f l R G \<equiv> 
  (\<forall> A \<in> all_anns (com l). ann_stable R A) \<and>
  (\<forall> l' \<in> f ` compute_reachable_locs l. 
     next_step_ensures_guar G l' \<and>
     next_step_preserves_ann l')"

lemma verif_conds_sound:
  assumes "verif_conds f l R G"
  assumes "simplify_func f"
  shows "rg_sat (l, R, G)"
  using sound_anns sound_guars assms
  unfolding rg_sat.simps preds_stable_def
  apply (simp only: verif_conds_def)
  by fastforce

lemma id_simplify_fun:
  "simplify_func id"
  unfolding simplify_func_def by auto

end
end