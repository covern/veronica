theory EvalNondet
  imports Main Language TypeSystem Reverse_Transitive_Closure
begin

context lang
begin

inductive_set eval_nondet :: "('level, 'val) conf rel" and
  eval_nondet_abv :: "('level, 'val) conf \<Rightarrow> ('level, 'val) conf \<Rightarrow> bool" (infix "\<leadsto>\<^sub>n" 60)
  where
  "x \<leadsto>\<^sub>n y \<equiv> (x, y) \<in> eval_nondet" |
  step: "\<lbrakk> (l, g) \<leadsto> (l', g') \<rbrakk> \<Longrightarrow> (l, g) \<leadsto>\<^sub>n (l', g')" |
  env: "(l, g) \<leadsto>\<^sub>n (l, g')"

abbreviation eval_nondet_multi_abv :: "('level, 'val) conf \<Rightarrow> ('level, 'val) conf \<Rightarrow> bool" (infix "\<leadsto>\<^sub>n\<^sup>*" 60) 
  where
  "x \<leadsto>\<^sub>n\<^sup>* y \<equiv> (x, y) \<in> eval_nondet\<^sup>*"


lemma eval_global_to_nondet:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and i_len: "i < length ls"
    shows "(ls ! i, g) \<leadsto>\<^sub>n (ls' ! i, g')"
  using assms
proof (induction rule: global_eval.induct)
  case (global_step ls n g sched l' g' ls')
  then show ?case 
    apply (cases "n = i")
    apply (simp add: eval_nondet.simps)
    by (simp add: eval_nondet.env)
next
  case (global_wait ls n g sched)
  then show ?case
    using eval_nondet.env by blast
qed

lemma eval_global_to_nondet_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and i_len: "i < length ls"
    shows "(ls ! i, g) \<leadsto>\<^sub>n\<^sup>* (ls' ! i, g')"
  using assms
proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  case (rtrancl_refl a a b)
  then show ?case by auto
next
  case (rtrancl_into_rtrancl a a b a a b a a b)
  then show ?case
    using eval_global_to_nondet
    by (metis (no_types, lifting) map_eq_imp_length_eq rtrancl.simps tid_constant_multi)
qed

lemma eval_nondet_skip:
  "(l, g) \<leadsto>\<^sub>n (l', g') \<Longrightarrow> com l = Skip \<Longrightarrow> com l' = Skip"
  apply (induct rule: eval_nondet.induct)
  using eval_skip_elim by blast+

lemma eval_nondet_skip_multi:
  "(l, g) \<leadsto>\<^sub>n\<^sup>* (l', g') \<Longrightarrow> com l = Skip \<Longrightarrow> com l' = Skip"
  apply (induction rule: rtrancl.induct[where r = eval_nondet, split_format(complete)])
  by (auto simp: eval_nondet_skip)

lemma eval_preserves_forall_prop:
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  assumes P_eq: "P = (\<lambda> l. \<forall> g l' g' l'' g''. (l, g) \<leadsto>\<^sub>n\<^sup>* (l', g') \<and> (l', g') \<leadsto> (l'', g'') \<longrightarrow>
                       Q l' g' l'' g'')"
  assumes P_init: "P l\<^sub>1"
  shows "P l\<^sub>2"
  using ev P_init
proof (induction rule: eval.induct)
  case (eval_assign l A x e g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_dassign l A x e g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_out l A lev e g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_dout l A lev e g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_in l A x lev v g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then have ev\<^sub>1: "(l, g) \<leadsto>\<^sub>n (l\<lparr>com := c\<^sub>1\<rparr>, g)"
    using eval.eval_iftrue eval_nondet.step by auto  
  show ?case
    using P_eq
  proof (clarify)
    fix g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3
    assume ev\<^sub>2: "(l\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
    assume ev': "(l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3)"
    have "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto>\<^sub>n (l\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1)"
      using eval_nondet.env by blast
    from ev\<^sub>1 ev\<^sub>2 have "(l, g) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
      using \<open>(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto>\<^sub>n (l\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1)\<close> by auto
    thus "Q l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3"
      using eval_iftrue ev' P_eq
      by simp
  qed
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then have ev\<^sub>1: "(l, g) \<leadsto>\<^sub>n (l\<lparr>com := c\<^sub>2\<rparr>, g)"
    using eval.eval_iffalse eval_nondet.step by auto  
  show ?case
    using P_eq
  proof (clarify)
    fix g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3
    assume ev\<^sub>2: "(l\<lparr>com := c\<^sub>2\<rparr>, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
    assume ev': "(l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3)"
    have "(l\<lparr>com := c\<^sub>2\<rparr>, g) \<leadsto>\<^sub>n (l\<lparr>com := c\<^sub>2\<rparr>, g\<^sub>1)"
      using eval_nondet.env by blast
    from ev\<^sub>1 ev\<^sub>2 have "(l, g) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
      using \<open>(l\<lparr>com := c\<^sub>2\<rparr>, g) \<leadsto>\<^sub>n (l\<lparr>com := c\<^sub>2\<rparr>, g\<^sub>1)\<close> by auto
    thus "Q l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3"
      using eval_iffalse ev' P_eq
      by simp
  qed
next
  case (eval_seq l c\<^sub>1 c\<^sub>2 g l' g')
  then have ev': "(l, g) \<leadsto>\<^sub>n (l'\<lparr>com := com l' ;; c\<^sub>2\<rparr>, g')"
    by (simp add: eval.eval_seq eval_nondet.step eval_seq.hyps(1) eval_seq.hyps(2))
  then have ev\<^sub>1: "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto>\<^sub>n (l', g')"
    by (simp add: eval_seq.hyps(2) eval_nondet.step)
  show ?case
    using P_eq
  proof (clarify)
    fix g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3
    assume ev\<^sub>2: "(l'\<lparr>com := com l' ;; c\<^sub>2\<rparr>, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
    assume ev\<^sub>3: "(l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3)"
    from ev' have "(l'\<lparr>com := com l' ;; c\<^sub>2\<rparr>, g') \<leadsto>\<^sub>n (l'\<lparr>com := com l' ;; c\<^sub>2\<rparr>, g\<^sub>1)"
      using eval_nondet.env by blast
    with ev' ev\<^sub>2 have "(l, g) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
      by auto
    with P_eq eval_seq ev\<^sub>3 show "Q l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3"
      by simp
  qed
next
  case (eval_seqskip  l c\<^sub>1 c\<^sub>2 g l' g')
  then have ev': "(l, g) \<leadsto>\<^sub>n (l'\<lparr>com := c\<^sub>2\<rparr>, g')"
    by (simp add: eval.eval_seqskip eval_nondet.step eval_seqskip.hyps)
  then have ev\<^sub>1: "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto>\<^sub>n (l', g')"
    by (simp add: eval_seqskip.hyps(2) eval_nondet.step)
  show ?case
    using P_eq
  proof (clarify)
    fix g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3
    assume ev\<^sub>2: "(l'\<lparr>com := c\<^sub>2\<rparr>, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
    assume ev\<^sub>3: "(l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3)"
    from ev' have "(l'\<lparr>com := c\<^sub>2\<rparr>, g') \<leadsto>\<^sub>n (l'\<lparr>com := c\<^sub>2\<rparr>, g\<^sub>1)"
      using eval_nondet.env by blast
    with ev' ev\<^sub>2 have "(l, g) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
      by auto
    with P_eq eval_seq ev\<^sub>3 show "Q l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3"
      using eval_seqskip.prems by blast
  qed
next
  case (eval_whiletrue l A e I c g)
  then have ev\<^sub>1: "(l, g) \<leadsto>\<^sub>n (l\<lparr>com := c ;; While I e I c\<rparr>, g)"
    using eval_nondet.step eval.eval_whiletrue
    by auto
  show ?case (* FIXME: separate out duplicated reasoning in this and previous cases into lemma *)
    using P_eq
  proof (clarify)
    fix g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3
    assume ev': "(l\<lparr>com := c ;; While I e I c\<rparr>, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2) "
    assume ev'': "(l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3)"
    have "(l\<lparr>com := c ;; While I e I c\<rparr>, g) \<leadsto>\<^sub>n (l\<lparr>com := c ;; While I e I c\<rparr>, g\<^sub>1)"
      using eval_nondet.env by blast
    with ev\<^sub>1 have "(l, g) \<leadsto>\<^sub>n\<^sup>* (l\<lparr>com := c ;; While I e I c\<rparr>, g\<^sub>1)"
      by auto
    with ev' have "(l, g) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2)"
      by auto
    with eval_whiletrue ev'' P_eq show "Q l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3"
      by simp
  qed
next
  case (eval_whilefalse l A e I c g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_acquire l A loc g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
next
  case (eval_release l A loc g)
  then show ?case
    by (smt P_eq eval_skip_elim eval_nondet_skip_multi local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
qed

lemma eval_nondet_tid_const:
  "(l, g) \<leadsto>\<^sub>n (l', g') \<Longrightarrow> tid l' = tid l"
  by (cases rule: eval_nondet.cases, auto simp: tid_constant)

lemma eval_nondet_tid_const_multi:
  "(l, g) \<leadsto>\<^sub>n\<^sup>* (l', g') \<Longrightarrow> tid l' = tid l"
  apply (induction rule: rtrancl.induct[where r = eval_nondet, split_format(complete)])
  by (auto simp: eval_nondet_tid_const)

end

end