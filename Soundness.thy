theory Soundness
  imports Main Language Security TypeSystem HOL.Orderings "~~/src/HOL/Library/Prefix_Order"  "~~/src/HOL/Library/Lattice_Syntax"
begin


context lang
begin

inductive_cases bisimilar_elim[elim!]: "bisimilar l l\<^sub>1 l\<^sub>1'"

fun is_low_declassification :: "'level \<Rightarrow> ('level, 'val) event \<Rightarrow> bool"
  where "is_low_declassification l (Declassification l' _ _) = (l' \<le> l)" |
        "is_low_declassification _ _ = False"

inductive \<R>\<^sub>1 :: "bool \<Rightarrow> ('level, 'val) bisim"
  for ah :: bool and l :: "'level" where
  intro[intro!]: "\<lbrakk> ah, l \<turnstile> com l\<^sub>1 \<rbrakk> \<Longrightarrow> \<R>\<^sub>1 ah l l\<^sub>1 l\<^sub>1"

inductive \<R>\<^sub>2 :: "bool \<Rightarrow> ('level, 'val) bisim"
  for ah :: bool and l :: "'level" where
  intro[intro!]: "\<lbrakk> ah, l \<turnstile> com l\<^sub>1 ; ah, l \<turnstile> com l\<^sub>1' ; tid l\<^sub>1 = tid l\<^sub>1' ; l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1' \<rbrakk> \<Longrightarrow>
   \<R>\<^sub>2 ah l l\<^sub>1 l\<^sub>1'"

inductive \<R>\<^sub>3 :: "bool \<Rightarrow> ('level, 'val) bisim"
  for ah :: bool and l :: "'level" where
  intro\<^sub>1[intro]: "\<lbrakk> \<R>\<^sub>1 ah l l\<^sub>1 l\<^sub>1 ; ah, l \<turnstile> c ; l\<^sub>2 = l\<^sub>1 \<lparr> com := com l\<^sub>1 ;; c \<rparr> ; ah, l \<turnstile> c \<rbrakk> \<Longrightarrow> \<R>\<^sub>3 ah l l\<^sub>2 l\<^sub>2" |
  intro\<^sub>2[intro]: "\<lbrakk> \<R>\<^sub>2 ah l l\<^sub>1 l\<^sub>1' ; ah, l \<turnstile> c ; 
                    l\<^sub>2 = l\<^sub>1 \<lparr> com := com l\<^sub>1 ;; c \<rparr> ; 
                    l\<^sub>2' = l\<^sub>1' \<lparr> com := com l\<^sub>1' ;; c \<rparr> \<rbrakk> \<Longrightarrow> \<R>\<^sub>3 ah l l\<^sub>2 l\<^sub>2'" |
  intro\<^sub>3[intro]: "\<lbrakk> \<R>\<^sub>3 ah l l\<^sub>1 l\<^sub>1' ;  ah, l \<turnstile> c ;
                    l\<^sub>2 = l\<^sub>1 \<lparr> com := com l\<^sub>1 ;; c \<rparr> ; 
                    l\<^sub>2' = l\<^sub>1' \<lparr> com := com l\<^sub>1' ;; c \<rparr> \<rbrakk> \<Longrightarrow> \<R>\<^sub>3 ah l l\<^sub>2 l\<^sub>2'"

(* This is the main bisimulation we are going to use to show soundness; it's the union
  of \<R>\<^sub>1, \<R>\<^sub>2, and \<R>\<^sub>3. *)
inductive \<R> :: "bool \<Rightarrow> ('level, 'val) bisim"
  for ah :: bool and l :: "'level"  where
  intro\<^sub>1[intro]: "\<lbrakk> \<R>\<^sub>1 ah l l\<^sub>1 l\<^sub>1' \<rbrakk> \<Longrightarrow> \<R> ah l l\<^sub>1 l\<^sub>1'" |
  intro\<^sub>2[intro]: "\<lbrakk> \<R>\<^sub>2 ah l l\<^sub>1 l\<^sub>1' \<rbrakk> \<Longrightarrow> \<R> ah l l\<^sub>1 l\<^sub>1'" |
  intro\<^sub>3[intro]: "\<lbrakk> \<R>\<^sub>3 ah l l\<^sub>1 l\<^sub>1' \<rbrakk> \<Longrightarrow> \<R> ah l l\<^sub>1 l\<^sub>1'"

inductive_cases \<R>\<^sub>1_elim[elim!]: "\<R>\<^sub>1 ah l l\<^sub>1 l\<^sub>1'"
inductive_cases \<R>\<^sub>2_elim[elim!]: "\<R>\<^sub>2 ah l l\<^sub>2 l\<^sub>2'"
inductive_cases \<R>\<^sub>3_elim[elim]: "\<R>\<^sub>3 ah l l\<^sub>3 l\<^sub>3'"
inductive_cases \<R>_elim[elim]: "\<R> ah l l\<^sub>1 l\<^sub>1'"

lemma bisimilar_sym:
  "sym (to_rel (\<lambda> x y. x \<approx>\<^bsub>l\<^esub> y))"
  by (auto simp: secure_bisim_def to_rel_def sym_def)

lemma \<R>\<^sub>1_sym:
  "sym (to_rel (\<R>\<^sub>1 ah l))"
  unfolding to_rel_def sym_def
  by auto

lemma \<R>\<^sub>2_sym:
  "sym (to_rel (\<R>\<^sub>2 ah l))"
  by (auto simp: secure_bisim_def to_rel_def sym_def bisimilar_sym)

thm \<R>\<^sub>3.induct

lemma \<R>\<^sub>3_sym_aux:
  assumes "\<R>\<^sub>3 ah l l\<^sub>1 l\<^sub>2"
  shows "\<R>\<^sub>3 ah l l\<^sub>2 l\<^sub>1"
  using assms
proof (induction rule: \<R>\<^sub>3.induct)
  case (intro\<^sub>1 l\<^sub>1 c l\<^sub>2)
  then show ?case 
    by blast
next
  case (intro\<^sub>2 l\<^sub>1 l\<^sub>1' c l\<^sub>2 l\<^sub>2')
  with \<R>\<^sub>2_sym have "\<R>\<^sub>2 ah l l\<^sub>1' l\<^sub>1"
    unfolding to_rel_def sym_def by auto
  with intro\<^sub>2 show ?case 
    using \<R>\<^sub>3.intro\<^sub>2
    by blast
next
  case (intro\<^sub>3 l\<^sub>1 l\<^sub>2 c l\<^sub>2' l\<^sub>1')
  then show ?case 
    by blast
qed


lemma \<R>\<^sub>3_sym:
  "sym (to_rel (\<R>\<^sub>3 ah l))"
  unfolding to_rel_def sym_def
  using \<R>\<^sub>3_sym_aux by auto

lemma \<R>_sym:
  "sym (to_rel (\<R> ah l))"
  using bisimilar_sym \<R>\<^sub>1_sym \<R>\<^sub>2_sym \<R>\<^sub>3_sym
  unfolding to_rel_def sym_def
  apply clarify
  apply (cases rule: \<R>.cases)
  apply (clarsimp | safe)+
  by (meson \<R>.cases \<R>.intro\<^sub>1 \<R>.intro\<^sub>2 \<R>.intro\<^sub>3 | auto)+

lemma has_type_preservation:
  assumes typed: "ah, lev \<turnstile> c"
  assumes c_eq: "com l = c"
  assumes ev: "(l, g) \<leadsto> (l', g')"
  shows "ah, lev \<turnstile> com l'"
  using typed ev c_eq
proof(induct arbitrary: l l' g' rule: has_type.induct)
  case (Skip_type)
  then show ?case
    using eval_skip_elim by blast
next
  case (Seq_type c\<^sub>1 c\<^sub>2 l l')
  from Seq_type(5-6) show ?case
  proof (rule eval_seq_elim)
    fix g'' 
    fix l'' :: "('level, 'val) local_state"
    assume "com l'' = Skip"
    assume "l' = l''\<lparr>com := c\<^sub>2\<rparr>"
    with Seq_type show "ah, lev \<turnstile> com l'"
      by auto 
  next
    fix g'' l''
    assume ev': "(l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l'', g'')"
    assume not_skip: "com l'' \<noteq> Skip "
    assume l'_eq: "l' = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>"
    with Seq_type ev' have "ah, lev \<turnstile> com l''"
      by force
    then show "ah, lev \<turnstile> com l'"
      using l'_eq lang.has_type.intros(2) local.Seq_type(3) lang_axioms
      by fastforce
  qed
next
  case (UAssign_type e x A l)
  thus ?case
    using Skip_type
    by auto
next
  case (LAssign_type x A e l)
  thus ?case
    using Skip_type
    by auto
next
  case (DAssign_type A e x \<Gamma>)
  thus ?case
    using Skip_type
    by auto
next
  case (If_type c\<^sub>1 c\<^sub>2 A e)
  then show ?case
    by auto
next
  case (Out_type lev A e l)
  hence "com l' = Skip" by auto
  then show ?case
    using Skip_type
    by auto
next
  case (DOut_type lev A e l)
  hence "com l' = Skip" by auto
  then show ?case
    using Skip_type
    by auto
next
  case (UIn_type lev x A)
  then show ?case
    using Skip_type
    by auto
next
  case (LIn_type x lev l' A)
  then show ?case
    using Skip_type
    by auto
next
  case (While_type c A e I)
  thus ?case
    by (smt Seq_type Skip_type lang.While_type lang.eval_while_elim lang_axioms local_state.ext_inject local_state.surjective local_state.update_convs(1))
next
  case (Acquire_type A lo)
  then show ?case
    using Skip_type by auto
next
  case (Release_type A lo)
  then show ?case
    using Skip_type by auto
qed

lemma high_bisim_secure:
  "secure_bisim l high_br_bisim"
  oops

lemma typed_implies_bisim:
  assumes typed: "ah, l \<turnstile> com l\<^sub>1"
  shows "\<R> ah l l\<^sub>1 l\<^sub>1"
  using \<R>.intro\<^sub>1 typed by auto

(* FIXME: better name *)
lemma mem_loweq_eval_same:
  assumes "mem\<^sub>1 =\<^sup>m\<^bsub>l\<^esub> mem\<^sub>2"
  assumes "exp_level_syn e = Some l'" "l' \<le> l"
  shows "eval_exp e mem\<^sub>1 = eval_exp e mem\<^sub>2"
  using assms
proof (induction e arbitrary: l')
  case (Var x)
  then show ?case
    by (simp add: mem_loweq_def)
next
  case (Val x)
  then show ?case by auto
next
  case (UnOp f e)
  then show ?case 
    by auto
next
  case (BinOp f e\<^sub>1 e\<^sub>2)
  then show ?case
    apply clarsimp
    by (metis le_sup_iff lift_option2.elims option.distinct(1) option.sel)
qed

lemma trace_append_visible:
  assumes le: "event_level e \<le> l"
  shows "(t @ [e]) \<upharpoonleft>\<^sub>\<le> l = (t \<upharpoonleft>\<^sub>\<le> l) @ [e]"
  using assms by (induction t, auto)

lemma trace_append_not_visible:
  assumes nle: "\<not> (event_level e \<le> l)"
  shows "(t @ [e]) \<upharpoonleft>\<^sub>\<le> l = t \<upharpoonleft>\<^sub>\<le> l"
  using assms by (induction t, auto)

lemma trace_last_event_le:
  assumes le: "event_level e \<le> l"
  shows "last ((t @ [e]) \<upharpoonleft>\<^sub>\<le> l) = e"
  using le trace_append_visible
  by auto

lemmas global_equiv_defs = global_state_equiv_def mem_loweq_def env_loweq_def trace_equiv_def lock_state_equiv_def

lemma filter_sublist: "t' \<le> t \<Longrightarrow> filter P t' \<le> filter P t"
  by (metis Prefix_Order.prefixE Prefix_Order.prefixI filter_append)

lemma trace_proj_le_commute:
  "t' \<le> t \<Longrightarrow> (t' \<upharpoonleft>\<^sub>\<le> l) \<le> (t \<upharpoonleft>\<^sub>\<le> l)"
  using trace_proj_filter filter_sublist by auto

fun list_eq :: "'a list \<Rightarrow> 'a list \<Rightarrow> bool" where
  "list_eq [] [] = True" |
  "list_eq (x # xs) [] = False" |
  "list_eq [] (y # ys) = False" |
  "list_eq (x # xs) (y # ys) = (x = y \<and> list_eq xs ys)"

lemma list_eq_correct:
  "list_eq xs ys \<longleftrightarrow> (xs = ys)"
  by (induction rule: list_eq.induct, auto)

lemma ann_implies_exp_level:
  "exp_level_opt A e = Some lev \<Longrightarrow> ann_implies_low lev A e"
  unfolding ann_implies_low_def
proof (clarify)
  fix g g'
  assume sat: "g \<Turnstile>\<^sub>P A" "g' \<Turnstile>\<^sub>P A"
  assume some: "exp_level_opt A e = Some lev"
  assume "g =\<^sup>g\<^bsub>lev\<^esub> g'"
  with sat some show "eval_exp e (mem g) = eval_exp e (mem g')"
    unfolding ann_implies_low_def
    unfolding exp_level_opt_def
    apply (cases "\<exists>l. exp_level l A e")
     defer
     apply clarsimp+
    oops


lemma exp_level_eval:
  assumes sat: "g \<Turnstile>\<^sub>P A" "g' \<Turnstile>\<^sub>P A"
      and eqv: "g =\<^sup>g\<^bsub>l\<^esub> g'"
      and lev: "exp_level lev A e" "lev \<le> l"
    shows "eval_exp e (mem g) = eval_exp e (mem g')"
proof -
  from eqv lev have "g =\<^sup>g\<^bsub>lev\<^esub> g'"
    using global_equiv_le_closed by auto
  with assms have "ann_implies_low lev A e"
    oops

lemma exp_level_opt_eval:
  assumes sat: "g \<Turnstile>\<^sub>P A" "g' \<Turnstile>\<^sub>P A"
      and eqv: "g =\<^sup>g\<^bsub>l\<^esub> g'"
      and lev: "exp_level_opt A e = Some lev" "lev \<le> l"
      (* and "ann_implies_low lev A e" *)
    shows "eval_exp e (mem g) = eval_exp e (mem g')"
  oops

text {*
  This is one property we want to prove of the typing rules. It says that the type system
  preserved low equivalence for steps with matching traces.
  We can't prove the existence of the matching step here, we have to assume it since we
  need the information that the trace was the same in both cases to
  tell us that the same values are being declassified.

  The intuition later will be that this lemma will tell us that matching traces preserve
  low equivalence of whole system executions. {\bf FIXME:} Actually that is not true since it would
  require the matching traces also to have the same number of execution steps as each other.
*}
lemma has_type_preserves_loweq:
  assumes typed: "ah, l \<turnstile> c"
      and ev\<^sub>1: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and leq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and ev\<^sub>2: "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
      and c_eq: "com l\<^sub>1 = c"
      and l_eq: "l\<^sub>1 = l\<^sub>1'"
      and same_trace: "trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l"
      and sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1"
    shows "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
  using assms
proof(induct arbitrary: g\<^sub>1 l\<^sub>1 l\<^sub>1' l\<^sub>2 l\<^sub>2' g\<^sub>1' g\<^sub>2' g\<^sub>2 rule: has_type.induct)
  case Skip_type
  with eval_skip_elim have False by blast
  thus ?case by auto
next
  case (UAssign_type x A e)
  note a = UAssign_type
  then have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by auto
  from a have [simp]: "g\<^sub>2' = g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1'))\<rparr>"
    by auto
  from a have "mem g\<^sub>2 =\<^sup>m\<^bsub>l\<^esub> mem g\<^sub>2'"
    unfolding global_state_equiv_def mem_loweq_def
    by auto
  with a show ?case 
    unfolding global_state_equiv_def
    by auto
next
  case (LAssign_type x lev l\<^sub>e A e)
  note a = LAssign_type
  then have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by auto
  moreover from a have [simp]: "g\<^sub>2' = g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1'))\<rparr>"
    by auto
  moreover have "mem g\<^sub>2 =\<^sup>m\<^bsub>l\<^esub> mem g\<^sub>2'"
  proof (cases "lev \<le> l")
    case True
    with a have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
      apply (clarsimp simp: ann_implies_low_def active_ann_def)
      using global_equiv_le_closed by auto
    with a show ?thesis
      unfolding global_equiv_defs
      by auto
  next
    case False
    with a show ?thesis
      by (auto simp: global_state_equiv_def mem_loweq_def)
  qed
  ultimately show ?case
    using a
    unfolding global_equiv_defs
    by auto
next
  case (Seq_type c\<^sub>1 c\<^sub>2)
  note a = Seq_type
  then have ev\<^sub>1': "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
    by auto
  from a have "active_ann l\<^sub>1 = active_ann_com c\<^sub>1" by (auto simp: active_ann_def)
  from `(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` show ?case
  proof (rule eval_seq_elim)
    fix g'' l''
    assume "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')"
    assume l''_skip[simp]: "com l'' = Skip"
    assume [simp]: "l\<^sub>2 = l''\<lparr>com := c\<^sub>2\<rparr>"
    assume [simp]: "g\<^sub>2 = g''"
    from ev\<^sub>1' `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` show "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
      apply (rule eval_seq_elim)
      using \<open>(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')\<close> a
      apply (smt \<open>active_ann l\<^sub>1 = active_ann_com c\<^sub>1\<close> \<open>g\<^sub>2 = g''\<close> active_ann_def local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
      by (smt \<open>(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')\<close> \<open>active_ann l\<^sub>1 = active_ann_com c\<^sub>1\<close> \<open>g\<^sub>2 = g''\<close> a(10) a(11) a(12) a(2) a(6) active_ann_def local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
  next
    fix g'' l''
    assume "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')"
    assume "com l'' \<noteq> Skip"
    assume "l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>"
    assume "g\<^sub>2 = g''"
    from ev\<^sub>1' `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` show "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
      apply (rule eval_seq_elim)
      using \<open>(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')\<close> \<open>g\<^sub>2 = g''\<close> a
      apply (smt \<open>active_ann l\<^sub>1 = active_ann_com c\<^sub>1\<close> active_ann_def local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
      by (smt \<open>(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')\<close> \<open>active_ann l\<^sub>1 = active_ann_com c\<^sub>1\<close> \<open>g\<^sub>2 = g''\<close> a(10) a(11) a(12) a(2) a(6) active_ann_def local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
  qed
next
  case (DAssign_type x lev e l\<^sub>e A)
  note a = DAssign_type
  then have g\<^sub>2_props: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1)),
                            trace := trace g\<^sub>1 @ [Declassification lev (eval_exp e (mem g\<^sub>1)) e]\<rparr>"
    by auto
  from a have g\<^sub>2'_props:
    "g\<^sub>2' = g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1')),
              trace := trace g\<^sub>1' @ [Declassification lev (eval_exp e (mem g\<^sub>1')) e]\<rparr>"
    by auto
  from a show ?case
  proof (cases "lev \<le> l")
    case True
    with a have "last (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = Declassification lev (eval_exp e (mem g\<^sub>1)) e"
      using trace_last_event_le g\<^sub>2_props
      apply clarsimp
      by (metis event_level.simps(3))
    moreover
    from a True have "last (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l) = Declassification lev (eval_exp e (mem g\<^sub>1')) e"
      using trace_last_event_le g\<^sub>2'_props
      by auto
    ultimately have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
      unfolding global_equiv_defs
      using a trace_last_event_le g\<^sub>2_props g\<^sub>2'_props
      using `trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l`
      by auto
    with a True show ?thesis 
      unfolding global_equiv_defs
      using g\<^sub>2'_props g\<^sub>2_props by auto
  next
    case False
    with a show ?thesis
      using trace_append_not_visible
      using g\<^sub>2_props g\<^sub>2'_props
      unfolding global_equiv_defs
      by auto
  qed
next
  case (DOut_type  e l\<^sub>e A l')
  note a = DOut_type
  then have g\<^sub>2_props: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1),
                            trace := trace g\<^sub>1 @ [Declassification l' (eval_exp e (mem g\<^sub>1)) e]\<rparr>"
    by auto
  from a have g\<^sub>2'_props:
    "g\<^sub>2' = g\<^sub>1'\<lparr>mem := (mem g\<^sub>1'),
              trace := trace g\<^sub>1' @ [Declassification l' (eval_exp e (mem g\<^sub>1')) e]\<rparr>"
    by auto
  from a show ?case
  proof (cases "l' \<le> l")
    case True
    with a have "last (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = Declassification l' (eval_exp e (mem g\<^sub>1)) e"
      using trace_last_event_le g\<^sub>2_props
      apply clarsimp
      by (metis event_level.simps(3))
    moreover
    from a True have "last (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l) = Declassification l' (eval_exp e (mem g\<^sub>1')) e"
      using trace_last_event_le g\<^sub>2'_props
      by auto
    ultimately have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
      unfolding global_equiv_defs
      using a trace_last_event_le g\<^sub>2_props g\<^sub>2'_props
      using `trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l`
      by auto
    with a True show ?thesis 
      unfolding global_equiv_defs
      using g\<^sub>2'_props g\<^sub>2_props by auto
  next
    case False
    with a show ?thesis
      using trace_append_not_visible
      using g\<^sub>2_props g\<^sub>2'_props
      unfolding global_equiv_defs
      by auto
  qed
next
  case (If_type c\<^sub>1 c\<^sub>2 A e)
  then show ?case
    using eval_if_elim by auto
next
  case (Out_type l' A e)
  note a = Out_type
  then have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>2)"
    using mem_loweq_eval_same
    by auto
  moreover from a have "g\<^sub>2 = g\<^sub>1\<lparr>trace := trace g\<^sub>1 @ [Output l' (eval_exp e (mem g\<^sub>1)) e]\<rparr>" 
    by auto
  moreover from a have "g\<^sub>2' = g\<^sub>1'\<lparr>trace := trace g\<^sub>1' @ [Output l' (eval_exp e (mem g\<^sub>1')) e]\<rparr>"
    by auto
  ultimately show ?case
    unfolding global_state_equiv_def trace_equiv_def mem_loweq_def env_loweq_def
    using a(3) a(7) env_loweq_def global_state_equiv_def mem_loweq_def by force
next
  case (UIn_type x A l')
  note a = this
  from a have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 l')),
                              env := (env g\<^sub>1)(l' := ltl (env g\<^sub>1 l')),
                              trace := trace g\<^sub>1 @ [Input l' (lhd (env g\<^sub>1 l'))]\<rparr>" by auto
  from a have [simp]: "g\<^sub>2' = g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := lhd (env g\<^sub>1' l')),
                                env := (env g\<^sub>1')(l' := ltl (env g\<^sub>1' l')),
                                trace := trace g\<^sub>1' @ [Input l' (lhd (env g\<^sub>1' l'))]\<rparr>"
    by auto
  with a show ?case
    unfolding global_equiv_defs
    apply (cases "l' \<le> l")
     apply clarsimp
    using order_trans by auto
next
  case (LIn_type x lev l' A)
  note a = LIn_type
  from a have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 l')),
                              env := (env g\<^sub>1)(l' := ltl (env g\<^sub>1 l')),
                              trace := trace g\<^sub>1 @ [Input l' (lhd (env g\<^sub>1 l'))]\<rparr>" by auto
  from a have [simp]: "g\<^sub>2' = g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := lhd (env g\<^sub>1' l')),
                                env := (env g\<^sub>1')(l' := ltl (env g\<^sub>1' l')),
                                trace := trace g\<^sub>1' @ [Input l' (lhd (env g\<^sub>1' l'))]\<rparr>"
    by auto
  with a show ?case
    unfolding global_equiv_defs
    apply (cases "l' \<le> l")
     apply clarsimp
    using order_trans
    apply clarsimp
    by fastforce
next
  case (While_type c A e I)
  then show ?case
    using eval_while_elim
    by auto
next
  case (Acquire_type A loc)
  hence "g\<^sub>2 = g\<^sub>1\<lparr>lock_state := (lock_state g\<^sub>1)(loc := Some (tid l\<^sub>1))\<rparr>" 
        "g\<^sub>2' = g\<^sub>1'\<lparr>lock_state := (lock_state g\<^sub>1')(loc := Some (tid l\<^sub>1'))\<rparr>"
        "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" "l\<^sub>2' = l\<^sub>1'\<lparr>com := Skip\<rparr>"
    by auto
  with Acquire_type show ?case
    unfolding global_equiv_defs
    by simp
next
  case (Release_type A loc)
  hence "g\<^sub>2 = g\<^sub>1\<lparr>lock_state := (lock_state g\<^sub>1)(loc := None)\<rparr>" 
        "g\<^sub>2' = g\<^sub>1'\<lparr>lock_state := (lock_state g\<^sub>1')(loc := None)\<rparr>"
        "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" "l\<^sub>2' = l\<^sub>1'\<lparr>com := Skip\<rparr>"
    by auto
  with Release_type show ?case
    unfolding global_equiv_defs by simp
qed

lemma subtrace_empty:
  "t =\<^sup>t\<^bsub>l\<^esub> [] \<Longrightarrow> t' \<le> t \<Longrightarrow> t' =\<^sup>t\<^bsub>l\<^esub> []"
  apply (induction t, clarsimp)
  apply (case_tac "a", clarsimp)
  unfolding trace_equiv_def apply clarsimp
    apply (case_tac "x11 \<le> l", (clarsimp | safe)+)[1]
  apply (cases t')
     apply blast
  apply (metis Prefix_Order.prefix_Nil lang_axioms event_level.simps(2) lang.project_trace_upto.simps(2) lang.trace_proj_le_commute)
  apply (case_tac "x21 \<le> l", (clarsimp | safe)+)[1]
   apply (cases t')
    apply blast
   apply (metis Prefix_Order.prefix_Nil event_level.simps(1) project_trace_upto.simps(2) trace_proj_le_commute)
  apply clarsimp
  by (metis Prefix_Order.prefix_Nil lang_axioms event_level.simps(3) lang.project_trace_upto.simps(2) lang.trace_proj_le_commute)

lemma trace_equiv_append_same:
  assumes eqv: "t\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> t\<^sub>2"
  shows "t\<^sub>1 @ [e] =\<^sup>t\<^bsub>l\<^esub> t\<^sub>2 @ [e]"
  using assms
  unfolding trace_equiv_def
  by (metis lang_axioms lang.trace_append_not_visible lang.trace_append_visible)
 

lemma trace_equiv_append_high:
  assumes eqv: "t\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> t\<^sub>2"
  assumes high: "\<not> (event_level e \<le> l)" "\<not> (event_level e' \<le> l)"
  shows "t\<^sub>1 @ [e] =\<^sup>t\<^bsub>l\<^esub> t\<^sub>2 @ [e']"
  using assms
  unfolding trace_equiv_def
  by (metis lang.trace_append_not_visible lang_axioms)

inductive_cases has_type_dassign_elim[elim]: "ah, l \<turnstile> \<Gamma> [{A} x :D= e] \<Gamma>'"
inductive_cases has_type_dout_elim[elim]: "ah, l \<turnstile> \<Gamma> [DOut A lev e] \<Gamma>'"
inductive_cases has_type_seq_elim[elim]: "ah, l \<turnstile> \<Gamma> [c\<^sub>1 ;; c\<^sub>2] \<Gamma>'"

lemma has_type_decl_event_secure:
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and typed: "ah, l \<turnstile> com l\<^sub>1"
      and satisfies_preds: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1"
      and decl: "trace g\<^sub>2 = trace g\<^sub>1 @ [Declassification l' v e]"
      and eq: "c = head_com (com l\<^sub>1)"
    shows "\<exists> l\<^sub>e. exp_level_syn e = Some l\<^sub>e \<and> \<D> l\<^sub>e l' g\<^sub>1 v c"
  using assms
proof (induction rule: eval.induct)
  case (eval_assign l\<^sub>1 A x e g)
  then show ?case by auto
next
  case (eval_dassign l\<^sub>1 A x e l' g)
  from eval_dassign have "g \<Turnstile>\<^sub>P A"
    unfolding active_ann_def by auto
  from eval_dassign have "ah, l \<turnstile> {A} x :D= e"
    by auto
  then show ?case
    apply (rule has_type_dassign_elim)
    using eval_dassign `g \<Turnstile>\<^sub>P A`
    by auto
next
  print_cases
  case (eval_dout l\<^sub>1 A lev e g)
  from eval_dout have "g \<Turnstile>\<^sub>P A"
    unfolding active_ann_def by auto
  from eval_dout have "ah, l \<turnstile> DOut A lev e"
    by auto
  then show ?case
    apply (rule has_type_dout_elim)
    using eval_dout `g \<Turnstile>\<^sub>P A`
    by auto
next
  case (eval_out l A lev e g)
  then show ?case by auto
next
  case (eval_in l A x lev v g)
  then show ?case by auto
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then show ?case by auto
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then show ?case by auto
next
  case (eval_seq l\<^sub>1 c\<^sub>1 c\<^sub>2 g l' g')
  moreover then have "ah, l \<turnstile> c\<^sub>1"
    using has_type_seq_elim
    by auto
  moreover from eval_seq have "c\<^sub>1 \<noteq> Skip"
    by (metis (mono_tags) eval_skip_elim local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
  ultimately show ?case 
    using has_type_seq_elim 
    unfolding active_ann_def
    by simp
next
  case (eval_seqskip l\<^sub>1 c\<^sub>1 c\<^sub>2 g l' g')
  moreover then have "ah, l \<turnstile> c\<^sub>1"
    using has_type_seq_elim
    by auto
  moreover from eval_seqskip have "g \<Turnstile>\<^sub>P active_ann (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>)"
    unfolding active_ann_def by auto
  ultimately show ?case
    using active_ann_def 
    by simp
next
  case (eval_whiletrue l A e c g)
  then show ?case by auto
next
  case (eval_whilefalse l A e c g)
  then show ?case by auto
next
  case (eval_acquire l A loc g)
  then show ?case by auto
next
  case (eval_release l A loc g)
  then show ?case by auto
qed


lemma bisimilar_tid:
  "l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1' \<Longrightarrow> tid l\<^sub>1 = tid l\<^sub>1'"
  by (auto simp: secure_bisim_def)

lemma bisim_sym:
  assumes "l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1'"
  shows "l\<^sub>1' \<approx>\<^bsub>l\<^esub> l\<^sub>1"
  using assms
  by (auto simp: secure_bisim_def to_rel_def sym_def)

lemma bisim_step:
  assumes "l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1'"
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  assumes "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assumes "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1"
  assumes "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
  shows "\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> l\<^sub>2 \<approx>\<^bsub>l\<^esub> l\<^sub>2' \<and>
                   sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
proof -
  note secure_bisim_def[simp]
  from assms(1) obtain \<R> where \<R>_sec: "\<R> l l\<^sub>1 l\<^sub>1'" "secure_bisim l \<R>"
    by auto
  with assms(2-) obtain l\<^sub>2' g\<^sub>2' where "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> l l\<^sub>2 l\<^sub>2' \<and>
                                       sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
    unfolding secure_bisim_def by force
  moreover with \<R>_sec have "l\<^sub>2 \<approx>\<^bsub>l\<^esub> l\<^sub>2'" by auto
  ultimately show ?thesis by auto
qed


lemma trace_equiv_sym[sym]:
  "t =\<^sup>t\<^bsub>l\<^esub> t' \<Longrightarrow> t' =\<^sup>t\<^bsub>l\<^esub> t"
  unfolding trace_equiv_def by auto

lemma mem_loweq_sym[sym]:
  "m =\<^sup>m\<^bsub>l\<^esub> m' \<Longrightarrow> m' =\<^sup>m\<^bsub>l\<^esub> m"
  unfolding mem_loweq_def by auto

lemma env_loweq_sym[sym]:
  "e =\<^sup>e\<^bsub>l\<^esub> e' \<Longrightarrow> e' =\<^sup>e\<^bsub>l\<^esub> e"
  unfolding env_loweq_def by auto

lemma lock_state_equiv_sym[sym]:
  "los\<^sub>1 =\<^sup>l los\<^sub>2 \<Longrightarrow> los\<^sub>2 =\<^sup>l los\<^sub>1"
  unfolding lock_state_equiv_def by auto

lemma global_equiv_sym[sym]:
  "g =\<^sup>g\<^bsub>l\<^esub> g' \<Longrightarrow> g' =\<^sup>g\<^bsub>l\<^esub> g"
  unfolding global_state_equiv_def 
  using trace_equiv_sym mem_loweq_sym env_loweq_sym lock_state_equiv_sym
  by auto

lemma trace_equiv_trans[trans]:
  "t =\<^sup>t\<^bsub>l\<^esub> t' \<Longrightarrow> t' =\<^sup>t\<^bsub>l\<^esub> t'' \<Longrightarrow> t =\<^sup>t\<^bsub>l\<^esub> t''"
  unfolding trace_equiv_def by auto

lemma trace_equiv_refl:
  "t =\<^sup>t\<^bsub>l\<^esub> t"
  unfolding trace_equiv_def by auto

lemma bisim_stuck_iff:
  assumes bisim: "l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1'"
  assumes loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assumes ann_sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
  assumes stuck: "stuck (l\<^sub>1, g\<^sub>1)"
  shows "stuck (l\<^sub>1', g\<^sub>1')"
proof (rule ccontr)
  assume "\<not> stuck (l\<^sub>1', g\<^sub>1')"
  then obtain l\<^sub>2' g\<^sub>2' where "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
    by (auto simp: stuck_def)
  moreover from bisim have "l\<^sub>1' \<approx>\<^bsub>l\<^esub> l\<^sub>1"
    using bisim_sym by auto
  ultimately obtain l\<^sub>2 g\<^sub>2 where "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
    using bisim_step ann_sat loweq
    by (meson global_equiv_sym)
  with stuck show False unfolding stuck_def by auto
qed

lemma \<R>_skip_iff:
  assumes "\<R> ah l l\<^sub>1 l\<^sub>1'"
      and "com l\<^sub>1 = Skip"
    shows "com l\<^sub>1' = Skip"
  using assms
proof (cases rule: \<R>.cases)
  case intro\<^sub>1
  with assms show ?thesis 
    by auto
next
  case intro\<^sub>2
  with assms show ?thesis
    by (auto simp: secure_bisim_def)
next
  case intro\<^sub>3
  with assms show ?thesis by auto
qed

lemma \<R>_closed_seq:
  assumes R: "\<R> ah l l\<^sub>1 l\<^sub>1'"
      and "l\<^sub>2 = l\<^sub>1\<lparr>com := com l\<^sub>1 ;; c\<rparr>"
          "l\<^sub>2' = l\<^sub>1'\<lparr>com := com l\<^sub>1' ;; c\<rparr>"
      and "ah, l \<turnstile> c"
    shows "\<R> ah l l\<^sub>2 l\<^sub>2'"
  using R
  apply (cases rule: \<R>.cases)
  using assms by blast+

lemma event_occurrence_independent_of_mem:
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and ev': "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')" 
    shows "(trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1) = (trace g\<^sub>2' =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1')"
  using assms unfolding trace_equiv_def
proof (induction arbitrary: l\<^sub>2 l\<^sub>2' g\<^sub>2' rule: eval.induct)
  case (eval_assign l A x e g)
  then show ?case by fastforce
next
  case (eval_dassign l\<^sub>1 A x e l' g\<^sub>1)
  then have [simp]: "trace g\<^sub>2' = trace g\<^sub>1' @ [Declassification l' (eval_exp e (mem g\<^sub>1')) e]"
    using eval_dassign_elim by auto
  with eval_dassign show ?case
    apply (cases "l \<le> l'")
     apply clarsimp
    apply (metis append_self_conv event_level.simps(3) not_Cons_self2 trace_append_not_visible trace_append_visible)
    apply clarsimp
    by (metis append_self_conv event_level.simps(3) list.distinct(1) trace_append_not_visible trace_append_visible)
next
  print_cases
  case (eval_dout l\<^sub>1 A lev e g\<^sub>1)
  then have [simp]: "trace g\<^sub>2' = trace g\<^sub>1' @ [Declassification lev (eval_exp e (mem g\<^sub>1')) e]"
    using eval_dassign_elim by auto
  with eval_dassign show ?case
    apply (cases "l \<le> lev")
     apply clarsimp
    apply (metis append_self_conv event_level.simps(3) not_Cons_self2 trace_append_not_visible trace_append_visible)
    apply clarsimp
    by (metis append_self_conv event_level.simps(3) list.distinct(1) trace_append_not_visible trace_append_visible)
next
  case (eval_out l\<^sub>1 A lev e g\<^sub>1)
  then have [simp]: "trace g\<^sub>2' = trace g\<^sub>1' @ [Output lev (eval_exp e (mem g\<^sub>1')) e]"
    by auto
  then show ?case
    apply (cases "lev \<le> l")
    by (auto simp: trace_append_visible trace_append_not_visible)
next
  case (eval_in l\<^sub>1 A x lev v g\<^sub>1)
  then have [simp]: "trace g\<^sub>2' = trace g\<^sub>1' @ [Input lev (lhd (env g\<^sub>1' lev))]"
    by auto
  then show ?case
    by (cases "lev \<le> l", auto simp: trace_append_visible trace_append_not_visible)
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then show ?case by fastforce
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then show ?case by fastforce
next
  case (eval_seq l\<^sub>1 c\<^sub>1 c\<^sub>2 g\<^sub>1 l\<^sub>2 g\<^sub>2)
  from `(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')` show ?case
    using `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2`
    apply (rule eval_seq_elim)
    using eval_seq by auto
next
  case (eval_seqskip l\<^sub>1 c\<^sub>1 c\<^sub>2 g\<^sub>1 l\<^sub>2 g\<^sub>2)
  from `(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')` show ?case
    using `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2`
    apply (rule eval_seq_elim)
    using eval_seqskip by auto
next
  case (eval_whiletrue l A e c g)
  then show ?case by fastforce
next
  case (eval_whilefalse l A e c g)
  then show ?case by fastforce
next
  case (eval_acquire l A loc g)
  then show ?case by fastforce
next
  case (eval_release l A loc g)
  then show ?case by fastforce
qed

lemma trace_append_same_level:
  assumes "length (t\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (t\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
      and "event_level ev = event_level ev'"
    shows "length ((t\<^sub>1 @ [ev]) \<upharpoonleft>\<^sub>\<le> l) = length ((t\<^sub>1' @ [ev']) \<upharpoonleft>\<^sub>\<le> l)"
  using assms
  unfolding trace_proj_filter
  by (induction t\<^sub>1 arbitrary: t\<^sub>1', auto)

lemma eval_same_command_trace_length:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, g\<^sub>2')"
      and "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
    shows "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
  using event_occurrence_independent_of_mem
  apply (clarsimp simp: trace_equiv_def)
  oops

text {*
  This is a second property we want to prove of the type system.

  It says if the event performed from @{term c\<^sub>1} is not a declassification event
  then there exists a matching execution step from @{term mem\<^sub>1'} that preserves low equivalence.

  The intuition is that we can then use this later on in the soundness proof to show
  there exists a final matching step to give us the overall security property
  we want, I think, modulo some details about scheduling and checking that programs get
  stuck at the same time independent of @{term H} data etc.
*}
lemma has_type_progress:
  assumes typed: "ah, l \<turnstile> c"
      and ev\<^sub>1: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and eqv: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and c_eq: "com l\<^sub>1 = c"
      and l_eq: "l\<^sub>1 = l\<^sub>1'"
      and sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1"
      (* and not_decl: "\<And> e. trace g\<^sub>2 = t\<^sub>1 @ [e] \<Longrightarrow> \<not> is_low_declassification l e" *)
    shows "\<exists> g\<^sub>2' l\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
  using assms
proof (induction arbitrary: g\<^sub>1 g\<^sub>2 l\<^sub>2 l\<^sub>1 l\<^sub>1' g\<^sub>1' rule: has_type.induct)
  case (Skip_type)
  then show ?case 
    using eval_skip_elim 
    by blast
next
  case (Seq_type c\<^sub>1 c\<^sub>2)
  note a = Seq_type
  from `(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` show ?case (is "\<exists> g l. ?P g l \<and> ?Q g l")
  proof (rule eval_seq_elim)
    fix g'' 
    fix l'' :: "('level, 'val) local_state"
    assume ev\<^sub>1: "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')"
    assume l''_skip[simp]: "com l'' = Skip"
    assume [simp]: "l\<^sub>2 = l''\<lparr>com := c\<^sub>2\<rparr>"
    assume [simp]: "g\<^sub>2 = g''"
    have A: "active_ann l\<^sub>1' = active_ann_com c\<^sub>1" using a 
      by (auto simp: active_ann_def)
    have A': "active_ann l\<^sub>1 = active_ann_com c\<^sub>1" using a 
      by (auto simp: active_ann_def)
    note foo = a(3)[where l\<^sub>11 = "l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>" and g\<^sub>11 = g\<^sub>1 and l\<^sub>21 = "l''" and g\<^sub>1'1 = g\<^sub>1' 
                    and l\<^sub>1'1 = "l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>"]

    have "\<exists> g\<^sub>2'' l\<^sub>2''. (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2'', g\<^sub>2'') \<and> \<R> ah l l'' l\<^sub>2'' \<and>
                       sec_step l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) g\<^sub>1 g\<^sub>1' g'' g\<^sub>2''"
      using a foo ev\<^sub>1
      by (clarsimp simp: active_ann_def sec_step_def)
    then obtain l\<^sub>2'' g\<^sub>2'' where ev\<^sub>2': "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2'', g\<^sub>2'')" 
      and \<R>': "\<R> ah l l'' l\<^sub>2''"
      and sec: "sec_step l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) g\<^sub>1 g\<^sub>1' g'' g\<^sub>2''"
      by blast
      have [simp]: "head_com (com l\<^sub>1) = head_com c\<^sub>1"
        using Seq_type by auto
    then have sec': "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2''"
      using sec
      unfolding sec_step_def
      apply clarsimp
      by (case_tac ev, auto)
    have "l\<^sub>2 = l\<^sub>2''\<lparr>com := c\<^sub>2\<rparr>"
      using ev\<^sub>1 ev\<^sub>2' tid_constant by auto
    from \<R>' show ?thesis
    proof (cases rule: \<R>.cases)
      case intro\<^sub>1
      hence "l'' = l\<^sub>2''"
        by auto
      with ev\<^sub>2' ev\<^sub>1 l''_skip have [simp]: "com l\<^sub>2'' = Skip"
        by auto
      with ev\<^sub>2' have "?P  g\<^sub>2'' (l\<^sub>2''\<lparr>com := c\<^sub>2\<rparr>)"
        using a(7) a(8) eval.eval_seqskip by auto
      moreover
      from Seq_type have "\<R> ah l l\<^sub>2 (l\<^sub>2''\<lparr>com := c\<^sub>2\<rparr>)"
        using \<R>.intro\<^sub>1 typed_implies_bisim
        by (simp add: \<open>l'' = l\<^sub>2''\<close>)
      moreover
      have [simp]: "head_com (com l\<^sub>1) = head_com c\<^sub>1"
        using Seq_type by auto
      then have "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2''"
        using sec
        unfolding sec_step_def
        apply clarsimp
        by (case_tac ev, auto)
      moreover
      from sec \<R>' have "?Q g\<^sub>2'' (l\<^sub>2'' \<lparr>com := c\<^sub>2\<rparr>)"
        using calculation \<R>.intro\<^sub>1 typed_implies_bisim
        by force
      ultimately show ?thesis by force
    next
      case intro\<^sub>2
      hence bisim': "l'' \<approx>\<^bsub>l\<^esub> l\<^sub>2''"
        by auto
      have tids_eq: "tid l'' = tid l\<^sub>2''"
        using local.intro\<^sub>2 by blast
      from bisim' have l\<^sub>2''_Skip: "com l\<^sub>2'' = Skip"
        apply (clarsimp simp: secure_bisim_def)
        using l''_skip by blast
      hence  "com l'' = com l\<^sub>2''" by simp
      hence "l'' = l\<^sub>2''" using tids_eq by simp

      from l\<^sub>2''_Skip ev\<^sub>2' have "?P  g\<^sub>2'' (l\<^sub>2''\<lparr>com := c\<^sub>2\<rparr>)"
        using a(7) a(8) eval.eval_seqskip by auto
      moreover
      from Seq_type have "\<R> ah l l\<^sub>2 (l\<^sub>2''\<lparr>com := c\<^sub>2\<rparr>)"
        using \<R>.intro\<^sub>1 typed_implies_bisim
        by (simp add: \<open>l'' = l\<^sub>2''\<close>)
      moreover
      have [simp]: "head_com (com l\<^sub>1) = head_com c\<^sub>1"
        using Seq_type by auto
      then have "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2''"
        using sec
        unfolding sec_step_def
        apply clarsimp
        by (case_tac ev, auto)
      moreover
      from sec \<R>' have "?Q g\<^sub>2'' (l\<^sub>2'' \<lparr>com := c\<^sub>2\<rparr>)"
        using calculation \<R>.intro\<^sub>1 typed_implies_bisim
        by force
      ultimately show ?thesis by force
    next
      case intro\<^sub>3
      then show ?thesis 
        using Seq_type
      proof (induction rule: \<R>\<^sub>3.induct)
        case (intro\<^sub>1 l\<^sub>1\<^sub>I c l\<^sub>2\<^sub>I)
        then obtain l\<^sub>N where "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l\<^sub>N, g\<^sub>2)"
          using \<open>g\<^sub>2 = g''\<close> ev\<^sub>1 by blast
        with intro\<^sub>1 obtain l\<^sub>N' g\<^sub>2' where ev\<^sub>N':
          "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>N', g\<^sub>2')" 
          (* "\<R> l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1'\<lparr>com := c\<^sub>1\<rparr>)" *)
          "\<R> ah l l\<^sub>N l\<^sub>N'"
          "sec_step l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1'\<lparr>com := c\<^sub>1\<rparr>) g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
          by (metis (no_types, lifting) A' active_ann_def local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
        show ?case
        proof (cases "com l\<^sub>N = Skip")
          case True
          with intro\<^sub>1 have "com l\<^sub>2 = c\<^sub>2"
            by (simp add: intro\<^sub>1.prems(3) intro\<^sub>1.prems(4))
          from True ev\<^sub>N' have "com l\<^sub>N' = Skip"
            using \<R>_skip_iff
            by auto
          (* have "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> ( *)
          then show ?thesis 
            using intro\<^sub>1 True ev\<^sub>N'
          proof -
            have "l\<^sub>2'' = l''"
              using \<R>' \<R>_skip_iff ev\<^sub>1 ev\<^sub>2' tid_constant by auto
            then show ?thesis
              by (metis (no_types) \<R>.intro\<^sub>1 \<R>\<^sub>1.simps \<open>com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2\<close> \<open>com l\<^sub>2 = c\<^sub>2\<close> \<open>ah, l \<turnstile> c\<^sub>2\<close> \<open>l\<^sub>1 = l\<^sub>1'\<close> \<open>l\<^sub>2 = l''\<lparr>com := c\<^sub>2\<rparr>\<close> ev\<^sub>2' eval.eval_seqskip l''_skip sec')
          qed
        next
          case False
          then show ?thesis
            using intro\<^sub>1 ev\<^sub>N'
            by (metis \<open>(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l\<^sub>N, g\<^sub>2)\<close> ev\<^sub>1 eval_deterministic l''_skip)
        qed
      next 
        case (intro\<^sub>2 l\<^sub>1''' l\<^sub>1'''' c l\<^sub>2''' l\<^sub>2'''') 
        have "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2'', g\<^sub>2'')" by (rule ev\<^sub>2')
        have eqs: "l\<^sub>2'' = l''"
          using \<R>' \<R>_skip_iff ev\<^sub>1 ev\<^sub>2' tid_constant by auto
        with intro\<^sub>2 show ?case
          by (metis \<open>l\<^sub>2 = l''\<lparr>com := c\<^sub>2\<rparr>\<close> ev\<^sub>2' eval.eval_seqskip has_type.Seq_type has_type_preservation l''_skip sec' typed_implies_bisim)
      next
        case (intro\<^sub>3 l\<^sub>1 l\<^sub>1' c l\<^sub>2 l\<^sub>2')
        then show ?case
          by blast 
      qed
    qed
  next
    fix g'' l''
    assume ev\<^sub>1: "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')"
    assume not_skip: "com l'' \<noteq> Skip"
    assume "l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>"
    assume [simp]: "g\<^sub>2 = g''"
    have A: "active_ann l\<^sub>1' = active_ann_com c\<^sub>1" using a 
      by (auto simp: active_ann_def)
    have A': "active_ann l\<^sub>1 = active_ann_com c\<^sub>1" using a 
      by (auto simp: active_ann_def)
    have "\<exists> g\<^sub>2'' l\<^sub>2''. (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2'', g\<^sub>2'') \<and> \<R> ah l l'' l\<^sub>2'' \<and>
                       sec_step l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) g\<^sub>1 g\<^sub>1' g'' g\<^sub>2''"
      using a ev\<^sub>1
      by (clarsimp simp: active_ann_def sec_step_def)
    then obtain l\<^sub>2'' g\<^sub>2'' where ev\<^sub>2': "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2'', g\<^sub>2'')" 
      and \<R>': "\<R> ah l l'' l\<^sub>2''"
      and sec: "sec_step l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) g\<^sub>1 g\<^sub>1' g'' g\<^sub>2''"
      by blast
    with not_skip have "com l\<^sub>2'' \<noteq> Skip"
      apply (clarsimp simp: \<R>_skip_iff)
      by (metis (no_types, lifting) A' \<R>_skip_iff a(10) a(3) a(6) a(9) active_ann_def ev\<^sub>1 eval_deterministic global_equiv_sym local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
    with Seq_type ev\<^sub>2' have "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2''\<lparr>com := com l\<^sub>2'' ;; c\<^sub>2\<rparr>, g\<^sub>2'')"
      by (simp add: \<open>\<And>l\<^sub>2 l\<^sub>1' l\<^sub>1 g\<^sub>2 g\<^sub>1' g\<^sub>1. \<lbrakk>(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2); g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'; com l\<^sub>1 = c\<^sub>1; l\<^sub>1 = l\<^sub>1'; g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1; g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1\<rbrakk> \<Longrightarrow> \<exists>g\<^sub>2' l\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'\<close> \<open>\<And>l\<^sub>2 l\<^sub>1' l\<^sub>1 g\<^sub>2 g\<^sub>1' g\<^sub>1. \<lbrakk>(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2); g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'; com l\<^sub>1 = c\<^sub>2; l\<^sub>1 = l\<^sub>1'; g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1; g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1\<rbrakk> \<Longrightarrow> \<exists>g\<^sub>2' l\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'\<close> eval.eval_seq)
    moreover
    have "(l\<^sub>1, g\<^sub>1) \<leadsto> (l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>, g\<^sub>2)"
      using \<open>l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>\<close> a(5) by blast
    have [simp]: "head_com (com l\<^sub>1) = head_com c\<^sub>1"
      using Seq_type by auto
    then have sec': "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2''"
      using sec
      unfolding sec_step_def
      apply clarsimp
      by (case_tac ev, auto)
    moreover have "\<R> ah l l\<^sub>2 (l\<^sub>2''\<lparr>com := com l\<^sub>2'' ;; c\<^sub>2\<rparr>)"
      using \<R>_closed_seq
      using \<R>' \<open>l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>\<close> a(2) by blast
    ultimately show ?thesis
      using a(8) by blast
  qed
next
  case (LAssign_type x lev l\<^sub>e A e)
  note a = this
  hence [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1'))\<rparr>"
  from a have ev\<^sub>2: "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    using eval_assign by force
  from a show ?case
  proof (cases "lev \<le> l")
    case True
    with a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      unfolding global_equiv_defs
      apply clarsimp
      apply (clarsimp simp: ann_implies_low_def active_ann_def)
      using a global_equiv_le_closed
      by auto
    then show ?thesis
      using a ev\<^sub>2
      apply (clarsimp simp: sec_step_def)
      by (metis \<R>.intro\<^sub>1 \<R>\<^sub>1.intros eval_assign_elim has_type_preservation lang.LAssign_type lang_axioms)
  next
    case False
    with a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      unfolding global_equiv_defs
      by auto
    with a ev\<^sub>2 show ?thesis
      apply (clarsimp simp: sec_step_def)
      by (metis \<R>.intro\<^sub>1 \<R>\<^sub>1.intros eval_assign_elim has_type_preservation lang.LAssign_type lang_axioms)
  qed
next
  case (UAssign_type x A e)
  note a = this
  hence [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1'))\<rparr>"
  from a have ev\<^sub>2: "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    using eval_assign by force
  moreover from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
    unfolding global_state_equiv_def mem_loweq_def
    by auto
  ultimately show ?case
    using a apply (clarsimp simp: sec_step_def)
    by (metis \<R>.intro\<^sub>1 \<R>\<^sub>1.simps eval_assign_elim has_type.UAssign_type has_type_preservation)
next
  case (DAssign_type x lev ex l\<^sub>e A)
  note a = DAssign_type
  hence g\<^sub>2_def: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp ex (mem g\<^sub>1)),
                        trace := trace g\<^sub>1 @ [Declassification lev (eval_exp ex (mem g\<^sub>1)) ex]\<rparr>"
    using eval_dassign_elim
    by auto
  from a have the_eq[simp]: "(THE l. exp_level_syn ex = Some l) = l\<^sub>e"
    by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp ex (mem g\<^sub>1')),
                 trace := trace g\<^sub>1' @ [Declassification lev (eval_exp ex (mem g\<^sub>1')) ex]\<rparr>"
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  from a have same_len: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
    by (auto simp: global_equiv_defs)
  show ?case (is "\<exists> g' l'. ?ev g' l' \<and> ?R g' l' \<and> ?sec g' l'")
  proof -
    from a have "?ev ?g\<^sub>2' ?l\<^sub>2'"
      by (auto simp: sec_step_def g\<^sub>2_def eval_dassign)
    moreover from DAssign_type have "?R ?g\<^sub>2' ?l\<^sub>2'"
      by (auto simp: sec_step_def g\<^sub>2_def Skip_type typed_implies_bisim)
    moreover from DAssign_type have "?sec ?g\<^sub>2' ?l\<^sub>2'"
    proof (cases "lev \<le> l")
      case True
      moreover then have
        "trace g\<^sub>1 @ [Declassification lev (eval_exp ex (mem g\<^sub>1)) ex] =\<^sup>t\<^bsub>l\<^esub>
         trace g\<^sub>1' @ [Declassification lev (eval_exp ex (mem g\<^sub>1')) ex] \<Longrightarrow>
         eval_exp ex (mem g\<^sub>1) = eval_exp ex (mem g\<^sub>1')"
      proof - 
        let ?ev = "Declassification lev (eval_exp ex (mem g\<^sub>1)) ex"
        let ?ev' = "Declassification lev (eval_exp ex (mem g\<^sub>1')) ex"
        assume 
          "trace g\<^sub>1 @ [?ev] =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1' @ [?ev']"
        moreover

        from True have "event_level ?ev \<le> l"
          by auto
        with True trace_append_visible have
          "(trace g\<^sub>1 @ [?ev]) \<upharpoonleft>\<^sub>\<le> l = (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) @ [?ev]"
          by auto
        moreover
        from True have "event_level ?ev' \<le> l"
          by auto
        with True trace_append_visible have
          "(trace g\<^sub>1 @ [?ev']) \<upharpoonleft>\<^sub>\<le> l = (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) @ [?ev']"
          by auto
        ultimately have "?ev = ?ev'"
          by (metis \<open>event_level (Declassification lev (eval_exp ex (mem g\<^sub>1')) ex) \<le> l\<close> last_snoc trace_equiv_def trace_last_event_le)
        thus "eval_exp ex (mem g\<^sub>1) = eval_exp ex (mem g\<^sub>1')"
          by auto
      qed
      moreover 
      from same_len a have "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace ?g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
        using trace_append_same_level
        by (auto simp: g\<^sub>2_def global_equiv_defs)
      ultimately show ?thesis 
        using DAssign_type
        by (auto simp: sec_step_def g\<^sub>2_def active_ann_def mem_loweq_def global_state_equiv_def)
    next
      case False
      with a same_len show ?thesis
        apply (clarsimp simp: sec_step_def g\<^sub>2_def global_state_equiv_def trace_append_same_level)
        apply (simp add: trace_equiv_append_high)
        by (simp add: mem_loweq_def)
    qed
    ultimately show ?thesis by force
  qed
next
  case (DOut_type ex l\<^sub>e A lev)
  note a = DOut_type
  hence g\<^sub>2_def: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1),
                        trace := trace g\<^sub>1 @ [Declassification lev (eval_exp ex (mem g\<^sub>1)) ex]\<rparr>"
    using eval_dassign_elim
    by auto
  from a have the_eq[simp]: "(THE l. exp_level_syn ex = Some l) = l\<^sub>e"
    by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1'),
                 trace := trace g\<^sub>1' @ [Declassification lev (eval_exp ex (mem g\<^sub>1')) ex]\<rparr>"
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  from a have same_len: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
    by (auto simp: global_equiv_defs)
  show ?case (is "\<exists> g' l'. ?ev g' l' \<and> ?R g' l' \<and> ?sec g' l'")
  proof -
    from a have "?ev ?g\<^sub>2' ?l\<^sub>2'"
      by (auto simp: sec_step_def g\<^sub>2_def eval_dout)
    moreover from DOut_type have "?R ?g\<^sub>2' ?l\<^sub>2'"
      by (auto simp: sec_step_def g\<^sub>2_def Skip_type typed_implies_bisim)
    moreover from DOut_type have "?sec ?g\<^sub>2' ?l\<^sub>2'"
    proof (cases "lev \<le> l")
      case True
      moreover then have
        "trace g\<^sub>1 @ [Declassification lev (eval_exp ex (mem g\<^sub>1)) ex] =\<^sup>t\<^bsub>l\<^esub>
         trace g\<^sub>1' @ [Declassification lev (eval_exp ex (mem g\<^sub>1')) ex] \<Longrightarrow>
         eval_exp ex (mem g\<^sub>1) = eval_exp ex (mem g\<^sub>1')"
      proof - 
        let ?ev = "Declassification lev (eval_exp ex (mem g\<^sub>1)) ex"
        let ?ev' = "Declassification lev (eval_exp ex (mem g\<^sub>1')) ex"
        assume 
          "trace g\<^sub>1 @ [?ev] =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1' @ [?ev']"
        moreover

        from True have "event_level ?ev \<le> l"
          by auto
        with True trace_append_visible have
          "(trace g\<^sub>1 @ [?ev]) \<upharpoonleft>\<^sub>\<le> l = (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) @ [?ev]"
          by auto
        moreover
        from True have "event_level ?ev' \<le> l"
          by auto
        with True trace_append_visible have
          "(trace g\<^sub>1 @ [?ev']) \<upharpoonleft>\<^sub>\<le> l = (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) @ [?ev']"
          by auto
        ultimately have "?ev = ?ev'"
          by (metis \<open>event_level (Declassification lev (eval_exp ex (mem g\<^sub>1')) ex) \<le> l\<close> last_snoc trace_equiv_def trace_last_event_le)
        thus "eval_exp ex (mem g\<^sub>1) = eval_exp ex (mem g\<^sub>1')"
          by auto
      qed
      moreover 
      from same_len a have "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace ?g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
        using trace_append_same_level
        by (auto simp: g\<^sub>2_def global_equiv_defs)
      ultimately show ?thesis 
        using DOut_type
        by (auto simp: sec_step_def g\<^sub>2_def active_ann_def mem_loweq_def global_state_equiv_def)
    next
      case False
      with a same_len show ?thesis
        apply (clarsimp simp: sec_step_def g\<^sub>2_def global_state_equiv_def trace_append_same_level)
        apply (simp add: trace_equiv_append_high)
        done
    qed
    ultimately show ?thesis by force
  qed
next
  case (If_type c\<^sub>1 c\<^sub>2 A e)
  note a = this
  show ?case
  proof (cases "val_true (eval_exp e (mem g\<^sub>1)) = val_true (eval_exp e (mem g\<^sub>1'))")
    case True
    note exps_eq = True
    let ?c' = "if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2"
    let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := ?c'\<rparr>"
    from exps_eq a have "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', g\<^sub>1')" 
      using eval.intros by auto
    moreover from a True have "l\<^sub>2 = l\<^sub>1\<lparr>com := ?c'\<rparr>" "g\<^sub>2 = g\<^sub>1" by auto
    moreover with a True have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      by auto
    ultimately show ?thesis
      using a

    proof -
      have "ah, l \<turnstile> com (l\<^sub>1\<lparr>com := ?c'\<rparr>)"
        by (simp add: a)
      then show ?thesis
        apply (simp add: sec_step_def)
        apply (cases "val_true (eval_exp e (mem g\<^sub>1))")
        apply (metis (no_types, lifting) Prefix_Order.same_prefix_nil \<open>(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>1' \<lparr>com := if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2\<rparr>, g\<^sub>1')\<close> \<open>g\<^sub>2 = g\<^sub>1\<close> \<open>g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'\<close> \<open>ah, l \<turnstile> com (l\<^sub>1 \<lparr>com := if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2\<rparr>)\<close> \<open>l\<^sub>2 = l\<^sub>1 \<lparr>com := if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2\<rparr>\<close> a(6) a(9) global_state_equiv_def list_eq.simps(1) list_eq.simps(3) trace_equiv_def trace_monotonic typed_implies_bisim)
        by (metis (no_types, lifting) Prefix_Order.same_prefix_nil \<open>(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>1' \<lparr>com := if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2\<rparr>, g\<^sub>1')\<close> \<open>g\<^sub>2 = g\<^sub>1\<close> \<open>g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'\<close> \<open>ah, l \<turnstile> com (l\<^sub>1 \<lparr>com := if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2\<rparr>)\<close> \<open>l\<^sub>2 = l\<^sub>1 \<lparr>com := if val_true (eval_exp e (mem g\<^sub>1)) then c\<^sub>1 else c\<^sub>2\<rparr>\<close> a(6) a(9) global_state_equiv_def list_eq.simps(1) list_eq.simps(3) trace_equiv_def trace_monotonic typed_implies_bisim)
    qed
  next
    case False
    note ff = this
    have B: "\<not> ann_implies_low l A e"
    proof (rule ccontr)
      assume "\<not> (\<not> ann_implies_low l A e)"
      hence "ann_implies_low l A e" by auto
      with a have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
        unfolding ann_implies_low_def
        using If_type
        by (auto simp: global_state_equiv_def active_ann_def)
      with ff show False by auto
    qed
    with a have "ah"
      by presburger
    with a B have bisim: "\<lparr>com = c\<^sub>1, tid = tid l\<^sub>1\<rparr> \<approx>\<^bsub>l\<^esub> \<lparr>com = c\<^sub>2, tid = tid l\<^sub>1\<rparr>"
      by auto
    hence bisim': "l\<^sub>1\<lparr>com := c\<^sub>1\<rparr> \<approx>\<^bsub>l\<^esub> l\<^sub>1\<lparr>com := c\<^sub>2\<rparr>"
      by (metis (mono_tags, lifting) local_state.surjective local_state.update_convs(1) old.unit.exhaust)
    show ?thesis
    proof (cases "val_true (eval_exp e (mem g\<^sub>1))")
      case True
      with a have [simp]: "l\<^sub>2 = l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>" by auto
      let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := c\<^sub>2\<rparr>"
      from a False True have "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', g\<^sub>1')"
        by (simp add: a(4) a(5) eval.eval_iffalse)
      moreover from a bisim' have "\<R> ah l l\<^sub>2 ?l\<^sub>2'"
        by auto
      with a True show ?thesis
        apply (clarsimp simp: sec_step_def)
        by (metis (no_types, lifting) calculation eval.eval_iftrue eval_deterministic global_state_equiv_def list_eq.simps(1) list_eq.simps(3) self_append_conv trace_equiv_def)
    next
      case False
      with a have [simp]: "l\<^sub>2 = l\<^sub>1\<lparr>com := c\<^sub>2\<rparr>" by auto
      let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := c\<^sub>1\<rparr>"
      from a False ff have "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', g\<^sub>1')"
        by (simp add: a(4) eval.eval_iftrue)
      moreover from a bisim' have "\<R> ah l l\<^sub>2 ?l\<^sub>2'"
        apply clarsimp
        apply (rule \<R>.intro\<^sub>2)
        by (auto simp: secure_bisim_def to_rel_def sym_def)
      ultimately show ?thesis
        using a(6) a(7) a(8) 
        apply (clarsimp simp: sec_step_def)
        apply (rule exI[where x=g\<^sub>1'])
        by auto
    qed
  qed
next
  case (Out_type l' A e)
  note a = Out_type
  hence ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" "g\<^sub>2 = g\<^sub>1\<lparr>trace := trace g\<^sub>1 @ [Output l' (eval_exp e (mem g\<^sub>1)) e]\<rparr>"
    using eval_out_elim by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>trace := trace g\<^sub>1' @ [Output l' (eval_exp e (mem g\<^sub>1')) e]\<rparr>"
  from a have ev': "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    using eval.intros by auto
  show ?case
  proof (cases "l' \<le> l")
    case True
    from True a have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
      unfolding ann_implies_low_def active_ann_def
      by auto
    with True a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'" 
      unfolding global_state_equiv_def
      using trace_equiv_append_same
      by fastforce
    with ev' show ?thesis using a
      apply (clarsimp simp: sec_step_def)
      apply (rule exI[where x = ?g\<^sub>2'])
      apply (clarsimp simp: ev)
      apply (rule exI[where x = ?l\<^sub>2'])
      apply (clarsimp simp: ev)
      by (smt \<open>eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')\<close> global_state_equiv_def has_type.Out_type has_type_preservation trace_append_not_visible trace_append_visible trace_equiv_def typed_implies_bisim)
  next
    case False
    with a ev have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_high
      unfolding global_equiv_defs
      by auto
    with a False have eqv\<^sub>2: "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      unfolding global_state_equiv_def by fastforce
    have no_decl: "\<exists>ev. Output l' (eval_exp e (mem g\<^sub>1)) e = ev \<longrightarrow>
          (case ev of
           Declassification to v ea \<Rightarrow> False
          | _ \<Rightarrow> True)"
      by auto
    with eqv\<^sub>2 ev' a show ?thesis
      apply (clarsimp simp: sec_step_def)
      apply (rule exI[where x = ?g\<^sub>2'])
      apply (clarsimp simp: ev)
      apply (rule exI[where x = ?l\<^sub>2'])
      apply (clarsimp simp: ev)
      by (auto simp: trace_append_same_level Skip_type typed_implies_bisim a global_equiv_defs)
  qed
next
  case (LIn_type x lev l' A)
  note a = this
  hence ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" 
    "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 l')),
            trace := trace g\<^sub>1 @ [Input l' (lhd (env g\<^sub>1 l'))],
            env := (env g\<^sub>1)(l' := ltl (env g\<^sub>1 l'))\<rparr>"
    using eval_in_elim by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>env := (env g\<^sub>1')(l' := ltl (env g\<^sub>1' l')),
                 mem := (mem g\<^sub>1')(x := lhd (env g\<^sub>1' l')),
                 trace := trace g\<^sub>1' @ [Input l' (lhd (env g\<^sub>1' l'))]\<rparr>"
  from a have ev': "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    using eval.eval_in by auto
  show ?case
  proof (cases "l' \<le> l")
    case True
    with a have env_eq[simp]: "env g\<^sub>1 l' = env g\<^sub>1' l'"
      unfolding global_equiv_defs
      by auto
    with ev a True have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_same
      unfolding global_state_equiv_def
      by auto
    moreover from ev a True have "env g\<^sub>2 =\<^sup>e\<^bsub>l\<^esub> env ?g\<^sub>2'"
      unfolding global_equiv_defs
      by simp
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a True ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' True a
      apply (clarsimp simp: sec_step_def ev)
      apply (rule exI[where x = ?g\<^sub>2'])
      apply (rule exI[where x = ?l\<^sub>2'])
      by (auto simp: Skip_type typed_implies_bisim)
  next
    case False
    hence "\<not> lev \<le> l"
      using LIn_type
      by (meson order_trans)
    moreover from False ev a have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_high
      unfolding global_equiv_defs
      by fastforce
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a False ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' a
      apply (clarsimp simp: sec_step_def)
      apply (rule exI[where x = ?g\<^sub>2'])
      apply (rule exI[where x = ?l\<^sub>2'])
      by (auto simp: Skip_type typed_implies_bisim)
  qed
next
  case (UIn_type x A l')
  note a = this
  hence ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" 
    "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 l')),
            trace := trace g\<^sub>1 @ [Input l' (lhd (env g\<^sub>1 l'))],
            env := (env g\<^sub>1)(l' := ltl (env g\<^sub>1 l'))\<rparr>"
    using eval_in_elim by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>env := (env g\<^sub>1')(l' := ltl (env g\<^sub>1' l')),
                 mem := (mem g\<^sub>1')(x := lhd (env g\<^sub>1' l')),
                 trace := trace g\<^sub>1' @ [Input l' (lhd (env g\<^sub>1' l'))]\<rparr>"
  from a have ev': "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    using eval.eval_in by auto
  show ?case
  proof (cases "l' \<le> l")
    case True
    with a have env_eq[simp]: "env g\<^sub>1 l' = env g\<^sub>1' l'"
      unfolding global_equiv_defs
      by auto
    with ev a True have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_same
      unfolding global_state_equiv_def
      by auto
    moreover from ev a True have "env g\<^sub>2 =\<^sup>e\<^bsub>l\<^esub> env ?g\<^sub>2'"
      unfolding global_equiv_defs
      by simp
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a True ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' True a
      apply (clarsimp simp: sec_step_def ev)
      apply (rule exI[where x = ?g\<^sub>2'])
      apply (rule exI[where x = ?l\<^sub>2'])
      by (auto simp: Skip_type typed_implies_bisim)
  next
    case False
    moreover from False ev a have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_high
      unfolding global_equiv_defs
      by fastforce
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a False ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' a
      apply (clarsimp simp: sec_step_def ev)
      apply (rule exI[where x = ?g\<^sub>2'])
      apply (rule exI[where x = ?l\<^sub>2'])
      by (auto simp: Skip_type typed_implies_bisim)
  qed
next
  case (While_type c A e I)
  then have exps_same: "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
    unfolding ann_implies_low_def active_ann_def by auto
  note a = While_type exps_same
  show ?case (is "\<exists> g' l'. ?P g' l'")
  proof (cases "val_true (eval_exp e (mem g\<^sub>1))")
    case False
    with a have ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" "g\<^sub>2 = g\<^sub>1"
    by auto
    let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
    from a ev False have "?P g\<^sub>1' ?l\<^sub>2'"
      apply (clarsimp simp: sec_step_def)
      using Skip_type eval.eval_whilefalse
      by (metis (no_types, lifting) global_state_equiv_def lang.trace_equiv_def lang.typed_implies_bisim lang_axioms local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
    thus ?thesis by blast
  next
    case True
    with a have ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := c ;; While I e I c\<rparr>" "g\<^sub>2 = g\<^sub>1"
      by auto
    let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := c ;; While I e I c\<rparr>"
    from a True have "val_true (eval_exp e (mem g\<^sub>1'))"
      by auto
    with a ev have "?P g\<^sub>1' ?l\<^sub>2'"
      using eval.intros
    proof -
      have "com l\<^sub>1' = While A e I c"
        using \<open>com l\<^sub>1 = While A e I c\<close> \<open>l\<^sub>1 = l\<^sub>1'\<close> by blast
      then show ?thesis
        by (smt True \<open>val_true (eval_exp e (mem g\<^sub>1'))\<close> a(1) a(2) a(3) a(5) a(6) a(8) ev(1) eval_whiletrue_elim has_type.While_type has_type_preservation lang.eval.eval_whiletrue lang_axioms not_Cons_self2 sec_step_def self_append_conv typed_implies_bisim)
    qed
    then show ?thesis by blast
  qed
next
  case (Acquire_type A lo)
  then have ev: "g\<^sub>2 = g\<^sub>1\<lparr>lock_state := (lock_state g\<^sub>1)(lo \<mapsto> tid l\<^sub>1)\<rparr>" by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  and ?g\<^sub>2' = "g\<^sub>1'\<lparr>lock_state := (lock_state g\<^sub>1')(lo := Some (tid l\<^sub>1'))\<rparr>"
  note a = Acquire_type
  then have "l\<^sub>2 = ?l\<^sub>2'"
    by auto
  moreover from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
    unfolding global_equiv_defs by (clarsimp, fastforce)
  moreover
  from a have "lock_state g\<^sub>1' lo = None"
    unfolding global_equiv_defs by auto
  with a have "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    using eval_acquire
    by auto
  ultimately show ?case
    apply (clarsimp simp: Acquire_type sec_step_def ev)
    apply (rule exI[where x = ?g\<^sub>2'])
    apply (rule exI[where x = ?l\<^sub>2'])
    by (auto simp: Skip_type global_equiv_defs)
next
  case (Release_type A lo)
  then have ev: "g\<^sub>2 = g\<^sub>1\<lparr>lock_state := (lock_state g\<^sub>1)(lo := None)\<rparr>" by auto
  let ?l\<^sub>2' = "l\<^sub>1'\<lparr>com := Skip\<rparr>"
  and ?g\<^sub>2' = "g\<^sub>1'\<lparr>lock_state := (lock_state g\<^sub>1')(lo := None)\<rparr>"
  note a = Release_type
  hence "l\<^sub>2 = ?l\<^sub>2'" by auto
  moreover
  from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
    unfolding global_equiv_defs
    by (simp, fastforce)
  moreover
  from a have "(l\<^sub>1', g\<^sub>1') \<leadsto> (?l\<^sub>2', ?g\<^sub>2')"
    unfolding global_equiv_defs
    using eval_release
    by auto
  ultimately show ?case
    apply (clarsimp simp: ev sec_step_def)
    apply (rule exI[where x = ?g\<^sub>2'])
    apply (rule exI[where x = ?l\<^sub>2'])
    by (auto simp: Skip_type global_equiv_defs)
qed

lemma \<R>_tid:
  "\<R> ah l l\<^sub>1 l\<^sub>1' \<Longrightarrow> tid l\<^sub>1 = tid l\<^sub>1'"
proof (induction rule: \<R>.cases)
  case intro\<^sub>1
  then show ?thesis by auto
next
  case intro\<^sub>2
  then show ?thesis by auto
next
  case intro\<^sub>3
  hence "\<R>\<^sub>3 ah l l\<^sub>1 l\<^sub>1'" by auto
  then show ?thesis
    by (induction rule: \<R>\<^sub>3.induct, auto)
qed

lemma \<R>\<^sub>1_step:
  assumes R\<^sub>1: "\<R>\<^sub>1 ah l l\<^sub>1 l\<^sub>1'"
  assumes loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assumes anns_sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  shows "\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
  using assms has_type_progress 
  by (auto, fastforce)

lemma \<R>\<^sub>2_step:
  assumes R\<^sub>2: "\<R>\<^sub>2 ah l l\<^sub>1 l\<^sub>1'"
  assumes loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assumes anns_sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  shows "\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
proof -
  from R\<^sub>2 obtain \<T> where "secure_bisim l \<T>" "\<T> l l\<^sub>1 l\<^sub>1'"
    by auto
  then obtain l\<^sub>2' g\<^sub>2' where
    "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')" "\<T> l l\<^sub>2 l\<^sub>2'" "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
    using loweq anns_sat ev
    unfolding secure_bisim_def
    by meson
  moreover then have "\<R> ah l l\<^sub>2 l\<^sub>2'"
    using \<R>.intro\<^sub>2
    by (metis (full_types) R\<^sub>2 \<R>\<^sub>2.intros \<open>secure_bisim l \<T>\<close> bisimilar.intros ev lang.\<R>\<^sub>2_elim lang.has_type_preservation lang_axioms tid_constant)
  ultimately show ?thesis
    using loweq anns_sat ev
    unfolding secure_bisim_def
    by force
qed

lemma sec_step_closed_seq:
  assumes "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
  shows "sec_step l (l\<^sub>1\<lparr>com := com l\<^sub>1 ;; c\<rparr>) (l\<^sub>1'\<lparr>com := com l\<^sub>1' ;; c\<rparr>) g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
  using assms
  unfolding sec_step_def
  by auto

lemma eval_seq_skipD:
  assumes "(l\<^sub>1\<lparr>com := com l\<^sub>1 ;; c\<rparr>, g\<^sub>1) \<leadsto> (l\<^sub>b, g\<^sub>2)"
    and  "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
    and "com l\<^sub>2 = Skip"
  shows "com l\<^sub>b = c"
  using assms
  apply -
  apply (erule eval_seq_elim[where c\<^sub>1 = "com l\<^sub>1" and c\<^sub>2 = "c"])
    apply auto[1]
  apply simp
  by (smt eval_deterministic local_state.surjective local_state.update_convs(1))

lemma \<R>\<^sub>3_step:
  assumes R\<^sub>3: "\<R>\<^sub>3 ah l l\<^sub>1 l\<^sub>1'"
  assumes loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assumes anns_sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  shows "\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
proof -
  from R\<^sub>3 obtain c\<^sub>1 c\<^sub>2 c\<^sub>1' where seq: "com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2" "com l\<^sub>1' = c\<^sub>1' ;; c\<^sub>2"
    "ah, l \<turnstile> c\<^sub>2"
    apply (induction rule: \<R>\<^sub>3.induct)

    by auto
  from assms show ?thesis
    using seq(3)
  proof (induction arbitrary: l\<^sub>2 rule: \<R>\<^sub>3.induct)
    case (intro\<^sub>1 l\<^sub>C c l\<^sub>1\<^sub>R)
    with ev obtain l\<^sub>I where ev\<^sub>I: "(l\<^sub>C, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2)"
      by auto
    moreover
    obtain l\<^sub>I' g\<^sub>2' where ev\<^sub>I': "(l\<^sub>C, g\<^sub>1') \<leadsto> (l\<^sub>I', g\<^sub>2')"
      using has_type_progress
      by (meson \<open>\<And>thesis. (\<And>l\<^sub>I. (l\<^sub>C, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2) \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> global_state_equiv_def intro\<^sub>1.prems(1) lang.eval_independent_of_memory lang.lock_state_equiv_def lang_axioms)
    ultimately show ?case 
      using \<R>\<^sub>1_step loweq anns_sat ev \<R>_closed_seq
      using Seq_type \<R>\<^sub>1.simps intro\<^sub>1.hyps(1) intro\<^sub>1.hyps(2) intro\<^sub>1.hyps(3) intro\<^sub>1.prems(2) intro\<^sub>1.prems(3) intro\<^sub>1.prems(4) 
      by force
  next
    case (intro\<^sub>2 l\<^sub>1 l\<^sub>1' c l\<^sub>X l\<^sub>X')
    then obtain l\<^sub>I where ev\<^sub>I: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2)"
      by auto
    moreover
    from intro\<^sub>2 assms obtain l\<^sub>I' g\<^sub>2' where ev\<^sub>I': "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>I', g\<^sub>2')"
      "\<R> ah l l\<^sub>I l\<^sub>I'"
      "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
      using \<R>\<^sub>2_step[where l\<^sub>1 = l\<^sub>1 and l\<^sub>1' = l\<^sub>1' and g\<^sub>1 = g\<^sub>1 and g\<^sub>1' = g\<^sub>1']
      apply (clarsimp simp: active_ann_def)
      by (meson calculation intro\<^sub>2.hyps(1))
    hence "sec_step l l\<^sub>X l\<^sub>X' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
      using intro\<^sub>2 sec_step_closed_seq
      by (simp add: sec_step_def)
    from intro\<^sub>2(8) intro\<^sub>2(9) show ?case
    proof (cases "com l\<^sub>I = Skip")
      case True
      from intro\<^sub>2 have c\<^sub>X: "com l\<^sub>X = com l\<^sub>1 ;; c" by auto
      from intro\<^sub>2 have "l\<^sub>X\<lparr>com := com l\<^sub>1\<rparr> = l\<^sub>1"
        by auto
      with True have ev\<^sub>X: "(l\<^sub>X, g\<^sub>1) \<leadsto> (l\<^sub>I\<lparr>com := c\<rparr>, g\<^sub>2)"
        using ev\<^sub>I intro\<^sub>2
        using eval_seqskip
        by force
      moreover
      from True ev\<^sub>I' have "com l\<^sub>I' = Skip"
        using \<R>_skip_iff
        by auto
      moreover
      with ev\<^sub>I' intro\<^sub>2 have "(l\<^sub>X', g\<^sub>1') \<leadsto> (l\<^sub>I'\<lparr>com := c\<rparr>, g\<^sub>2')"
        using eval_seqskip by auto
      moreover have "com l\<^sub>2 = c"
        using ev\<^sub>I intro\<^sub>2 True
        apply clarsimp
        using eval_seq_skipD
        by meson
      moreover have "tid l\<^sub>2 = tid l\<^sub>I'"
        using True intro\<^sub>2 ev\<^sub>I' ev\<^sub>I tid_constant
        by clarsimp
      moreover
      then have [simp]: "l\<^sub>2 = l\<^sub>I'\<lparr>com := c\<rparr>"
        using calculation
        by auto
      ultimately show ?thesis
        using True intro\<^sub>2 ev\<^sub>I' ev\<^sub>I
        by (metis \<open>sec_step l l\<^sub>X l\<^sub>X' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'\<close> typed_implies_bisim)
    next
      case False
      then have "(l\<^sub>X, g\<^sub>1) \<leadsto> (l\<^sub>I\<lparr>com := com l\<^sub>I ;; c\<rparr>, g\<^sub>2)"
        using ev\<^sub>I intro\<^sub>2 eval_seq 
        by force
      moreover
      from False ev\<^sub>I' have "com l\<^sub>I' \<noteq> Skip"
        using \<R>_skip_iff
        by (metis \<R>.intro\<^sub>2 \<R>.intro\<^sub>3 \<R>\<^sub>1_elim \<R>\<^sub>2.intros \<R>\<^sub>2_elim \<R>\<^sub>3_sym_aux \<R>_elim bisim_sym)
      then have "(l\<^sub>X', g\<^sub>1') \<leadsto> (l\<^sub>I'\<lparr>com := com l\<^sub>I' ;; c\<rparr>, g\<^sub>2')"
        using ev\<^sub>I' intro\<^sub>2 eval_seq by force
      ultimately show ?thesis
        by (smt \<open>sec_step l l\<^sub>X l\<^sub>X' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'\<close> ev\<^sub>I'(2) eval_deterministic lang.\<R>_closed_seq lang_axioms local.intro\<^sub>2(2) local.intro\<^sub>2(8) local_state.surjective local_state.update_convs(1))
    qed
  next
    text {* FIXME: This case contains a lot of repetition and
      is pretty similar to @{thm has_type_progress}; should figure how much
      we can factor out into a reusable lemma. *}
    case (intro\<^sub>3 l\<^sub>1 l\<^sub>1' c l\<^sub>X l\<^sub>X')
    then obtain l\<^sub>I where ev\<^sub>I: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2)"
      by auto
    from intro\<^sub>3 obtain l\<^sub>I' g\<^sub>2' where ev\<^sub>I':
      "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>I', g\<^sub>2')"
      "\<R> ah l l\<^sub>I l\<^sub>I'"
      "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
      apply (clarsimp simp: active_ann_def)
      by (meson ev\<^sub>I)
    from ev\<^sub>I' have sec': "sec_step l l\<^sub>X l\<^sub>X' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
      using sec_step_closed_seq intro\<^sub>3 by fastforce
    show ?case 
    proof (cases "com l\<^sub>I = Skip")
      case True
      with ev\<^sub>I have "(l\<^sub>X, g\<^sub>1) \<leadsto> (l\<^sub>I\<lparr>com := c\<rparr>, g\<^sub>2)"
        using intro\<^sub>3 eval_seqskip by force
      with intro\<^sub>3 have "l\<^sub>2 = l\<^sub>I\<lparr>com := c\<rparr>"
        using eval_deterministic by auto
      moreover
      from True ev\<^sub>I' have "com l\<^sub>I' = Skip"
        using \<R>_skip_iff by auto
      with ev\<^sub>I' have "(l\<^sub>X', g\<^sub>1') \<leadsto> (l\<^sub>I'\<lparr>com := c\<rparr>, g\<^sub>2')"
        using eval_seqskip intro\<^sub>3 by force
      moreover
      from intro\<^sub>3 have "\<R> ah l l\<^sub>2 (l\<^sub>I'\<lparr>com := c\<rparr>)"
        using \<R>_closed_seq
      proof -
        have f1: "\<forall>l. \<lparr>com = com l::('level, 'val) com, tid = tid l\<rparr> = l"
          by simp
        have "com l\<^sub>2 = c"
          by (simp add: calculation(1))
        then show ?thesis
          using f1 by (metis (no_types) \<R>_tid calculation(1) ev\<^sub>I'(2) intro\<^sub>3.hyps(2) local_state.update_convs(1) typed_implies_bisim)
      qed
      ultimately show ?thesis 
        using sec'
        by blast
    next
      case False
      with intro\<^sub>3 ev\<^sub>I have "(l\<^sub>X, g\<^sub>1) \<leadsto> (l\<^sub>I\<lparr>com := com l\<^sub>I ;; c\<rparr>, g\<^sub>2)"
        using eval_seq by force
      then have [simp]: "l\<^sub>2 = l\<^sub>I\<lparr>com := com l\<^sub>I ;; c\<rparr>"
        using eval_deterministic intro\<^sub>3 by auto
      moreover
      from False ev\<^sub>I' intro\<^sub>3 have "com l\<^sub>I' \<noteq> Skip" 
        using \<R>_skip_iff
        by (metis (no_types, lifting) \<R>.simps \<R>\<^sub>1.cases \<R>\<^sub>3_sym_aux bisim_sym lang.\<R>\<^sub>2.simps lang_axioms)
      with ev\<^sub>I' intro\<^sub>3 have "(l\<^sub>X', g\<^sub>1') \<leadsto> (l\<^sub>I'\<lparr>com := com l\<^sub>I' ;; c\<rparr>, g\<^sub>2')"
        using eval_seq by force
      moreover
      from intro\<^sub>3 have "\<R> ah l l\<^sub>2 (l\<^sub>I'\<lparr>com := com l\<^sub>I' ;; c\<rparr>)"
        using \<R>_closed_seq ev\<^sub>I'
        by simp
      ultimately show ?thesis
        using sec' by auto
    qed
  qed
qed

lemma typed_bisim_secure:
  "secure_bisim l (\<R> ah)"
  unfolding secure_bisim_def
proof (auto) (* TODO: write an explicit rule for this instead of using auto *)
  show "sym (to_rel (\<R> ah l))"
    using \<R>_sym by auto
next
  fix l\<^sub>1 l\<^sub>1'
  assume "\<R> ah l l\<^sub>1 l\<^sub>1'"
  with \<R>_tid show "tid l\<^sub>1 = tid l\<^sub>1'" by auto
next
  fix l\<^sub>1 l\<^sub>1'
  assume "\<R> ah l l\<^sub>1 l\<^sub>1'" "com l\<^sub>1 = Skip"
  thus "com l\<^sub>1' = Skip"
    using \<R>_skip_iff by auto
next
  fix l\<^sub>1 l\<^sub>1'
  assume "\<R> ah l l\<^sub>1 l\<^sub>1'" "com l\<^sub>1' = Skip"
  moreover
  hence "\<R> ah l l\<^sub>1' l\<^sub>1" using \<R>_sym
    by (auto simp: to_rel_def sym_def)
  ultimately show "com l\<^sub>1 = Skip"
    using \<R>_skip_iff
    by auto
next
  fix l\<^sub>1 l\<^sub>1' l\<^sub>2 g\<^sub>1 g\<^sub>1' g\<^sub>2
  assume R: "\<R> ah l l\<^sub>1 l\<^sub>1'"
  assume loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assume anns_sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
  assume ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  from R show "\<exists>l\<^sub>2' g\<^sub>2'.
          (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> ah l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
  proof (cases rule: \<R>.cases)
    case intro\<^sub>1
    then show ?thesis 
      using \<R>\<^sub>1_step R loweq anns_sat ev by auto
  next
    case intro\<^sub>2
    then show ?thesis
      using \<R>\<^sub>2_step R loweq anns_sat ev by auto
  next
    case intro\<^sub>3
    with \<R>\<^sub>3_step show ?thesis
      by (simp add: anns_sat(1) anns_sat(2) ev loweq) 
  qed
qed

definition bisimilar_list :: "'level \<Rightarrow> ('level, 'val) local_state list \<Rightarrow> 
  ('level, 'val) local_state list \<Rightarrow> bool" (infix "\<approx>\<^sup>L\<index>" 60) where [simp]:
  "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1' \<equiv> length ls\<^sub>1 = length ls\<^sub>1' \<and> list_all2 (bisimilar l) ls\<^sub>1 ls\<^sub>1'"


fun exec_same_trace :: "'level \<Rightarrow> ('level, 'val) execution \<Rightarrow> ('level, 'val) execution \<Rightarrow> bool" (infix "=\<^sup>t\<^sup>e\<index>" 60) where
  "[] =\<^sup>t\<^sup>e\<^bsub>l\<^esub> [] = True" |
  "(x # xs) =\<^sup>t\<^sup>e\<^bsub>l\<^esub> [] = False" |
  "[] =\<^sup>t\<^sup>e\<^bsub>l\<^esub> (x # xs) = False" |
  "((ls\<^sub>1, g\<^sub>1, sched\<^sub>1) # exec\<^sub>1) =\<^sup>t\<^sup>e\<^bsub>l\<^esub> ((ls\<^sub>2, g\<^sub>2, sched\<^sub>2) # exec\<^sub>2) = 
    (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l \<and> exec\<^sub>1 =\<^sup>t\<^sup>e\<^bsub>l\<^esub> exec\<^sub>2)"

fun exec_low_sim :: "'level \<Rightarrow> ('level, 'val) execution \<Rightarrow> ('level, 'val) execution \<Rightarrow> bool" (infix "\<sim>\<^sup>L\<index>" 60) where
  "[] \<sim>\<^sup>L\<^bsub>l\<^esub> [] = True" |
  "(x # xs) \<sim>\<^sup>L\<^bsub>l\<^esub> [] = False" |
  "[] \<sim>\<^sup>L\<^bsub>l\<^esub> (x # xs) = False" |
  "((ls\<^sub>1, g\<^sub>1, sched\<^sub>1) # exec\<^sub>1) \<sim>\<^sup>L\<^bsub>l\<^esub> ((ls\<^sub>2, g\<^sub>2, sched\<^sub>2) # exec\<^sub>2) =
    (ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2 \<and> sched\<^sub>1 = sched\<^sub>2 \<and> g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2 \<and> exec\<^sub>1 \<sim>\<^sup>L\<^bsub>l\<^esub> exec\<^sub>2)"

lemma anns_satisfied_step:
  "anns_satisfied (ls, g, sched) \<Longrightarrow> (ls, g, sched) \<rightarrow> (ls', g', sched') \<Longrightarrow>
  anns_satisfied (ls', g', sched')"
  unfolding anns_satisfied.simps
  by (meson converse_rtrancl_into_rtrancl)

lemma anns_satisfied_multi:
  "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched') \<Longrightarrow> anns_satisfied (ls, g, sched) \<Longrightarrow>
  anns_satisfied (ls', g', sched')"
  apply (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  using anns_satisfied_step by auto


lemma event_occurrence_independent_of_mem_global:
  assumes ev: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and ev': "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
      and los_eq: "lock_state g\<^sub>1 = lock_state g\<^sub>1'"
    shows "(trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1) \<longleftrightarrow> (trace g\<^sub>2' =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1')"
  using assms
  apply (induction rule: global_eval.induct)
  apply (metis lang_axioms eval_independent_of_memory global_eval.global_step global_eval_deterministic lang.event_occurrence_independent_of_mem next_thread.simps)
  using stuck_def stuck_independent_of_memory
  by (metis (no_types, lifting) global_eval.cases next_sched_independent_of_memory trace_equiv_def)

fun glob_state :: "('level, 'val) global_conf \<Rightarrow> ('level, 'val) global_state" where
  "glob_state (ls, g, sched) = g"


lemma trace_proj_length_eq:
  "t =\<^sup>t\<^bsub>l\<^esub> t' \<Longrightarrow> length (t \<upharpoonleft>\<^sub>\<le> l) = length (t' \<upharpoonleft>\<^sub>\<le> l)"
  unfolding trace_equiv_def trace_proj_filter
  by auto

lemma length_prefix_ne:
  "t \<le> t' \<Longrightarrow> t \<noteq> t' \<Longrightarrow> length t < length t'"
  apply (induction t', clarsimp)
  apply (induction t, clarsimp)
  using Prefix_Order.prefixE by fastforce

lemma length_filter_ne:
  "t \<le> t' \<Longrightarrow> filter P t \<noteq> filter P t' \<Longrightarrow> length t < length t'"
  using length_prefix_ne trace_proj_le_commute filter_sublist
  by fastforce

lemma exec_events_shorten:
  assumes ex: "global_exec (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) exec\<^sub>2"
      and ev: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and ex_eq: "exec_events (trace g\<^sub>2) exec\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> t"
      and t_eq: "trace g\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> t"
  shows "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1"
proof (rule ccontr)
  assume neq: "\<not> (trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1)"
  from ev trace_monotonic have "trace g\<^sub>1 \<le> trace g\<^sub>2" by auto
  with neq have length_ne: "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) > length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l)"
    unfolding trace_equiv_def trace_proj_filter
    by (simp add: filter_sublist length_prefix_ne)
  with exec_trace_monotonic ex have 
    "length (exec_events (trace g\<^sub>2) exec\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) \<ge> length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l)"
  proof -
    from ex have "trace g\<^sub>2 \<le> exec_events (trace g\<^sub>2) exec\<^sub>2"
      using exec_trace_monotonic
      by blast
    thus ?thesis
      by (simp add: Prefix_Order.prefix_length_le trace_proj_le_commute)
  qed
  with ex_eq t_eq length_ne show False
    unfolding trace_equiv_def trace_proj_filter
    by auto
qed

lemma eval_at_most_one_event_proj:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  shows "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) \<or> 
         length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) + 1"
  using assms unfolding trace_proj_filter
  by (induction rule: eval.induct, auto)

lemma eval_at_most_one_event_proj_global:
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  shows "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) \<or> 
         length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) + 1"
  using assms eval_at_most_one_event_proj
  by (induction rule: global_eval.induct, auto)

lemma exec_events_split:
  assumes ex: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
      and ex': "global_exec (ls\<^sub>1', g\<^sub>1', sched\<^sub>1') exec\<^sub>1'"
      and ex_eq: "exec_events (trace g\<^sub>1) exec\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>1') exec\<^sub>1'"
      and same_length: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
    shows "trace g\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1'"
proof (rule ccontr)
  let ?g\<^sub>1 = "trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l"
  let ?g\<^sub>1' = "trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l"
  let ?e\<^sub>1 = "exec_events (trace g\<^sub>1) exec\<^sub>1 \<upharpoonleft>\<^sub>\<le> l"
  let ?e\<^sub>1' = "exec_events (trace g\<^sub>1') exec\<^sub>1' \<upharpoonleft>\<^sub>\<le> l"
  assume "\<not> (trace g\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1')"
  with same_length obtain i where elt_neq: "i < length ?g\<^sub>1" "?g\<^sub>1 ! i \<noteq> ?g\<^sub>1' ! i"
    using trace_equiv_def nth_equalityI by blast
  moreover from ex have "trace g\<^sub>1 \<le> exec_events (trace g\<^sub>1) exec\<^sub>1"
    using exec_trace_monotonic by auto
  hence g\<^sub>1_le: "trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l \<le> exec_events (trace g\<^sub>1) exec\<^sub>1 \<upharpoonleft>\<^sub>\<le> l"
    unfolding trace_proj_filter
    using filter_sublist by auto
  moreover from ex' have "trace g\<^sub>1' \<le> exec_events (trace g\<^sub>1') exec\<^sub>1'"
    using exec_trace_monotonic by auto
  hence g\<^sub>1'_le: "trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l \<le> exec_events (trace g\<^sub>1') exec\<^sub>1' \<upharpoonleft>\<^sub>\<le> l"
    unfolding trace_proj_filter
    using filter_sublist by auto
  ultimately have "?e\<^sub>1 ! i \<noteq> ?e\<^sub>1' ! i"
  proof -
    have "?e\<^sub>1 ! i = ?g\<^sub>1 ! i"
      using g\<^sub>1_le sublist_index elt_neq
      by fastforce
    moreover have "?e\<^sub>1' ! i = ?g\<^sub>1' ! i"
      using g\<^sub>1'_le sublist_index elt_neq same_length
      by fastforce
    ultimately show ?thesis using elt_neq by auto
  qed
  with ex_eq show False
    unfolding trace_equiv_def by auto
qed

lemma eval_preserves_trace_length:
    assumes ev\<^sub>1: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and ev\<^sub>1': "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
      and len_eq: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
      and bisim: "l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1'"
      and sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
      and eqv: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and los_eq: "lock_state g\<^sub>1 = lock_state g\<^sub>1'"
    shows "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
proof -
  from assms obtain l\<^sub>I' g\<^sub>I' where ev\<^sub>I':
    "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>I', g\<^sub>I') \<and> l\<^sub>2 \<approx>\<^bsub>l\<^esub> l\<^sub>I' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>I'"
    apply clarsimp
    using bisim_step local.bisim by presburger
  moreover
  with assms eval_deterministic have [simp]: "l\<^sub>I' = l\<^sub>2'" "g\<^sub>I' = g\<^sub>2'"
    by auto
  ultimately show ?thesis
    apply (clarsimp simp: sec_step_def global_equiv_defs)
    apply (cases "\<exists> ev. trace g\<^sub>2 = trace g\<^sub>1 @ [ev]", clarsimp)
    apply (rename_tac ev)
    by (case_tac ev, auto simp: global_equiv_defs)
qed

lemma global_eval_preserves_trace_length_eq:
  assumes ev\<^sub>1: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and ev\<^sub>1': "(ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
      and len_eq: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
      and bisim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'"
      and loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and anns_sat: "anns_satisfied_now (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)" "anns_satisfied_now (ls\<^sub>1', g\<^sub>1', sched\<^sub>1)"
      and los_eq: "lock_state g\<^sub>1 = lock_state g\<^sub>1'"
    shows "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
  using ev\<^sub>1
proof (cases rule: global_eval.cases)
  case (global_step n l\<^sub>2)
  from assms have bisim': "ls\<^sub>1 ! n \<approx>\<^bsub>l\<^esub> ls\<^sub>1' ! n"
    by (metis lang.bisimilar_list_def lang_axioms list_all2_nthD local.global_step(2) local.global_step(3) mod_less_divisor next_thread.simps)
  moreover 
  with global_step assms bisim_stuck_iff have "\<not> stuck (ls\<^sub>1' ! n, g\<^sub>1')"
    apply (clarsimp simp: next_thread.simps)
    by (metis (no_types, lifting) bisim_step calculation list_all_length local.global_step(2) mod_less_divisor stuck_def)
  with assms global_step obtain l\<^sub>2' where "(ls\<^sub>1' ! n, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
    apply (clarsimp simp: stuck_def)
    by (metis \<open>\<not> stuck (ls\<^sub>1' ! n, g\<^sub>1')\<close> global_eval_elim next_thread.simps)
  moreover from assms global_step have "next_thread (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) = n"
    by (auto simp: next_thread.simps)
  moreover
  note foo = eval_preserves_trace_length[where l\<^sub>1 = "ls\<^sub>1 ! n" and l\<^sub>1' = "ls\<^sub>1' ! n" and l\<^sub>2 = "l\<^sub>2"
                                         and l\<^sub>2' = l\<^sub>2' and g\<^sub>1 = g\<^sub>1 and g\<^sub>1' = g\<^sub>1' and l = l
                                         and g\<^sub>2 = g\<^sub>2 and g\<^sub>2' = g\<^sub>2']
  ultimately show ?thesis
    using global_step eval_preserves_trace_length assms
    apply simp
    apply (rule foo)
    using assms apply clarsimp
          apply (metis list_all_length local.global_step(2) mod_less_divisor next_thread.simps)
    by (clarsimp | (metis list_all_length local.global_step(2) mod_less_divisor next_thread.simps))+
next
  case (global_wait n)
  with assms have "ls\<^sub>1 ! n \<approx>\<^bsub>l\<^esub> ls\<^sub>1' ! n"
    by (clarsimp simp: next_thread.simps list_all2_nthD)
  with assms global_wait have "stuck (ls\<^sub>1' ! n, g\<^sub>1')"
    using bisim_stuck_iff
    apply clarsimp
    by (metis (no_types, lifting) \<open>ls\<^sub>1 ! n \<approx>\<^bsub>l\<^esub> ls\<^sub>1' ! n\<close> length_0_conv list_all_length mod_less_divisor neq0_conv next_thread.simps)
  with assms have "g\<^sub>2' = g\<^sub>1'"
    apply clarsimp
    by (metis global_eval_elim local.global_wait(5) next_thread.simps stuck_def)
  with assms global_wait show ?thesis 
    by (auto simp: global_equiv_defs)
qed

fun sec_step_global :: "'level \<Rightarrow>
                        ('level, 'val) global_conf \<Rightarrow> ('level, 'val) global_conf \<Rightarrow>
                        ('level, 'val) global_conf \<Rightarrow> ('level, 'val) global_conf \<Rightarrow> bool"
  where
  "sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1') (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2') =
    (let n = next_thread (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) in
     let n' = next_thread (ls\<^sub>1', g\<^sub>1', sched\<^sub>1') in
     sec_step l (ls\<^sub>1 ! n) (ls\<^sub>1' ! n') g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2')"

declare sec_step_global.simps[simp del]

lemma bisim_step_global:
  assumes step: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  assumes anns_sat: "list_all (\<lambda> l\<^sub>1. g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1) ls\<^sub>1"
                    "list_all (\<lambda> l\<^sub>1'. g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1') ls\<^sub>1'"
  assumes bisim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'"
  assumes loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  notes secure_bisim_def[simp]
 
  shows "\<exists> g\<^sub>2' ls\<^sub>2'. (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2) \<and> 
                     ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2' \<and>
                     sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) 
                                       (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
  using step
proof (cases rule: global_eval.cases)
  case (global_step n l\<^sub>2)
  hence n_le: "n < length ls\<^sub>1"
    using next_thread.simps by force
  with global_step bisim have bisiml: "ls\<^sub>1 ! n \<approx>\<^bsub>l\<^esub> ls\<^sub>1' ! n"
    by (simp add: list_all2_nthD)
  moreover then have "tid (ls\<^sub>1 ! n) = tid (ls\<^sub>1' ! n)"
    using bisimilar_tid by auto
  moreover from n_le anns_sat have "g\<^sub>1 \<Turnstile>\<^sub>P active_ann (ls\<^sub>1 ! n)"
    using list_all_length by fastforce
  moreover from n_le bisim anns_sat have "g\<^sub>1' \<Turnstile>\<^sub>P active_ann (ls\<^sub>1' ! n)"
    using list_all_length by fastforce
  ultimately obtain l\<^sub>2' g\<^sub>2' where ev\<^sub>1': "(ls\<^sub>1' ! n, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
      "sec_step l (ls\<^sub>1 ! n) (ls\<^sub>1' ! n) g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'" "l\<^sub>2 \<approx>\<^bsub>l\<^esub> l\<^sub>2'"
    using loweq anns_sat global_step
    using bisim_step[where l\<^sub>1 = "ls\<^sub>1 ! n" and l\<^sub>1' = "ls\<^sub>1' ! n" and l\<^sub>2 = "l\<^sub>2"]
    by meson
  moreover
  define ls\<^sub>2' where [simp]: "ls\<^sub>2' = ls\<^sub>1' [n := l\<^sub>2']"
  from bisim global_step have "next_thread (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) = n"
    by (simp add: next_thread.simps)
  with ev\<^sub>1' have "(ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
    using local.bisim local.global_step(1) local.global_step(2) by auto
  moreover have "\<And> i. \<lbrakk> i \<noteq> n ; i < length ls\<^sub>2 \<rbrakk> \<Longrightarrow> ls\<^sub>2 ! i \<approx>\<^bsub>l\<^esub> ls\<^sub>2' ! i"
    by (metis ev\<^sub>1'(3) lang.bisimilar_list_def lang_axioms list_all2_nthD list_all2_update_cong local.bisim local.global_step(5) ls\<^sub>2'_def)
  ultimately have "\<And> i. i < length ls\<^sub>2 \<Longrightarrow> ls\<^sub>2 ! i \<approx>\<^bsub>l\<^esub> ls\<^sub>2' ! i"
    using ev\<^sub>1' global_step
    apply (case_tac "i = n", clarsimp)
    using local.bisim by auto
  moreover from ev\<^sub>1' have
    "sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
    apply (simp add: Let_def global_step)
    by (simp add: sec_step_global.simps sec_step_def)
  then show ?thesis
    apply (clarsimp simp: Let_def)
    by (metis \<open>(ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)\<close> ev\<^sub>1'(3) lang.bisimilar_list_def lang_axioms length_list_update list_all2_update_cong local.bisim local.global_step(5) ls\<^sub>2'_def)
next
  case (global_wait n)
  hence n_lt: "n < length ls\<^sub>1"
    using next_thread.simps by auto
  define l\<^sub>1 where [simp]: "l\<^sub>1 = ls\<^sub>1 ! n" 
  define l\<^sub>1' where [simp]: "l\<^sub>1' = ls\<^sub>1' ! n"
  from bisim have "l\<^sub>1 \<approx>\<^bsub>l\<^esub> l\<^sub>1'"
    apply simp
    by (meson n_lt list_all2_nthD)
  with global_wait have "stuck (l\<^sub>1', g\<^sub>1')"
    using bisim_stuck_iff
    by (metis assms(2) assms(3) l\<^sub>1'_def l\<^sub>1_def lang.bisimilar_list_def lang_axioms list_all_length local.bisim loweq n_lt)
  moreover from bisim global_wait have "next_thread (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) = n"
    by (auto simp: next_thread.simps)
  moreover have "sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) 
                                   (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>1', g\<^sub>1', sched\<^sub>2)"
    using global_wait
    by (simp add: loweq sec_step_global.simps sec_step_def)
  ultimately show ?thesis 
    using n_lt
    apply -
    apply (rule exI[where x=g\<^sub>1'])
    apply (rule exI[where x=ls\<^sub>1'])
    using global_eval.global_wait
    using local.bisim local.global_wait(1) local.global_wait(3) by auto
qed

lemma trace_eqv_no_decl:
  assumes "t\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> t\<^sub>2"
      and "t\<^sub>2 = t\<^sub>1 @ [ev]"
      and "ev = Declassification to v e"
    shows "\<not> to \<le> l"
  using assms
  unfolding trace_equiv_def trace_proj_filter
  by clarsimp

lemma has_type_preservation_global:
  assumes typed: "list_all (has_type ah lev \<circ> com) ls\<^sub>1"
      and meval: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    shows "list_all (has_type ah lev \<circ> com) ls\<^sub>2"
  using meval typed
proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  case (rtrancl_refl ls\<^sub>1 g\<^sub>1 sched\<^sub>1)
  then show ?case 
    by simp
next
  case (rtrancl_into_rtrancl ls\<^sub>1 g\<^sub>1 sched\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>3 g\<^sub>3 sched\<^sub>3)
  hence IH: "list_all (\<lambda>l. ah, lev \<turnstile> com l) ls\<^sub>2"
    by auto
  from `(ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<rightarrow> (ls\<^sub>3, g\<^sub>3, sched\<^sub>3)` show ?case
    using rtrancl_into_rtrancl IH
  proof (induction rule: global_eval.induct)
    case (global_step ls n g sched l' g' ls')
    then show ?case
      using has_type_preservation
      by (simp add: lift_property_global)
  next
    case (global_wait ls n g sched)
    then show ?case
      by blast 
  qed
qed

lemma has_type_decl_event_secure_global:
  assumes ev: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and typed: "list_all (has_type ah l \<circ> com) ls\<^sub>1"
      and satisfies_preds: "list_all (\<lambda> l\<^sub>1. g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1) ls\<^sub>1"
      and decl: "trace g\<^sub>2 = trace g\<^sub>1 @ [Declassification l' v e]"
      and c_eq: "c = head_com (com (ls\<^sub>1 ! next_thread (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)))"
    shows "\<exists> l\<^sub>e. exp_level_syn e = Some l\<^sub>e \<and> \<D> l\<^sub>e l' g\<^sub>1 v c"
  using assms
proof (induction rule: global_eval.induct)
  case (global_step ls n g sched l' g' ls')
  then show ?case
    using has_type_decl_event_secure list_all_length mod_less_divisor
    unfolding next_thread.simps
    apply clarsimp
    by (metis (no_types, lifting) global_step.hyps(1) list_all_length mod_less_divisor)
next
  case (global_wait ls n g sched)
  then show ?case by auto
qed

(* The following isn't actually true right now, since equality
   of declassification events doesn't tell us that the result
   of the declassification doesn't tell us that they got assigned
   to the same variable; we need to either add this requirement
   to the bisimulation or declassification event. *)
lemma decl_same_event_loweq:
  assumes "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1 @ [Declassification lev v e]"
      and "trace g\<^sub>2' =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1' @ [Declassification lev v e]"
      and "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (l\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and "(ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow> (l\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      and "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  shows "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
  oops

text {* Try to exploit more symmetry than in the previous version of the catch up lemma. *}
lemma global_exec_catch_up':
  assumes eval\<^sub>1: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
      and eval\<^sub>1': "global_exec (ls\<^sub>1', g\<^sub>1', sched\<^sub>1') exec\<^sub>1'"
      and length_le: "length exec\<^sub>1' \<le> length exec\<^sub>1"
      and loweq: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and cmds_sim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'"
      and typed: "list_all (has_type ah l \<circ> com) ls\<^sub>1"
      and scheds_eq: "sched\<^sub>1' = sched\<^sub>1"
      and sat: "anns_satisfied (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)" "anns_satisfied (ls\<^sub>1', g\<^sub>1', sched\<^sub>1)"
      and same_events: "exec_events (trace g\<^sub>1) exec\<^sub>1 =\<^sup>t\<^bsub>l\<^esub>
                        exec_events (trace g\<^sub>1') exec\<^sub>1'"
    shows "\<exists> exec'. exec_events (trace g\<^sub>1') (exec\<^sub>1' @ exec') =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>1') exec\<^sub>1' \<and>
                    global_exec (ls\<^sub>1', g\<^sub>1', sched\<^sub>1') (exec\<^sub>1' @ exec') \<and>
                    exec\<^sub>1 \<sim>\<^sup>L\<^bsub>l\<^esub> (exec\<^sub>1' @ exec')"
  using assms(2-)
proof (induction arbitrary: exec\<^sub>1' g\<^sub>1' ls\<^sub>1' sched\<^sub>1' rule: global_exec_induct[OF eval\<^sub>1])
  case (1 ls g sched)
  note refl = 1
  from 1(1) show ?case
    using 1(2-)
  proof (induction rule: global_exec.induct)
    case (global_meval_full_refl ls g sched)
    show ?case
      apply (rule exI[where x = "[]"])
      unfolding trace_equiv_def  
      using global_exec.intros global_meval_full_refl
      by auto
  next
    case (global_meval_full_step ls g sched ls' g' sched' exec)
    (* Contradicts length assumption: *)
    hence False 
      by auto
    then show ?case by auto
  qed    
next
  case (2 ls\<^sub>1 g\<^sub>1 sched\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 exec\<^sub>2)
  note gstep = 2
  show ?case
    using gstep(1-3, 5-)
  proof (induction arbitrary: g\<^sub>1 sched\<^sub>1 ls\<^sub>1 rule: global_exec_induct[OF 2(4)])
    case (1 ls\<^sub>1' g\<^sub>1' sched\<^sub>1')
    note a = 1
    then have cmd\<^sub>1_eqs: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'" "sched\<^sub>1' = sched\<^sub>1"
      by auto
    from a have "trace g\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1'"
      unfolding global_state_equiv_def
      by blast
    from a obtain ls\<^sub>2' g\<^sub>2' where
      eval\<^sub>1': "(ls\<^sub>1', g\<^sub>1', sched\<^sub>1') \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)" "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2'"
              "sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) 
                                 (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      using a 
      using bisim_step_global[where ls\<^sub>1 = ls\<^sub>1 and ls\<^sub>2 = ls\<^sub>2 and ls\<^sub>1' = ls\<^sub>1' and g\<^sub>1 = g\<^sub>1 and g\<^sub>2 = g\<^sub>2
                              and g\<^sub>1' = g\<^sub>1' and l = l and sched\<^sub>1 = sched\<^sub>1 and sched\<^sub>2 = sched\<^sub>2]
      apply clarsimp
      by blast
    from a have "exec_events (trace g\<^sub>1) ((ls\<^sub>2, g\<^sub>2, sched\<^sub>2) # exec\<^sub>2) = exec_events (trace g\<^sub>2) exec\<^sub>2"
      using exec_events_cons by auto
    moreover with a have "exec_events (trace g\<^sub>2) exec\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>1') []"
      by simp
    moreover with a have "exec_events (trace g\<^sub>2) exec\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1'"
      by simp
    moreover with a have "exec_events (trace g\<^sub>2) exec\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1"
      unfolding global_state_equiv_def 
      using trace_equiv_trans
      by (simp add: trace_equiv_def)
    note trace_facts = calculation
    ultimately have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1"
      using exec_events_shorten a
      unfolding global_state_equiv_def
      by auto
    moreover have sat\<^sub>2: "anns_satisfied (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "anns_satisfied (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      using a(1) a(9) anns_satisfied_step apply blast
      using anns_satisfied_step eval\<^sub>1' a by blast
    moreover have "anns_satisfied_now (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "anns_satisfied_now (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      using sat\<^sub>2 unfolding anns_satisfied.simps by blast+
     
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
      using a eval\<^sub>1 eval\<^sub>1'
      
      apply (clarsimp simp: sec_step_global.simps sec_step_def)
      apply (cases "\<exists> ev. trace g\<^sub>2 = trace g\<^sub>1 @ [ev]")
       apply clarsimp
      apply (case_tac ev, simp_all)
      by (meson trace_equiv_sym trace_eqv_no_decl)
    moreover have ex\<^sub>2': "global_exec (ls\<^sub>2', g\<^sub>2', sched\<^sub>2) []"
      by (simp add: global_meval_full_refl)
    note foo = a(3)[where ls\<^sub>1'1 = ls\<^sub>2' and g\<^sub>1'1 = g\<^sub>2' and sched\<^sub>1'1 = sched\<^sub>2 and exec\<^sub>1'1 = "[]"]
    ultimately have "\<exists> exec\<^sub>2'.
           exec_events (trace g\<^sub>2') ([] @ exec\<^sub>2') =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>2') [] \<and>
           global_exec (ls\<^sub>2', g\<^sub>2', sched\<^sub>2) ([] @ exec\<^sub>2') \<and>
           exec\<^sub>2 \<sim>\<^sup>L\<^bsub>l\<^esub> ([] @ exec\<^sub>2')"
      apply -
      apply (rule foo)
      using a ex\<^sub>2' eval\<^sub>1' has_type_preservation lift_property_global sat\<^sub>2
      apply (simp_all del: anns_satisfied.simps)
      by (metis \<open>trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1\<close> lang.global_state_equiv_def lang.trace_equiv_def lang_axioms)
    then obtain exec\<^sub>2' where
      IH: "exec_events (trace g\<^sub>2') ([] @ exec\<^sub>2') =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>2') [] \<and>
           global_exec (ls\<^sub>2', g\<^sub>2', sched\<^sub>2) ([] @ exec\<^sub>2') \<and>
           exec\<^sub>2 \<sim>\<^sup>L\<^bsub>l\<^esub> ([] @ exec\<^sub>2')"
      using a
      by auto
    show ?case (is "\<exists> x. ?P x")
    proof -
      from a IH have "exec_events (trace g\<^sub>1') ((ls\<^sub>2, g\<^sub>2', sched\<^sub>2) # exec\<^sub>2') =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>1') []"
        using trace_facts 
        apply clarsimp
        using \<open>g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'\<close> \<open>trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>1\<close> global_state_equiv_def trace_equiv_def by auto
      with a IH have "?P ((ls\<^sub>2', g\<^sub>2', sched\<^sub>2) # exec\<^sub>2')"
        apply safe
        using global_exec.global_meval_full_step eval\<^sub>1' apply force
        using eval\<^sub>1' global_exec.global_meval_full_step apply fastforce
        using `g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'` eval\<^sub>1' by auto
      thus ?thesis
        by blast
    qed
  next
    case (2 ls\<^sub>1' g\<^sub>1' sched\<^sub>1' ls\<^sub>2' g\<^sub>2' sched\<^sub>2' exec\<^sub>2')
    note a = this
    then have cmd\<^sub>1_sim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'" and [simp]:"sched\<^sub>1' = sched\<^sub>1"
      by auto
    with a have [simp]: "sched\<^sub>2' = sched\<^sub>2"
      by (metis global_eval_elim)
    have "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2' \<and> sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) 
                                             (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
    proof -
      from `anns_satisfied (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)` have "list_all (\<lambda>l\<^sub>1. g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1) ls\<^sub>1"
        unfolding anns_satisfied.simps anns_satisfied_now.simps
        by auto
      moreover
      from `anns_satisfied (ls\<^sub>1', g\<^sub>1', sched\<^sub>1)` have "list_all (\<lambda>l\<^sub>1'. g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1') ls\<^sub>1'"
        unfolding anns_satisfied.simps anns_satisfied_now.simps
        by auto
      ultimately obtain ls\<^sub>I' g\<^sub>I' where "(ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>I', g\<^sub>I', sched\<^sub>2)"
                           "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>I'"
                           "sec_step_global l (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) 
                                              (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
        using bisim_step_global[where ls\<^sub>1 = ls\<^sub>1 and g\<^sub>1 = g\<^sub>1 and ls\<^sub>1' = ls\<^sub>1' and g\<^sub>1' = g\<^sub>1'
                       and g\<^sub>2 = g\<^sub>2 and ls\<^sub>2 = ls\<^sub>2 and l = l and sched\<^sub>1 = sched\<^sub>1 and sched\<^sub>2 = sched\<^sub>2]
        using cmd\<^sub>1_sim a
        by (metis \<open>sched\<^sub>1' = sched\<^sub>1\<close> global_eval_deterministic)
      moreover with global_eval_deterministic have "ls\<^sub>I' = ls\<^sub>2'" "g\<^sub>I' = g\<^sub>2'"
        using `(ls\<^sub>1', g\<^sub>1', sched\<^sub>1') \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')` by auto
      ultimately show ?thesis by auto
    qed
    moreover
    have sat\<^sub>2: "anns_satisfied (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "anns_satisfied (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      using 2 gstep anns_satisfied_step apply metis
      using 2 gstep anns_satisfied_step `sched\<^sub>2' = sched\<^sub>2` by metis
    moreover have "anns_satisfied_now (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "anns_satisfied_now (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      using sat\<^sub>2 unfolding anns_satisfied.simps by blast+
    moreover
    have "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
      using global_eval_preserves_trace_length_eq a cmd\<^sub>1_sim
      unfolding global_state_equiv_def
      by (smt \<open>sched\<^sub>1' = sched\<^sub>1\<close> anns_satisfied.elims(2) global_exec_to_meval global_meval_full_refl last_config.simps(1) lock_state_equiv_def trace_proj_length_eq)
    with a have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2'"
      using exec_events_split by auto
    from `trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2'` a calculation have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
      apply (clarsimp simp: sec_step_global.simps sec_step_def)
      apply (cases "\<exists> ev. trace g\<^sub>2 = trace g\<^sub>1 @ [ev]")
       apply clarsimp
      apply (case_tac ev, simp_all)
      apply (case_tac "x31 \<le> l")
      defer 1
       apply fastforce
      using a
      apply -
      by presburger
    ultimately show ?case
      using a
      apply clarsimp
      by (metis \<open>g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'\<close> a(4) global_meval_full_step has_type_preservation lift_property_global)
  qed
qed

lemma exec_events_last_config:
  assumes ex: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
    shows "exec_events (trace g\<^sub>1) exec\<^sub>1 = trace (glob_state (last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1))"
  apply (induction rule: global_exec_induct[OF ex])
  by auto

lemma last_config_sim:
  assumes "exec\<^sub>1 \<sim>\<^sup>L\<^bsub>l\<^esub> exec\<^sub>1'"
      and "last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1 = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and "last_config (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) exec\<^sub>1' = (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
      and "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'"
      and "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
    shows "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2' \<and> g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2' \<and> sched\<^sub>2' = sched\<^sub>2"
  using assms
  apply (induction arbitrary: ls\<^sub>1 ls\<^sub>1' g\<^sub>1 g\<^sub>1' sched\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>2' g\<^sub>2' sched\<^sub>2' rule: exec_low_sim.induct)
  by auto

lemma soundness_aux:
  assumes anns_sat: "\<And> g. anns_satisfied (ls\<^sub>1, g, sched\<^sub>1)" "\<And> g. anns_satisfied (ls\<^sub>1', g, sched\<^sub>1)"
      and ex\<^sub>1: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1" 
               "last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1 = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and ex\<^sub>1': "global_exec (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) exec\<^sub>1'"
                "last_config (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) exec\<^sub>1' = (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      and ev\<^sub>2: "(ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<rightarrow> (ls\<^sub>3, g\<^sub>3, sched\<^sub>3)"
      and eqv: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and sim: "exec\<^sub>1 \<sim>\<^sup>L\<^bsub>l\<^esub> exec\<^sub>1'"
      and bisim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'"
    shows "\<exists> ls\<^sub>3' g\<^sub>3'. (ls\<^sub>2', g\<^sub>2', sched\<^sub>2) \<rightarrow> (ls\<^sub>3', g\<^sub>3', sched\<^sub>3) \<and>
                       sec_step_global l (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)
                                         (ls\<^sub>3, g\<^sub>3, sched\<^sub>3) (ls\<^sub>3', g\<^sub>3', sched\<^sub>3)"
proof -
  from assms have "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2'" "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
    using last_config_sim by auto
  moreover have "anns_satisfied_now (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    using anns_sat(1) global_exec_to_meval ex\<^sub>1
    unfolding anns_satisfied.simps
    by force
  moreover have "anns_satisfied_now (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
    using anns_sat(2) global_exec_to_meval ex\<^sub>1'
    unfolding anns_satisfied.simps
    by force
  ultimately show ?thesis
    using assms
    unfolding anns_satisfied_now.simps
    using bisim_step_global[where ls\<^sub>1 = ls\<^sub>2 and ls\<^sub>2 = ls\<^sub>3 and ls\<^sub>1' = ls\<^sub>2' and l = l
                            and g\<^sub>1 = g\<^sub>2 and g\<^sub>1' = g\<^sub>2' and g\<^sub>2 = g\<^sub>3 
                            and sched\<^sub>1 = sched\<^sub>2 and sched\<^sub>2 = sched\<^sub>3]
    apply clarsimp
    by blast
qed

lemma bisimilar_list_sym:
  "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1' \<Longrightarrow> ls\<^sub>1' \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1"
  unfolding bisimilar_list_def
  using bisimilar_sym by (auto simp: to_rel_def sym_def list_all2_conv_all_nth)

lemma soundness_uncertainty:
  assumes anns_sat: "\<And> g. P g \<Longrightarrow> anns_satisfied (ls\<^sub>1, g, sched\<^sub>1)"
                    "\<And> g. P g \<Longrightarrow> anns_satisfied (ls\<^sub>1', g, sched\<^sub>1)"
      and Pgs: "P g\<^sub>1" "P g\<^sub>1'"
      and ev\<^sub>1: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and ev\<^sub>2: "(ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<rightarrow> (ls\<^sub>3, g\<^sub>3, sched\<^sub>3)"
      and well_typed: "list_all (has_type ah l \<circ> com) ls\<^sub>1" "list_all (has_type ah l \<circ> com) ls\<^sub>1'"
      and new_event: "trace g\<^sub>3 = trace g\<^sub>2 @ [ev]"
      and not_decl: "\<not> is_low_declassification l ev"
      and ev\<^sub>1': "(ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>U', g\<^sub>U', sched\<^sub>U')"
      and eqv: "g\<^sub>1' =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1"
      and trace_eq: "trace g\<^sub>U' =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2"
      and bisim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1'"
    shows "\<exists> ls\<^sub>3' g\<^sub>3' sched\<^sub>3'. (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>3', g\<^sub>3', sched\<^sub>3') \<and>
                       trace g\<^sub>3 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>3'"
proof -
  from ev\<^sub>1 obtain exec\<^sub>1 where ex\<^sub>1:
    "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1" "last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1 = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    using global_meval_to_exec
    by blast  
  from ev\<^sub>1' obtain exec\<^sub>1'' where ex\<^sub>1'': "global_exec (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) exec\<^sub>1''"
    "last_config (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) exec\<^sub>1'' = (ls\<^sub>U', g\<^sub>U', sched\<^sub>U')"
    using global_meval_to_exec
    by blast
  from trace_eq have same_events\<^sub>1: "exec_events (trace g\<^sub>1) exec\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>1') exec\<^sub>1''"
    by (metis ex\<^sub>1''(1) ex\<^sub>1''(2) ex\<^sub>1(1) ex\<^sub>1(2) exec_events_last_trace trace_equiv_sym)
  show ?thesis
  proof (cases "length exec\<^sub>1'' \<le> length exec\<^sub>1")
    case True
    note le = True
    from le obtain exec\<^sub>U' where ex\<^sub>U':
      "exec_events (trace g\<^sub>1') (exec\<^sub>1'' @ exec\<^sub>U') =\<^sup>t\<^bsub>l\<^esub>
           exec_events (trace g\<^sub>1') exec\<^sub>1''"
      "global_exec (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) (exec\<^sub>1'' @ exec\<^sub>U')"
      "exec\<^sub>1 \<sim>\<^sup>L\<^bsub>l\<^esub> (exec\<^sub>1'' @ exec\<^sub>U')"
      using eqv ex\<^sub>1'' ex\<^sub>1 well_typed same_events\<^sub>1
      using global_exec_catch_up' anns_sat bisim Pgs
      by (metis lang.global_equiv_sym lang_axioms)
    define exec\<^sub>1' where "exec\<^sub>1' = exec\<^sub>1'' @ exec\<^sub>U'"
    with ex\<^sub>U' obtain ls\<^sub>2' g\<^sub>2' sched\<^sub>2' where cnf\<^sub>2':
      "last_config (ls\<^sub>1', g\<^sub>1', sched\<^sub>1) exec\<^sub>1' = (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
      using next_thread.cases by blast
    have cnf\<^sub>2'_eqv: "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>2'" "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'" "sched\<^sub>2' = sched\<^sub>2"
      using last_config_sim ex\<^sub>U' eqv ex\<^sub>1 cnf\<^sub>2' global_equiv_sym exec\<^sub>1'_def local.bisim apply blast
      using last_config_sim ex\<^sub>U' eqv ex\<^sub>1 cnf\<^sub>2' global_equiv_sym exec\<^sub>1'_def local.bisim apply blast
      using last_config_sim ex\<^sub>U' eqv ex\<^sub>1 cnf\<^sub>2' global_equiv_sym exec\<^sub>1'_def local.bisim apply blast
      done
    from anns_sat Pgs have "anns_satisfied_now (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      using anns_satisfied.simps ev\<^sub>1 by blast
    moreover
    from anns_sat have "anns_satisfied_now (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)"
      using anns_satisfied.simps Pgs 
      using cnf\<^sub>2' ex\<^sub>U'(2) exec\<^sub>1'_def global_exec_to_meval 
      by (metis cnf\<^sub>2'_eqv(3))
    ultimately obtain ls\<^sub>3' g\<^sub>3' sched\<^sub>3' where cnf\<^sub>3':
      "(ls\<^sub>2', g\<^sub>2', sched\<^sub>2) \<rightarrow> (ls\<^sub>3', g\<^sub>3', sched\<^sub>3')" "ls\<^sub>3 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>3'"
      "sec_step_global l (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2)
                         (ls\<^sub>3, g\<^sub>3, sched\<^sub>3) (ls\<^sub>3', g\<^sub>3', sched\<^sub>3)"
      using ev\<^sub>2 ex\<^sub>U' cnf\<^sub>2'
      using bisim_step_global
      apply (clarsimp)
      by (meson bisimilar_list_def cnf\<^sub>2'_eqv(1) cnf\<^sub>2'_eqv(2))
    hence "g\<^sub>3 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>3'"
      using not_decl new_event
      apply (clarsimp simp: sec_step_global.simps sec_step_def)
      by (cases ev, simp_all)
    with cnf\<^sub>3' show ?thesis
      unfolding global_state_equiv_def
      by (metis (no_types, lifting) cnf\<^sub>2' cnf\<^sub>2'_eqv(3) ev\<^sub>2 ex\<^sub>U'(2) exec\<^sub>1'_def global_eval_elim global_exec_to_meval rtrancl.rtrancl_into_rtrancl)
  next
    case False
    have "anns_satisfied (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "anns_satisfied (ls\<^sub>U', g\<^sub>U', sched\<^sub>U')"
      using anns_sat anns_satisfied_multi ev\<^sub>1 Pgs apply blast
      using anns_sat anns_satisfied_multi ev\<^sub>1' Pgs by blast
    obtain exec\<^sub>C where ex\<^sub>C:
      "exec_events (trace g\<^sub>1) (exec\<^sub>1 @ exec\<^sub>C) =\<^sup>t\<^bsub>l\<^esub> exec_events (trace g\<^sub>1) exec\<^sub>1 \<and>
       global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (exec\<^sub>1 @ exec\<^sub>C) \<and>
       exec\<^sub>1'' \<sim>\<^sup>L\<^bsub>l\<^esub> exec\<^sub>1 @ exec\<^sub>C"
      using ex\<^sub>1'' same_events\<^sub>1 False ex\<^sub>1 trace_equiv_sym
      using global_exec_catch_up'[where exec\<^sub>1 = exec\<^sub>1'' and exec\<^sub>1' = exec\<^sub>1 and ls\<^sub>1 = ls\<^sub>1' and g\<^sub>1 = g\<^sub>1'
                                  and g\<^sub>1' = g\<^sub>1 and ls\<^sub>1' = ls\<^sub>1 and sched\<^sub>1 = sched\<^sub>1 
                                  and sched\<^sub>1' = sched\<^sub>1 and l = l]
      using assms(1) assms(2) bisimilar_list_sym eqv le_cases local.bisim well_typed(2) Pgs  by (metis global_exec_catch_up')
   obtain ls\<^sub>C g\<^sub>C sched\<^sub>C where cnf\<^sub>C: "last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (exec\<^sub>1 @ exec\<^sub>C) = (ls\<^sub>C, g\<^sub>C, sched\<^sub>C)"
     using next_thread.cases by blast
   have "g\<^sub>C =\<^sup>g\<^bsub>l\<^esub> g\<^sub>U'" "ls\<^sub>C \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>U'" "sched\<^sub>C = sched\<^sub>U'"
     using ex\<^sub>1'' eqv ex\<^sub>1 ex\<^sub>C
     using last_config_sim[where exec\<^sub>1 = "exec\<^sub>1 @ exec\<^sub>C"]
     apply (meson cnf\<^sub>C global_equiv_sym last_config_sim)
     using ex\<^sub>1'' eqv ex\<^sub>1 ex\<^sub>C last_config_sim[where exec\<^sub>1 = "exec\<^sub>1 @ exec\<^sub>C"]
     apply (meson bisimilar_list_sym cnf\<^sub>C global_equiv_sym last_config_sim local.bisim)
     using bisimilar_list_sym cnf\<^sub>C eqv ex\<^sub>1''(2) ex\<^sub>C last_config_sim local.bisim apply blast
     using ex\<^sub>1'' eqv ex\<^sub>1 ex\<^sub>C last_config_sim[where exec\<^sub>1 = "exec\<^sub>1 @ exec\<^sub>C"]
     apply (meson bisimilar_list_sym cnf\<^sub>C global_equiv_sym last_config_sim local.bisim)
     done
   text {* Either there was no further step needed, or the catching up actually begins
   with the last step of the first run from the assumptions *}
   show ?thesis
   proof (cases "exec\<^sub>C")
     case Nil
     from Nil ex\<^sub>C have "exec\<^sub>1'' \<sim>\<^sup>L\<^bsub>l\<^esub> exec\<^sub>1"
       by auto
     with ex\<^sub>C ex\<^sub>1'' ex\<^sub>1 have cnf\<^sub>U'_eqv: "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>U'" "ls\<^sub>2 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>U'" "sched\<^sub>U' = sched\<^sub>2"
       using \<open>g\<^sub>C =\<^sup>g\<^bsub>l\<^esub> g\<^sub>U'\<close> cnf\<^sub>C local.Nil apply (clarsimp, safe)
       using \<open>ls\<^sub>C \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>U'\<close> cnf\<^sub>C ex\<^sub>1(2) local.Nil apply clarsimp
       using \<open>exec\<^sub>1'' \<sim>\<^sup>L\<^bsub>l\<^esub> exec\<^sub>1\<close> bisimilar_list_sym eqv ex\<^sub>1''(2) ex\<^sub>1(2) last_config_sim local.bisim by blast
     have sat': "anns_satisfied_now (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "anns_satisfied_now (ls\<^sub>U', g\<^sub>U', sched\<^sub>2)"
       using anns_sat anns_satisfied.simps ev\<^sub>1 Pgs apply blast
       using anns_sat anns_satisfied.simps cnf\<^sub>U'_eqv(2) cnf\<^sub>U'_eqv(3) ev\<^sub>1'  Pgs
       by blast
     then obtain ls\<^sub>3' g\<^sub>3' sched\<^sub>3' where
       ev\<^sub>U': "(ls\<^sub>U', g\<^sub>U', sched\<^sub>U') \<rightarrow> (ls\<^sub>3', g\<^sub>3', sched\<^sub>3')" "sec_step_global l (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) (ls\<^sub>U', g\<^sub>U', sched\<^sub>2)
                                                                             (ls\<^sub>3, g\<^sub>3, sched\<^sub>3) (ls\<^sub>3', g\<^sub>3', sched\<^sub>3)"
       using ev\<^sub>2 has_type_progress  cnf\<^sub>U'_eqv not_decl sat' 
       using bisim_step_global
       by (metis anns_satisfied_now.simps)
     hence "g\<^sub>3 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>3'"
       using new_event not_decl
       unfolding sec_step_global.simps sec_step_def Let_def
       by (cases ev; auto)
     show ?thesis
       apply (rule exI[where x = "ls\<^sub>3'"])  
       apply (rule exI[where x = "g\<^sub>3'"])
       by (meson \<open>g\<^sub>3 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>3'\<close> ev\<^sub>1' ev\<^sub>U'(1) global_state_equiv_def rtrancl.rtrancl_into_rtrancl)
    next
      case (Cons cnf exec\<^sub>D)
      then have "exec\<^sub>1 @ exec\<^sub>C = exec\<^sub>1 @ [cnf] @ exec\<^sub>D"
        by auto
      from Cons have ex\<^sub>2: "global_exec (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) exec\<^sub>C"
        using global_exec_split ex\<^sub>1 ex\<^sub>C
        by force
      with Cons have cnf_eq: "cnf = (ls\<^sub>3, g\<^sub>3, sched\<^sub>3)"
        using ev\<^sub>2 ex\<^sub>1
        using global_eval_deterministic
        apply simp
        apply (erule global_exec.cases)
         apply blast
        by (metis list.inject)
      have "trace g\<^sub>3 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2"
      proof (rule ccontr) (* This is much more convoluted than it should be. *)
        let ?t\<^sub>2 = "trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l"
        let ?t\<^sub>3 = "trace g\<^sub>3 \<upharpoonleft>\<^sub>\<le> l"
        assume neq: "\<not> (trace g\<^sub>3 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2)"
        moreover have "length (trace g\<^sub>C \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l)"
          using ex\<^sub>C ex\<^sub>1 cnf\<^sub>C
          using exec_events_last_config
          by (auto simp: trace_equiv_def)
        moreover from ev\<^sub>2 have "trace g\<^sub>2 \<le> trace g\<^sub>3"
          using trace_monotonic_global
          by auto
        moreover hence "trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l \<le> trace g\<^sub>3 \<upharpoonleft>\<^sub>\<le> l"
          unfolding trace_proj_filter 
          using filter_sublist by auto
        moreover 
        have ex\<^sub>3: "global_exec (ls\<^sub>3, g\<^sub>3, sched\<^sub>3) exec\<^sub>D"
          using ev\<^sub>2 ex\<^sub>2
          using \<open>cnf = (ls\<^sub>3, g\<^sub>3, sched\<^sub>3)\<close> global_exec.cases local.Cons by blast
        hence "trace g\<^sub>3 \<le> exec_events (trace g\<^sub>3) exec\<^sub>D"
          using exec_trace_monotonic by auto
        moreover have "last_config (ls\<^sub>3, g\<^sub>3, sched\<^sub>3) exec\<^sub>D = (ls\<^sub>C, g\<^sub>C, sched\<^sub>C)"
          using last_config_split
          using cnf\<^sub>C cnf_eq local.Cons by auto
        moreover have "length (trace g\<^sub>3 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>C \<upharpoonleft>\<^sub>\<le> l)"
          using ex\<^sub>C cnf\<^sub>C ex\<^sub>1
          by (metis calculation(1) calculation(6) ev\<^sub>2 ex\<^sub>3 exec_events_last_trace exec_events_shorten trace_equiv_refl) 
        then have "length ?t\<^sub>2 = length ?t\<^sub>3"
          using calculation
          by auto
        then obtain i where "i < length ?t\<^sub>2" "?t\<^sub>2 ! i \<noteq> ?t\<^sub>3 ! i"
          using nth_equalityI calculation neq
          unfolding trace_equiv_def
          by metis
        ultimately show False
          using ex\<^sub>C ex\<^sub>1 cnf\<^sub>C
          using exec_trace_monotonic filter_sublist
          unfolding trace_equiv_def trace_proj_filter
          by (simp add: sublist_index)
      qed
      then show ?thesis
        using ev\<^sub>1' trace_eq trace_equiv_def
        by fastforce
    qed
  qed
qed

theorem soundness:
  assumes anns_sat: "\<And> g. P g \<Longrightarrow> anns_satisfied (ls\<^sub>1, g, sched\<^sub>1)"
  assumes well_typed: "list_all (has_type ah l \<circ> com) ls\<^sub>1"
  shows "system_secure l P sched\<^sub>1 ls\<^sub>1"
  unfolding system_secure_def
proof (clarsimp)
  fix g\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>3 g\<^sub>3 sched\<^sub>3 ev
  assume Pg: "P g\<^sub>1"
  assume ev\<^sub>1: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  assume ev\<^sub>2: "(ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<rightarrow> (ls\<^sub>3, g\<^sub>3, sched\<^sub>3)"
  assume new_event: "trace g\<^sub>3 = trace g\<^sub>2 @ [ev]"
  from well_typed have bisim: "ls\<^sub>1 \<approx>\<^sup>L\<^bsub>l\<^esub> ls\<^sub>1"
    using typed_implies_bisim typed_bisim_secure
    by (metis (mono_tags, lifting) comp_def lang.bisimilar.simps lang.bisimilar_list_def lang_axioms list_all2_conv_all_nth list_all_length)
  show "case ev of
       Output l' v e \<Rightarrow>
         uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2)
         \<subseteq> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)
       | Input l' v \<Rightarrow>
         uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2)
         \<subseteq> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)
       | Declassification l' v e \<Rightarrow>
           let c = head_com
                  (com
                    (ls\<^sub>2 !
                     next_thread
                      (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)))
           in (l' \<le> l \<longrightarrow>
                (let A = active_ann_com c
                in (case exp_level_syn e of
                           Some from \<Rightarrow> \<D> from l' g\<^sub>2 v c
                         | None \<Rightarrow> False))) \<and>
           (\<not> (l' \<le> l) \<longrightarrow>
            (let A = active_ann_com c
            in uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2)
               \<subseteq> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)))"
  proof (cases "ev")
    case (Output l' v e)
    then have "\<not> is_low_declassification l ev"
      by auto
    with Output show ?thesis
      using soundness_uncertainty anns_sat ev\<^sub>1 ev\<^sub>2 new_event bisim Pg
      unfolding uncertainty_def
      apply (clarsimp simp: Let_def simp del: anns_satisfied.simps)
      by (metis (mono_tags, hide_lams) Output \<open>\<not> is_low_declassification l ev\<close> anns_sat ev\<^sub>1 ev\<^sub>2 lang.trace_equiv_sym lang_axioms new_event well_typed)
  next
    case (Input l' v)
    then have "\<not> is_low_declassification l ev"
      by auto
    with Input show ?thesis 
      using soundness_uncertainty anns_sat ev\<^sub>1 ev\<^sub>2 new_event bisim Pg
      unfolding uncertainty_def
      apply (clarsimp simp: Let_def simp del: anns_satisfied.simps)
      by (metis (mono_tags, hide_lams) Input \<open>\<not> is_low_declassification l ev\<close> anns_sat ev\<^sub>1 ev\<^sub>2 lang.trace_equiv_sym lang_axioms new_event well_typed)
  next
    case (Declassification l' v e)
    then show ?thesis
    proof (cases "l' \<le> l")
      let ?c = "head_com (com (ls\<^sub>2 ! next_thread (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)))"
      case True
      moreover from ev\<^sub>1 well_typed have "list_all (has_type ah l \<circ> com) ls\<^sub>2"
        using has_type_preservation_global
        by auto
      moreover from ev\<^sub>1 anns_sat Pg have "list_all (\<lambda> l\<^sub>2. g\<^sub>2 \<Turnstile>\<^sub>P active_ann l\<^sub>2) ls\<^sub>2"
        by auto
      ultimately show ?thesis
        using Declassification new_event ev\<^sub>2
        using has_type_decl_event_secure_global True
        apply (cases "exp_level_syn e")
        by (auto, fastforce+)
    next
      case False
      moreover with Declassification have "\<not> is_low_declassification l ev"
        by auto
      ultimately show ?thesis
        using soundness_uncertainty anns_sat ev\<^sub>1 ev\<^sub>2 new_event bisim Pg
        unfolding uncertainty_def
        apply (clarsimp simp: Declassification Let_def simp del: anns_satisfied.simps)
        by (metis (mono_tags, hide_lams) Declassification \<open>\<not> is_low_declassification l ev\<close> anns_sat ev\<^sub>1 ev\<^sub>2 lang.trace_equiv_sym lang_axioms new_event well_typed)
    qed
  qed
qed

end
end
