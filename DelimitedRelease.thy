theory DelimitedRelease
  imports Main Language Security Soundness RG_Verify
begin

text {* This file shows how to encode delimited-release policies as declassification predicates and
shows soundness of the encoding. Since we don't capture all modifications to the initial state in
the trace, we need to assume that all secrets are supplied as inputs (or not altered by a program
for this embedding to always work). Both are arguably reasonable assumptions for interactive
programs that may only get key material as secrets in the initial memory. For simplicity, we model
initial memories that do not contain any secrets for now. *}

text {* In the presence of concurrency, it's more natural to talk about the last
consumed inputs instead of inputs at the beginning of a stream. To allow expressing
such policies more naturally, we model a delimited-release escape hatch
as a mapping from inputs seen so far (at some level) to a value. *}

type_synonym ('level, 'val) escape_hatch = "'val list \<Rightarrow> 'val"

text {* A delimited release policy specifies a set of escape hatches
for each pair of levels, representing what declassifications are allowed between to levels.
For a @{term dr_policy} @{term \<E>}, @{term "\<E> from to"} indicates the allowed escape
hatches for declassifying from level @{term from} to @{term to}. 
*}

type_synonym ('level, 'val) dr_policy = "'level \<Rightarrow> 'level \<Rightarrow> ('level, 'val) escape_hatch set"

text {* In order to check a delimited release policy later on in time,
we need to extract all the inputs that have been consumed already and put
them back into the environment to pass to the DR policy. *}

definition same_input_count :: "('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool" where
  "same_input_count g g' = (\<forall> l'. length (level_inputs l' g) = length (level_inputs l' g'))"

definition eval_hatch :: "('level, 'val) escape_hatch \<Rightarrow> 'val list \<Rightarrow> 'val" where
  "eval_hatch h is = h is"

definition all_inputs :: "('level, 'val) global_state \<Rightarrow> ('level, 'val) env" where
  "all_inputs g l = stream_append (level_inputs l g) (env g l)"

definition dr_equiv' :: "'level::complete_lattice \<Rightarrow> ('level, 'val) dr_policy \<Rightarrow>
  ('level, 'val) env \<Rightarrow> ('level, 'val) env \<Rightarrow> bool" 
   where
  "dr_equiv' l \<E> e e' \<equiv> \<forall> n from. \<forall> to \<le> l. \<forall> h \<in> \<E> from to. 
      eval_hatch h (stream_take n (e from)) = eval_hatch h (stream_take n (e' from))"

definition embed_dr :: "('level, 'val) dr_policy \<Rightarrow> 'level::complete_lattice \<Rightarrow> ('level, 'val) decl_pred" where
  "embed_dr \<E> from to g v c \<equiv> 
    (case c of
      {A} x :D= e \<Rightarrow>
       (\<exists> H \<in> \<E> from to.
        (\<forall> g'. g' \<Turnstile>\<^sub>P A \<longrightarrow> eval_exp e (mem g') = eval_hatch H (level_inputs from g')))
      | DOut A lev e \<Rightarrow>
       (\<exists> H \<in> \<E> from to.
        (\<forall> g'. g' \<Turnstile>\<^sub>P A \<longrightarrow> eval_exp e (mem g') = eval_hatch H (level_inputs from g')))
      | _ \<Rightarrow> True)"

locale delimited_release = lang \<L> "embed_dr \<E>" level_rename val_rename
  for \<L> \<E>
  and level_rename :: "'level::complete_lattice"
  and val_rename :: "'val"

context delimited_release
begin

text {* @{term dr_equiv} states that two global states yield the same value
for all escape hatch functions (on all levels below some level @{term l}). *}

text {* Delimited release requires low-equivalent traces for any initial states
that agree on both low variables and escape hatch expressions that may be declassified
to an attacker-visible level. *}

definition dr_equiv :: "'level \<Rightarrow> ('level, 'val) env \<Rightarrow> ('level, 'val) env \<Rightarrow> bool" 
  (infix "=\<^sup>\<E>\<index>" 50) where
  "e =\<^sup>\<E>\<^bsub>l\<^esub> e' \<equiv> dr_equiv' l \<E> e e'"

definition dr_secure :: "'level \<Rightarrow> (('level,'val) global_state \<Rightarrow> bool) \<Rightarrow> sched \<Rightarrow> ('level, 'val) local_state list \<Rightarrow> bool"
  where
  "dr_secure l P sched ls \<equiv>
   \<forall> g\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 g\<^sub>1'. P g\<^sub>1 \<and> P g\<^sub>1' \<and> mem g\<^sub>1 = mem g\<^sub>1' \<and> g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1' \<and> all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1' \<and>
   same_input_count g\<^sub>1 g\<^sub>1' \<and>
   (ls, g\<^sub>1, sched) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<longrightarrow>
      (\<exists> ls\<^sub>2' g\<^sub>2' sched\<^sub>2'. (ls, g\<^sub>1', sched) \<rightarrow>\<^sup>* (ls\<^sub>2', g\<^sub>2', sched\<^sub>2') \<and> trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2')"

text {*
  When @{term dr_secure} is talking about initial states in which no activity has
  taken place, its conditions become much simpler.
*}
lemma
  "trace g = [] \<Longrightarrow> all_inputs g = env g"
  by(rule ext, rule ext, simp add: all_inputs_def level_inputs_def)

lemma
  "trace g = [] \<Longrightarrow> trace g' = [] \<Longrightarrow> same_input_count g g'"
  by(simp add: same_input_count_def level_inputs_def)

lemma
  "trace g = [] \<Longrightarrow> all_inputs g = env g"
  by(rule ext, rule ext, simp add: all_inputs_def level_inputs_def)

text {* We need to restrict the number of inputs read to rule out examples
like this one:
\begin{verbatim}
h <- H''
x <- H
if (h)
  x <- H
else
  y <- H'
l :D= x
\end{verbatim}

If the policy allows declassifying the parity of the last input, then
without requiring that the same number of inputs is consumed on
each level, the previous example would leak information about h
while still satisfying our security predicate. To rule out
such examples, we require that all low-equivalent runs always
consume an equal amount of inputs on each level. *}


definition no_high_br :: "'level \<Rightarrow> (('level, 'val) global_state \<Rightarrow> bool) \<Rightarrow> ('level, 'val) local_state list \<Rightarrow> bool" where
  "no_high_br l P ls\<^sub>1 \<equiv>
    \<forall> sched\<^sub>1 g\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 g\<^sub>1'.
      (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<and> g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1' \<and> all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1' \<and>
      P g\<^sub>1 \<and> P g\<^sub>1' \<and>
      same_input_count g\<^sub>1 g\<^sub>1'
        \<longrightarrow>
        (\<exists> g\<^sub>2'. (ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2', sched\<^sub>2) \<and> 
                length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l) \<and>
                same_input_count g\<^sub>2 g\<^sub>2')"

fun is_input :: "('level, 'val) event \<Rightarrow> bool" where
  "is_input (Input l v) = True" |
  "is_input _ = False"

lemma inputs_app_not_input:
  "\<not> is_input ev \<Longrightarrow> inputs (t @ [ev]) = inputs (t)"
  by (induction rule: inputs.induct ; cases ev ; auto)

lemma trace_proj_eq_filter:
  "t \<upharpoonleft> l = filter (\<lambda> ev. event_level ev = l) t"
  by (induction t, auto)

lemma inputs_app_not_input_proj:
  "\<not> is_input ev \<Longrightarrow> inputs ((t @ [ev]) \<upharpoonleft> l) = inputs (t \<upharpoonleft> l)"
  unfolding trace_proj_eq_filter
  apply auto
  by (induction rule: inputs.induct ; cases ev ; auto)

lemma inputs_app_input:
  "inputs (t @ [Input l v]) = inputs t @ [(l, v)]"
  by (induction rule: inputs.induct ; auto)

lemma inputs_app_input_same_level:
  "inputs ((t @ [Input l v]) \<upharpoonleft> l) = inputs (t \<upharpoonleft> l) @ [(l, v)] "
  unfolding level_inputs_def trace_proj_eq_filter
  apply (induction t, auto)
  by (metis append_Cons inputs_app_input)

lemma inputs_app_input_different_level:
  "l \<noteq> l' \<Longrightarrow> inputs ((t @ [Input l' v]) \<upharpoonleft> l) = inputs (t \<upharpoonleft> l)"
  unfolding level_inputs_def trace_proj_eq_filter
  by auto

lemma same_trace_no_input_read_step:
  assumes ev: "(l, g) \<leadsto> (l', g')"
      and tr: "trace g' = trace g \<or> (trace g' = trace g @ [ev] \<and> \<not> is_input ev)"
    shows "env g' = env g"
  using assms
  by (induction rule: eval.induct, auto)

lemma same_trace_no_input_read:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and tr: "trace g' = trace g \<or> (trace g' = trace g @ [ev] \<and> \<not> is_input ev)"
    shows "env g' = env g"
  using assms same_trace_no_input_read_step
  by (cases rule: global_eval.cases, auto)

lemma eval_read_input:
  assumes "(l, g) \<leadsto> (l', g')"
      and "trace g' = trace g @ [Input lev v]"
    shows "env g lev = stream_cons v (env g' lev) \<and> 
    (\<forall> lev'. lev' \<noteq> lev \<longrightarrow> env g lev' = env g' lev')"
  using assms 
  by (induction rule: eval.induct, auto simp: stream_cons_tail)

lemma eval_read_input_global:
  assumes "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and "trace g' = trace g @ [Input lev v]"
    shows "env g lev = stream_cons v (env g' lev) \<and> 
          (\<forall> lev'. lev' \<noteq> lev \<longrightarrow> env g lev' = env g' lev')"
  using assms eval_read_input
  by (cases rule: global_eval.cases, auto)


lemma eval_all_inputs_step:
  assumes ev: "(l, g) \<leadsto> (l', g')"
  shows "all_inputs g' = all_inputs g"
proof -
  from ev have "trace g' = trace g \<or> (\<exists> ev. trace g' = trace g @ [ev])"
    using eval_at_most_one_event
    by auto
  thus ?thesis
  proof
    assume "trace g' = trace g"
    thus ?thesis unfolding all_inputs_def level_inputs_def
      using same_trace_no_input_read_step ev
      by auto
  next
    assume "\<exists> ev. trace g' = trace g @ [ev]"
    then obtain ev where ev_prop: "trace g' = trace g @ [ev]" by blast
    show ?thesis 
    proof (cases ev)
      case (Output l' v e)
      hence "\<not> is_input ev" by auto
      with same_trace_no_input_read_step ev have "env g' = env g"
        using ev_prop by force
      moreover have "\<And> lev. inputs (trace g' \<upharpoonleft> lev) = inputs (trace g \<upharpoonleft> lev)"
        using inputs_app_not_input ev_prop `\<not> is_input ev`
        unfolding all_inputs_def level_inputs_def trace_proj_eq_filter
        by simp
      ultimately show ?thesis 
        using inputs_app_not_input_proj ev_prop
        unfolding all_inputs_def level_inputs_def
        by simp
    next
      case (Input l' v)
      hence A: "env g l' = stream_cons v (env g' l') \<and> (\<forall> lev'. lev'\<noteq> l' \<longrightarrow> env g lev' = env g' lev')"
        using ev ev_prop eval_read_input by auto
      have "\<And> l. all_inputs g' l = all_inputs g l"
      proof -
        fix l
        show "?thesis l"
        proof (cases "l = l'")
          case True
          then show ?thesis 
            unfolding all_inputs_def level_inputs_def trace_proj_eq_filter
            by (auto simp: Input ev_prop A stream_append_singleton inputs_app_input)
        next
          case False
          with A show ?thesis 
            unfolding all_inputs_def level_inputs_def trace_proj_eq_filter
            by (auto simp: Input ev_prop)
        qed
      qed
      thus ?thesis by auto
    next
      case (Declassification x31 x32 x33)
      hence "\<not> is_input ev" by auto
      with same_trace_no_input_read_step ev have "env g' = env g"
        using ev_prop by force
      moreover have "\<And> lev. inputs (trace g' \<upharpoonleft> lev) = inputs (trace g \<upharpoonleft> lev)"
        using inputs_app_not_input ev_prop `\<not> is_input ev`
        unfolding all_inputs_def level_inputs_def trace_proj_eq_filter
        by simp
      ultimately show ?thesis 
        using inputs_app_not_input_proj ev_prop
        unfolding all_inputs_def level_inputs_def
        by simp
    qed
  qed
qed

lemma eval_all_inputs:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
  shows "all_inputs g' = all_inputs g"
  using ev eval_all_inputs_step
  by (cases rule: global_eval.cases, auto)

lemma eval_all_inputs_multi:
  assumes "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
  shows "all_inputs g' = all_inputs g"
  using assms eval_all_inputs
  by (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)], auto)

definition is_seq :: "('level, 'val) com \<Rightarrow> bool" where
  "is_seq c \<equiv> \<exists> c\<^sub>1 c\<^sub>2. c = c\<^sub>1 ;; c\<^sub>2"


lemma head_com_next_states:
  "next_states l = next_states (l\<lparr>com := head_com (com l)\<rparr>)"
proof -
  obtain c where c_def: "com l = c" by auto
  have "\<And> g. comp_next_states_com l c g = comp_next_states_com l (head_com c) g"
    by (induction c, auto)
  hence "\<And> g. comp_next_states l g = comp_next_states (l\<lparr>com := head_com (com l)\<rparr>) g"
    using c_def comp_next_states.simps
    by (simp add: comp_next_states_com_ignores_loc_com)    
  then show ?thesis
    using c_def comp_next_states_correct
    by blast
qed

lemma eval_head_com:
  assumes ev: "(l', g) \<leadsto> (l'', g'')"
      and l'_def: "l' = l\<lparr>com := head_com (com l)\<rparr>"
    shows "\<exists> l'''. (l, g) \<leadsto> (l''', g'')"
proof -
  from ev have "g'' \<in> next_states l' g"
    unfolding next_states.simps
    by auto
  with head_com_next_states have "g'' \<in> next_states l g"
    using l'_def
    by auto
  thus ?thesis unfolding next_states.simps by blast
qed

lemma decl_implies_decl_com:
  assumes ev: "(l, g) \<leadsto> (l', g')"
      and "trace g' = trace g @ [Declassification lev v e]"
    shows "(\<exists> A x. head_com (com l) = {A} x :D= e \<and> v = eval_exp e (mem g) \<and> \<L> x = Some lev) \<or>
           (\<exists> A. head_com (com l) = DOut A lev e \<and> v = eval_exp e (mem g))"
  using assms by (induction rule: eval.induct, auto)

lemma decl_implies_decl_com_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and "trace g' = trace g @ [Declassification lev v e]"
    shows "(\<exists> A x. head_com (com (ls ! next_thread (ls, g, sched))) = {A} x :D= e \<and>
                  v = eval_exp e (mem g) \<and> \<L> x = Some lev) \<or>
           (\<exists> A. head_com (com (ls ! next_thread (ls, g, sched))) = DOut A lev e \<and> v = eval_exp e (mem g))"
  using assms decl_implies_decl_com
  by (induction rule: global_eval.induct, auto)

fun is_prefix :: "'a list \<Rightarrow> 'a list \<Rightarrow> bool" where
  "is_prefix [] ys = True" |
  "is_prefix xs [] = False" |
  "is_prefix (x # xs) (y # ys) = (x = y \<and> is_prefix xs ys)"

lemma is_prefix_correct:
  "t \<le> t' \<longleftrightarrow> is_prefix t t'"
  by (induction rule: is_prefix.induct, auto)

lemma length_same_prefix_same:
  assumes pref: "t \<le> t'"
      and len: "length t = length t'" 
    shows "t = t'"
proof -
  from pref have "is_prefix t t'" using is_prefix_correct by auto
  with len show ?thesis
    by (induction rule: is_prefix.induct, auto)
qed

lemma trace_monotonic_multi:
  "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched') \<Longrightarrow> trace g \<le> trace g'"
  apply (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
   apply auto[1]
  using trace_monotonic_global
  by (metis (no_types, lifting) exec_events_last_trace exec_trace_monotonic global_meval_to_exec rtrancl.rtrancl_into_rtrancl)

lemma global_exec_trace_pref':
  assumes ex: "global_exec (ls, g, sched) exec"
      and last: "last_config (ls, g, sched) exec = (ls', g', sched')"
    shows "trace g \<le> trace g'"
proof -
  from ex last have "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
    using global_exec_to_meval
    by force
  thus ?thesis using trace_monotonic_multi by auto
qed

lemma exec_pref_trace_pref:
  assumes ex: "global_exec (ls, g, sched) exec"
      and ex': "global_exec (ls, g, sched) exec'"
      and pref: "exec \<le> exec'"
      and last: "last_config (ls, g, sched) exec = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and last': "last_config (ls, g, sched) exec' = (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
    shows "trace g\<^sub>2 \<le> trace g\<^sub>2'"
proof -
  from pref obtain exec'' where exec''_def: "exec' = exec @ exec''"
    using Prefix_Order.prefixE by blast
  hence "global_exec (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) exec''"
    using ex' global_exec_split last by fastforce
  moreover have "last_config (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) exec'' = (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
    by (metis (no_types, lifting) append_Nil2 append_is_Nil_conv exec''_def last last' last_appendR last_config_last_eq)
  ultimately show ?thesis 
    using global_exec_trace_pref'
    using last' last exec''_def
    by auto
qed

lemma eval_trace_prefix:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
      and ev': "(ls, g, sched) \<rightarrow>\<^sup>* (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
    shows "trace g\<^sub>2 \<le> trace g\<^sub>2' \<or> trace g\<^sub>2' \<le> trace g\<^sub>2"
proof -
  obtain exec where exec_props:
    "global_exec (ls, g, sched) exec" 
    "last_config (ls, g, sched) exec = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    using ev global_meval_to_exec by blast
  moreover from ev' obtain exec' where exec'_props:
    "global_exec (ls, g, sched) exec'"
    "last_config (ls, g, sched) exec' = (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
    using ev global_meval_to_exec by blast
  ultimately have "exec \<le> exec' \<or> exec' \<le> exec"
    using global_exec_deterministic by blast
  thus ?thesis 
    using exec_pref_trace_pref
    using exec'_props(1) exec'_props(2) exec_props(1) exec_props(2) by blast
qed


lemma stream_take_level_inputs:
  "stream_take (length (level_inputs lev g)) (all_inputs g lev) = level_inputs lev g"
  apply (induction "trace g")
  unfolding all_inputs_def
  by (auto simp: stream_take_append)


lemma eval_level_inputs:
  assumes ev: "(l, g) \<leadsto> (l', g')"
  shows "stream_take (length (level_inputs lev g')) (all_inputs g lev) = level_inputs lev g'"
proof -
  from ev have "all_inputs g lev = all_inputs g' lev"
    using eval_all_inputs_step by auto
  then show ?thesis using stream_take_level_inputs 
    by auto
qed

lemma eval_level_inputs_multi:
  assumes "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
  shows "stream_take (length (level_inputs lev g')) (all_inputs g lev) = level_inputs lev g'"
  using assms
proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  case (rtrancl_refl a a b)
  then show ?case
    using stream_take_level_inputs
    by force
next
  case (rtrancl_into_rtrancl ls g sched ls' g' sched' ls'' g'' sched'')
  from `(ls', g', sched') \<rightarrow> (ls'', g'', sched'')` show ?case
    apply (cases rule: global_eval.cases)
    using rtrancl_into_rtrancl eval_level_inputs eval_all_inputs_multi apply fastforce
    using rtrancl_into_rtrancl by auto
qed

theorem embedding_sound:
  assumes ssec: "system_secure l P sched ls"
      and anns: "\<And> g. P g \<Longrightarrow> anns_satisfied (ls, g, sched)"
      and high_br: "no_high_br l P ls"
    shows "dr_secure l P sched ls"
  unfolding dr_secure_def
proof (clarsimp)
  fix g\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 g\<^sub>1'
  assume geqv: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
  assume meq: "mem g\<^sub>1 = mem g\<^sub>1'"
  assume eeqv: "all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1'"
  assume inpt_cnt: "same_input_count g\<^sub>1 g\<^sub>1'"
  assume Pgs: "P g\<^sub>1" "P g\<^sub>1'"
  assume ev: "(ls, g\<^sub>1, sched) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  then show "\<exists>ls\<^sub>2' g\<^sub>2'.
          (\<exists>sched\<^sub>2'.
              (ls, g\<^sub>1', sched) \<rightarrow>\<^sup>* (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')) \<and> trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2'"
    using geqv meq eeqv ssec anns high_br Pgs inpt_cnt
  proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
    case (rtrancl_refl ls g sched)
    then show ?case
      using global_state_equiv_def by blast
  next
    case (rtrancl_into_rtrancl ls\<^sub>1 g\<^sub>1 sched\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>3 g\<^sub>3 sched\<^sub>3)
    then obtain ls\<^sub>2' g\<^sub>2' sched\<^sub>2' where
      IH: "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')" "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2'"
      by blast
    show ?case
    proof (cases "trace g\<^sub>3 = trace g\<^sub>2")
      case True
      then show ?thesis 
        using rtrancl_into_rtrancl
        unfolding trace_equiv_def
        by auto
    next
      case False
      then obtain ev where ev_def: "trace g\<^sub>3 = trace g\<^sub>2 @ [ev]"
        using eval_at_most_one_event_global rtrancl_into_rtrancl.hyps(2) by blast
      show ?thesis
      proof (cases ev)
        case (Output x11 x12 x13)
        with ev_def rtrancl_into_rtrancl  have sub:
          "uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2) \<subseteq> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)"
          unfolding system_secure_def
          apply simp
          by fastforce
        have "g\<^sub>1' \<in> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2)"
          using IH rtrancl_into_rtrancl
          unfolding uncertainty_def using global_equiv_sym by fastforce
        with sub have "g\<^sub>1' \<in> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)"
          by auto
        then show ?thesis 
          using rtrancl_into_rtrancl
          unfolding uncertainty_def
          using mem_Collect_eq trace_equiv_sym by blast
      next
        case (Input x21 x22)
        with ev_def rtrancl_into_rtrancl have sub:
          "uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2) \<subseteq> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)"
          unfolding system_secure_def
          apply simp
          by fastforce
        have "g\<^sub>1' \<in> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>2)"
          using IH rtrancl_into_rtrancl
          unfolding uncertainty_def using global_equiv_sym by fastforce
        with sub have "g\<^sub>1' \<in> uncertainty l P ls\<^sub>1 g\<^sub>1 sched\<^sub>1 (trace g\<^sub>3)"
          by auto
        then show ?thesis 
          using rtrancl_into_rtrancl
          unfolding uncertainty_def
          by blast 
      next
        case (Declassification to v e)
        from rtrancl_into_rtrancl obtain g\<^sub>2'' where
          ev\<^sub>2': "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2'', sched\<^sub>2)" 
          and len_eq: "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2'' \<upharpoonleft>\<^sub>\<le> l)"
          and inp_len: "\<forall> l'. length (level_inputs l' g\<^sub>2) = length (level_inputs l' g\<^sub>2'')"
          unfolding no_high_br_def same_input_count_def
          by blast
        moreover with IH have "trace g\<^sub>2'' \<le> trace g\<^sub>2' \<or> trace g\<^sub>2' \<le> trace g\<^sub>2''"
          using eval_trace_prefix by auto
        hence "trace g\<^sub>2'' \<upharpoonleft>\<^sub>\<le> l \<le> trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l \<or> trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l \<le> trace g\<^sub>2'' \<upharpoonleft>\<^sub>\<le> l"
          using trace_proj_le_commute by blast
        moreover have "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
          using IH unfolding trace_equiv_def
          by auto
        ultimately have "trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2'' \<upharpoonleft>\<^sub>\<le> l"
          using IH length_same_prefix_same
          unfolding trace_equiv_def
          apply (auto simp: trace_proj_filter)
          apply fastforce
          by simp
        define l\<^sub>2 where "l\<^sub>2 = ls\<^sub>2 ! next_thread (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
        then obtain A where is_decl: "(\<exists>x. head_com (com l\<^sub>2) = {A} x :D= e \<and> \<L> x = Some to) \<or>
                                      head_com (com l\<^sub>2) = DOut A to e" 
          "(eval_exp e (mem g\<^sub>2)) = v"
          using decl_implies_decl_com_global
          using Declassification ev_def rtrancl_into_rtrancl.hyps(2)
          by blast
        let ?from = "exp_level_tot A e"
        from is_decl have t\<^sub>3_eq: "trace g\<^sub>3 = trace g\<^sub>2 @ [Declassification to  (eval_exp e (mem g\<^sub>2)) e]"
          using ev_def Declassification by auto
        let ?t\<^sub>3' = "trace g\<^sub>2'' @ [Declassification to (eval_exp e (mem g\<^sub>2'')) e]"
        define mem_updater where "mem_updater = (\<lambda>mem'. case head_com (com l\<^sub>2) of {A} x :D= e \<Rightarrow> (mem')(x := eval_exp e mem') | _ \<Rightarrow> mem')"
        let ?g\<^sub>3' = "g\<^sub>2''\<lparr>mem := mem_updater (mem g\<^sub>2''),
                        trace := ?t\<^sub>3'\<rparr>"
        define l\<^sub>2' where "l\<^sub>2' = l\<^sub>2\<lparr>com := head_com (com l\<^sub>2)\<rparr>"
        then have ev\<^sub>2'': "(l\<^sub>2', g\<^sub>2'') \<leadsto> (l\<^sub>2'\<lparr>com := Skip\<rparr>, ?g\<^sub>3')"
          using is_decl mem_updater_def
                eval.intros(2)[where l=l\<^sub>2'] 
                eval.intros(4)[where l=l\<^sub>2'] by fastforce
        then obtain l\<^sub>3' where "(l\<^sub>2, g\<^sub>2'') \<leadsto> (l\<^sub>3', ?g\<^sub>3')"
          using eval_head_com is_decl l\<^sub>2'_def
          by metis
        define ls\<^sub>3' where "ls\<^sub>3' = ls\<^sub>2[next_thread (ls\<^sub>2, g\<^sub>2'', sched\<^sub>2) := l\<^sub>3']"
        have ev\<^sub>2''_glob: "(ls\<^sub>2, g\<^sub>2'', sched\<^sub>2) \<rightarrow> (ls\<^sub>3', ?g\<^sub>3', next_sched sched\<^sub>2)"
          using global_eval.intros(1) ev\<^sub>2'' `(l\<^sub>2, g\<^sub>2'') \<leadsto> (l\<^sub>3', ?g\<^sub>3')`
          by (metis global_eval.simps l\<^sub>2_def ls\<^sub>3'_def next_sched_independent_of_memory rtrancl_into_rtrancl.hyps(2))
        then have trace_eq: "trace g\<^sub>3 =\<^sup>t\<^bsub>l\<^esub> ?t\<^sub>3'"
        proof (cases "to \<le> l")
          case True
          let ?c = "head_com (com (ls\<^sub>2 ! next_thread (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)))"
          note system_secure_def[simp]
          note sec_decl = `system_secure l P sched\<^sub>1 ls\<^sub>1`[simplified, rule_format, 
              where ev="Declassification to v e" and g' = g\<^sub>2 and g'' = g\<^sub>3]
          note system_secure_def[simp del]
          from sec_decl True obtain l\<^sub>e where "exp_level_syn e = Some l\<^sub>e"
            using ev_def Declassification
            apply clarsimp
            using rtrancl_into_rtrancl l\<^sub>2_def
            by force
          with rtrancl_into_rtrancl have "embed_dr \<E> l\<^sub>e to g\<^sub>2 v (head_com (com l\<^sub>2))"
            unfolding system_secure_def
            apply clarsimp
            using Declassification True ev_def is_decl(2) l\<^sub>2_def active_ann_head_com
            apply (simp add: Let_def)
            by fastforce
          with is_decl have "embed_dr \<E> l\<^sub>e to g\<^sub>2 v (head_com (com l\<^sub>2))"
            by (simp add: l\<^sub>2_def)
          with embed_dr_def obtain H where H_props:
            "H \<in> \<E> l\<^sub>e to"
            "\<forall> g'. g' \<Turnstile>\<^sub>P A \<longrightarrow> eval_exp e (mem g') = eval_hatch H (level_inputs l\<^sub>e g')"
            using is_decl
            apply (clarsimp)
            by (smt com.simps(123) com.simps(125) embed_dr_def)
          have A_ann: "A = active_ann (ls\<^sub>2 ! next_thread (ls\<^sub>2, g\<^sub>2, sched\<^sub>2))"
            unfolding active_ann_def
            using is_decl active_ann_head_com  l\<^sub>2_def
            apply clarsimp
            by (smt active_ann_com.simps(3) active_ann_com.simps(6) active_ann_head_com)
          have "g\<^sub>2 \<Turnstile>\<^sub>P A"
          proof -
            have "next_thread (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) < length ls\<^sub>2"
              unfolding next_thread.simps
              using rtrancl_into_rtrancl.hyps(2) by auto 
            thus ?thesis
              using anns rtrancl_into_rtrancl  A_ann
              unfolding anns_satisfied.simps anns_satisfied_now.simps
              by (meson \<open>next_thread (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) < length ls\<^sub>2\<close> list_all_length)
          qed
          with H_props have A: "eval_exp e (mem g\<^sub>2) = eval_hatch H (level_inputs l\<^sub>e g\<^sub>2)"
            by auto
          moreover
          have "g\<^sub>2'' \<Turnstile>\<^sub>P A"
          proof -
            have "next_thread (ls\<^sub>2, g\<^sub>2'', sched\<^sub>2) < length ls\<^sub>2"
              unfolding next_thread.simps
              using rtrancl_into_rtrancl.hyps(2) by auto 
            moreover have "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2'', sched\<^sub>2)"
              using IH ev\<^sub>2'
              by auto
            ultimately show ?thesis
              using anns rtrancl_into_rtrancl A_ann
              unfolding anns_satisfied.simps anns_satisfied_now.simps
              by (metis (no_types, lifting) list_all_length next_sched_independent_of_memory)
          qed
          with H_props have B: "eval_exp e (mem g\<^sub>2'') = eval_hatch H (level_inputs l\<^sub>e g\<^sub>2'')"
            by auto
          moreover
          let ?i\<^sub>2'' = "level_inputs ?from g\<^sub>2''"
          have C: "?i\<^sub>2'' = stream_take (length ?i\<^sub>2'') (all_inputs g\<^sub>1' ?from)"
            using eval_level_inputs_multi rtrancl_into_rtrancl IH ev\<^sub>2'
            by auto
          moreover
          let ?i\<^sub>2 = "level_inputs ?from g\<^sub>2"
          have D: "?i\<^sub>2 = stream_take (length ?i\<^sub>2) (all_inputs g\<^sub>1 ?from)"
            using rtrancl_into_rtrancl eval_level_inputs_multi by auto
          from rtrancl_into_rtrancl have E: "length ?i\<^sub>2 = length ?i\<^sub>2''" 
            using IH ev\<^sub>2' length_map inp_len
            by auto
          have "eval_exp e (mem g\<^sub>2) = eval_exp e (mem g\<^sub>2'')"
            using mem_loweq_eval_same
            by (smt A B H_props(1) True dr_equiv'_def dr_equiv_def ev\<^sub>2' eval_all_inputs_multi inp_len rtrancl_into_rtrancl.hyps(1) rtrancl_into_rtrancl.prems(3) stream_take_level_inputs)
          then show ?thesis
            by (simp add: True \<open>trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2'' \<upharpoonleft>\<^sub>\<le> l\<close> trace_append_visible trace_equiv_def t\<^sub>3_eq)
        next
          case False
          thus "trace g\<^sub>3 =\<^sup>t\<^bsub>l\<^esub> ?t\<^sub>3'"
            using IH(2) ev\<^sub>2' trace_equiv_def t\<^sub>3_eq trace_append_not_visible \<open>trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l = trace g\<^sub>2'' \<upharpoonleft>\<^sub>\<le> l\<close>
            unfolding trace_equiv_def
            by auto            
        qed
        moreover
        from IH ev\<^sub>2' ev\<^sub>2''_glob have "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>3', ?g\<^sub>3', next_sched sched\<^sub>2)"
          by auto
        ultimately show ?thesis
          by force
      qed
    qed
  qed
qed

end
end