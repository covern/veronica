theory TypeSystem
  imports Main Language Security "~~/src/HOL/Eisbach/Eisbach" "~~/src/HOL/Library/Lattice_Syntax"
begin

section "A Compositional Type System for @{term system_secure}"

(* Leaving the typing state as a record since we're not sure yet what exactly we want to keep in
there. 

For the moment I'm not sure if we need any state other than the current assumptions. I'm very
uncertain about this, so I'm leaving some dummy state in the rules for now, so I don't have to
change it back again later. *)

record ('level, 'val) type_state =
  nothing :: unit

type_synonym ('level, 'val) bisim =
 "'level \<Rightarrow> ('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state \<Rightarrow> bool"


context lang
begin


definition to_rel :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a rel" where
  "to_rel P = {(x, y). P x y}"

definition sec_step where
  "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2' =
    (if (\<exists> ev. trace g\<^sub>2  = trace g\<^sub>1 @ [ev]) then
      (case (THE ev. trace g\<^sub>2 = trace g\<^sub>1 @ [ev]) of
         Declassification to v e \<Rightarrow>
         ((if to \<le> l then (trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2' \<longrightarrow> g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2')
                     else g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2') \<and>
          (length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)))
       | _ \<Rightarrow> g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2') else g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2')"

definition secure_bisim :: "'level \<Rightarrow> ('level, 'val) bisim \<Rightarrow> bool" where
  "secure_bisim l \<R> \<equiv>
  sym (to_rel (\<R> l)) \<and>
  (\<forall> l\<^sub>1 l\<^sub>1'. \<R> l l\<^sub>1 l\<^sub>1' \<longrightarrow>
       tid l\<^sub>1 = tid l\<^sub>1' \<and>
       ((com l\<^sub>1 = Skip) \<longleftrightarrow> (com l\<^sub>1' = Skip)) \<and>
       (\<forall> l\<^sub>2 g\<^sub>1 g\<^sub>1' g\<^sub>2. 
          (g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1' \<and> g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1 \<and> g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1' \<and>
           (l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)) \<longrightarrow>
             (\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<R> l l\<^sub>2 l\<^sub>2' \<and> 
                        sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2')))"

inductive bisimilar :: "'level \<Rightarrow> ('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state \<Rightarrow> bool"
  (infix "\<approx>\<index>" 60)
  where
  bisim[intro!]: "\<lbrakk> secure_bisim l \<R> ; \<R> l l\<^sub>1 l\<^sub>1' \<rbrakk> \<Longrightarrow> bisimilar l l\<^sub>1 l\<^sub>1'"

definition join_list :: "'level list \<Rightarrow> 'level" ("\<Squnion>\<^sub>l _" [90] 999) where
  "join_list ls = fold (\<squnion>) ls \<bottom>"


text {*
  In this branch we try to extend the type system to handle high branching. This is somewhat critical
  to support the point that decoupling functional correctness from security is useful: If this
  would only work in the absence of high branching, then the result would be quite unsurprising
  and much less useful. On the other hand, if we do manage to allow for high-branching, this
  shows that high branching is indeed somewhat orthogonal to this.

  To simplify the type system we enforce something slightly stronger than what is needed to ensure
  security at level l. For outputs and assignments we ensure security at all levels, since to allow
  insecure behavior at higher levels would make the system rather ugly.
*}
inductive
  has_type :: "bool \<Rightarrow> 'level \<Rightarrow> ('level, 'val) com \<Rightarrow> bool" 
  ("_, _ \<turnstile> _" [40, 40] 50)
  for ah :: bool and l :: "'level"
where
  Skip_type: "ah, l \<turnstile> Skip" |
  Seq_type: "\<lbrakk> ah, l \<turnstile> c\<^sub>1 ;  ah, l \<turnstile> c\<^sub>2 \<rbrakk> \<Longrightarrow> ah, l \<turnstile> c\<^sub>1 ;; c\<^sub>2" |
  UAssign_type: "\<lbrakk> \<L> x = None \<rbrakk> \<Longrightarrow> ah, l \<turnstile> {A} x ::= e" |
  LAssign_type: "\<lbrakk> \<L> x = Some lev ; ann_implies_low l\<^sub>e A e ; l\<^sub>e \<le> lev \<rbrakk> \<Longrightarrow>
    ah, l \<turnstile> {A} x ::= e" |
  DAssign_type: "\<lbrakk> \<L> x = Some lev ; exp_level_syn e = Some l\<^sub>e ;
    \<And> g. g \<Turnstile>\<^sub>P A \<Longrightarrow> \<D> l\<^sub>e lev g (eval_exp e (mem g)) ({A} x :D= e) \<rbrakk> \<Longrightarrow>
    ah, l \<turnstile> {A} x :D= e" |
  If_type: "\<lbrakk> ah, l \<turnstile> c\<^sub>1 ; ah, l \<turnstile> c\<^sub>2 ; 
              if ah then (\<not> ann_implies_low l A e \<longrightarrow>
                  (\<forall> t. \<lparr>com = c\<^sub>1, tid = t\<rparr> \<approx>\<^bsub>l\<^esub> \<lparr>com = c\<^sub>2, tid = t\<rparr>)) 
              else ann_implies_low l A e \<rbrakk> \<Longrightarrow>
    ah, l \<turnstile> If A e c\<^sub>1 c\<^sub>2" |
  DOut_type: "\<lbrakk> exp_level_syn e = Some l\<^sub>e ; 
               \<And> g. g \<Turnstile>\<^sub>P A \<Longrightarrow> \<D> l\<^sub>e l' g (eval_exp e (mem g)) (DOut A l' e) \<rbrakk> \<Longrightarrow> 
    ah, l \<turnstile> DOut A l' e" |
  Out_type: "\<lbrakk> l' \<le> l \<Longrightarrow> ann_implies_low l A e \<rbrakk> \<Longrightarrow> 
    ah, l \<turnstile> Out A l' e" |
  UIn_type: "\<lbrakk> \<L> x = None \<rbrakk> \<Longrightarrow> 
    ah, l \<turnstile> {A} x <- l'" |
  LIn_type: "\<lbrakk> \<L> x = Some lev ; l' \<le> lev \<rbrakk> \<Longrightarrow> 
    ah, l \<turnstile> {A} x <- l'" |
  While_type: "\<lbrakk> ah, l \<turnstile> c ; ann_implies_low l A e ; ann_implies_low l I e \<rbrakk> \<Longrightarrow>
     ah, l \<turnstile> While A e I c" |
  Acquire_type: "ah, l \<turnstile> Acquire A lo" |
  Release_type: "ah, l \<turnstile> Release A lo"

(* Initial typing state (not really used at this point). *)
definition \<Gamma>\<^sub>0 :: "('level, 'val) type_state" where
  "\<Gamma>\<^sub>0 = \<lparr>nothing = ()\<rparr>"


method typing =
  ((match conclusion in
      "has_type ?ah ?l (?c\<^sub>1 ;; ?c\<^sub>2)" \<Rightarrow> \<open>rule Seq_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l Skip" \<Rightarrow> \<open>rule Skip_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l (Out ?A ?l' ?e)" \<Rightarrow> \<open>rule Out_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l (DOut ?A ?l' ?e)" \<Rightarrow> \<open>rule DOut_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l (Acquire ?A ?loc)" \<Rightarrow> \<open>rule Acquire_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l (Release ?A ?loc)" \<Rightarrow> \<open>rule Release_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l (If ?A ?e ?c\<^sub>1 ?c\<^sub>2)" \<Rightarrow> \<open>rule If_type\<close>) |
   (match conclusion in
      "has_type ?ah ?l ({?A} ?x :D= ?e)" \<Rightarrow> \<open>rule DAssign_type, simp add: satisfies_ann_preds_def\<close>)) ;
  (typing | -)


end 

definition \<D>\<^sub>p :: "'level \<Rightarrow> ('level::complete_lattice, nat) decl_pred" where
  "\<D>\<^sub>p from to g v e \<equiv> (v = last_input \<top> g mod 2)"

locale lang'' = lang 
  where level_rename_param = lr and val_rename_param = 0
    and val_true = "(<) 0"
  for lr :: "'level::complete_lattice"

locale parity' = lang'' where \<D> = \<D>\<^sub>p and lr = lr
  for lr :: "'level::complete_lattice"

context parity'
begin

definition parity_branch :: "('level, nat) com" where
  "parity_branch =
    (let li = (\<lambda> g. last_input \<top> g = mem g ''h'')
     in let x_dec = (\<lambda> g. mem g ''x'' = mem g ''h'' mod 2)
     in
     ({ann\<^sub>0} ''h'' <- \<top> ;;
     {\<lparr>preds = {li}\<rparr>} ''x'' :D= BinOp (\<lambda> n m. n mod m) (Var ''h'') (Val 2) ;;
     (If (\<lparr>preds = {li, x_dec}\<rparr>) 
         (UnOp (\<lambda> n. if (n > 0) then 1 else 0) (Var ''x'')) 
            (Out ann\<^sub>0 \<bottom> (Val 0))
            Skip)))"
(*
lemma parity_branch_typed:
  assumes "\<L> ''x'' = Some \<bottom>" "\<L> ''h'' = Some \<top>"
  shows "\<exists> \<Gamma> \<Gamma>'. has_type \<bottom> [] \<Gamma> (parity_branch) \<Gamma>'" (is "\<exists> G G'. ?P G G'")
proof -
  have "?P \<Gamma>\<^sub>0 \<Gamma>\<^sub>0"
    apply (simp add: parity_branch_def Let_def)
    apply (rule Seq_type[where \<Gamma>' = \<Gamma>\<^sub>0])
     apply (rule LIn_type[where lev = \<top>])
      apply (auto simp: assms)[1]
     apply auto[1]
    apply (rule Seq_type[where \<Gamma>' = \<Gamma>\<^sub>0])
    apply (rule DAssign_type[where lev = \<bottom>])
      apply (auto simp: \<D>\<^sub>p_def satisfies_ann_preds_def \<Gamma>\<^sub>0_def assms)
    apply (rule If_type)
      apply (rule Out_type)
      apply (auto simp: ann_implies_low_def)[1]
     apply (rule Skip_type)
      apply (auto simp: ann_implies_low_def)[1]
      apply (auto simp: \<D>\<^sub>p_def satisfies_ann_preds_def \<Gamma>\<^sub>0_def assms)
    oops
*)

end
end