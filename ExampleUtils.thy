theory ExampleUtils
  imports Main Language Security TypeSystem
begin

subsection {* Utility functions for writing examples *}

definition nat_eq :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "nat_eq x y = (if x = y then 1 else 0)"

definition exp_plus :: "'val exp \<Rightarrow> 'val exp \<Rightarrow> 'val::numeral exp" (infixr "+\<^sub>e" 80) where
  "e\<^sub>1 +\<^sub>e e\<^sub>2 = BinOp (+) e\<^sub>1 e\<^sub>2"


subsection "Example: Parity of last input"


definition \<L>\<^sub>h :: "var \<Rightarrow> 'level::complete_lattice option" where
  "\<L>\<^sub>h x = (case x of
            c # cs \<Rightarrow> (if (c = CHR ''h'') then Some \<top> else 
                          (if c = CHR ''l'' then Some \<bottom> else None))
          | _ \<Rightarrow> None)"

end
