# VERONICA: Expressive and Precise Concurrent Information Flow Security 

The VERONICA system uses program functional correctness annotations to
provide precise, compositional reasoning about expressive information flow
policies, including various forms of declassification, for concurrent
programs.

It is implemented in the Isabelle/HOL thoerem prover and the approach is
described in detail in the paper:

Daniel Schoepe, Toby Murray, Andrei Sabelfeld. 
"[VERONICA: Expressive and Precise Concurrent Information Flow Security](https://arxiv.org/abs/2001.11142)".
IEEE Computer Security Foundations Symposium (CSF), 2020.

## Building

Build using Isabelle 2018, which can be obtained from:
https://isabelle.in.tum.de/website-Isabelle2018/index.html

To build, run:
`isabelle build -d .  -v Knowledge`

The `-v` is optional and will result in verbose output.

Building the entire contents takes approximately 7.5 minutes on a
mid-2015 MacBook Pro (2.2GHz Core i7, 16GB RAM)

Verbose build output looks something like this:

```
ISABELLE_BUILD_OPTIONS=""

ML_PLATFORM="x86-darwin"
ML_HOME="/Applications/Isabelle2018.app/Contents/Resources/Isabelle2018/contrib/polyml-5.7.1-8/x86-darwin"
ML_SYSTEM="polyml-5.7.1"
ML_OPTIONS="--minheap 500"

Session Pure/Pure
Session FOL/FOL
Session HOL/HOL (main)
Session HOL/HOL-Eisbach
Session Unsorted/Knowledge
Running Knowledge ...
Knowledge: theory Knowledge.Lattice_Syntax
Knowledge: theory Knowledge.Reverse_Transitive_Closure
Knowledge: theory Knowledge.Stream
Knowledge: theory Knowledge.OG_Com
Knowledge: theory Knowledge.Sublist
Knowledge: theory Knowledge.Eisbach
Knowledge: theory Knowledge.Prefix_Order
Knowledge: theory Knowledge.Language
Knowledge: theory Knowledge.OG_Tran
Knowledge: theory Knowledge.OG_Hoare
Knowledge: theory Knowledge.OG_Tactics
Knowledge: theory Knowledge.Security
Knowledge: theory Knowledge.TypeSystem
Knowledge: theory Knowledge.Automation
Knowledge: theory Knowledge.EvalNondet
Knowledge: theory Knowledge.ExampleUtils
Knowledge: theory Knowledge.Soundness
Knowledge: theory Knowledge.RG
Knowledge: theory Knowledge.RG_Verify
Knowledge: theory Knowledge.HighBranching
Knowledge: theory Knowledge.OGTranslation
Knowledge: theory Knowledge.Infer
Knowledge: theory Knowledge.RG_Trim_Commands
Knowledge: theory Knowledge.DelimitedRelease
Knowledge: theory Knowledge.RunningAvg
Knowledge: theory Knowledge.VDBuffer
Knowledge: theory Knowledge.VDBuffer_OG
Knowledge: theory Knowledge.VDBuffer_Declass
Knowledge: theory Knowledge.VDBuffer_Declass_Confirm
Knowledge: theory Knowledge.VDBuffer_Declass_Confirm_OG
Knowledge: theory Knowledge.VDBuffer_Declass_OG
Timing Knowledge (4 threads, 448.390s elapsed time, 1348.735s cpu time, 165.163s GC time, factor 3.01)
Finished Knowledge (0:07:29 elapsed time, 0:22:30 cpu time, factor 3.00)
```

## A Guide to Theories

Here we list the major theories and which sections of the paper each
corresponds to. Each theory is found in the file of the same name with the
extension '.thy', e.g. 'Language' is found in the file 'Language.thy'.

* [Language](Language.thy): language definition and semantics (Section 4)
* [Security](Security.thy): the system security property (Sections 3.1, 3.2)
* [DelimitedRelease](DelimitedRelease.thy): delimited release definition and embedding (Section 3.4)
* [TypeSystem](TypeSystem.thy): the logic rules (Section 5, 5.1, 5.2)
* [Soundness](Soundness.thy): soundness proof for the logic (Section 5.3)
* [SoundnessNoHighBr](SoundnessNoHighBr.thy): logic proves "free of \<E>-secret branching" (Section 6.1)
* [HighBranching](HighBranching.thy): rules for reasoning about secret-branching (Section 5.2)
* [Infer](Infer.thy): functional correctness annotation inference (Section 2.3, Step (2))
* [RG](RG.thy)/[RG_Verify](RG_Verify.thy)/[RG_Trim_Commands](RG_Trim_Commands.thy): Rely Guarantee method (Section 2.3, Step (3))
* [OGTranslation](OGTranslation.thy): Owicki Gries method (Section 2.3, Step (3), Section 6.1)
* [VDBuffer_Declass](examples/VDBuffer_Declass.thy): definitions and security proof for Figure 1 (Sections 2, 6.1)
* [VDBuffer_Declass_OG](examples/VDBuffer_Declass_OG.thy): functional correctness proof for Fig 1 (Sections 2, 6.1)
* [VDBuffer_Declass_Confirm](examples/VDBuffer_Declass_Confirm.thy): confirmed declassification example (Section 6.2)
* [VDBuffer_Declass_Confirm_OG](examples/VDBuffer_Declass_Confirm_OG.thy): functional correctness proofs for same (Sect 6.2)
* [RunningAvg](examples/RunningAvg.thy): running average example, definitions and proof (Sect 6.3)
* [RunningAvgExtensional](examples/RunningAvgExtensional.thy): extensional running average example (Sect 6.3)
* [VDBuffer](examples/VDBuffer.thy): definitions and security proof for Figure 9 (Section 6.4)
* [VDBuffer_OG](examples/VDBuffer_OG.thy): functional correctness proofs for same (Section 6.4)

## Where to find the main theorems

Theorem 3.4.1 (Delimited Release Embedding)
 - see the `embedding_sound` theorem in [DelimitedRelease.thy](DelimitedRelease.thy)

Theorem 5.3.1 (Soundness)
 - see the `soundness` theorem in [Soundness.thy](Soundness.thy)

