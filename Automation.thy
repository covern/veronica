theory Automation
  imports Language Security TypeSystem
begin

context lang
begin


lemma trace_equiv_last:
  "trace g =\<^sup>t\<^bsub>l\<^esub> trace g' \<Longrightarrow> l' \<le> l \<Longrightarrow> last_input l' g = last_input l' g'"
  using trace_equiv_le_closed project_trace_upto_eq_project_trace_eq
        last_input_def trace_equiv_def level_inputs_def by metis

lemma trace_equiv_bot_last:
  "trace g =\<^sup>t\<^bsub>l\<^esub> trace g' \<Longrightarrow> last_input \<bottom> g = last_input \<bottom> g'"
  using trace_equiv_last
  using bot.extremum by blast

definition mem_labelled_eq :: "'val mem \<Rightarrow> 'val mem \<Rightarrow> bool"
  where "mem_labelled_eq m\<^sub>1 m\<^sub>2 \<equiv> (\<forall> x l'. \<L> x = Some l' \<longrightarrow> m\<^sub>1 x = m\<^sub>2 x)"

lemma top_equiv_eq:
  "g =\<^sup>g\<^bsub>\<top>\<^esub> g' \<Longrightarrow> mem_labelled_eq (mem g) (mem g')"
  by(clarsimp simp: global_state_equiv_def mem_loweq_def mem_labelled_eq_def)
  


method solve methods m = (m ; fail)

named_theorems trace_lems

lemma inputs_app [simp]:
  "inputs (xs @ ys) = (inputs xs) @ (inputs ys)"
  apply(induction xs arbitrary: ys rule: rev_induct, fastforce)
  apply clarsimp
  apply(case_tac x, auto)
  done


lemma outputs_app [simp]:
  "outputs (xs @ ys) = (outputs xs) @ (outputs ys)"
  apply(induction xs arbitrary: ys rule: rev_induct, fastforce)
  apply clarsimp
  apply(case_tac x, auto)
  done

lemma inputs_Cons:
  "inputs (y # ys) = (inputs [y]) @ (inputs ys)"
  using inputs_app
  by (metis append.left_neutral append_Cons)
lemma outputs_Cons:
  "outputs (y # ys) = (outputs [y]) @ (outputs ys)"
  using outputs_app
  by (metis append.left_neutral append_Cons)

lemma project_trace_app [simp]:
  "project_trace (t @ u) l = (project_trace t l) @ (project_trace u l)"
  by(induction t, auto)

(* not the world's cleanest proof... *)
lemma last_input_lem1[trace_lems]:
  "ev = last_input lev (s\<lparr>trace := t @ [Input lev ev]\<rparr>)"
  apply (induction t arbitrary: lev ev)
   apply (clarsimp simp: last_input_def level_inputs_def)+
  apply (subst last_map)
   apply (subst inputs_Cons)
   apply clarsimp
  apply (subst inputs_Cons)
  apply clarsimp
  done
lemma last_output_lem1[trace_lems]:
  "ev = last_output lev (s\<lparr>trace := t @ [Output lev ev e]\<rparr>)"
  apply (induction t arbitrary: lev ev)
   apply (clarsimp simp: last_output_def level_outputs_def)+
  apply (subst last_map)
   apply (subst outputs_Cons)
   apply clarsimp
  apply (subst outputs_Cons)
  apply clarsimp
  done

lemma level_inputs_upd_mem[trace_lems]:
  "level_inputs lev (s\<lparr>mem := m'\<rparr>) = level_inputs lev s"
  unfolding level_inputs_def
  by simp

lemma level_inputs_upd_ls[trace_lems]:
  "level_inputs lev (s\<lparr>lock_state := locs'\<rparr>) = level_inputs lev s"
  unfolding level_inputs_def
  by simp

lemma level_inputs_upd_env[trace_lems]:
  "level_inputs lev (s\<lparr>env := e'\<rparr>) = level_inputs lev s"
  unfolding level_inputs_def
  by simp

lemma level_inputs_upd_out[trace_lems]:
  "level_inputs lev
             (s\<lparr>trace := (trace s) @ [Output lev ev v]\<rparr>) = level_inputs lev s"
  by(simp add: level_inputs_def)

lemma level_inputs_upd_declas[trace_lems]:
  "level_inputs lev
             (s\<lparr>mem := m',
                trace := (trace s) @ [Declassification lev' ev v]\<rparr>) = level_inputs lev s"
  by(simp add: level_inputs_def)

lemma level_inputs_upd_last[trace_lems]:
  "level_inputs lev
             (s\<lparr>env := e',
                mem := m',
                trace := (trace s) @ [Input lev ev]\<rparr>) = (level_inputs lev s) @ [ev]"
  by(simp add: level_inputs_def)

lemma level_inputs_upd_last_other[trace_lems]:
  "lev' \<noteq> lev \<Longrightarrow> level_inputs lev
             (s\<lparr>env := e',
                mem := m',
                trace := (trace s) @ [Input lev' ev]\<rparr>) = (level_inputs lev s)"
  by(simp add: level_inputs_def)


lemma last_input_upd_mem[trace_lems]:
  "last_input lev (s\<lparr>mem := m'\<rparr>) = last_input lev s"
  unfolding last_input_def 
  by (simp add: trace_lems)

lemma last_input_upd_ls[trace_lems]:
  "last_input lev (s\<lparr>lock_state := locs'\<rparr>) = last_input lev s"
  unfolding last_input_def
  by (simp add: trace_lems)

lemma last_input_upd_env[trace_lems]:
  "last_input lev (s\<lparr>env := e'\<rparr>) = last_input lev s"
  unfolding last_input_def
  by (simp add: trace_lems)

lemma last_input_upd_out[trace_lems]:
  "last_input lev
             (s\<lparr>trace := (trace s) @ [Output lev ev v]\<rparr>) = last_input lev s"
  by(simp add: last_input_def trace_lems)

lemma last_input_upd_declas[trace_lems]:
  "last_input lev
             (s\<lparr>mem := m',
                trace := (trace s) @ [Declassification lev' ev v]\<rparr>) = last_input lev s"
  by(simp add: last_input_def level_inputs_def)

lemma last_input_upd_last[trace_lems]:
  "last_input lev
             (s\<lparr>env := e',
                mem := m',
                trace := t' @ [Input lev ev]\<rparr>) = ev"
  by (metis last_input_lem1)


lemma last_input_upd_last_other[trace_lems]:
  "lev' \<noteq> lev \<Longrightarrow> last_input lev
             (s\<lparr>env := e',
                mem := m',
                trace := (trace s) @ [Input lev' ev]\<rparr>) = last_input lev s"
  apply (auto simp: last_input_def trace_lems)
  done


lemma last_output_upd_out[trace_lems]:
  "last_output lev
             (s\<lparr>trace := (trace s) @ [Output lev ev v]\<rparr>) = ev"
  by (simp add: last_output_def trace_lems)

lemmas last_input_upds = last_input_upd_env 
  last_input_upd_last last_input_upd_mem last_input_upd_ls last_input_upd_out last_input_upd_declas
  last_output_upd_out last_input_upd_last_other

lemmas level_input_upds = level_inputs_upd_env 
    level_inputs_upd_last level_inputs_upd_mem level_inputs_upd_ls level_inputs_upd_out
    level_inputs_upd_last_other level_inputs_upd_declas

method crush = 
  solve \<open>(auto simp: trace_lems)[1]\<close>

method force_crush =
  solve \<open>insert trace_lems, fastforce\<close>

method crush_both =
  clarsimp simp: last_input_upds , (crush | force_crush)


end

end
