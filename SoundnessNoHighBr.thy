theory SoundnessNoHighBr
  imports Main Language Security TypeSystem Soundness DelimitedRelease HOL.Orderings "~~/src/HOL/Library/Prefix_Order"  "~~/src/HOL/Library/Lattice_Syntax"
begin

context delimited_release
begin

text {* In this file we show that a program satisfies the no-high-branching
condition from theorem @{thm embedding_sound}, if it's typeable,
satisfies the annotations, and the typing doesn't use high branching.  *}


lemma gequiv_impl_trace_len:
  assumes "g =\<^sup>g\<^bsub>l\<^esub> g'"
  shows "length (trace g \<upharpoonleft>\<^sub>\<le> l) = length (trace g' \<upharpoonleft>\<^sub>\<le> l)"
  using assms
  unfolding global_equiv_defs by auto

lemma same_input_length_append:
  assumes "length (inputs (t \<upharpoonleft> l)) = length (inputs (t' \<upharpoonleft> l))"
  shows "length (inputs (t @ [Input l' x] \<upharpoonleft> l)) = 
         length (inputs (t' @ [Input l' x'] \<upharpoonleft> l))"
  using assms
  unfolding trace_proj_filter'
  apply (cases "l' = l")
  apply (auto)
  apply (induction t ; auto)
   apply (induction t' ; auto)
  apply (metis (no_types, lifting) append_Cons inputs_app_input length_Suc_conv list.size(3) self_append_conv2)
   apply (induction t' ; auto)
   apply (metis append_Cons append_Nil inputs_app_input length_Suc_conv list.size(3))
  apply (rename_tac a aaa t' aa)
  apply (case_tac a ; auto)
  by (metis append_Cons inputs_app_input length_append_singleton)

lemma has_type_no_highbr_progress:
  assumes "has_type False l c"
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and "all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1'"
      and "same_input_count g\<^sub>1 g\<^sub>1'"
      and c_eq: "com l\<^sub>1 = c"
      and sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1"
    shows
        "\<exists> g\<^sub>2'. (l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, g\<^sub>2') \<and> 
                same_input_count g\<^sub>2 g\<^sub>2' \<and>
                g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
  using assms
proof (induction arbitrary: g\<^sub>1 g\<^sub>2 l\<^sub>2 l\<^sub>1 g\<^sub>1' rule: has_type.induct)
  case Skip_type
  then show ?case
    by (meson eval_skip_elim) 
next
  case (Seq_type c\<^sub>1 c\<^sub>2)
  note a = this
  from `(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` show ?case (is "\<exists> g. ?P g \<and> ?Q g")
  proof (rule eval_seq_elim)
   fix g'' 
    fix l'' :: "('level, 'val) local_state"
    assume ev\<^sub>1: "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')"
    assume l''_skip[simp]: "com l'' = Skip"
    assume [simp]: "l\<^sub>2 = l''\<lparr>com := c\<^sub>2\<rparr>"
    assume [simp]: "g\<^sub>2 = g''"
    have A': "active_ann l\<^sub>1 = active_ann_com c\<^sub>1" using a 
      by (auto simp: active_ann_def)
    note foo = a(3)[where l\<^sub>11 = "l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>" and g\<^sub>11 = g\<^sub>1 and g\<^sub>21=g'' and l\<^sub>21 = "l''" and g\<^sub>1'1 = g\<^sub>1']

    have "\<exists> g\<^sub>2''. (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l'', g\<^sub>2'') \<and> same_input_count g'' g\<^sub>2'' \<and>
                        g'' =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2''"
      using a foo ev\<^sub>1
      by (clarsimp simp: active_ann_def )

    then obtain  g\<^sub>2'' where ev\<^sub>2': "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l'', g\<^sub>2'')" 
      and \<R>': "same_input_count g'' g\<^sub>2''"
      and sec: "g'' =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2''"
      by blast
    moreover
    have [simp]: "head_com (com l\<^sub>1) = head_com c\<^sub>1"
      using Seq_type by auto
    then have sec': "same_input_count g\<^sub>2 g\<^sub>2''"
      using a
      apply clarsimp
      using \<R>' by blast
    moreover from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2''"
      using \<open>g\<^sub>2 = g''\<close> sec by blast
    moreover
    have "l\<^sub>2 = l''\<lparr>com := c\<^sub>2\<rparr>"
      using ev\<^sub>1 ev\<^sub>2' tid_constant by auto
    ultimately show ?case
      using A' a
      apply -
      apply (rule exI[where x="g\<^sub>2''"])
      apply auto
      apply (rule eval_seqskip[where l=l\<^sub>1 and c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 and g=g\<^sub>1' and l'=l'' and g'=g\<^sub>2''])
      using l''_skip by blast+
  next
    fix g'' l''
    assume ev\<^sub>1: "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l'', g'')"
    assume not_skip: "com l'' \<noteq> Skip"
    assume "l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>"
    assume [simp]: "g\<^sub>2 = g''"
    have A': "active_ann l\<^sub>1 = active_ann_com c\<^sub>1" using a 
      by (auto simp: active_ann_def)
    have "\<exists> g\<^sub>2''. (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l'', g\<^sub>2'') \<and> 
                       same_input_count g'' g\<^sub>2'' \<and>
                       g'' =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2''"
      using a ev\<^sub>1
      by (clarsimp simp: active_ann_def sec_step_def)
    then obtain g\<^sub>2'' where ev\<^sub>2': "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l'', g\<^sub>2'')" 
      and \<R>': "same_input_count g'' g\<^sub>2''"
      and sec: "g'' =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2''"
      by blast
    with Seq_type ev\<^sub>2' have "(l\<^sub>1, g\<^sub>1') \<leadsto> (l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>, g\<^sub>2'')"
      by (simp add: a(3) a(4) eval.eval_seq not_skip)
    moreover
    have "(l\<^sub>1, g\<^sub>1) \<leadsto> (l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>, g\<^sub>2)"
      using \<open>l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>\<close> a(5) by blast
    have [simp]: "head_com (com l\<^sub>1) = head_com c\<^sub>1"
      using Seq_type by auto
    ultimately show ?thesis
      using \<R>' \<open>g\<^sub>2 = g''\<close> \<open>l\<^sub>2 = l''\<lparr>com := com l'' ;; c\<^sub>2\<rparr>\<close> sec by blast
  qed
next
  case (UAssign_type x A e)
  note a = this
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1'))\<rparr>"
  from a have ev\<^sub>2: "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval_assign by force
  moreover from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
    unfolding global_state_equiv_def mem_loweq_def
    by auto
  moreover from a have "same_input_count g\<^sub>2 ?g\<^sub>2'"
    unfolding same_input_count_def level_inputs_def 
    by auto
  ultimately show ?case
    by auto
next
  case (LAssign_type x lev l\<^sub>e A e)
  note a = this
  hence [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp e (mem g\<^sub>1'))\<rparr>"
  from a have ev\<^sub>2: "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval_assign by force
  from a show ?case
  proof (cases "lev \<le> l")
    case True
    with a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      unfolding global_equiv_defs
      apply clarsimp
      apply (clarsimp simp: ann_implies_low_def active_ann_def)
      using a global_equiv_le_closed
      by auto
    moreover from True and a have "same_input_count g\<^sub>2 ?g\<^sub>2'"
      unfolding same_input_count_def level_inputs_def 
      by auto
    ultimately show ?thesis
      using a ev\<^sub>2
      by auto
  next
    case False
    with a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      unfolding global_equiv_defs
      by auto
    moreover from False a have "same_input_count g\<^sub>2 ?g\<^sub>2'"
      unfolding same_input_count_def level_inputs_def 
      by auto
    ultimately show ?thesis
      using a ev\<^sub>2 
      by auto
  qed
next
  case (DAssign_type x lev ex l\<^sub>e A)
  then have "active_ann l\<^sub>1 = A"
    unfolding active_ann_def
    by auto
  note a = DAssign_type
  hence g\<^sub>2_def: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp ex (mem g\<^sub>1)),
                        trace := trace g\<^sub>1 @ [Declassification lev (eval_exp ex (mem g\<^sub>1)) ex]\<rparr>"
    using eval_dassign_elim
    by auto
  from a have the_eq[simp]: "(THE l. exp_level_syn ex = Some l) = l\<^sub>e"
    by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1')(x := eval_exp ex (mem g\<^sub>1')),
                 trace := trace g\<^sub>1' @ [Declassification lev (eval_exp ex (mem g\<^sub>1')) ex]\<rparr>"
  let ?l\<^sub>2' = "l\<^sub>1\<lparr>com := Skip\<rparr>"
  from a have same_len: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
    by (auto simp: global_equiv_defs)
  show ?case (is "\<exists> g'. ?ev g' \<and> ?inps g' \<and> ?eqv g'")
  proof -
    from a have "?ev ?g\<^sub>2'"
      by (auto simp: sec_step_def g\<^sub>2_def eval_dassign)
    moreover from DAssign_type have "?ev ?g\<^sub>2'"
      using calculation by blast
    moreover from DAssign_type have "?eqv ?g\<^sub>2'"
    proof (cases "lev \<le> l")
      case True
      with a obtain H where H_props:
            "H \<in> \<E> l\<^sub>e lev"
            "\<forall> g'. g' \<Turnstile>\<^sub>P A \<longrightarrow> eval_exp ex (mem g') = eval_hatch H (level_inputs l\<^sub>e g')"
        unfolding embed_dr_def
        apply clarsimp
        using \<open>active_ann l\<^sub>1 = A\<close> by blast
      with a have "eval_exp ex (mem g\<^sub>1) = eval_exp ex (mem g\<^sub>1')"
        apply (auto simp: embed_dr_def dr_equiv_def dr_equiv'_def)
        by (metis True \<open>active_ann l\<^sub>1 = A\<close> same_input_count_def stream_take_level_inputs)
      moreover 
      from same_len a have "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace ?g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
        using trace_append_same_level
        by (auto simp: g\<^sub>2_def global_equiv_defs)
      ultimately show ?thesis 
        using a DAssign_type
        apply (auto simp: global_equiv_defs g\<^sub>2_def )
        by (metis trace_append_not_visible trace_append_visible)
    next
      case False
      with a same_len show ?thesis
        apply (clarsimp simp: sec_step_def g\<^sub>2_def global_state_equiv_def trace_append_same_level)
        apply (simp add: trace_equiv_append_high)
        by (simp add: mem_loweq_def)
    qed
    moreover have "?inps ?g\<^sub>2'"
      using a
      unfolding same_input_count_def level_inputs_def 
      apply (auto simp: g\<^sub>2_def)
      by (simp add: inputs_app_not_input_proj)
    ultimately show ?thesis
      by auto
  qed
next
  case (If_type c\<^sub>1 c\<^sub>2 A e)
  hence "active_ann l\<^sub>1 = A"
    unfolding active_ann_def by auto
  from If_type have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
    apply (auto simp add: ann_implies_low_def global_state_equiv_def)[1]
    using \<open>active_ann l\<^sub>1 = A\<close> by blast
  with If_type show ?case 
    apply auto
    by (smt lang.eval.eval_iffalse lang.eval.eval_iftrue lang.eval_if_elim lang_axioms)
next
  case (DOut_type ex l\<^sub>e A lev)
  then have [simp]: "active_ann l\<^sub>1 = A"
    unfolding active_ann_def
    by auto
  note a = DOut_type
  hence g\<^sub>2_def: "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1),
                        trace := trace g\<^sub>1 @ [Declassification lev (eval_exp ex (mem g\<^sub>1)) ex]\<rparr>"
    using eval_dassign_elim
    by auto
  from a have the_eq[simp]: "(THE l. exp_level_syn ex = Some l) = l\<^sub>e"
    by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>mem := (mem g\<^sub>1'),
                 trace := trace g\<^sub>1' @ [Declassification lev (eval_exp ex (mem g\<^sub>1')) ex]\<rparr>"
  from a have same_len: "length (trace g\<^sub>1 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>1' \<upharpoonleft>\<^sub>\<le> l)"
    by (auto simp: global_equiv_defs)
  show ?case (is "\<exists> g'. ?ev g' \<and> ?inps g' \<and> ?eqv g'")
  proof -
    from a have "?ev ?g\<^sub>2'"
      by (auto simp: sec_step_def g\<^sub>2_def eval_dout)
    moreover from DOut_type have "?inps ?g\<^sub>2'"
      using a
      unfolding same_input_count_def level_inputs_def 
      apply (auto simp: g\<^sub>2_def)
      by (simp add: inputs_app_not_input_proj)
    moreover from DOut_type have "?eqv ?g\<^sub>2'"
    proof (cases "lev \<le> l")
      case True
      moreover then have
        "eval_exp ex (mem g\<^sub>1) = eval_exp ex (mem g\<^sub>1')"
        using a
          unfolding embed_dr_def
          apply clarsimp
          by (smt dr_equiv'_def dr_equiv_def same_input_count_def stream_take_level_inputs)
      moreover 
      from same_len a have "length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace ?g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l)"
        using trace_append_same_level
        by (auto simp: g\<^sub>2_def global_equiv_defs)
      ultimately show ?thesis 
        using DOut_type
        by (auto simp: global_equiv_defs trace_append_visible g\<^sub>2_def)
    next
      case False
      with a same_len show ?thesis
        apply (clarsimp simp: sec_step_def g\<^sub>2_def global_state_equiv_def trace_append_same_level)
        apply (simp add: trace_equiv_append_high)
        done
    qed
    ultimately show ?thesis by force
  qed
next
  case (Out_type l' A e)
  note a = Out_type
  hence ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" "g\<^sub>2 = g\<^sub>1\<lparr>trace := trace g\<^sub>1 @ [Output l' (eval_exp e (mem g\<^sub>1)) e]\<rparr>"
    using eval_out_elim by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>trace := trace g\<^sub>1' @ [Output l' (eval_exp e (mem g\<^sub>1')) e]\<rparr>"
  from a have ev': "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval.intros by auto
  show ?case
  proof (cases "l' \<le> l")
    case True
    from True a have "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
      unfolding ann_implies_low_def active_ann_def
      by auto
    with True a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'" 
      unfolding global_state_equiv_def
      using trace_equiv_append_same
      by fastforce
    with ev' show ?thesis using a
      apply -
      apply (rule exI[where x = ?g\<^sub>2'])
      by (clarsimp simp: ev same_input_count_def level_inputs_def inputs_app_not_input_proj)
  next
    case False
    with a ev have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_high
      unfolding global_equiv_defs
      by auto
    with a False have eqv\<^sub>2: "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      unfolding global_state_equiv_def by fastforce
    have no_decl: "\<exists>ev. Output l' (eval_exp e (mem g\<^sub>1)) e = ev \<longrightarrow>
          (case ev of
           Declassification to v ea \<Rightarrow> False
          | _ \<Rightarrow> True)"
      by auto
    with eqv\<^sub>2 ev' a show ?thesis
      apply -
      apply (rule exI[where x = ?g\<^sub>2'])
      by (auto simp: ev global_equiv_defs same_input_count_def  inputs_app_not_input_proj level_inputs_def)
  qed
next
  case (UIn_type x A l')
  note a = this
  hence ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" 
    "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 l')),
            trace := trace g\<^sub>1 @ [Input l' (lhd (env g\<^sub>1 l'))],
            env := (env g\<^sub>1)(l' := ltl (env g\<^sub>1 l'))\<rparr>"
    using eval_in_elim by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>env := (env g\<^sub>1')(l' := ltl (env g\<^sub>1' l')),
                 mem := (mem g\<^sub>1')(x := lhd (env g\<^sub>1' l')),
                 trace := trace g\<^sub>1' @ [Input l' (lhd (env g\<^sub>1' l'))]\<rparr>"
  from a have ev': "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval.eval_in by auto
  show ?case
  proof (cases "l' \<le> l")
    case True
    with a have env_eq[simp]: "env g\<^sub>1 l' = env g\<^sub>1' l'"
      unfolding global_equiv_defs
      by auto
    with ev a True have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_same
      unfolding global_state_equiv_def
      by auto
    moreover from ev a True have "env g\<^sub>2 =\<^sup>e\<^bsub>l\<^esub> env ?g\<^sub>2'"
      unfolding global_equiv_defs
      by simp
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a True ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' True a
      apply (clarsimp simp: sec_step_def ev)
      apply (rule exI[where x = ?g\<^sub>2'])
      by (auto simp: same_input_count_def level_inputs_def same_input_length_append)
  next
    case False
    moreover from False ev a have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_high
      unfolding global_equiv_defs
      by fastforce
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a False ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' a
      apply (clarsimp simp: sec_step_def ev)
      apply (rule exI[where x = ?g\<^sub>2'])
      by (auto simp: same_input_count_def level_inputs_def same_input_length_append)
  qed
next
  case (LIn_type x lev l' A)
  note a = this
  hence ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" 
    "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 l')),
            trace := trace g\<^sub>1 @ [Input l' (lhd (env g\<^sub>1 l'))],
            env := (env g\<^sub>1)(l' := ltl (env g\<^sub>1 l'))\<rparr>"
    using eval_in_elim by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>env := (env g\<^sub>1')(l' := ltl (env g\<^sub>1' l')),
                 mem := (mem g\<^sub>1')(x := lhd (env g\<^sub>1' l')),
                 trace := trace g\<^sub>1' @ [Input l' (lhd (env g\<^sub>1' l'))]\<rparr>"
  from a have ev': "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval.eval_in by auto
  show ?case
  proof (cases "l' \<le> l")
    case True
    with a have env_eq[simp]: "env g\<^sub>1 l' = env g\<^sub>1' l'"
      unfolding global_equiv_defs
      by auto
    with ev a True have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_same
      unfolding global_state_equiv_def
      by auto
    moreover from ev a True have "env g\<^sub>2 =\<^sup>e\<^bsub>l\<^esub> env ?g\<^sub>2'"
      unfolding global_equiv_defs
      by simp
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a True ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' True a
      apply (clarsimp simp: sec_step_def ev)
      apply (rule exI[where x = ?g\<^sub>2'])
      by (auto simp: same_input_count_def level_inputs_def same_input_length_append)
  next
    case False
    hence "\<not> lev \<le> l"
      using LIn_type
      by (meson order_trans)
    moreover from False ev a have "trace g\<^sub>2 =\<^sup>t\<^bsub>l\<^esub> trace ?g\<^sub>2'"
      using trace_equiv_append_high
      unfolding global_equiv_defs
      by fastforce
    ultimately have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
      using a False ev
      unfolding global_equiv_defs
      by simp
    then show ?thesis
      using ev ev' a
      apply (clarsimp simp: sec_step_def)
      apply (rule exI[where x = ?g\<^sub>2'])
      by (auto simp: same_input_count_def level_inputs_def same_input_length_append)
  qed
next
  case (While_type c A e I)
  then have exps_same: "eval_exp e (mem g\<^sub>1) = eval_exp e (mem g\<^sub>1')"
    unfolding ann_implies_low_def active_ann_def by auto
  note a = While_type exps_same
  show ?case (is "\<exists> g'. ?P g'")
  proof (cases "val_true (eval_exp e (mem g\<^sub>1))")
    case False
    with a have ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := Skip\<rparr>" "g\<^sub>2 = g\<^sub>1"
    by auto
    from a ev False have "?P g\<^sub>1'"
      apply (clarsimp simp: sec_step_def)
      using Skip_type eval.eval_whilefalse
      by (metis (no_types, lifting) global_state_equiv_def lang.trace_equiv_def lang.typed_implies_bisim lang_axioms local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
    thus ?thesis by blast
  next
    case True
    with a have ev: "l\<^sub>2 = l\<^sub>1\<lparr>com := c ;; While I e I c\<rparr>" "g\<^sub>2 = g\<^sub>1"
      by auto
    from a True have "val_true (eval_exp e (mem g\<^sub>1'))"
      by auto
    with a ev have "?P g\<^sub>1'"
      using eval.intros
    proof -
      have "com l\<^sub>1 = While A e I c"
        using \<open>com l\<^sub>1 = While A e I c\<close> by blast
      then show ?thesis
        by (smt True \<open>val_true (eval_exp e (mem g\<^sub>1'))\<close> a(1) a(2) a(3) a(5) a(6) a(8) ev(1) eval_whiletrue_elim has_type.While_type has_type_preservation lang.eval.eval_whiletrue lang_axioms not_Cons_self2 sec_step_def self_append_conv typed_implies_bisim)
    qed
    then show ?thesis by blast
  qed
next
  case (Acquire_type A lo)
  then have ev: "g\<^sub>2 = g\<^sub>1\<lparr>lock_state := (lock_state g\<^sub>1)(lo \<mapsto> tid l\<^sub>1)\<rparr>" by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>lock_state := (lock_state g\<^sub>1')(lo := Some (tid l\<^sub>1))\<rparr>"
  note a = Acquire_type
  moreover from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
    unfolding global_equiv_defs by (clarsimp, fastforce)
  moreover
  from a have "lock_state g\<^sub>1' lo = None"
    unfolding global_equiv_defs by auto
  with a have "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval_acquire
    by auto
  ultimately show ?case
    using a
    apply (clarsimp simp: Acquire_type sec_step_def ev)
    apply (rule exI[where x = ?g\<^sub>2'])
    using a
    unfolding same_input_count_def
    by (auto simp: Acquire_type global_equiv_defs same_input_count_def level_inputs_def)
next
  case (Release_type A lo)
  then have ev: "g\<^sub>2 = g\<^sub>1\<lparr>lock_state := (lock_state g\<^sub>1)(lo := None)\<rparr>"  by auto
  let ?g\<^sub>2' = "g\<^sub>1'\<lparr>lock_state := (lock_state g\<^sub>1')(lo := None)\<rparr>"
  note a = Release_type
  moreover from a have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> ?g\<^sub>2'"
    unfolding global_equiv_defs ev
    by clarsimp
  moreover
  from a have "(l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2, ?g\<^sub>2')"
    using eval_release
    using global_state_equiv_def lock_state_equiv_def by auto
  ultimately show ?case
    using a
    apply (clarsimp simp: Acquire_type sec_step_def ev)
    apply (rule exI[where x = ?g\<^sub>2'])
    using a
    unfolding same_input_count_def
    by (auto simp: Acquire_type global_equiv_defs same_input_count_def level_inputs_def)
qed

lemma has_type_no_highbr_progress_global:
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  assumes "list_all (has_type False l \<circ> com) ls\<^sub>1"
      and "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
      and "all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1'"
      and "same_input_count g\<^sub>1 g\<^sub>1'"
      and sat: "\<Turnstile> (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)" "\<Turnstile> (ls\<^sub>1, g\<^sub>1', sched\<^sub>1)"
    shows
        "\<exists> g\<^sub>2'. (ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2', sched\<^sub>2) \<and> 
                same_input_count g\<^sub>2 g\<^sub>2' \<and>
                g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
  using assms
proof (induction rule: global_eval.induct)
  case (global_step ls\<^sub>1 n g\<^sub>1 sched\<^sub>1 l\<^sub>2 g\<^sub>2 ls\<^sub>2)
  let ?l\<^sub>1 = "ls\<^sub>1 ! n"
  from global_step have "has_type False l (com ?l\<^sub>1)"
    by (metis lang.next_thread.simps lang_axioms list_all_length mod_less_divisor o_apply)
  moreover
  from global_step have "g\<^sub>1 \<Turnstile>\<^sub>P active_ann ?l\<^sub>1"
    unfolding anns_satisfied.simps
    by (smt anns_satisfied_now.simps lang.next_thread.simps lang_axioms list_all_length mod_less_divisor rtrancl.intros(1))
  moreover
  from global_step have "g\<^sub>1' \<Turnstile>\<^sub>P active_ann ?l\<^sub>1"
    unfolding anns_satisfied.simps
    by (smt anns_satisfied_now.simps lang.next_thread.simps lang_axioms list_all_length mod_less_divisor rtrancl.intros(1))
  ultimately obtain g\<^sub>2' where "(ls\<^sub>1 ! n, g\<^sub>1') \<leadsto> (l\<^sub>2, g\<^sub>2')"
    "same_input_count g\<^sub>2 g\<^sub>2'"
    "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
    using has_type_no_highbr_progress global_step
    by metis
  then show ?case
    using global_step.hyps(1) global_step.hyps(2) global_step.hyps(4) next_sched_independent_of_memory by fastforce
next
  case (global_wait ls n g sched)
  then show ?case
    by (metis (no_types, lifting) global_eval.global_wait global_state_equiv_def lang.stuck_independent_of_memory lang_axioms lock_state_equiv_def next_sched_independent_of_memory) 
qed

lemma has_type_no_highbr_progress_multi:
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  assumes "list_all (has_type False l \<circ> com) ls\<^sub>1"
      and "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'" "P g\<^sub>1" "P g\<^sub>1'"
      and "all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1'"
      and "same_input_count g\<^sub>1 g\<^sub>1'"
      and sat: "\<And> g. P g \<Longrightarrow> \<Turnstile> (ls\<^sub>1, g, sched\<^sub>1)"
    shows
        "\<exists> g\<^sub>2'. (ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2', sched\<^sub>2) \<and> 
                same_input_count g\<^sub>2 g\<^sub>2' \<and>
                g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
  using assms
proof (induction rule: rtrancl.induct[where r=global_eval, split_format(complete)])
  case (rtrancl_refl a a b)
  then show ?case
    by blast 
next
  case (rtrancl_into_rtrancl ls\<^sub>1 g\<^sub>1 sched\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>3 g\<^sub>3 sched\<^sub>3)
  note a = this
  then obtain g\<^sub>2' where ev\<^sub>1': "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2', sched\<^sub>2)"
    "same_input_count g\<^sub>2 g\<^sub>2'"
    "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'" by blast
  moreover
  from a have "\<Turnstile> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)" "\<Turnstile> (ls\<^sub>2, g\<^sub>2', sched\<^sub>2)"
    using anns_satisfied_multi apply blast
    using ev\<^sub>1' anns_satisfied_multi a by blast
  moreover
  from a have "list_all (has_type False l \<circ> com) ls\<^sub>2"
    using has_type_preservation_global
    by blast
  ultimately obtain g\<^sub>3' where "(ls\<^sub>2, g\<^sub>2', sched\<^sub>2) \<rightarrow> (ls\<^sub>3, g\<^sub>3', sched\<^sub>3)"
    "same_input_count g\<^sub>3 g\<^sub>3'"
    "g\<^sub>3 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>3'"
    by (metis eval_all_inputs_multi has_type_no_highbr_progress_global rtrancl_into_rtrancl.hyps(1) rtrancl_into_rtrancl.hyps(2) rtrancl_into_rtrancl.prems(5))
  then show ?case
    by (meson \<open>(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2', sched\<^sub>2)\<close> rtrancl.rtrancl_into_rtrancl) 
qed

lemma has_type_no_high_branching:
  assumes "list_all (has_type False l \<circ> com) ls\<^sub>1"
      and "\<And> g\<^sub>1 sched\<^sub>1. P g\<^sub>1 \<Longrightarrow> \<Turnstile> (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)"
    shows "no_high_br l P ls\<^sub>1"
  unfolding no_high_br_def
proof (clarify)
  fix sched\<^sub>1 g\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 g\<^sub>1'
  assume A: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
    "all_inputs g\<^sub>1 =\<^sup>\<E>\<^bsub>l\<^esub> all_inputs g\<^sub>1'"
    "P g\<^sub>1" "P g\<^sub>1'"
    "same_input_count g\<^sub>1 g\<^sub>1'"
  then obtain g\<^sub>2' where "(ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2', sched\<^sub>2)"
                "same_input_count g\<^sub>2 g\<^sub>2'"
                "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
    using assms(1) assms(2) has_type_no_highbr_progress_multi by blast
  thus "\<exists>g\<^sub>2'.
          (ls\<^sub>1, g\<^sub>1', sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2', sched\<^sub>2) \<and>
          length (trace g\<^sub>2 \<upharpoonleft>\<^sub>\<le> l) = length (trace g\<^sub>2' \<upharpoonleft>\<^sub>\<le> l) \<and>
          same_input_count g\<^sub>2 g\<^sub>2'"
    using gequiv_impl_trace_len by blast
qed

text {* This means that we get delimited release security whenever
we can type the program without using high branching. *}
lemma has_type_false_dr_sec:
  assumes "list_all (has_type False l \<circ> com) ls\<^sub>1"
      and "\<And> g\<^sub>1 sched\<^sub>1. P g\<^sub>1 \<Longrightarrow> \<Turnstile> (ls\<^sub>1, g\<^sub>1, sched\<^sub>1)"
    shows "dr_secure l P sched\<^sub>1 ls\<^sub>1"
proof -
  have "system_secure l P sched\<^sub>1 ls\<^sub>1"
    using soundness
    using assms(1) assms(2) by auto
  moreover from assms have "no_high_br l P ls\<^sub>1"
    using has_type_no_high_branching
    by auto
  ultimately show "dr_secure l P sched\<^sub>1 ls\<^sub>1"
    using assms embedding_sound[where P=P and l=l and ls=ls\<^sub>1 and sched=sched\<^sub>1]
    by blast
qed

end
end
