theory OGTranslation
  imports Main Language Security TypeSystem Soundness "~~/src/HOL/Hoare_Parallel/OG_Hoare"
begin


type_synonym ('level, 'val) og_state = "('level, 'val) global_state"
type_synonym ('level, 'val) og_com = "('level, 'val) og_state OG_Com.com"
type_synonym ('level, 'val) og_ann_com = "('level, 'val) og_state OG_Com.ann_com"
type_synonym ('level, 'val) lcom = "('level, 'val) Language.com"
type_synonym ('level, 'val) og_ann = "('level, 'val) og_state assn"

context lang
begin

definition com_to_local_state :: "('level, 'val) lcom \<Rightarrow> thread_id \<Rightarrow> ('level, 'val) local_state" where
  "com_to_local_state c t_id = \<lparr>Language.local_state.com = c, tid = t_id\<rparr>"

fun translate_ann :: "('level, 'val) ann \<Rightarrow> ('level, 'val) og_ann" where
  "translate_ann A = {g. \<forall> p \<in> preds A. p g}"

fun translate_exp :: "'val exp \<Rightarrow> ('level, 'val) og_state bexp" where
  "translate_exp e = {g. val_true (eval_exp e (mem g))}"

definition negate_bexp :: "('level, 'val) og_state bexp \<Rightarrow> ('level, 'val) og_state bexp" where
  "negate_bexp e = (UNIV - e)"

definition nop :: "('level, 'val) og_ann_com" where
  "nop = AnnBasic UNIV id"

declare nop_def[simp]

text {* Due to the different way termination is represented in theory OG_Hoare,
we need to use a slightly modified function to extract the next annotation:
For a command of the form @{term "c\<^sub>1 ;; c\<^sub>2"}, we extract the annotation from
the first non-sequential-composition in it that isn't @{term Skip}. This annotation
is either the same as @{term "active_ann_com c\<^sub>1"} or @{term "active_ann_com c\<^sub>1 = ann\<^sub>0"}. *}

fun active_ann_com_aux' :: "('level, 'val) lcom \<Rightarrow> ('level, 'val) ann option" where
  "active_ann_com_aux' Skip = None" |
  "active_ann_com_aux' ({A} x ::= e) = Some A" |
  "active_ann_com_aux' ({A} x :D= e) = Some A" |
  "active_ann_com_aux' ({A} x <- l) = Some A" |
  "active_ann_com_aux' (Out A l e) = Some A" |
  "active_ann_com_aux' (DOut A l e) = Some A" |
  "active_ann_com_aux' (If A e c\<^sub>1 c\<^sub>2) = Some A" |
  "active_ann_com_aux' (Language.While A e I c) = Some A" |
  "active_ann_com_aux' (Acquire A loc) = Some A" |
  "active_ann_com_aux' (Release A loc) = Some A" |
  "active_ann_com_aux' (c\<^sub>1 ;; c\<^sub>2) =
    (case active_ann_com_aux' c\<^sub>1 of
       None \<Rightarrow> active_ann_com_aux' c\<^sub>2
     | Some A \<Rightarrow> Some A)"

fun active_ann_com' :: "('level, 'val) lcom \<Rightarrow> ('level, 'val) ann" where
  "active_ann_com' c = (case active_ann_com_aux' c of
                         Some A \<Rightarrow> A
                       | None \<Rightarrow> ann\<^sub>0)"

definition active_ann' :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) ann" where
  "active_ann' l = active_ann_com' (local_state.com l)"

fun anns_satisfied_now' :: "('level, 'val) global_conf \<Rightarrow> bool" where
  "anns_satisfied_now' (ls, g, sched) = (list_all (\<lambda> l. g \<Turnstile>\<^sub>P active_ann' l) ls)"

fun translate_com :: "thread_id \<Rightarrow> ('level, 'val) lcom \<Rightarrow> ('level, 'val) og_ann_com option" where
  "translate_com t ({A} x ::= e) =
     Some (AnnBasic (translate_ann A) (\<lambda> g. g\<lparr>mem := (mem g)(x := eval_exp e (mem g))\<rparr>))" |
  "translate_com t Skip = None" |
  "translate_com t (Out A l' e) =
     Some (AnnBasic (translate_ann A)
       (\<lambda> g. g\<lparr>trace := trace g @ [Output l' (eval_exp e (mem g)) e]\<rparr>))" |
  "translate_com t (DOut A l' e) =
     Some (AnnBasic (translate_ann A)
       (\<lambda> g. g\<lparr>trace := trace g @ [Declassification l' (eval_exp e (mem g)) e]\<rparr>))" |
  "translate_com t ({A} x <- l') =
     Some (AnnBasic (translate_ann A)
       (\<lambda> g. g\<lparr>env := (env g)(l' := ltl (env g l')),
               mem := (mem g)(x := lhd (env g l')),
               trace := trace g @ [Input l' (lhd (env g l'))]\<rparr>))" |
  "translate_com t ({A} x :D= e) =
     Some (AnnBasic (translate_ann A)
       (\<lambda> g. g\<lparr>mem := (mem g)(x := eval_exp e (mem g)),
               trace := trace g @ [Declassification (the (\<L> x)) (eval_exp e (mem g)) e]\<rparr>))" |
  "translate_com t (If A e c\<^sub>1 c\<^sub>2) = 
     (case (translate_com t c\<^sub>1, translate_com t c\<^sub>2) of
        (Some c\<^sub>1', Some c\<^sub>2') \<Rightarrow>
          Some (AnnCond1 (translate_ann A) (translate_exp e) c\<^sub>1' c\<^sub>2')
      | (Some c\<^sub>1', None) \<Rightarrow> Some (AnnCond2 (translate_ann A) (translate_exp e) c\<^sub>1')
      | (None, Some c\<^sub>2') \<Rightarrow> Some (AnnCond2 (translate_ann A) (negate_bexp (translate_exp e)) c\<^sub>2')
      | (None, None) \<Rightarrow> Some (AnnBasic (translate_ann A) id))" |
  "translate_com t (Language.While A e I c) =
     (case translate_com t c of
        Some c' \<Rightarrow> Some (AnnWhile (translate_ann A) (translate_exp e) (translate_ann I) c')
      | None \<Rightarrow> Some (AnnWhile (translate_ann A) (translate_exp e) (translate_ann I) nop))" |
  "translate_com t (c\<^sub>1 ;; c\<^sub>2) =
      (case (translate_com t c\<^sub>1, translate_com t c\<^sub>2) of
         (Some c\<^sub>1', Some c\<^sub>2') \<Rightarrow> Some (AnnSeq c\<^sub>1' c\<^sub>2')
       | (Some c\<^sub>1', None) \<Rightarrow> Some c\<^sub>1'
       | (None, Some c\<^sub>2') \<Rightarrow> Some c\<^sub>2'
       | (None, None) \<Rightarrow> None)" |
  "translate_com t (Acquire A loc) =
      Some (AnnAwait (translate_ann A) {g. lock_state g loc = None}
                     (Basic (\<lambda> g. g\<lparr>lock_state := (lock_state g)(loc := Some t)\<rparr>)))" |
  "translate_com t (Release A loc) =
      Some (AnnAwait (translate_ann A) {g. lock_state g loc = Some t}
                     (Basic (\<lambda> g. g\<lparr>lock_state := (lock_state g)(loc := None)\<rparr>)))"

fun translate_local_state :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) og_ann_com option" where
  "translate_local_state l = translate_com (tid l) (local_state.com l)"

fun translate_coms_list :: "('level, 'val) local_state list \<Rightarrow> (('level, 'val) og_ann_com option * ('level, 'val) og_ann) list" where
  "translate_coms_list ls = map (\<lambda> l. (translate_local_state l, UNIV)) ls"

definition translate_coms :: "('level, 'val) local_state list \<Rightarrow> ('level, 'val) og_com" where
  [simp]: "translate_coms ls = Parallel (translate_coms_list ls)"


inductive_cases ann_transition_step_elim: "(c, g) -1\<rightarrow> (c', g')"

lemma og_eval_none: "(c, g) -*\<rightarrow> (c', g') \<Longrightarrow> c = None  \<Longrightarrow> g = g' \<and> c' = None"
  apply (induction rule: rtrancl.induct[where r = ann_transition, split_format(complete)])
   apply clarsimp
  by (metis ann_transition_cases(1))

lemma og_eval_some:
  assumes "(c, g) -1\<rightarrow> (c', g')"
  shows "\<exists> cg. c = Some cg"
  using assms
  by (rule ann_transition_step_elim, auto)

lemma og_eval_seq_multi:
  assumes "(cg, g) -*\<rightarrow> (cg', g')"
  and "cg = Some c" "cg' = Some c'"
  shows "(Some (AnnSeq c c\<^sub>2), g) -*\<rightarrow> (Some (AnnSeq c' c\<^sub>2), g')"
  using assms
proof (induction arbitrary: c c' rule: rtrancl.induct[where r = ann_transition, split_format(complete)])
  case (rtrancl_refl c g)
  then show ?case
    by auto
next
  case (rtrancl_into_rtrancl cg g cg' g' cg'' g'')
  then obtain c\<^sub>I where "cg' = Some c\<^sub>I"
    using og_eval_some 
    by fastforce
  with rtrancl_into_rtrancl show ?case
    apply simp
    by (meson ann_transition_transition.AnnSeq2 rtrancl.simps)
qed

lemma og_eval_skipseq_multi_aux:
  assumes ev: "(cg, g) -*\<rightarrow> (cg', g')"
      and some: "cg = Some c"
    shows "(cg' = None \<and> (Some (AnnSeq c c\<^sub>2), g) -*\<rightarrow> (Some c\<^sub>2, g')) \<or>
           (\<exists> c'. cg' = Some c' \<and> (Some (AnnSeq c c\<^sub>2), g) -*\<rightarrow> (Some (AnnSeq c' c\<^sub>2), g'))"
  using assms
proof (induction rule: rtrancl.induct[where r = ann_transition, split_format(complete)])
  case (rtrancl_refl a b)
  let "?P \<or> (\<exists> c'. ?Q c')" = ?case
  from rtrancl_refl have "?Q c"
    by blast
  thus ?case by blast
next
  case (rtrancl_into_rtrancl cg\<^sub>1 g\<^sub>1 cg\<^sub>2 g\<^sub>2 cg\<^sub>3 g\<^sub>3)
  from rtrancl_into_rtrancl have "cg\<^sub>2 = None \<and>
    (Some (AnnSeq c c\<^sub>2), g\<^sub>1) -*\<rightarrow> (Some c\<^sub>2, g\<^sub>2) \<or>
    (\<exists>c'. cg\<^sub>2 = Some c' \<and>
          (Some (AnnSeq c c\<^sub>2),
           g\<^sub>1) -*\<rightarrow> (Some (AnnSeq c' c\<^sub>2), g\<^sub>2))"
    by auto
  then show ?case
  proof
    assume "cg\<^sub>2 = None \<and> (Some (AnnSeq c c\<^sub>2), g\<^sub>1) -*\<rightarrow> (Some c\<^sub>2, g\<^sub>2)"
    with rtrancl_into_rtrancl have False
      by (meson ann_transition_cases(1))
    thus ?case
      by auto
  next
    assume "\<exists>c'. cg\<^sub>2 = Some c' \<and>
         (Some (AnnSeq c c\<^sub>2),
          g\<^sub>1) -*\<rightarrow> (Some (AnnSeq c' c\<^sub>2), g\<^sub>2)"
    with rtrancl_into_rtrancl show ?case
      apply (cases "cg\<^sub>3")
       apply simp_all
      apply (meson ann_transition_transition.AnnSeq1 rtrancl.simps)
      by (meson og_eval_seq_multi rtrancl.rtrancl_into_rtrancl)
  qed
qed

lemma og_eval_skipseq_multi:
  assumes ev: "(Some c, g) -*\<rightarrow> (None, g')"
  shows "(Some (AnnSeq c c'), g) -*\<rightarrow> (Some c', g')"
  using og_eval_skipseq_multi_aux ev
  by (metis option.simps(3))

lemma translate_preserves_sem_multi:
  assumes "(l, g) \<leadsto> (l', g')"
  shows "(translate_local_state l, g) -*\<rightarrow> 
         (translate_local_state l', g')"
  using assms
proof (induction rule: eval.induct)
  case (eval_assign l A x e g)
  then show ?case by auto
next
  case (eval_dassign l A x e g)
  then show ?case by auto
next
  case (eval_out l A lev e g)
  then show ?case by auto
next
  case (eval_dout l A lev e g)
  then show ?case by auto
next
  case (eval_in l A x lev v g)
  then show ?case by auto
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then show ?case
   apply (cases "translate_com (tid l) c\<^sub>1" ; cases "translate_com (tid l) c\<^sub>2")
       apply simp_all
       apply (metis (no_types, lifting) ann_transition_transition.AnnBasic id_apply r_into_rtrancl)
    by (auto simp: negate_bexp_def)
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then show ?case 
    apply (cases "translate_com (tid l) c\<^sub>1" ; cases "translate_com (tid l) c\<^sub>2")
       apply simp_all
       apply (metis (no_types, lifting) ann_transition_transition.AnnBasic id_apply r_into_rtrancl)
    by (auto simp: negate_bexp_def ann_transition_transition.AnnCond1F ann_transition_transition.AnnCond2F r_into_rtrancl ann_transition_transition.AnnCond2T)
next
  case (eval_seq l c\<^sub>1 c\<^sub>2 g l' g')
  hence "tid l' = tid l"
    using tid_constant
    by auto
  with eval_seq show ?case 
    apply (cases "translate_com (tid l) c\<^sub>1" ; cases "translate_com (tid l) c\<^sub>2" ;
        cases "translate_com (tid l') (Language.com l')")
       apply simp_all
    apply (metis ann_transition_cases(1) converse_rtranclE2 old.prod.inject rtrancl.rtrancl_refl)
    apply (meson Pair_inject ann_transition_cases(1) converse_rtranclE2 option.distinct(1))
    apply (simp add: og_eval_skipseq_multi)
    by (simp add: og_eval_seq_multi)
next
  case (eval_seqskip l c\<^sub>1 c\<^sub>2 g l' g')
  hence "tid l' = tid l"
    using tid_constant
    by auto
  from eval_seqskip have "(translate_local_state (l\<lparr>com := c\<^sub>1\<rparr>), g) -*\<rightarrow> (None, g')"
    by auto
  
  with eval_seqskip show ?case
    apply simp
proof -
  assume a1: "(translate_com (tid l) c\<^sub>1, g) -*\<rightarrow> (None, g')"
  have f2: "\<forall>z f za. if za = None then (case za of None \<Rightarrow> z::('level, 'val) global_state ann_com option | Some (x::('level, 'val) global_state ann_com) \<Rightarrow> f x) = z else (case za of None \<Rightarrow> z | Some x \<Rightarrow> f x) = f (the za)"
by force
  have f3: "\<forall>a g ga aa. ((Some a, g::('level, 'val) global_state), None, ga) \<notin> ann_transition\<^sup>* \<or> (Some (AnnSeq a aa), g) -*\<rightarrow> (Some aa, ga)"
    by (meson og_eval_skipseq_multi)
  have f4: "\<forall>z. z = None \<or> Some (the z::('level, 'val) global_state ann_com) = z"
    using option.collapse by blast
  have f5: "translate_com (tid l) c\<^sub>1 \<noteq> None \<or> g = g'"
    using a1 by (metis og_eval_none)
  { assume "((translate_com (tid l) c\<^sub>1, g), None, g') \<noteq> ((case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g), translate_com (tid l') c\<^sub>2, g')"
    moreover
    { assume "translate_com (tid l) c\<^sub>1 \<noteq> (case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa))"
      moreover
      { assume "(case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa)) \<noteq> (case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some (the (translate_com (tid l) c\<^sub>1)) | Some a \<Rightarrow> Some (AnnSeq (the (translate_com (tid l) c\<^sub>1)) a))"
        then have "translate_com (tid l) c\<^sub>1 = None"
          using f2 by meson }
          ultimately have "Some (the (translate_com (tid l) c\<^sub>1)) = translate_com (tid l) c\<^sub>1 \<and> translate_com (tid l) c\<^sub>2 = None \<longrightarrow> translate_com (tid l) c\<^sub>1 = None"
            by fastforce }
        ultimately have "Some (the (translate_com (tid l) c\<^sub>1)) = translate_com (tid l) c\<^sub>1 \<and> translate_com (tid l) c\<^sub>2 = None \<longrightarrow> translate_com (tid l) c\<^sub>1 = None"
          using \<open>tid l' = tid l\<close> by fastforce
        moreover
        { assume "translate_com (tid l) c\<^sub>2 \<noteq> None"
          moreover
          { assume "translate_com (tid l) c\<^sub>2 \<noteq> None \<and> (Some (AnnSeq (the (translate_com (tid l) c\<^sub>1)) (the (translate_com (tid l) c\<^sub>2))), g) -*\<rightarrow> (Some (the (translate_com (tid l) c\<^sub>2)), g')"
            moreover
            { assume "translate_com (tid l) c\<^sub>2 \<noteq> None \<and> ((Some (AnnSeq (the (translate_com (tid l) c\<^sub>1)) (the (translate_com (tid l) c\<^sub>2))), g), Some (the (translate_com (tid l) c\<^sub>2)), g') \<noteq> ((case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g), translate_com (tid l') c\<^sub>2, g')"
              then have "translate_com (tid l) c\<^sub>2 \<noteq> None \<and> Some (AnnSeq (the (translate_com (tid l) c\<^sub>1)) (the (translate_com (tid l) c\<^sub>2))) \<noteq> (case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa))"
                using \<open>tid l' = tid l\<close> by force
              moreover
              { assume "(case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa)) \<noteq> (case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some (the (translate_com (tid l) c\<^sub>1)) | Some a \<Rightarrow> Some (AnnSeq (the (translate_com (tid l) c\<^sub>1)) a))"
                then have "translate_com (tid l) c\<^sub>1 = None"
                  using f2 by meson }
              ultimately have "Some (the (translate_com (tid l) c\<^sub>1)) = translate_com (tid l) c\<^sub>1 \<longrightarrow> translate_com (tid l) c\<^sub>1 = None"
                by fastforce }
            ultimately have "Some (the (translate_com (tid l) c\<^sub>1)) = translate_com (tid l) c\<^sub>1 \<longrightarrow> (case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g) -*\<rightarrow> (translate_com (tid l') c\<^sub>2, g') \<or> translate_com (tid l) c\<^sub>1 = None"
              by force }
          ultimately have "Some (the (translate_com (tid l) c\<^sub>1)) = translate_com (tid l) c\<^sub>1 \<longrightarrow> (case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g) -*\<rightarrow> (translate_com (tid l') c\<^sub>2, g') \<or> translate_com (tid l) c\<^sub>1 = None"
            using f3 a1 by presburger }
        moreover
        { assume "translate_com (tid l) c\<^sub>1 = None"
      moreover
          { assume "translate_com (tid l) c\<^sub>1 = None \<and> ((case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g), translate_com (tid l') c\<^sub>2, g') \<notin> ann_transition\<^sup>*"
            then have "translate_com (tid l) c\<^sub>1 = None \<and> ((case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g), translate_com (tid l') c\<^sub>2, g') \<noteq> ((translate_com (tid l) c\<^sub>1, g), None, g')"
              using a1 by force
            then have "translate_com (tid l) c\<^sub>1 = None \<and> translate_com (tid l) c\<^sub>2 \<noteq> None"
              using \<open>tid l' = tid l\<close> by fastforce
            then have "(case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g) -*\<rightarrow> (translate_com (tid l') c\<^sub>2, g')"
              using f5 f2 by (simp add: \<open>tid l' = tid l\<close>) }
          ultimately have "(case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g) -*\<rightarrow> (translate_com (tid l') c\<^sub>2, g')"
            by fastforce }
        ultimately have "(case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g) -*\<rightarrow> (translate_com (tid l') c\<^sub>2, g')"
          using f4 by meson }
      then show "(case translate_com (tid l) c\<^sub>1 of None \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> None | Some x \<Rightarrow> Some x | Some a \<Rightarrow> case translate_com (tid l) c\<^sub>2 of None \<Rightarrow> Some a | Some aa \<Rightarrow> Some (AnnSeq a aa), g) -*\<rightarrow> (translate_com (tid l') c\<^sub>2, g')"
        using a1 by fastforce
    qed
next
  case (eval_whiletrue l A e I c g)
  then show ?case
  proof (cases "translate_com (tid l) c")
    let ?I = "translate_ann I"
    let ?e = "translate_exp e"
    case None
    with eval_whiletrue have "(translate_com (tid l) (Language.While A e I c), g) -*\<rightarrow> 
          (Some (AnnSeq nop (AnnWhile ?I ?e ?I nop)), g)"
      by auto
    moreover have "(Some (AnnSeq nop (AnnWhile ?I ?e ?I nop)), g) -1\<rightarrow>
     (Some (AnnWhile ?I ?e ?I nop), g)"
      by (metis ann_transition_transition.AnnBasic ann_transition_transition.AnnSeq1 id_apply nop_def)
    ultimately show ?thesis
      using None eval_whiletrue.hyps(1) by auto
  next
    case (Some a)
    then show ?thesis 
      apply simp
      using eval_whiletrue.hyps(1) eval_whiletrue.hyps(2) by auto
  qed
next
  case (eval_whilefalse l A e I c g)
  then show ?case 
    apply (cases "translate_com (tid l) c")
    by (auto simp: ann_transition_transition.AnnWhileF r_into_rtrancl)
next
  case (eval_acquire l A loc g)
  define g' where [simp]: "g' = g\<lparr>lock_state := (lock_state g)(loc := Some (tid l))\<rparr>"
  define loc_upd where [simp]: 
    "loc_upd = (\<lambda>g. (g :: ('level, 'val) global_state)\<lparr>lock_state := lock_state g(loc \<mapsto> tid l)\<rparr>)"
  from eval_acquire have "(Basic loc_upd, g) -P*\<rightarrow> (Parallel [], g')"
    by auto
  moreover from eval_acquire have "g \<in> {g. lock_state g loc = None}" by auto 
  ultimately have
    "(Some (AnnAwait (translate_ann A) {g. lock_state g loc = None} (Basic loc_upd)), g) -1\<rightarrow>
     (None, g')"
    using eval_acquire by auto
  then show ?case
    using eval_acquire
    by simp
next
  case (eval_release l A loc g)
  define g' where [simp]: "g' = g\<lparr>lock_state := (lock_state g)(loc := None)\<rparr>"
  define loc_upd where [simp]: 
    "loc_upd = (\<lambda>g. (g :: ('level, 'val) global_state)\<lparr>lock_state := (lock_state g)(loc := None)\<rparr>)"
  from eval_release have "(Basic loc_upd, g) -P*\<rightarrow> (Parallel [], g')"
    by auto
  moreover from eval_release have "g \<in> {g. lock_state g loc = Some (tid l)}" by auto
  ultimately have
    "(Some (AnnAwait (translate_ann A) {g. lock_state g loc = Some (tid l)} (Basic loc_upd)), g)
     -1\<rightarrow>
     (None, g')" by auto
  then show ?case
    using eval_release by auto
qed

lemma ann_steps_to_par_steps:
  assumes "(cg, g) -*\<rightarrow> (cg', g')"
  assumes "cs ! i = (cg, q)" "i < length cs"
  shows "(Parallel cs, g) -P*\<rightarrow> (Parallel (cs[i := (cg', q)]), g')"
  using assms
proof (induction arbitrary: cs rule: rtrancl.induct[where r = ann_transition, split_format(complete)])
  case (rtrancl_refl a b)
  then show ?case
    by (metis list_update_id rtrancl.rtrancl_refl)
next
  case (rtrancl_into_rtrancl cg g cg' g' cg'' g'')
  define cs' where "cs' = cs[i := (cg', q)]"
  with rtrancl_into_rtrancl have
  "(Parallel cs, g) -P*\<rightarrow> (Parallel cs', g')"
    by auto
  moreover
  from rtrancl_into_rtrancl(2) obtain scg' where "cg' = Some scg'"
    apply (rule ann_transition_step_elim)
    by fastforce+
  with rtrancl_into_rtrancl have
    "(Parallel cs', g') -P1\<rightarrow> (Parallel (cs'[i := (cg'', q)]), g'')"
    using ann_transition_transition.Parallel[where i = i and Ts=cs']
    by (simp add: cs'_def)
  ultimately show ?case
    using cs'_def by auto
qed

lemma translate_perserves_sem_step:
  assumes "(ls, g, sched) \<rightarrow> (ls', g', sched')"
  shows "(translate_coms ls, g) -P*\<rightarrow> (translate_coms ls', g')"
  using assms
proof (induction rule: global_eval.induct)
  case (global_step ls n g sched l' g' ls')
  define c where "c = local_state.com (ls ! n)"
  define c' where "c' = local_state.com l'"
  define cg where "cg = translate_local_state (ls ! n)"
  define cg' where "cg' = translate_local_state l'"
  note c_defs = cg_def cg'_def
  have "(cg, g) -*\<rightarrow> (cg', g')"
    using translate_preserves_sem_multi
    by (simp add: c_defs global_step.hyps(3))
  moreover with global_step have "n < length ls"
    by (simp add: next_thread.simps)
  ultimately show ?case
    using global_step
    using ann_steps_to_par_steps
    apply simp
    by (simp add: cg'_def cg_def ann_steps_to_par_steps map_update)
next
  case (global_wait ls n g sched)
  then show ?case
    by blast
qed

lemma translate_preserves_sem:
  assumes "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
  shows "(translate_coms ls, g) -P*\<rightarrow> (translate_coms ls', g')"
  using assms
proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  case (rtrancl_refl ls g sched)
  then show ?case 
    by auto
next
  case (rtrancl_into_rtrancl ls g sched ls' g' sched' ls'' g'' sched'')
  then show ?case
    by (meson translate_perserves_sem_step rtrancl_trans)
qed

fun translate_initial_anns :: "('level, 'val) local_state list \<Rightarrow> ('level, 'val) og_ann" where
  "translate_initial_anns ls = {g. \<forall> sched. anns_satisfied_now' (ls, g, sched)}"

lemma no_ann_translate_none:
  "active_ann_com_aux' c = None \<Longrightarrow> translate_com l c = None"
  apply (induction c)
         apply simp_all
  by (metis (no_types, lifting) option.case_eq_if option.distinct(1))

lemma translate_none_no_ann:
  "translate_com l c = None \<Longrightarrow> active_ann_com_aux' c = None"
  apply (induction c, clarsimp, simp_all)
  apply (metis (no_types, lifting) option.case_eq_if option.distinct(1))
  apply (metis option.case_eq_if option.distinct(1))
  by (metis (no_types, lifting) option.case_eq_if option.distinct(1))

lemma pre_same_as_ann:
  shows "translate_com l c = None \<or> (translate_ann (active_ann_com' c) = pre (the (translate_com l c)))"
proof (induction c)
  case Skip
  then show ?case by force
next
  case (Assign x1 x2 x3)
  then show ?case by force
next
  case (DAssign x1 x2 x3)
  then show ?case by force
next
  case (Out x1 x2 x3)
  then show ?case by force
next
  case (DOut x1 x2 x3)
  then show ?case by force
next
  case (In x1 x2 x3)
  then show ?case by force
next
  case (If A e c\<^sub>1 c\<^sub>2)
  then show ?case
    apply (cases "translate_com l c\<^sub>1" ; cases "translate_com l c\<^sub>2")
    by auto
next
  case (While x1 x2 x3 c)
  then show ?case
    apply (cases "translate_com l c")
    by auto
next
  case (Seq c\<^sub>1 c\<^sub>2)
  then show ?case 
    apply simp
    apply (cases "translate_com l c\<^sub>1" ; cases "translate_com l c\<^sub>2" ; cases "active_ann_com_aux' c\<^sub>1")
    apply simp_all
    apply (simp add: translate_none_no_ann)
    apply (simp add: no_ann_translate_none)
    by (simp add: no_ann_translate_none)
next
  case (Acquire x1 x2)
  then show ?case 
    by simp
next
  case (Release x1 x2)
  then show ?case by simp
qed

lemma parallel_hoare_elim:
  assumes h: "\<parallel>- p c q"
  assumes "c = Parallel ps"
  shows "interfree ps \<and> (\<forall>i<length ps. \<exists>c q. ps!i = (Some c, q) \<and> \<turnstile> c q)"
  using assms
  by (induction, auto)

lemma active_ann'_None:
  "active_ann_com_aux' c = None \<Longrightarrow> active_ann_com c = ann\<^sub>0"
  apply (induction c)
  by fastforce+

lemma active_ann'_ann_same_or_ann\<^sub>0:
  "active_ann_com' c = active_ann_com c \<or> active_ann_com c = ann\<^sub>0"
  apply (induction c, auto)
  apply (metis option.case_eq_if option.simps(5))
  by (metis option.case_eq_if option.simps(5))


lemma active_ann'_implies_active_ann:
  assumes "g \<Turnstile>\<^sub>P active_ann' l"
  notes ann\<^sub>0_def[simp] 
  shows "g \<Turnstile>\<^sub>P active_ann l"
  using assms unfolding active_ann_def active_ann'_def
  using active_ann'_ann_same_or_ann\<^sub>0
  unfolding ann\<^sub>0_def satisfies_ann_preds_def
  apply simp
  by (metis ann.select_convs(1) empty_iff)

lemma og_hoare_par_soundness:
  assumes val: "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
  assumes initially_sat: "anns_satisfied_now' (ls, g, sched)"
  shows "anns_satisfied (ls, g, sched)"
  unfolding anns_satisfied.simps
proof (clarify)
  fix ls' g' sched'
  assume ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
  then have "(translate_coms ls, g) -P*\<rightarrow> (translate_coms ls', g')"
    using translate_preserves_sem by auto
  moreover
  obtain ps where ps_def: "translate_coms ls = Parallel ps"
    by auto
  moreover
  with val have ps_sat: "\<parallel>- (translate_initial_anns ls) (Parallel ps) UNIV" by auto
  hence "interfree ps"
    using parallel_hoare_elim by auto
  moreover
  obtain ps' where ps'_eq: "translate_coms ls' = Parallel ps'" by auto
  have "\<forall>i<length ps.
       \<exists>c q.
          ps ! i = (Some c, q) \<and> g \<in> pre c \<and> \<turnstile> c q"
  proof (clarify)
    fix i
    assume len: "i < length ps"
    with ps_def obtain cg where cg_eq: "ps ! i = (Some cg, UNIV) \<and> \<turnstile> cg UNIV"
      using parallel_hoare_elim
      apply clarsimp
      by (metis (mono_tags, lifting) ps_sat Pair_inject parallel_hoare_elim length_map nth_map)
    moreover
    have "Some cg = translate_local_state (ls ! i)"
      using ps_def cg_eq len
      by auto
    moreover have "pre cg = translate_ann (active_ann_com' (local_state.com (ls ! i)))"
      by (metis calculation(2) pre_same_as_ann option.sel option.simps(3) translate_local_state.simps)
    moreover with len initially_sat have "g \<in> pre cg"
      using pre_same_as_ann
      apply simp
      by (metis (no_types, lifting) OG_Com.com.inject(1) active_ann'_def active_ann_com'.elims length_map list_all_length ps_def satisfies_ann_preds_def translate_coms_def translate_coms_list.simps)
    ultimately show "\<exists>c q. ps ! i = (Some c, q) \<and> g \<in> pre c \<and> \<turnstile> c q"
      by blast
  qed
  ultimately have sound:
    "\<And> j. j < length ps' \<Longrightarrow> 
        if OG_Hoare.com (ps' ! j) = None
        then g' \<in> OG_Hoare.post (ps ! j)
        else g' \<in> pre (the (OG_Hoare.com (ps' ! j)))"
    using Parallel_Strong_Soundness
    by (metis  \<open>translate_coms ls' = Parallel ps'\<close> ps_def)
  have "\<And> j. j < length ls' \<Longrightarrow> satisfies_ann_preds g' (active_ann_com' (local_state.com (ls' ! j)))"
  proof -
    fix j
    assume len: "j < length ls'"
    let ?l\<^sub>j = "ls' ! j"
    show "satisfies_ann_preds g' (active_ann_com' (local_state.com ?l\<^sub>j))"
    proof (cases "translate_local_state ?l\<^sub>j")
      case None
      hence "active_ann' ?l\<^sub>j = ann\<^sub>0"
        by (simp add: active_ann'_def translate_none_no_ann)
      then show ?thesis
        unfolding ann\<^sub>0_def satisfies_ann_preds_def active_ann'_def by auto
    next
      case (Some cg)
      with len have "OG_Hoare.com (ps' ! j) = Some cg"
        using ps'_eq
        by auto
      from sound have "g' \<in> pre cg"
        using \<open>OG_Hoare.com (ps' ! j) = Some cg\<close> len ps'_eq by fastforce
      then show ?thesis 
        unfolding satisfies_ann_preds_def active_ann_def
        apply simp
        using Some pre_same_as_ann
        by (metis (mono_tags, lifting) active_ann_com'.elims mem_Collect_eq option.distinct(1) option.sel translate_ann.elims translate_local_state.simps) 
    qed
  qed
  hence "anns_satisfied_now' (ls', g', sched')"
    apply clarsimp
    unfolding satisfies_ann_preds_def active_ann'_def
    apply clarsimp
    using list_all_length
    by auto
  thus "anns_satisfied_now (ls', g', sched')"
    apply clarsimp
    using active_ann'_implies_active_ann
    by (auto simp: list_all_length)
qed

end
end