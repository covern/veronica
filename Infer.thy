theory Infer
  imports Main Language RG_Verify ExampleUtils 
begin

text {* We maintain a list of symbolic representations of things that happened to
the state to keep track of which changes are still relevant and which ones
may have been overridden. *}
datatype ('level, 'val) state_change =
  MemUpdate var "'val exp"
  | Read var 'level
  | Outp 'level "'val exp"
  | Decl var "'val exp"
  | GetLock lock
  | ReleaseLock lock
  | PathCond "'val exp"

datatype ('level, 'val) sym_exp =
  SVar var
  | SVal 'val
  | SBinOp "'val \<Rightarrow> 'val \<Rightarrow> 'val" "('level, 'val) sym_exp" "('level, 'val) sym_exp"
  | LastInput 'level nat

fun list_any :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> bool" where
  "list_any P [] = False" |
  "list_any P (x # xs) = (P x \<or> list_any P xs)"

type_synonym ('level, 'val) sym_mem = "var \<Rightarrow> ('level, 'val) sym_exp"

context lang
begin

text {* True iff the list contains at least one memupdate or Read operation
  with the given variable as its target. *}
fun changes_var :: "('level, 'val) state_change list \<Rightarrow> var \<Rightarrow> bool" where
  "changes_var xs v = list_any (\<lambda> upd. (\<exists> e. upd = MemUpdate v e) \<or> (\<exists> l'. upd = Read v l')) xs"

definition count_reads :: "('level, 'val) state_change list \<Rightarrow> 'level \<Rightarrow> nat" where
  [simp]: "count_reads upds l = length (filter (\<lambda> upd. (\<exists> x. upd = Read x l)) upds)"

definition count_outputs :: "('level, 'val) state_change list \<Rightarrow> 'level \<Rightarrow> nat" where
  [simp]: "count_outputs upds l = length (filter (\<lambda> upd. (\<exists> e. upd = Outp l e)) upds)"

definition count_decls :: "('level, 'val) state_change list \<Rightarrow> 'level \<Rightarrow> nat" where
  [simp]: "count_decls upds l = length (filter (\<lambda> upd. (\<exists> e x. upd = Decl x e \<and> \<L> x = Some l)) upds)"


fun eval_sym_exp :: "'val exp \<Rightarrow> ('level, 'val) sym_mem \<Rightarrow> ('level, 'val) sym_exp" where
  "eval_sym_exp (Var x) m = m x" |
  "eval_sym_exp (Val v) m = SVal v" |
  "eval_sym_exp (BinOp f e\<^sub>1 e\<^sub>2) m = SBinOp f (eval_sym_exp e\<^sub>1 m) (eval_sym_exp e\<^sub>2 m)"

fun shift_exp_inputs :: "'level \<Rightarrow> ('level, 'val) sym_exp \<Rightarrow> ('level, 'val) sym_exp" where
  "shift_exp_inputs l (SVar x) = SVar x" |
  "shift_exp_inputs l (SVal v) = SVal v" |
  "shift_exp_inputs l (LastInput l' n) = (if l = l' then LastInput l (Suc n) else LastInput l' n)" |
  "shift_exp_inputs l (SBinOp f e\<^sub>1 e\<^sub>2) = SBinOp f (shift_exp_inputs l e\<^sub>1) (shift_exp_inputs l e\<^sub>2)"

fun shift_inputs :: "'level \<Rightarrow> ('level, 'val) sym_mem \<Rightarrow> ('level, 'val) sym_mem" where
  "shift_inputs l' m x = shift_exp_inputs l' (m x)"

fun update_sym_mem :: "('level, 'val) state_change \<Rightarrow> ('level, 'val) sym_mem \<Rightarrow> ('level, 'val) sym_mem" where
  "update_sym_mem (MemUpdate x e) m y =
    (if x = y then eval_sym_exp e m else SVar y)" |
  "update_sym_mem (Decl x e) m y = 
    (if x = y then eval_sym_exp e m else SVar y)" |
  "update_sym_mem (Read x l') m y =
    (if x = y then LastInput l' 0 else shift_inputs l' m y)" |
  "update_sym_mem (Outp l' e) m y = m y" |
  "update_sym_mem (GetLock loc) m y = m y" |
  "update_sym_mem (ReleaseLock loc) m y = m y" |
  "update_sym_mem (PathCond P) m y = m y"

definition sym_mem\<^sub>0 :: "('level, 'val) sym_mem" where
  "sym_mem\<^sub>0 x = SVar x"

fun sym_mem :: "('level, 'val) state_change list \<Rightarrow> ('level, 'val) sym_mem" where
  "sym_mem upds = foldl (\<lambda> m upd. update_sym_mem upd m) sym_mem\<^sub>0 upds"

fun sym_exp_to_val :: "('level, 'val) sym_exp \<Rightarrow> ('level, 'val) global_state \<Rightarrow> 'val" where
  "sym_exp_to_val (SVar x) g = mem g x" |
  "sym_exp_to_val (SVal v) g = v" |
  "sym_exp_to_val (SBinOp f e\<^sub>1 e\<^sub>2) g = f (sym_exp_to_val e\<^sub>1 g) (sym_exp_to_val e\<^sub>2 g)" |
  "sym_exp_to_val (LastInput l n) g = rev (level_inputs l g) ! n"

text {*
- A memory update can only be retained if the variables in the assigned expressions
are not updated later on:
*}
fun changes_to_preds' :: "('level, 'val) local_state \<Rightarrow> 
    ('level, 'val) state_change list \<Rightarrow>  ('level, 'val) state_change list \<Rightarrow> ('level, 'val) pred set" where
  "changes_to_preds' l before (MemUpdate x e # after) = 
    (if changes_var after x 
     then {} 
      else {\<lambda> g. mem g x = sym_exp_to_val (eval_sym_exp e (sym_mem before)) g}) \<union>
    changes_to_preds' l (before @ [MemUpdate x e]) after" |
  "changes_to_preds' l before (Read x l' # after) =
    {\<lambda> g. length (level_inputs l' g) > count_reads after l'} \<union>
    (if changes_var after x then {} else 
      {\<lambda> g. mem g x = rev (level_inputs l' g) ! count_reads after l'}) \<union>
    changes_to_preds' l (before @ [Read x l']) after" |
  "changes_to_preds' l before (Outp l' e # after) = 
    {\<lambda> g. length (level_outputs l' g) > count_outputs after l'} \<union>
    (if (\<exists> x \<in> exp_vars e. changes_var after x)
     then {}
     else {\<lambda> g. rev (level_outputs l' g) ! count_outputs after l' = 
                (sym_exp_to_val (eval_sym_exp e (sym_mem before)) g, e)})
    \<union> changes_to_preds' l (before @ [Outp l' e]) after" | (* TODO: better heuristics if variables have been modified later *)
  "changes_to_preds' l before (GetLock loc # after) = 
    (if list_any (\<lambda> upd. upd = ReleaseLock loc) after
     then {}
     else {\<lambda> g. holds_lock (tid l) loc g}) \<union> 
    changes_to_preds' l (before @ [GetLock loc]) after" |
  "changes_to_preds' l before (ReleaseLock loc # after) =
    (if list_any (\<lambda> upd. upd = GetLock loc) after
     then {}
     else {\<lambda> g. lock_state g loc \<noteq> Some (tid l)}) \<union> 
    changes_to_preds' l (before @ [ReleaseLock loc]) after" |
  "changes_to_preds' l before (Decl x e # after) =
    {\<lambda> g. length (level_decls (the (\<L> x)) g) > count_decls after (the (\<L> x))} \<union>
    (if changes_var after x 
     then {}
     else {\<lambda> g. mem g x = sym_exp_to_val (eval_sym_exp e (sym_mem before)) g,
           \<lambda> g. rev (level_decls (the (\<L> x)) g) ! count_decls after (the (\<L> x)) = (eval_exp e (mem g), e)}) \<union> 
    changes_to_preds' l (before @ [Decl x e]) after" | 
  "changes_to_preds' l before [] = {}" |
  "changes_to_preds' l before (PathCond e # after) =
    {\<lambda> g. val_true (eval_exp e (mem g))} \<union>
    changes_to_preds' l (before @ [PathCond e]) after" 
  (* ^ TODO: don't ignore path condition *)

definition changes_to_preds :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) state_change list \<Rightarrow> ('level, 'val) pred set" where
  "changes_to_preds l upds = changes_to_preds' l [] upds"

definition changes_to_ann :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) state_change list \<Rightarrow> ('level, 'val) ann" where
  "changes_to_ann l upds = \<lparr>preds = changes_to_preds l upds\<rparr>"


definition negate_exp :: "'val exp \<Rightarrow> 'val exp" where
  "negate_exp e = BinOp (\<lambda> v\<^sub>1 v\<^sub>2. if (val_true v\<^sub>1) then ff else tt) e (Val undefined)"

text {* Currently, this will override all existing annotations. We probably
need to add a way to keep some user-provided annotations as well. *}
fun infer' :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) com \<Rightarrow> ('level, 'val) state_change list \<Rightarrow>
               ('level, 'val) com \<times> ('level, 'val) state_change list" where
  "infer' l ({A} x ::= e) upds = ({changes_to_ann l upds} x ::= e, upds @ [MemUpdate x e])" |
  "infer' l Skip upds = (Skip, upds)" |
  "infer' l ({A} x :D= e) upds = ({changes_to_ann l upds} x :D= e, upds @ [Decl x e])" |
  "infer' l ({A} x <- l') upds = ({changes_to_ann l upds} x <- l', upds @ [Read x l'])" |
  "infer' l (Out A l' e) upds = (Out (changes_to_ann l upds) l' e, upds @ [Outp l' e])" |
  "infer' l (If A e c\<^sub>1 c\<^sub>2) upds =
    (case (infer' l c\<^sub>1 (upds @ [PathCond e]), infer' l c\<^sub>2 (upds @ [PathCond (negate_exp e)])) of
      ((c\<^sub>1', upds\<^sub>1'), (c\<^sub>2', upds\<^sub>2')) \<Rightarrow> (If (changes_to_ann l upds) e c\<^sub>1' c\<^sub>2', upds))" |
  "infer' l (c\<^sub>1 ;; c\<^sub>2) upds =
    (case infer' l c\<^sub>1 upds of
       (c\<^sub>1', upds') \<Rightarrow> 
         (case infer' l c\<^sub>2 upds' of
            (c\<^sub>2', upds'') \<Rightarrow> (c\<^sub>1' ;; c\<^sub>2', upds'')))" |
  "infer' l (Acquire A loc) upds = 
    (Acquire (changes_to_ann l upds) loc, upds @ [GetLock loc])" |
  "infer' l (Release A loc) upds =
    (Release (changes_to_ann l upds) loc, upds @ [ReleaseLock loc])" |
  "infer' l (While A e I c) upds = (While A e I c, upds)" (* <- TODO *)


definition infer :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) com" where
  "infer l = fst (infer' l (com l) [])"

(* helper function for testing inference on pre-annotated examples; this
is not really necessary for the current version that doesn't actually
preserve old annotation, but will help in the future. *)
fun replace_anns :: "('level, 'val) ann \<Rightarrow> ('level, 'val) com \<Rightarrow> ('level, 'val) com" where
  "replace_anns B ({A} x ::= e) = {B} x ::= e" |
  "replace_anns B ({A} x :D= e) = {B} x :D= e" |
  "replace_anns B ({A} x <- l') = {B} x <- l'" |
  "replace_anns B (Out A l' e) = Out B l' e" |
  "replace_anns B (Acquire A loc) = Acquire B loc" |
  "replace_anns B (Release A loc) = Release B loc" |
  "replace_anns B (If A e c\<^sub>1 c\<^sub>2) = If B e (replace_anns B c\<^sub>1) (replace_anns B c\<^sub>2)" |
  "replace_anns B (While A e I c) = While B e I (replace_anns B c)" |
  "replace_anns B (c\<^sub>1 ;; c\<^sub>2) = replace_anns B c\<^sub>1 ;; replace_anns B c\<^sub>2" |
  "replace_anns B Skip = Skip"

definition infer_loc :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state" where
  "infer_loc l = l\<lparr>com := infer l\<rparr>"

definition infer_loc_replace :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state" where
  "infer_loc_replace l = l\<lparr>com := infer (l\<lparr>com := replace_anns ann\<^sub>0 (com l)\<rparr>)\<rparr>"

lemmas infer_defs = infer_loc_replace_def infer_def changes_to_ann_def changes_to_preds_def

end
end