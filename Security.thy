theory Security
  imports Main Language HOL.Orderings "~~/src/HOL/Library/Prefix_Order"  "~~/src/HOL/Library/Lattice_Syntax"
begin

section {* Security definitions *}

context lang
begin

definition env_loweq :: "'level \<Rightarrow> ('level, 'val) env \<Rightarrow> ('level, 'val) env \<Rightarrow> bool" (infix "=\<^sup>e\<index>" 50)
  where "\<E>\<^sub>1 =\<^sup>e\<^bsub>l\<^esub> \<E>\<^sub>2 \<equiv> (\<forall> l' \<le> l. \<E>\<^sub>1 l' = \<E>\<^sub>2 l')"

(* Probably only needed as an intermediate condition in the proofs now: *)
definition mem_loweq :: "'level \<Rightarrow> 'val mem \<Rightarrow> 'val mem \<Rightarrow> bool" (infix "=\<^sup>m\<index>" 50)
  where "m\<^sub>1 =\<^sup>m\<^bsub>l\<^esub> m\<^sub>2 \<equiv> (\<forall> x l'. \<L> x = Some l' \<and> l' \<le> l \<longrightarrow> m\<^sub>1 x = m\<^sub>2 x)"
end

fun event_level :: "('level, 'val) event \<Rightarrow> 'level" where
  "event_level (Input l _) = l" |
  "event_level (Output l _ _) = l" |
  "event_level (Declassification l _ _) = l"

(* Remove all events not on the exact same level. *)
fun project_trace :: "('level, 'val) trace \<Rightarrow> 'level \<Rightarrow> ('level, 'val) trace" (infix "\<upharpoonleft>" 60) where
  "[] \<upharpoonleft> l = []" |
  "(ev # t) \<upharpoonleft> l = (if event_level ev = l then ev # (t \<upharpoonleft> l) else t \<upharpoonleft> l)"

context lang
begin

fun project_trace_upto :: "('level, 'val) trace \<Rightarrow> 'level \<Rightarrow> ('level, 'val) trace" (infix "\<upharpoonleft>\<^sub>\<le>" 60) where
  "[] \<upharpoonleft>\<^sub>\<le> l = []" |
  "(ev # t) \<upharpoonleft>\<^sub>\<le> l = (if event_level ev \<le> l then ev # (t \<upharpoonleft>\<^sub>\<le> l) else t \<upharpoonleft>\<^sub>\<le> l)"

lemma project_trace_upto_filter:
  "project_trace_upto t l = filter (\<lambda>e. event_level e \<le> l) t"
  by(induct t, auto)

definition trace_equiv :: "'level \<Rightarrow> ('level, 'val) trace \<Rightarrow> ('level, 'val) trace \<Rightarrow> bool" (infix "=\<^sup>t\<index>" 50) where
  "t\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> t\<^sub>2 \<equiv> t\<^sub>1 \<upharpoonleft>\<^sub>\<le> l = t\<^sub>2 \<upharpoonleft>\<^sub>\<le> l"

definition lock_state_equiv :: "lock_state \<Rightarrow> lock_state \<Rightarrow> bool" (infix "=\<^sup>l" 50) where
  "locs\<^sub>1 =\<^sup>l locs\<^sub>2 \<equiv> locs\<^sub>1 = locs\<^sub>2"

definition global_state_equiv :: "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool"
  (infix "=\<^sup>g\<index>" 50) where 
  "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2 \<equiv> (trace g\<^sub>1 =\<^sup>t\<^bsub>l\<^esub> trace g\<^sub>2 \<and> mem g\<^sub>1 =\<^sup>m\<^bsub>l\<^esub> mem g\<^sub>2 \<and>
                 env g\<^sub>1 =\<^sup>e\<^bsub>l\<^esub> env g\<^sub>2 \<and> lock_state g\<^sub>1 =\<^sup>l lock_state g\<^sub>2)"

text {* This defines uncertainty for a run of a global configuration: *}
definition uncertainty :: "'level \<Rightarrow> (('level, 'val) global_state \<Rightarrow> bool) \<Rightarrow> ('level, 'val) local_state list \<Rightarrow> ('level, 'val) global_state \<Rightarrow> sched \<Rightarrow> 
                           ('level, 'val) trace \<Rightarrow> ('level, 'val) global_state set"
  where "uncertainty l P ls g\<^sub>0 sched t =
    { g . P g \<and> g =\<^sup>g\<^bsub>l\<^esub> g\<^sub>0 \<and> (\<exists> ls' g' sched'. t =\<^sup>t\<^bsub>l\<^esub> trace g' \<and> (ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')) }"

fun lift_option2 :: "('a \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> 'a option \<Rightarrow> 'a option \<Rightarrow> 'a option" where
  "lift_option2 f (Some x) (Some y) = Some (f x y)" |
  "lift_option2 f _ _ = None"

fun exp_level_syn :: "'val exp \<Rightarrow> 'level option" where
  "exp_level_syn (Var x) = \<L> x" |
  "exp_level_syn (UnOp f e) = exp_level_syn e" |
  "exp_level_syn (BinOp f e\<^sub>1 e\<^sub>2) = lift_option2 (\<squnion>) (exp_level_syn e\<^sub>1) (exp_level_syn e\<^sub>2)" |
  "exp_level_syn (Val v) = Some \<bottom>"


definition ann_implies_low :: "'level \<Rightarrow> ('level, 'val) ann \<Rightarrow>
   'val exp \<Rightarrow> bool" where
  "ann_implies_low l A e \<equiv> \<forall> g g'. g =\<^sup>g\<^bsub>l\<^esub> g' \<and> g \<Turnstile>\<^sub>P A \<and> g' \<Turnstile>\<^sub>P A  \<longrightarrow>
       eval_exp e (mem g) = eval_exp e (mem g')"


inductive exp_level :: "'level \<Rightarrow> ('level, 'val) ann \<Rightarrow> 'val exp \<Rightarrow> bool" where
  intro[intro!]: "\<lbrakk> ann_implies_low l A e ; \<And> l'. l' < l \<Longrightarrow> \<not> ann_implies_low  l' A e \<rbrakk> \<Longrightarrow>
     exp_level l A e"

inductive_cases exp_level_elim[elim!]: "exp_level l A e"

(* TODO: we may want to replace this by a relation in the type system. *)
definition exp_level_tot :: "('level, 'val) ann \<Rightarrow> 'val exp \<Rightarrow> 'level" where
  "exp_level_tot A e \<equiv>
    SOME l. ann_implies_low l A e"

lemma trace_proj_filter':
  "t \<upharpoonleft> l = filter (\<lambda> e. event_level e = l) t"
  by (induction t, auto)

lemma trace_proj_filter:
  "t \<upharpoonleft>\<^sub>\<le> l = filter (\<lambda> e. event_level e \<le> l) t"
  by (induction t, auto)

(* FIXME: this proof can most probably be simplified a lot (maybe do case distinction on Q x only) *)
lemma filter_stronger:
  assumes stronger: "\<And> x. P x \<Longrightarrow> Q x"
      and eq: "filter Q xs = filter Q ys"
    shows "filter P xs = filter P ys"
  using eq
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case
    by (metis filter.simps(1) filter_empty_conv stronger)
next
  case (Cons x xs')
  show ?case 
  proof (cases "P x")
    case True
    with Cons obtain qys where "filter Q ys = x # qys"
      by (metis filter.simps(2) stronger)
    with Cons have "filter Q xs' = qys"
      by (simp add: True stronger)
    obtain ys\<^sub>0 ys' where "ys = ys\<^sub>0 @ (x # ys')" "filter Q ys\<^sub>0 = []"
      using Cons stronger
      by (metis Cons_eq_filterD \<open>filter Q ys = x # qys\<close> filter_False)
    moreover then have "filter P ys = x # filter P ys'"
      using Cons stronger
      by (metis Cons_eq_filter_iff True filter_empty_conv)
    with Cons have "filter P xs' = filter P ys'"
      by (metis append.left_neutral calculation(1) calculation(2) filter.simps(2) filter_append list.inject)
    with Cons show ?thesis
      by (metis True \<open>filter P ys = x # filter P ys'\<close> filter.simps(2))
  next
    case False
    then have "filter P (x # xs') = filter P xs'"
      by simp
    then show ?thesis
    proof (cases "Q x")
      case True
      then obtain ys\<^sub>0 ys' where "ys = ys\<^sub>0 @ (x # ys')" "filter Q ys\<^sub>0 = []"
        using Cons stronger
        by (metis Cons_eq_filterD filter.simps(2) filter_False)
      have "filter Q ys = x # filter Q xs'"
        using Cons.prems True by auto
      have "filter P ys' = filter P xs'"
        by (metis Cons.IH True \<open>filter Q ys = x # filter Q xs'\<close> \<open>filter Q ys\<^sub>0 = []\<close> \<open>ys = ys\<^sub>0 @ x # ys'\<close> append_Nil filter.simps(2) filter_append list.inject)
      then show ?thesis
        using Cons False stronger True
        by (metis \<open>filter Q ys\<^sub>0 = []\<close> \<open>ys = ys\<^sub>0 @ x # ys'\<close> filter.simps(2) filter_append self_append_conv2)
    next
      case False
      then show ?thesis
        apply auto
        using Cons stronger
        apply blast
        using Cons.IH Cons.prems by auto
    qed
  qed
qed

lemma trace_equiv_le_closed:
  assumes le: "l' \<le> l"
      and eq: "t =\<^sup>t\<^bsub>l\<^esub> t'"
    shows "t =\<^sup>t\<^bsub>l'\<^esub> t'"
proof -
  from le have "\<And> e. event_level e \<le> l' \<Longrightarrow> event_level e \<le> l"
    using order.trans by force
  with eq show ?thesis
    unfolding trace_equiv_def trace_proj_filter
    by (smt filter_stronger)
qed

lemma project_trace_upto_eq_project_trace_eq:
  "t \<upharpoonleft>\<^sub>\<le> l = t' \<upharpoonleft>\<^sub>\<le> l \<Longrightarrow> t \<upharpoonleft> l = t' \<upharpoonleft> l"
  using trace_proj_filter trace_proj_filter' filter_stronger
proof -
  assume a1: "t \<upharpoonleft>\<^sub>\<le> l = t' \<upharpoonleft>\<^sub>\<le> l"
  obtain ee :: "(('level, 'val) event \<Rightarrow> bool) \<Rightarrow> (('level, 'val) event \<Rightarrow> bool) \<Rightarrow> ('level, 'val) event" where
    "\<forall>x2 x3. (\<exists>v4. x3 v4 \<and> \<not> x2 v4) = (x3 (ee x2 x3) \<and> \<not> x2 (ee x2 x3))"
    by moura
  then have f2: "\<forall>p pa es esa. (p (ee pa p) \<and> \<not> pa (ee pa p) \<or> filter pa es \<noteq> filter pa esa) \<or> filter p es = filter p esa"
    by (metis (no_types) filter_stronger)
  have "[e\<leftarrow>t . event_level e \<le> l] = [e\<leftarrow>t' . event_level e \<le> l]"
    using a1 by (simp add: trace_proj_filter)
  then have "event_level (ee (\<lambda>e. event_level e \<le> l) (\<lambda>e. event_level e = l)) = l \<and> \<not> event_level (ee (\<lambda>e. event_level e \<le> l) (\<lambda>e. event_level e = l)) \<le> l \<or> [e\<leftarrow>t . event_level e = l] = [e\<leftarrow>t' . event_level e = l]"
    using f2 by meson
  then show ?thesis
    by (metis eq_iff trace_proj_filter')
qed

lemma global_equiv_le_closed:
  "l' \<le> l \<Longrightarrow> g =\<^sup>g\<^bsub>l\<^esub> g' \<Longrightarrow> g =\<^sup>g\<^bsub>l'\<^esub> g'"
  using trace_equiv_le_closed
  unfolding global_state_equiv_def
  by (auto simp: mem_loweq_def env_loweq_def)

lemma ann_implies_low_le_closed:
  "l \<le> l' \<Longrightarrow> ann_implies_low l A e \<Longrightarrow> ann_implies_low l' A e"
  unfolding ann_implies_low_def
  using global_equiv_le_closed by auto

(*"exp_level_tot A e \<equiv>
    THE l. ann_implies_low l A e \<and> (\<forall> l'. l' < l \<longrightarrow> \<not> ann_implies_low l' A e)" *)

definition compl_loweq :: 
  "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool" (infix "=\<^sup>g\<^sup>o\<index>" 60)
  where
  "compl_loweq l g g' = (\<forall> l'. l' \<noteq> l \<longrightarrow> g =\<^sup>g\<^bsub>l'\<^esub> g')"

text {* The key observation for uniqueness of expression levels is that if they are not bottom,
they are not spurious, i.e. there exist memories that only differ on the lowest level
that determines an expression's level and result in different values when evaluating
the expression; if this wouldn't be the case, we'd need more information than just
the information at level @{term l} to determine the value of @{term e}: *}
lemma exp_level_vary:
  assumes A: "ann_implies_low l A e"
      and min: "\<forall> l'. l' < l \<longrightarrow> \<not> ann_implies_low l' A e"
      and not_bot: "l > \<bottom>"
    shows "\<exists> g g'. eval_exp e (mem g) \<noteq> eval_exp e (mem g') \<and> g \<Turnstile>\<^sub>P A \<and> g' \<Turnstile>\<^sub>P A \<and> g =\<^sup>g\<^sup>o\<^bsub>l\<^esub> g'"
  oops

lemma
  assumes other_concl:  "eval_exp e (mem g) \<noteq> eval_exp e (mem g') \<and> g \<Turnstile>\<^sub>P A \<and> g' \<Turnstile>\<^sub>P A \<and> g =\<^sup>g\<^sup>o\<^bsub>l\<^esub> g'"
  assumes A: "ann_implies_low l A e"
  assumes nottop: "l < \<top>"
  shows "False"
proof -
  from nottop other_concl have "g =\<^sup>g\<^bsub>\<top>\<^esub> g'"
    by (simp add: compl_loweq_def less_imp_not_eq2)
  with nottop have "g =\<^sup>g\<^bsub>l\<^esub> g'"
    using global_equiv_le_closed less_le_not_le by blast
  with other_concl A show "False"
    using ann_implies_low_def by blast
qed


lemma exp_level_unique':
  fixes A :: "('level, 'val) ann"
  assumes A: "ann_implies_low l A e" "ann_implies_low l' A e"
             "\<forall> l\<^sub>1. l\<^sub>1 < l \<longrightarrow> \<not> ann_implies_low l\<^sub>1 A e"
             "\<forall> l\<^sub>1. l\<^sub>1 < l' \<longrightarrow> \<not> ann_implies_low l\<^sub>1 A e"
  shows "l \<le> l' \<or> l' \<le> l"
proof (rule ccontr, clarsimp)
  assume N: "\<not> l \<le> l'" "\<not> l' \<le> l"
  have not_bot: "l' \<noteq> \<bottom>" "l \<noteq> \<bottom>"
    using N by auto
  from N have neq: "l \<noteq> l'"
    by auto
  from A have
    "\<And> g g'. \<lbrakk> g \<Turnstile>\<^sub>P A ; g' \<Turnstile>\<^sub>P A ; g =\<^sup>g\<^bsub>l\<^esub> g' \<rbrakk> \<Longrightarrow> eval_exp e (mem g) = eval_exp e (mem g')"
    "\<And> g g'. \<lbrakk> g \<Turnstile>\<^sub>P A ; g' \<Turnstile>\<^sub>P A ; g =\<^sup>g\<^bsub>l'\<^esub> g' \<rbrakk> \<Longrightarrow> eval_exp e (mem g) = eval_exp e (mem g')"
    unfolding ann_implies_low_def by auto
  from not_bot A obtain g g' where diff: "eval_exp e (mem g) \<noteq> eval_exp e (mem g')" "g \<Turnstile>\<^sub>P A" "g' \<Turnstile>\<^sub>P A"
    "g =\<^sup>g\<^sup>o\<^bsub>l\<^esub> g'"
    oops

definition exp_level_opt :: "('level, 'val) ann \<Rightarrow> 'val exp \<Rightarrow> 'level option" where
  "exp_level_opt A e = (if (\<exists> l. exp_level l A e) then Some (SOME l. exp_level l A e) else None)"

definition system_secure :: "'level \<Rightarrow> (('level,'val) global_state \<Rightarrow> bool) \<Rightarrow> sched \<Rightarrow> ('level, 'val) local_state list  \<Rightarrow> bool"
  where "system_secure l P sched ls \<equiv>
   (\<forall> g ls' g' sched' ls'' g'' sched'' ev. P g \<and>
       (ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched') \<and>
       ((ls', g', sched') \<rightarrow> (ls'', g'', sched'')) \<and>
       trace g'' = trace g' @ [ev] \<longrightarrow>
         (case ev of
            Declassification to v e \<Rightarrow>
              (let c = head_com (com (ls' ! next_thread (ls', g', sched')))
               in let A = active_ann_com c
               in
               (if to \<le> l 
                then (case exp_level_syn e of
                        Some from \<Rightarrow> \<D> from to g' v c
                      | None \<Rightarrow> False)
                else uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched (trace g'')))
          | _ \<Rightarrow>
              uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched (trace g'')))"


definition event_secure
  where 
  "event_secure l P ls g sched ls' g' sched'  ev \<equiv>
         (case ev of
            Declassification to v e \<Rightarrow>
              (let c = head_com (com (ls' ! next_thread (ls', g', sched')))
               in
               (if to \<le> l 
                then (case exp_level_syn e of
                        Some from \<Rightarrow> \<D> from to g' v c
                      | None \<Rightarrow> False)
                else uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched ((trace g')@[ev])))
          | _ \<Rightarrow>
              uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched ((trace g')@[ev]))"

definition
  lift_\<D> where
  "lift_\<D> from to g' v c \<equiv> case from of None \<Rightarrow> False | Some from' \<Rightarrow> \<D> from' to g' v c"

definition
  not_declass_to where
  "not_declass_to l e \<equiv> (case e of Declassification to _ _ \<Rightarrow> \<not> (to \<le> l) | _ \<Rightarrow> True)"

lemma event_secure_def2:
  "event_secure l P ls g sched ls' g' sched' ev =
     ((\<forall> to v e.
        (ev = Declassification to v e \<and> to \<le> l \<longrightarrow>
           lift_\<D> (exp_level_syn e) to g' v (head_com (com (ls' ! next_thread (ls', g', sched')))))) \<and>
     (not_declass_to l ev \<longrightarrow>  
              uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched ((trace g')@[ev])))"
  by(auto simp: event_secure_def not_declass_to_def lift_\<D>_def split: event.splits option.splits)
  
lemma sys_secure_def2:
  "system_secure l P sched ls =
   (\<forall> g ls' g' sched' ls'' g'' sched'' ev. P g \<and>
       (ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched') \<and>
       ((ls', g', sched') \<rightarrow> (ls'', g'', sched'')) \<and>
       trace g'' = trace g' @ [ev] \<longrightarrow>
         event_secure l P ls g sched ls' g' sched' ev)"
  by(auto simp add: event_secure_def system_secure_def split: event.splits)

text {* Useful for making the security guarantee of an example more explicit: *}
lemma event_secure_no_lift:
  assumes \<D>_const: "\<And> from from' to g' v c. \<D> from to g' v c = \<D> from' to g' v c"
     and "event_secure l P ls g sched ls' g' sched' ev"
   shows "((\<forall> to v e.
        (ev = Declassification to v e \<and> to \<le> l \<longrightarrow>
           \<D> undefined to g' v (head_com (com (ls' ! next_thread (ls', g', sched')))))) \<and>
     (not_declass_to l ev \<longrightarrow>  
              uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched ((trace g')@[ev])))"
  using assms
  unfolding event_secure_def2
  apply (auto simp add: lift_\<D>_def)[1]
  by (metis (no_types, lifting) case_optionE)+

end

subsection {* Some useful definitions for examples and alternative security conditions. *}

fun inputs :: "('level, 'val) trace \<Rightarrow> ('level * 'val) list" where
  "inputs [] = []" |
  "inputs (Input l v # t) = (l, v) # inputs t" |
  "inputs (_ # t) = inputs t"

fun outputs :: "('level, 'val) trace \<Rightarrow> ('level * 'val * 'val exp) list" where
  "outputs [] = []" |
  "outputs (Output l v e # t) = (l, v, e) # outputs t" |
  "outputs (_ # t) = outputs t"


fun decls :: "('level, 'val) trace \<Rightarrow> ('level * 'val * 'val exp) list" where
  "decls [] = []" |
  "decls (Declassification l v e # t) = (l, v, e) # decls t" |
  "decls (_ # t) = decls t"

definition level_inputs :: "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> 'val list" where
  "level_inputs l g = map snd (inputs (trace g \<upharpoonleft> l))"

definition level_outputs :: "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('val * 'val exp) list" where
  "level_outputs l g = map snd (outputs (trace g \<upharpoonleft> l))"

definition level_decls :: "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('val * 'val exp) list" where
  "level_decls l g = map snd (decls (trace g \<upharpoonleft> l))"

subsection "Helper functions on traces"

definition last_input :: "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> 'val" where
  "last_input l g = last (level_inputs l g)"

definition last_output :: "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> 'val" where
  "last_output l g = last (map (fst \<circ> snd) (outputs (trace g \<upharpoonleft> l)))"

definition holds_lock :: "thread_id \<Rightarrow> lock \<Rightarrow> ('level, 'val) pred" where
  "holds_lock t loc g \<equiv> lock_state g loc = Some t"

context lang
begin


text {*
Problem: under the current definitions, expressions don't always have a well-defined
level, since equivalence at level @{term \<top>} does not imply equality between states; for example,
consider the following program:

l = 0      || l = 1
out(L, l)

By the time we reach the output statement, even @{term \<top>}-equivalence does not imply
that l will contain equal levels, since its value is determined by the scheduler,
which is not visible to assertions.

As a result, we need to turn the expression level into a relation and require
the user to show that such a level exists (we could also add a minimality
requirement to disambiguate it).
*}


lemma top_eqv_implies_eq:
  "g =\<^sup>g\<^bsub>\<top>\<^esub> g' \<Longrightarrow> g = g'"
  unfolding global_state_equiv_def mem_loweq_def trace_equiv_def env_loweq_def lock_state_equiv_def
  unfolding trace_proj_filter
  apply auto
  oops

lemma ann_implies_top:
  "ann_implies_low \<top> A e"
  oops

lemma expr_level_always_exists:
  "\<exists>! l. ann_implies_low l A e \<and> (\<forall> l'. l' < l \<longrightarrow> \<not> ann_implies_low l' A e)"
  oops

end
end
