theory HighBranching
  imports TypeSystem Security Soundness
begin

context lang
begin

fun count_steps :: "('level, 'val) com \<Rightarrow> nat option" where
  "count_steps Skip = Some 0" |
  "count_steps ({A} x ::= e) = Some 1" |
  "count_steps ({A} x :D= e) = Some 1" |
  "count_steps ({A} x <- l') = Some 1" |
  "count_steps (Out A e l) = Some 1" |
  "count_steps (DOut A e l) = Some 1" |
  "count_steps (Release A loc) = Some 1" |
  "count_steps (Acquire A loc) = Some 1" |
  "count_steps (While A e I c) = None" |
  "count_steps (c\<^sub>1 ;; c\<^sub>2) = (if c\<^sub>1 = Skip then None else
                             (case (count_steps c\<^sub>1, count_steps c\<^sub>2) of
                                (Some s\<^sub>1, Some s\<^sub>2) \<Rightarrow> Some (s\<^sub>1 + s\<^sub>2)
                              | _ \<Rightarrow> None))" |
  "count_steps (If A e c\<^sub>1 c\<^sub>2) = (case (count_steps c\<^sub>1, count_steps c\<^sub>2) of
                                  (Some s\<^sub>1, Some s\<^sub>2) \<Rightarrow> (if s\<^sub>1 = s\<^sub>2 then Some (Suc s\<^sub>1) else None)
                                | _ \<Rightarrow> None)"

inductive has_type_high :: "'level \<Rightarrow> ('level, 'val) com \<Rightarrow> bool"
  ("_ \<turnstile>\<^sub>H _" [40, 40] 50)
  where
  Skip: "l \<turnstile>\<^sub>H Skip" |
  LAssign: "\<lbrakk> \<L> x = Some l\<^sub>x ; l\<^sub>x > l \<rbrakk> \<Longrightarrow>
    l \<turnstile>\<^sub>H {A} x ::= e" |
  UAssign: "\<lbrakk> \<L> x = None \<rbrakk> \<Longrightarrow> l \<turnstile>\<^sub>H {A} x ::= e" |
  LIn: "\<lbrakk> \<L> x = Some l\<^sub>x ; l\<^sub>x > l; lev > l \<rbrakk> \<Longrightarrow>
    l \<turnstile>\<^sub>H {A} x <- lev" |
  UIn: "\<lbrakk> \<L> x = None ; \<not> (lev \<le> l) \<rbrakk> \<Longrightarrow>
    l \<turnstile>\<^sub>H {A} x <- lev" |
  Out: "\<lbrakk> \<not> (lev \<le> l) \<rbrakk> \<Longrightarrow> l \<turnstile>\<^sub>H Out A lev e" |
  Seq: "\<lbrakk> l \<turnstile>\<^sub>H c\<^sub>1 ; l \<turnstile>\<^sub>H c\<^sub>2 \<rbrakk> \<Longrightarrow> l \<turnstile>\<^sub>H c\<^sub>1 ;; c\<^sub>2" |
  If: "\<lbrakk> l \<turnstile>\<^sub>H c\<^sub>1 ; l \<turnstile>\<^sub>H c\<^sub>2 ; count_steps c\<^sub>1 = Some s ; count_steps c\<^sub>2 = Some s \<rbrakk> \<Longrightarrow>
    l \<turnstile>\<^sub>H If A e c\<^sub>1 c\<^sub>2"

definition no_low_changes :: "'level \<Rightarrow> ('level, 'val) local_state \<Rightarrow> bool" where
  "no_low_changes l l\<^sub>1 \<equiv> (\<forall> g\<^sub>1 l\<^sub>2 g\<^sub>2. (g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1 \<and> (l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)) \<longrightarrow>
      g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1)"

definition \<H> :: "('level, 'val) bisim" where
  "\<H> l l\<^sub>1 l\<^sub>1' \<equiv>
    ( (True, l \<turnstile> com l\<^sub>1) \<and> l \<turnstile>\<^sub>H com l\<^sub>1 \<and> (True, l \<turnstile> com l\<^sub>1') \<and> l \<turnstile>\<^sub>H com l\<^sub>1' \<and>
     tid l\<^sub>1 = tid l\<^sub>1' \<and>
     (com l\<^sub>1 = Skip \<longleftrightarrow> com l\<^sub>1' = Skip) \<and>
     count_steps (com l\<^sub>1) = count_steps (com l\<^sub>1') \<and> 
     count_steps (com l\<^sub>1) \<noteq> None \<and>
     no_low_changes l l\<^sub>1 \<and> no_low_changes l l\<^sub>1')"

inductive_cases If_type_elim[elim]: "ah, l \<turnstile> If A e c\<^sub>1 c\<^sub>2"
inductive_cases If_typeH_elim[elim]: "l \<turnstile>\<^sub>H If A e c\<^sub>1 c\<^sub>2"
inductive_cases Skip_type_elim[elim]: "ah, l \<turnstile> Skip"
inductive_cases Assign_type_elim[elim]: "ah, l \<turnstile> {A} x ::= e"
inductive_cases Seq_typeH_elim[elim]: "l \<turnstile>\<^sub>H c\<^sub>1 ;; c\<^sub>2"
inductive_cases Seq_type_elim[elim]: "ah, l \<turnstile> c\<^sub>1 ;; c\<^sub>2"
inductive_cases Release_typeH_elim[elim]: "l \<turnstile>\<^sub>H Release A loc"
inductive_cases Acquire_typeH_elim[elim]: "l \<turnstile>\<^sub>H Acquire A loc"
inductive_cases While_typeH_elim[elim]: "l \<turnstile>\<^sub>H While A e I c"


lemma global_equiv_refl:
  "g =\<^sup>g\<^bsub>l\<^esub> g"
  by (auto simp: global_equiv_defs)

lemma count_steps_seqD:
  "count_steps (c\<^sub>1 ;; c\<^sub>2) = Some n \<Longrightarrow> 
    \<exists> i j. count_steps c\<^sub>1 = Some i \<and> count_steps c\<^sub>2 = Some j \<and> n = i + j"
  apply auto[1]
  by (smt option.case_eq_if option.collapse option.distinct(1) option.inject)

lemma count_steps_seqD':
  assumes "count_steps (c\<^sub>1 ;; c\<^sub>2) \<noteq> None"
  shows "count_steps c\<^sub>1 \<noteq> None \<and> count_steps c\<^sub>2 \<noteq> None"
proof -
  from assms obtain n where "count_steps (c\<^sub>1 ;; c\<^sub>2) = Some n"
    by (metis option.exhaust)
  thus ?thesis
    using count_steps_seqD by fastforce
qed

lemma eval_count_steps_some:
  assumes typedH: "l \<turnstile>\<^sub>H c"
  assumes ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and typed: "ah, l \<turnstile> c"
      and "com l\<^sub>1 = c"
      and "count_steps (com l\<^sub>1) \<noteq> None"
    shows "\<exists> n. count_steps (com l\<^sub>1) = Some (Suc n)"
  using assms
proof (induction arbitrary: l\<^sub>1 l\<^sub>2 rule: has_type_high.induct)
  case (Skip l)
  then show ?case
    using eval_skip_elim by blast
next
  case (LAssign x l\<^sub>x l A e)
  then show ?case 
    by auto
next
  case (UAssign x l A e)
  then show ?case by auto
next
  case (LIn x l\<^sub>x l lev A)
  then show ?case by auto
next
  case (UIn x lev l A)
  then show ?case by auto
next
  case (Out lev l A e)
  then show ?case by auto
next
  case (Seq l c\<^sub>1 c\<^sub>2)
  then have typed': "ah, l \<turnstile> c\<^sub>1" "ah, l \<turnstile> c\<^sub>2" by force+
  have split_steps: "count_steps c\<^sub>1 \<noteq> None \<and> count_steps c\<^sub>2 \<noteq> None"
    using count_steps_seqD' Seq by auto
  from `(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` obtain l\<^sub>I g\<^sub>I where
    ev\<^sub>I: "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2)"
    by (rule eval_seq_elim, auto)
  from ev\<^sub>I Seq obtain n where "count_steps (com (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>)) = Some (Suc n)"
    using split_steps by force
  then show "\<exists>n. count_steps (com l\<^sub>1) = Some (Suc n)"
    using split_steps Seq
    by auto
next
  case (If l c\<^sub>1 c\<^sub>2 s A e)
  then show ?case 
    by auto
qed

lemma typed_no_low_changes:
  assumes "l \<turnstile>\<^sub>H c"
      and "True, l \<turnstile> c"
      and "com l\<^sub>1 = c"
    shows "no_low_changes l l\<^sub>1"
  using assms unfolding no_low_changes_def
proof (clarsimp ; induction arbitrary: l\<^sub>1 rule: has_type_high.induct)
  case (Skip l)
  then show ?case
    apply (auto)[1]
    using eval_skip_elim by blast
next
  case (LAssign x l\<^sub>x l A e)
  hence "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by blast
  then show ?case 
    apply (auto simp: global_equiv_defs)
    using LAssign
    by (metis less_le_not_le option.inject) 
next
  case (UAssign x l A e)
  hence "g\<^sub>2 = g\<^sub>1\<lparr>mem := (mem g\<^sub>1)(x := eval_exp e (mem g\<^sub>1))\<rparr>"
    by blast
  then show ?case
    by (auto simp: global_equiv_defs UAssign)
next
  case (LIn x l\<^sub>x l lev A)
  then have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>env := (env g\<^sub>1)(lev := ltl (env g\<^sub>1 lev)),
                        mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 lev)),
                        trace := trace g\<^sub>1 @ [Input lev (lhd (env g\<^sub>1 lev))]
                        \<rparr>"
    using eval_in_elim by blast
  then show ?case
    apply (auto simp: global_equiv_defs)[1]
    apply (metis LIn.hyps(3) event_level.simps(1) less_le_not_le trace_append_not_visible)
    using LIn by (metis less_le_not_le option.inject)+
next
  case (UIn x lev l A)
  then have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>env := (env g\<^sub>1)(lev := ltl (env g\<^sub>1 lev)),
                        mem := (mem g\<^sub>1)(x := lhd (env g\<^sub>1 lev)),
                        trace := trace g\<^sub>1 @ [Input lev (lhd (env g\<^sub>1 lev))]
                        \<rparr>"
    using eval_in_elim by blast
  then show ?case 
    apply (auto simp: global_equiv_defs)[1]
    apply (simp add: UIn.hyps(2) trace_append_not_visible)
    using UIn
    by (metis option.distinct(1))+
next
  case (Out lev l A e)
  then have [simp]: "g\<^sub>2 = g\<^sub>1\<lparr>trace := trace g\<^sub>1 @ [Output lev (eval_exp e (mem g\<^sub>1)) e]\<rparr>"
    using eval_out_elim by blast
  then show ?case 
    by (auto simp: global_equiv_defs Out trace_append_not_visible)
next
  case (Seq l c\<^sub>1 c\<^sub>2)
  from `(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` obtain l\<^sub>I where "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2)"
    apply (rule eval_seq_elim)
    by blast+
  moreover note foo = Seq(3)[where l\<^sub>11 = "l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>" and g\<^sub>11 = g\<^sub>1 and l\<^sub>21 = l\<^sub>I and g\<^sub>21 = g\<^sub>2]
  moreover from `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` `g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1` have "g\<^sub>1 \<Turnstile>\<^sub>P active_ann_com c\<^sub>1"
    unfolding active_ann_def
    by simp
  moreover from `l \<turnstile>\<^sub>H com l\<^sub>1` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` have "l \<turnstile>\<^sub>H com (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>)"
    by auto
  moreover from `True, l \<turnstile> com l\<^sub>1` `com l\<^sub>1 = c\<^sub>1 ;; c\<^sub>2` have "True, l \<turnstile> com (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>)"
    by auto
  ultimately show ?case
    by (clarsimp simp: active_ann_def)
next
  case (If l c\<^sub>1 c\<^sub>2 s A e)
  then have "g\<^sub>2 = g\<^sub>1"
    by blast
  then show ?case 
    by (auto simp: global_equiv_defs)
qed

lemma count_steps0_iff_Skip:
  "count_steps c = Some 0 \<longleftrightarrow> c = Skip"
  apply (induction c) (* Not exactly a clean proof *)
  by (auto split: option.splits)

lemma typed_implies_\<H>:
  assumes "l \<turnstile>\<^sub>H If A e c\<^sub>1 c\<^sub>2"
      and "True, l \<turnstile> If A e c\<^sub>1 c\<^sub>2"
      and "com l\<^sub>1 = If A e c\<^sub>1 c\<^sub>2"
      and "\<not> ann_implies_low l A e"
    shows "\<H> l (l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) (l\<^sub>1\<lparr>com := c\<^sub>2\<rparr>)"
  using assms
  unfolding \<H>_def
  apply (auto simp: If_type_elim If_typeH_elim typed_implies_bisim typed_no_low_changes)[1]
  using \<R>.intro\<^sub>2 \<R>\<^sub>2.intro
  using typed_no_low_changes
  using count_steps0_iff_Skip apply auto[1]
  apply (metis If_type_elim \<R>\<^sub>2.intros bisim_sym lang.\<R>.intro\<^sub>2 lang.\<R>_skip_iff lang_axioms local_state.select_convs(1) local_state.select_convs(2))
  using typed_no_low_changes by fastforce+

lemma \<H>_tid:
  assumes "\<H> l l\<^sub>1 l\<^sub>1'"
  shows "tid l\<^sub>1 = tid l\<^sub>1'"
  using assms by (auto simp: \<H>_def)

lemma \<H>_sym:
  shows "sym (to_rel (\<H> l))"
  apply (clarsimp simp: to_rel_def sym_def \<H>_def \<R>_sym)
  by metis

lemma \<H>_skip:
  assumes "\<H> l l\<^sub>1 l\<^sub>1'"
      and "com l\<^sub>1 = Skip"
    shows "com l\<^sub>1' = Skip"
  using assms
  by (auto simp: \<H>_def)

(* TODO: better name *)
lemma eval_to_skip_count1:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and "com l\<^sub>2 = Skip"
      and "l \<turnstile>\<^sub>H c"
      and "com l\<^sub>1 = c"
      and "count_steps (com l\<^sub>1) \<noteq> None"
    shows "count_steps c = Some (Suc 0)"
  using assms
proof (induction arbitrary: c rule: eval.induct)
  case (eval_assign l A x e g)
  then show ?case by auto
next
  case (eval_dassign l A x e l' g)
  then show ?case by auto
next
  case (eval_out l A lev e g)
  then show ?case by auto
next
  case (eval_dout l A lev e g)
  then show ?case by auto
next
  case (eval_in l A x lev v g)
  then show ?case by auto
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then show ?case by auto
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then show ?case by auto
next
  case (eval_seq l c\<^sub>1 c\<^sub>2 g l' g')
  then show ?case by auto
next
  case (eval_seqskip l c\<^sub>1 c\<^sub>2 g l' g')
  then show ?case 
    apply auto[1]
    by (metis (no_types, lifting) One_nat_def Seq_typeH_elim Suc_eq_plus1_left count_steps.simps(1) count_steps_seqD' eval_seqskip.prems(4) option.case_eq_if option.collapse option.sel)
next
  case (eval_whiletrue l A e I c g)
  then show ?case by auto
next
  case (eval_whilefalse l A e I c g)
  then show ?case by auto
next
  case (eval_acquire l A loc g)
  then show ?case by auto
next
  case (eval_release l A loc g)
  then show ?case by auto
qed

lemma has_type_high_progress:
  assumes "l \<turnstile>\<^sub>H c"
      and "com l\<^sub>1 = c"
      and "count_steps c = Some (Suc n)"
    shows "\<exists> l\<^sub>2 g\<^sub>2. (l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2) \<and>
            count_steps (com l\<^sub>2) = Some n"
  using assms
proof (induction arbitrary: n l\<^sub>1 rule: has_type_high.induct)
  case (Skip l)
  then show ?case 
    by auto
next
  case (LAssign x l\<^sub>x l A e)
  let ?l\<^sub>2 = "l\<^sub>1\<lparr>com := Skip\<rparr>"
  from LAssign show ?case
    apply -
    apply (rule exI[where x="?l\<^sub>2"])
    apply simp
    using eval_assign by force
next
  case (UAssign x l A e)
  let ?l\<^sub>2 = "l\<^sub>1\<lparr>com := Skip\<rparr>"
  from UAssign show ?case
    apply -
    apply (rule exI[where x="?l\<^sub>2"])
    apply simp
    using eval_assign by force
next
  case (LIn x l\<^sub>x l lev A)
  let ?l\<^sub>2 = "l\<^sub>1\<lparr>com := Skip\<rparr>"
  from LIn show ?case
    apply -
    apply (rule exI[where x="?l\<^sub>2"])
    apply simp
    using eval_in by force
next
  case (UIn x lev l A)
  let ?l\<^sub>2 = "l\<^sub>1\<lparr>com := Skip\<rparr>"
  from UIn show ?case
    apply -
    apply (rule exI[where x="?l\<^sub>2"])
    apply simp
    using eval_in by force
next
  case (Out lev l A e)
  let ?l\<^sub>2 = "l\<^sub>1\<lparr>com := Skip\<rparr>"
  from Out show ?case
    apply -
    apply (rule exI[where x="?l\<^sub>2"])
    apply simp
    using eval_out by force
next
  case (Seq l c\<^sub>1 c\<^sub>2)
  then obtain m where count\<^sub>1: "count_steps c\<^sub>1 = Some (Suc m)"
    by (metis (no_types, lifting) count_steps.simps(10) count_steps0_iff_Skip not0_implies_Suc old.prod.case option.distinct(1) option.exhaust option.simps(4))
  with Seq obtain l\<^sub>I g\<^sub>2 where ev\<^sub>I: "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1) \<leadsto> (l\<^sub>I, g\<^sub>2)" "count_steps (com l\<^sub>I) = Some m"
    by (metis local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
  moreover from Seq obtain k where k_props: "count_steps c\<^sub>2 = Some k" "Suc n = Suc m + k"
    by (metis \<open>count_steps c\<^sub>1 = Some (Suc m)\<close> count_steps_seqD option.sel)
  show ?case
  proof (cases "com l\<^sub>I = Skip")
    case True
    with ev\<^sub>I Seq have "m = 0"
      using eval_to_skip_count1
      by auto
    with k_props count\<^sub>1 ev\<^sub>I Seq.prems show ?thesis
      using eval_seqskip True by fastforce
  next
    case False
    then show ?thesis
      using False Seq.prems(1) ev\<^sub>I(1) ev\<^sub>I(2) eval.eval_seq k_props(1) k_props(2) by fastforce
  qed
next
  case (If l c\<^sub>1 c\<^sub>2 s A e)
  then show ?case
    apply (cases "val_true (eval_exp e (mem g\<^sub>1))")
     apply clarsimp
    using lang.eval.eval_iftrue lang_axioms apply fastforce
    apply clarsimp
    using lang.eval.eval_iffalse lang_axioms by fastforce
qed

lemma has_type_high_preservation:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and "com l\<^sub>1 = c"
      and "l \<turnstile>\<^sub>H c"
    shows "l \<turnstile>\<^sub>H com l\<^sub>2"
  using assms
  apply (induction arbitrary: c rule: eval.induct)
  using has_type_high.intros by auto

lemma global_equiv_trans[trans]:
  assumes "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2"
      and "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>3"
    shows "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>3"
  using assms
  by (auto simp: global_equiv_defs)

lemma global_equiv_implies_sec_step:
  assumes "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
  shows "sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
  using assms unfolding sec_step_def
  apply auto
  apply (case_tac ev, auto simp: global_state_equiv_def)
  using trace_proj_length_eq by blast

lemma \<H>_step:
  assumes in\<H>: "\<H> l l\<^sub>1 l\<^sub>1'"
      and ev: "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and sat: "g\<^sub>1 \<Turnstile>\<^sub>P active_ann l\<^sub>1" "g\<^sub>1' \<Turnstile>\<^sub>P active_ann l\<^sub>1'"
      and eqv: "g\<^sub>1 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
    shows "\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2') \<and> \<H> l l\<^sub>2 l\<^sub>2' \<and> sec_step l l\<^sub>1 l\<^sub>1' g\<^sub>1 g\<^sub>1' g\<^sub>2 g\<^sub>2'"
proof -
  from in\<H> have l\<^sub>1_typed: "True, l \<turnstile> com l\<^sub>1" "l \<turnstile>\<^sub>H com l\<^sub>1" and
                l\<^sub>1'_typed: "True, l \<turnstile> com l\<^sub>1'" "l \<turnstile>\<^sub>H com l\<^sub>1'"
    unfolding \<H>_def by auto
  from l\<^sub>1_typed ev sat have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1"
    using typed_no_low_changes unfolding no_low_changes_def
    by blast
  moreover
  from in\<H> ev l\<^sub>1_typed obtain n where "count_steps (com l\<^sub>1) = Some (Suc n)"
                                      "count_steps (com l\<^sub>1') = Some (Suc n)"
    unfolding \<H>_def
    using eval_count_steps_some
    by metis
  with l\<^sub>1'_typed obtain l\<^sub>2' g\<^sub>2' where ev': "(l\<^sub>1', g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')" and
    count\<^sub>2': "count_steps (com l\<^sub>2') = Some n"
    using has_type_high_progress
    by force
  moreover
  from l\<^sub>1_typed ev have "count_steps (com l\<^sub>2) = Some n"
    using has_type_high_progress eval_deterministic
    by (metis \<open>count_steps (com l\<^sub>1) = Some (Suc n)\<close>)
  with count\<^sub>2' have "count_steps (com l\<^sub>2) = count_steps (com l\<^sub>2')" 
    by auto
  moreover
  from ev' l\<^sub>1'_typed sat have "g\<^sub>2' =\<^sup>g\<^bsub>l\<^esub> g\<^sub>1'"
    using typed_no_low_changes unfolding no_low_changes_def
    by auto
  moreover
  from ev l\<^sub>1_typed have l\<^sub>2_typed: "True, l \<turnstile> com l\<^sub>2" "l \<turnstile>\<^sub>H com l\<^sub>2"
    using has_type_preservation has_type_high_preservation by auto
  moreover
  from l\<^sub>1'_typed ev' have l\<^sub>2'_typed: "True, l \<turnstile> com l\<^sub>2'" "l \<turnstile>\<^sub>H com l\<^sub>2'"
    using has_type_preservation has_type_high_preservation by auto
  moreover
  from l\<^sub>2_typed l\<^sub>2'_typed have "no_low_changes l l\<^sub>2" "no_low_changes l l\<^sub>2'"
    using typed_no_low_changes by auto
  moreover
  from eqv calculation have "g\<^sub>2 =\<^sup>g\<^bsub>l\<^esub> g\<^sub>2'"
    using global_equiv_sym global_equiv_trans
    by meson
  moreover
  from in\<H> ev ev' tid_constant have "tid l\<^sub>2 = tid l\<^sub>2'"
    by (auto simp: \<H>_def)
  ultimately show ?thesis
    unfolding \<H>_def
    apply -
    apply (rule exI[where x=l\<^sub>2'])
    apply (rule exI[where x=g\<^sub>2'])
    by (auto simp: global_equiv_implies_sec_step count_steps0_iff_Skip)
qed

lemma \<H>_secure_bisim: "secure_bisim l \<H>"
  unfolding secure_bisim_def
  apply (auto simp: \<H>_tid \<H>_sym \<H>_step \<H>_skip)[1]
  using \<H>_def by auto

lemma high_branching_secure:
  assumes "l \<turnstile>\<^sub>H com l\<^sub>1"
      and "com l\<^sub>1 = If A e c\<^sub>1 c\<^sub>2"
      and "True, l \<turnstile> com l\<^sub>1"
      and "\<not> ann_implies_low l A e"
    shows "(l\<^sub>1\<lparr>com := c\<^sub>1\<rparr>) \<approx>\<^bsub>l\<^esub> (l\<^sub>1\<lparr>com := c\<^sub>2\<rparr>)"
  using assms \<H>_secure_bisim typed_implies_\<H> by auto

end
end