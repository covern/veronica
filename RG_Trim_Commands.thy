theory RG_Trim_Commands
  imports Main Language RG_Verify
begin

context lang
begin

fun can_skip :: "('level, 'val) com \<Rightarrow> bool" where
  "can_skip Skip = False" |
  "can_skip ({A} x ::= e) = True" |
  "can_skip ({A} x :D= e) =
    (case \<L> x of
       Some lev \<Rightarrow> True
     | None \<Rightarrow> False)" |
  "can_skip ({A} x <- l') = True" |
  "can_skip (Out A l' e ) = True" |
  "can_skip (DOut A l' e ) = True" |
  "can_skip (Acquire A loc) = True" |
  "can_skip (Release A loc) = True" |
  "can_skip (If A e c\<^sub>1 c\<^sub>2) = 
    ((\<exists> m. val_true (eval_exp e m) \<and> c\<^sub>1 = Skip) \<or>
     (\<exists> m. \<not> val_true (eval_exp e m) \<and> c\<^sub>2 = Skip))" |
  "can_skip (While A e I c\<^sub>2) = (\<exists> m. \<not> val_true (eval_exp e m))" |
  "can_skip (c\<^sub>1 ;; c\<^sub>2)= (can_skip c\<^sub>1 \<and> c\<^sub>2 = Skip)"

fun trim_com :: "('level, 'val) com \<Rightarrow> ('level, 'val) com" where
  "trim_com Skip = Skip" |
  "trim_com ({A} x ::= e) = {A} x ::= e" |
  "trim_com ({A} x :D= e) = {A} x :D= e" |
  "trim_com ({A} x <- l') = {A} x <- l'" |
  "trim_com (Out A l' e) = Out A l' e" |
  "trim_com (DOut A l' e) = DOut A l' e" |
  "trim_com (Release A loc) = Release A loc" |
  "trim_com (Acquire A loc) = Acquire A loc" |
  "trim_com (If A e c\<^sub>1 c\<^sub>2) = If A e (trim_com c\<^sub>1) (trim_com c\<^sub>2)" |
  "trim_com (While A e I c) = While A e I (trim_com c)" |
  "trim_com (c\<^sub>1 ;; c\<^sub>2) =
     (if can_skip c\<^sub>1 then c\<^sub>1 ;; head_com c\<^sub>2
      else trim_com c\<^sub>1)"

definition trim :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) local_state" where
  "trim l = l\<lparr>com := trim_com (com l)\<rparr>"

lemma trim_next_states_com:
  "next_states l = next_states (trim l)"
proof -
  obtain c where c_def: "com l = c" by auto
  have "\<And> g. comp_next_states_com l c g = comp_next_states_com l (trim_com c) g"
    by (induction c arbitrary: l, auto)
  hence "\<And> g. comp_next_states l g = comp_next_states (trim l) g"
    using c_def comp_next_states.simps trim_def
    by (simp add: comp_next_states_com_ignores_loc_com)    
  thus ?thesis 
    using comp_next_states_correct c_def
    by blast
qed

lemma trim_active_ann:
  "active_ann_com c = active_ann_com (trim_com c)"
  by (induction c, auto)



lemma can_skip_to_skip:
  assumes "com l = c"
  shows "can_skip c \<longleftrightarrow> (\<exists> g. to_skip_com l c g)"
proof (induction c arbitrary: l)
  case Skip
  then show ?case by auto
next
  case (Assign x1 x2 x3)
  then show ?case by auto
next
  case (DAssign x1 x2 x3)
  then show ?case by auto
next
  case (Out x1 x2 x3)
  then show ?case by auto
next
  case (DOut x1 x2 x3)
  then show ?case by auto
next
  case (In x1 x2 x3)
  then show ?case by auto
next
  case (If A e c\<^sub>1 c\<^sub>2)
  show ?case
  proof (rule iffI)
    assume A: "can_skip (If A e c\<^sub>1 c\<^sub>2)"
    {
      fix m
      assume A: "c\<^sub>1 = Skip"
      assume B: "val_true (eval_exp e m)"
      define g :: "('level, 'val) global_state"
        where "g = undefined\<lparr>mem := m\<rparr>"
      with A B have "to_skip_com l (com.If A e c\<^sub>1 c\<^sub>2) g"
        by clarsimp
    }
    note thn = this
    {
      fix m
      assume A: "c\<^sub>2 = Skip"
      assume B: "\<not> val_true (eval_exp e m)"
      define g :: "('level, 'val) global_state"
        where "g = undefined\<lparr>mem := m\<rparr>"
      with A B have "to_skip_com l (com.If A e c\<^sub>1 c\<^sub>2) g"
        by clarsimp
    }
    note els = this
    from A thn els show "\<exists>g. to_skip_com l (com.If A e c\<^sub>1 c\<^sub>2) g" (is "\<exists> g. ?P g")
      apply simp
      by (meson els thn to_skip_com.simps(9))
  next
    assume "\<exists>g. to_skip_com l (com.If A e c\<^sub>1 c\<^sub>2) g"
    then obtain g where "to_skip_com l (If A e c\<^sub>1 c\<^sub>2) g"
      by auto
    show "can_skip (com.If A e c\<^sub>1 c\<^sub>2)"
      apply (cases "val_true (eval_exp e (mem g))"; simp)
      using \<open>to_skip_com l (com.If A e c\<^sub>1 c\<^sub>2) g\<close> by auto
  qed
next
  case (While A e I c)
  have "\<And> m. \<not> val_true (eval_exp e m) \<Longrightarrow> \<exists> g. \<not> val_true (eval_exp e (mem g))"
  proof -
    fix m
    assume A: "\<not> val_true (eval_exp e m)"
    define g :: "('level, 'val) global_state" where "g = undefined\<lparr>mem := m\<rparr>"
    with A have "\<not> val_true (eval_exp e (mem g))"
      by auto
    thus "?thesis m"
      by (metis global_state.select_convs(1))
  qed
  with While show ?case 
    by auto
next
  case (Seq c1 c2)
  then show ?case by auto
next
  case (Acquire A loc)
  define g :: "('level, 'val) global_state" where "g = undefined\<lparr>lock_state := \<lambda> x. None\<rparr>"
  then have "lock_state g loc = None"
    by auto
  hence "\<exists> g. lock_state g loc = None"
    by (metis global_state.select_convs(4))
  then show ?case by auto
next
  case (Release A loc)
  have "\<And> t. \<exists> g. lock_state g loc = Some t" 
  proof -
    fix t
    define g :: "('level, 'val) global_state" where "g = undefined\<lparr>lock_state := \<lambda> x. Some t\<rparr>"
    hence "lock_state g loc = Some t" by auto
    thus "?thesis t"
      by (metis global_state.select_convs(4))
  qed
  with Release show ?case 
    by auto
qed

lemma to_skip_com_seq:
  assumes "com l = c\<^sub>1 ;; c\<^sub>2"
  shows "to_skip_com l (c\<^sub>1 ;; c\<^sub>2) g \<Longrightarrow> to_skip_com l c\<^sub>1 g"
  by auto

lemma trim_next_anns:
  "next_anns l g = next_anns (trim l) g"
proof -
  obtain c where c_def: "com l = c" by auto
  have "comp_next_anns_com l c g = comp_next_anns_com l (trim_com c) g"
  proof (induction c arbitrary: l)
    case Skip
    then show ?case by auto
  next
    case (Assign x1 x2 x3)
    then show ?case by auto
  next
    case (DAssign x1 x2 x3)
    then show ?case by auto
  next
    case (Out x1 x2 x3)
    then show ?case by auto
  next
    case (DOut x1 x2 x3)
    then show ?case by auto
  next
    case (In x1 x2 x3)
    then show ?case by auto
  next
    case (If x1 x2 c1 c2)
    then show ?case
      using trim_active_ann by auto
  next
    case (While x1 x2 x3 c)
    then show ?case 
      using trim_active_ann by auto
  next
    case (Seq c\<^sub>1 c\<^sub>2)
    have "to_skip_com l c\<^sub>1 g \<Longrightarrow> can_skip c\<^sub>1"
    proof -
      assume A: "to_skip_com l c\<^sub>1 g"
      define l' where "l' = l\<lparr>com := c\<^sub>1\<rparr>"
      then have "com l' = c\<^sub>1" by auto
      hence "to_skip_com l' c\<^sub>1 g \<Longrightarrow> can_skip c\<^sub>1"
        using can_skip_to_skip 
        by force
      with A show ?thesis
        using to_skip_com_ignores_loc_com l'_def
        by auto
    qed
    with Seq show ?case
      apply auto[1]
      using trim_active_ann active_ann_head_com
      by blast
  next
    case (Acquire x1 x2)
    then show ?case by auto
  next
    case (Release x1 x2)
    then show ?case by auto
  qed
  thus ?thesis
    using comp_next_anns_correct trim_def
    apply simp
    by (metis c_def comp_next_anns_com_ignores_loc_com local_state.select_convs(1) local_state.surjective local_state.update_convs(1))
qed

lemma trim_simplifies:
  "simplify_func trim"
  using trim_next_anns trim_next_states_com trim_active_ann
  unfolding simplify_func_def trim_def
  by (auto simp: active_ann_def)

end
end