theory Reverse_Transitive_Closure
  imports Main
begin

inductive_set rtranclr :: "('a \<times> 'a) set \<Rightarrow> ('a \<times> 'a) set"  ("(_\<^sup>*\<^sup>r)" [1000] 999)
  for r :: "('a \<times> 'a) set"
  where
    rtranclr_refl [intro!, Pure.intro!]: "(a, a) \<in> r\<^sup>*\<^sup>r"
  | rtranclr_into_rtrancl [Pure.intro]: "(a, b) \<in> r \<Longrightarrow> (b, c) \<in> r\<^sup>*\<^sup>r \<Longrightarrow> (a, c) \<in> r\<^sup>*\<^sup>r"

definition deterministic :: "('a \<times> 'a) set \<Rightarrow> bool" 
  where "deterministic R \<equiv> \<forall> x y z. (x, y) \<in> R \<and> (x, z) \<in> R \<longrightarrow> y = z"

lemma rtrancl_split:
  assumes "(y, z) \<in> R\<^sup>*"
      and "(x, y) \<in> R"
    shows "\<exists> y'. (x, y') \<in> R\<^sup>* \<and> (y', z) \<in> R"
  by (meson assms(1) assms(2) rtrancl_into_trancl2 tranclD2)

lemma rtranclr_split:
  assumes ev: "(x, y) \<in> R\<^sup>*\<^sup>r"
      and st: "(y, z) \<in> R"
    shows "\<exists> y'. (x, y') \<in> R \<and> (y', z) \<in> R\<^sup>*\<^sup>r"
  using ev st
proof (induction rule: rtranclr.induct)
  case (rtranclr_refl a)
  then show ?case
    by blast
next
  case (rtranclr_into_rtrancl a b c)
  then show ?case
    by (meson rtranclr.rtranclr_into_rtrancl) 
qed


lemma rtranclr_rtrancl_same:
  shows "R\<^sup>* = R\<^sup>*\<^sup>r"
proof auto
  fix a b
  assume "(a, b) \<in> R\<^sup>*"
  then show "(a, b) \<in> R\<^sup>*\<^sup>r"
  proof (induction rule: rtrancl.induct)
    case (rtrancl_refl a)
    then show ?case
      by auto
  next
    case (rtrancl_into_rtrancl a b c)
    then show ?case
      by (meson rtranclr.simps rtranclr_split)
  qed
next
  fix a b
  assume "(a, b) \<in> R\<^sup>*\<^sup>r"
  then show "(a, b) \<in> R\<^sup>*"
    apply (induction rule: rtranclr.induct)
     apply blast
    by simp
qed


end