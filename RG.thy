theory RG
  imports Main Language TypeSystem Reverse_Transitive_Closure EvalNondet
begin

type_synonym ('level, 'val) rely = "('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool"
type_synonym ('level, 'val) guar = "('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool"
type_synonym ('level, 'val) rg_loc = "('level, 'val) local_state \<times> ('level, 'val) rely \<times> ('level, 'val) guar"

definition get_rely :: "('level, 'val) rg_loc \<Rightarrow> ('level, 'val) rely" where
  "get_rely = fst \<circ> snd"

definition get_guar :: "('level, 'val) rg_loc \<Rightarrow> ('level, 'val) guar" where
  "get_guar = snd \<circ> snd"

definition get_loc :: "('level, 'val) rg_loc \<Rightarrow> ('level, 'val) local_state" where
  "get_loc = fst"

definition get_com :: "('level, 'val) rg_loc \<Rightarrow> ('level, 'val) com" where
  "get_com = Language.com \<circ> fst"

context lang
begin

fun next_states :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state set" where
  "next_states l g = {g'. \<exists> l'. (l, g) \<leadsto> (l', g')}"

definition ann_stable :: "('level, 'val) rely \<Rightarrow> ('level, 'val) ann \<Rightarrow> bool" where
  "ann_stable R P \<equiv> \<forall> g g'. (g \<Turnstile>\<^sub>P P \<and> R g g') \<longrightarrow> g' \<Turnstile>\<^sub>P P"

fun all_anns :: "('level, 'val) com \<Rightarrow> ('level, 'val) ann set" where
  "all_anns Skip = {}" |
  "all_anns ({A} x ::= e) = {A}" |
  "all_anns ({A} x :D= e) = {A}" |
  "all_anns ({A} x <- l') = {A}" |
  "all_anns (Out A l e) = {A}" |
  "all_anns (DOut A l e) = {A}" |
  "all_anns (Acquire A loc) = {A}" |
  "all_anns (Release A loc) = {A}" |
  "all_anns (Language.If P e c\<^sub>1 c\<^sub>2) = {P} \<union> all_anns c\<^sub>1 \<union> all_anns c\<^sub>2" |
  "all_anns (Language.While A e I c) = {A, I} \<union> all_anns c" |
  "all_anns (c\<^sub>1 ;; c\<^sub>2) = all_anns c\<^sub>1 \<union> all_anns c\<^sub>2"

definition preds_stable :: "('level, 'val) rely \<Rightarrow> ('level, 'val) local_state \<Rightarrow> bool" where
  "preds_stable R l \<equiv> \<forall> A \<in> all_anns (com l). ann_stable R A"

definition guar_guaranteed :: "('level, 'val) guar \<Rightarrow> thread_id \<Rightarrow> ('level, 'val) com \<Rightarrow> ('level, 'val) ann \<Rightarrow> bool" where
  "guar_guaranteed G t c P \<equiv> \<forall> g g'. g \<Turnstile>\<^sub>P P \<and> 
     g' \<in> next_states \<lparr>com = c, tid = t\<rparr> g \<longrightarrow> G g g'"

definition preserves_guars :: "('level, 'val) guar \<Rightarrow> ('level, 'val) local_state \<Rightarrow> bool" where
  "preserves_guars G l\<^sub>1 \<equiv>
    (\<forall> g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3. (l\<^sub>1, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2) \<and> (l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3) \<longrightarrow>
                      (g\<^sub>2 \<Turnstile>\<^sub>P active_ann l\<^sub>2 \<longrightarrow> G g\<^sub>2 g\<^sub>3))"


definition anns_preserved :: "('level, 'val) local_state \<Rightarrow> bool" where
  "anns_preserved l\<^sub>1 \<equiv> 
    (\<forall> g\<^sub>1 l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3. (l\<^sub>1, g\<^sub>1) \<leadsto>\<^sub>n\<^sup>* (l\<^sub>2, g\<^sub>2) \<and> 
                      g\<^sub>2 \<Turnstile>\<^sub>P active_ann l\<^sub>2 \<and> 
                      (l\<^sub>2, g\<^sub>2) \<leadsto> (l\<^sub>3, g\<^sub>3) \<longrightarrow> g\<^sub>3 \<Turnstile>\<^sub>P active_ann l\<^sub>3)"

(* Only using fun for destructuring the tuple here *)
fun rg_sat :: "('level, 'val) rg_loc \<Rightarrow> bool" where
  "rg_sat (l, R, G) = (preds_stable R l \<and> preserves_guars G l \<and> anns_preserved l)"
declare rg_sat.simps[simp del]

text {* Rely and guarantee conditions are compatible if the rely condition
of each thread is implied by the conjunction of the guarantee conditions
of all other threads. *}
definition globally_compat :: "('level, 'val) rg_loc list \<Rightarrow> bool" where
  "globally_compat rgls \<equiv> \<forall> i < length rgls. \<forall> j < length rgls. i \<noteq> j \<longrightarrow>
     (\<forall> g g'. get_guar (rgls ! j) g g' \<longrightarrow> get_rely (rgls ! i) g g')"


lemma eval_all_anns_subset:
  assumes "(l, g) \<leadsto> (l', g')"
  shows "all_anns (com l') \<subseteq> all_anns (com l)"
  using assms
  by (induction rule: eval.induct, auto)

lemma eval_preserves_preds_stable:
  assumes "(l, g) \<leadsto> (l', g')"
      and "preds_stable R l"
    shows "preds_stable R l'"
  using assms eval_all_anns_subset
  unfolding preds_stable_def
  by blast

lemma eval_preserves_preds_stable_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and init: "\<And> i. i < length ls \<Longrightarrow> preds_stable (Rs ! i) (ls ! i)"
      and "ls = map get_loc rgls"
      and i_len: "i < length ls'"
    shows "preds_stable (Rs ! i) (ls' ! i)"
proof -
  let ?f = "\<lambda> i. Rs ! i"
  let ?P = "\<lambda> R l. preds_stable R l"
  from init i_len ev have "?P (?f i) (ls' ! i)"
    using eval_preserves_preds_stable 
    using lift_idx_property_global[where P = ?P and f = ?f]
    by force
  thus ?thesis by auto
qed

lemma eval_preserves_preds_stable_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and init: "\<And> i. i < length ls \<Longrightarrow> preds_stable (Rs ! i) (ls ! i)"
      and i_len: "i < length ls'"
    shows "preds_stable (Rs ! i) (ls' ! i)"
proof -
  let ?f = "\<lambda> i. Rs ! i"
  let ?P = "\<lambda> R l. preds_stable R l"
 from init i_len ev have "?P (?f i) (ls' ! i)"
    using eval_preserves_preds_stable 
    using lift_idx_property_multi[where P = ?P and f = ?f]
    by force
  thus ?thesis by auto
qed

lemma eval_preserves_guars:
  assumes "(l, g) \<leadsto> (l', g')"
      and "preserves_guars G l"
    shows "preserves_guars G l'"
  using eval_preserves_forall_prop[where Q = "(\<lambda> l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3. g\<^sub>2 \<Turnstile>\<^sub>P active_ann l\<^sub>2 \<longrightarrow> G g\<^sub>2 g\<^sub>3)"
        and P = "preserves_guars G" and l\<^sub>1 = l and l\<^sub>2 = l' and g\<^sub>1 = g and g\<^sub>2 = g'] assms
  unfolding preserves_guars_def
  by auto  

lemma eval_preserves_guars_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and init: "\<And> i. i < length ls \<Longrightarrow> preserves_guars (Gs ! i) (ls ! i)"
      and i_len: "i < length ls'"
    shows "preserves_guars (Gs ! i) (ls' ! i)"
proof -
  define f where "f = (\<lambda> i. Gs ! i)"
  define P where "P = (\<lambda> G l. preserves_guars G l)"
  from ev init i_len have "P (f i) (ls' ! i)"
    using eval_preserves_guars
    using lift_idx_property_global[where P = P and f = f]
    using \<open>P \<equiv> preserves_guars\<close> \<open>f \<equiv> (!) Gs\<close> by auto
  thus ?thesis unfolding f_def P_def by auto
qed

lemma eval_preserves_guars_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and init: "\<And> i. i < length ls \<Longrightarrow> preserves_guars (Gs ! i) (ls ! i)"
      and i_len: "i < length ls'"
    shows "preserves_guars (Gs ! i) (ls' ! i)"
proof -
  define f where "f = (\<lambda> i. Gs ! i)"
  define P where "P = (\<lambda> G l. preserves_guars G l)"
  from ev init i_len have "P (f i) (ls' ! i)"
    using eval_preserves_guars
    using lift_idx_property_multi[where P = P and f = f]
    using \<open>P \<equiv> preserves_guars\<close> \<open>f \<equiv> (!) Gs\<close> by auto
  thus ?thesis unfolding f_def P_def by auto
qed

lemma eval_preserves_anns_preserved:
  assumes "(l, g) \<leadsto> (l', g')"
      and "anns_preserved l"
    shows "anns_preserved l'"
  using eval_preserves_forall_prop
    [where Q = "(\<lambda> l\<^sub>2 g\<^sub>2 l\<^sub>3 g\<^sub>3. g\<^sub>2 \<Turnstile>\<^sub>P active_ann l\<^sub>2 \<longrightarrow> g\<^sub>3 \<Turnstile>\<^sub>P active_ann l\<^sub>3)"
        and P = "anns_preserved" and l\<^sub>1 = l and l\<^sub>2 = l' and g\<^sub>1 = g and g\<^sub>2 = g'] assms
  unfolding anns_preserved_def
  by blast

lemma anns_preserved_step_preserves_preds:
  assumes ev: "(l, g) \<leadsto> (l', g')"
      and anns: "anns_preserved l"
      and sat: "g \<Turnstile>\<^sub>P active_ann l"
    shows "g' \<Turnstile>\<^sub>P active_ann l'"
  using assms unfolding anns_preserved_def
  by blast

lemma eval_anns_preserved_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and init: "list_all anns_preserved ls"
    shows "list_all anns_preserved ls'"
  using assms eval_preserves_anns_preserved
  using lift_property_global[where P = anns_preserved]
  by auto 

lemma eval_anns_preserved_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and init: "list_all anns_preserved ls"
    shows "list_all anns_preserved ls'"
  using assms eval_preserves_anns_preserved
  using lift_property_multi[where P = anns_preserved]
  by blast

lemma eval_preserves_rg_sat:
  "(l, g) \<leadsto> (l', g') \<Longrightarrow> rg_sat (l, R, G) \<Longrightarrow> rg_sat (l', R, G)"
  using eval_preserves_anns_preserved eval_preserves_guars local.eval_preserves_preds_stable
  by (auto simp: rg_sat.simps)

lemma eval_preserves_rg_sat_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and init: "\<And> i. i < length ls \<Longrightarrow> rg_sat (ls ! i, Rs ! i, Gs ! i)"
      and i_len: "i < length ls'"
    shows "rg_sat (ls' ! i, Rs ! i, Gs ! i)"
proof -
  let ?P = "\<lambda> RG l. rg_sat (l, fst RG, snd RG)"
  let ?f = "\<lambda> i. (Rs ! i, Gs ! i)"
  from ev init i_len  have "?P (?f i) (ls' ! i)"
    using lift_idx_property_global[where P = ?P and f = ?f]
    using eval_preserves_rg_sat by force
  thus ?thesis
    by auto
qed

lemma eval_preserves_rg_sat_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and init: "\<And> i. i < length ls \<Longrightarrow> rg_sat (ls ! i, Rs ! i, Gs ! i)"
      and i_len: "i < length ls'"
    shows "rg_sat (ls' ! i, Rs ! i, Gs ! i)"
proof -
  let ?P = "\<lambda> RG l. rg_sat (l, fst RG, snd RG)"
  let ?f = "\<lambda> i. (Rs ! i, Gs ! i)"
  from ev init i_len  have "?P (?f i) (ls' ! i)"
    using lift_idx_property_multi[where P = ?P and f = ?f]
    using eval_preserves_rg_sat by force
  thus ?thesis
    by auto
qed

lemma active_ann_in_all_anns:
  "active_ann_com c = ann\<^sub>0 \<or> active_ann_com c \<in> all_anns c"
  by (induction c, auto)

lemma preds_stable_anns_stable:
  assumes ps: "preds_stable R l"
    shows "ann_stable R (active_ann l)"
proof -
  from ps have "active_ann l = ann\<^sub>0 \<or> active_ann l \<in> all_anns (com l)"
    using active_ann_in_all_anns
    unfolding active_ann_def by auto
  then show ?thesis
  proof
    assume "active_ann l = ann\<^sub>0"
    thus ?thesis unfolding ann\<^sub>0_def ann_stable_def satisfies_ann_preds_def by simp
  next
    assume "active_ann l \<in> all_anns (com l)"
    with ps show ?thesis
      unfolding active_ann_def preds_stable_def by auto
  qed
qed

lemma eval_preds_stable_anns:
  assumes ev: "(l, g) \<leadsto> (l', g')"
    and stable: "preds_stable R l"
  shows "ann_stable R (active_ann l')"
  using ev eval_preserves_preds_stable preds_stable_anns_stable stable by auto

lemma rgl_decomp:
  "rgl = (get_loc rgl, get_rely rgl, get_guar rgl)"
  unfolding get_loc_def get_rely_def get_guar_def
  by simp

lemma eval_preds_stable_eval_anns_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
  assumes init: "\<And> i. i < length ls \<Longrightarrow> preds_stable (Rs ! i) (ls ! i)"
  assumes i_len: "i < length ls'"
  shows "ann_stable (Rs ! i) (active_ann (ls' ! i))"
proof -
  from ev i_len init have "preds_stable (Rs ! i) (ls' ! i)"
    using eval_preserves_preds_stable_multi
    by blast
  thus ?thesis using preds_stable_anns_stable
    by auto
qed

lemma guar_guaranteed:
  assumes "preserves_guars G l"
  shows "guar_guaranteed G (tid l) (com l) (active_ann l)"
  using assms  unfolding preserves_guars_def guar_guaranteed_def
  apply simp
  by (metis (full_types) eval_nondet.env local_state.surjective old.unit.exhaust r_into_rtrancl)

lemma guars_guaranteed_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and sat: "\<And> i. i < length ls \<Longrightarrow> preserves_guars (Gs ! i) (ls ! i)"
      and i_len: "i < length ls"
    shows "guar_guaranteed (Gs ! i) (tid (ls' ! i)) 
                  (com (ls' ! i)) (active_ann (ls' ! i))"
proof -
  from ev have "\<And> i. i < length ls \<Longrightarrow> preserves_guars (Gs ! i) (ls' ! i)"
    by (metis eval_preserves_guars_multi map_eq_imp_length_eq sat tid_constant_multi)
  with guar_guaranteed i_len show ?thesis
    by blast
qed

lemma eval_preserves_length:
  "(ls, g, sched) \<rightarrow> (ls', g', sched') \<Longrightarrow> length ls' = length ls"
  by (induction rule: global_eval.induct, auto)

lemma eval_preserves_length_multi:
  "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched') \<Longrightarrow> length ls' = length ls"
  by (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)],
      auto simp: eval_preserves_length)

lemma rg_sat_sound:
  assumes sat: "list_all rg_sat rgls"
      and compat: "globally_compat rgls"
      and anns_initially_sat: "list_all (\<lambda> l. g \<Turnstile>\<^sub>P active_ann l) (map get_loc rgls)"
  shows "anns_satisfied (map get_loc rgls, g, sched)"
proof (clarsimp)
  fix ls' g' sched'
  assume ev: "(map get_loc rgls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"

  define ls where [simp]: "ls = map get_loc rgls"
  from ev have "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
    by auto
  then show "list_all (\<lambda>l. g' \<Turnstile>\<^sub>P active_ann l) ls'"
    using sat ls_def anns_initially_sat
  proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
    case (rtrancl_refl ls g sched)
    then show ?case 
      by auto
  next
    case (rtrancl_into_rtrancl ls g sched ls' g' sched' ls'' g'' sched'')
    hence sat': "list_all (\<lambda>l. g' \<Turnstile>\<^sub>P active_ann l) ls'"
      by auto
    have "\<And> i. i < length ls'' \<Longrightarrow> g'' \<Turnstile>\<^sub>P active_ann (ls'' ! i)"
    proof -
      fix i
      assume len: "i < length ls''"
      define G\<^sub>i where [simp]: "G\<^sub>i = get_guar (rgls ! i)"
      define R where [simp]: "R = map get_rely rgls ! i"
      define P' where [simp]: "P' = active_ann (ls' ! i)"
      define P'' where [simp]: "P'' = active_ann (ls'' ! i)"
      from `(ls', g', sched') \<rightarrow> (ls'', g'', sched'')` show "?thesis i"
      proof (cases rule: global_eval.cases)
        case (global_step n l'')

        show ?thesis 
        proof (cases "n = i")
          case True (* step in the same thread *)
          from rtrancl_into_rtrancl have "list_all anns_preserved ls"
            using rg_sat.simps
            by (metis rgl_decomp length_map list_all_length nth_map)
          then have "list_all anns_preserved ls'"
            using rtrancl_into_rtrancl eval_anns_preserved_multi
            by auto
          with global_step show ?thesis 
            using  sat'
            by (metis True anns_preserved_step_preserves_preds len length_list_update list_all_length nth_list_update_eq)
        next
          case False
          define G\<^sub>n where [simp]: "G\<^sub>n = map get_guar rgls ! n"
          from global_step have n_len: "n < length ls''" by (auto simp: next_thread.simps)
          with rtrancl_into_rtrancl have 
            "guar_guaranteed G\<^sub>n (tid (ls' ! n))
             (com (ls' ! n)) (active_ann (ls' ! n))"
            using guars_guaranteed_multi[where Gs = "map get_guar rgls"]
            using local.global_step(5) n_len
            (* FIXME: slow smt proof *)
            by (smt G\<^sub>n_def Pair_inject eval_preserves_length_multi rg_sat.elims(2) rgl_decomp length_list_update length_map list_all_length nth_map)
          moreover with global_step have "g'' \<in> next_states (ls' ! n) g'"
            by auto
          moreover from sat' have "g' \<Turnstile>\<^sub>P active_ann (ls' ! n)"
            using n_len
            by (simp add: list_all_length local.global_step(5))
          ultimately have "G\<^sub>n g' g''"
            using global_step rtrancl_into_rtrancl
            unfolding guar_guaranteed_def next_states.simps
            by (metis (mono_tags, lifting) \<open>g'' \<in> next_states (ls' ! n) g'\<close> \<open>guar_guaranteed G\<^sub>n (tid (ls' ! n)) (com (ls' ! n)) (active_ann (ls' ! n))\<close> guar_guaranteed_def local_state.surjective old.unit.exhaust)
          moreover have "length rgls = length ls''"
            using n_len len False eval_preserves_length_multi eval_preserves_length
            using rtrancl_into_rtrancl
            by auto
          ultimately have "R g' g''" using compat
            unfolding globally_compat_def G\<^sub>n_def R_def
            using n_len len False
            by simp
          moreover from rtrancl_into_rtrancl have "ann_stable R P'"
            unfolding rg_sat.simps
            using eval_preds_stable_anns len
            apply simp
            by (smt eval_preds_stable_eval_anns_multi rg_sat.simps rgl_decomp length_list_update length_map list_all_length local.global_step(5) nth_map)
          moreover from sat' have "g' \<Turnstile>\<^sub>P P'"
            using len
            by (simp add: list_all_length local.global_step(5))
          ultimately have "g'' \<Turnstile>\<^sub>P P''"
            unfolding ann_stable_def
            by (simp add: False local.global_step(5))
          then show ?thesis
            by auto
        qed
      next
        case (global_wait n)
        then show ?thesis
          using len list_all_length rtrancl_into_rtrancl.IH rtrancl_into_rtrancl.prems(2) rtrancl_into_rtrancl.prems(3) sat by fastforce 
      qed
    qed
    with rtrancl_into_rtrancl show ?case
      by (auto simp: list_all_length)
  qed
qed

definition no_write :: "var set \<Rightarrow> ('level, 'val) global_state \<Rightarrow> ('level, 'val) global_state \<Rightarrow> bool" where
  "no_write Xs g g' = (\<forall> x \<in> Xs. mem g x = mem g' x)"

end
end