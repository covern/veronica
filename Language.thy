theory Language
  imports Main HOL.String HOL.Orderings "~~/src/HOL/Library/Prefix_Order"
          Reverse_Transitive_Closure Stream
          "~~/src/HOL/Eisbach/Eisbach"
begin

declare [[ smt_oracle = false ]]
section {* Language definition *}

text {* This module defines a simple imperative language with shared
  memory concurrency using locks.  *}

type_synonym var = string

(* Each level in the lattice corresponds to a stream of inputs *)
type_synonym ('level, 'val) env = "'level \<Rightarrow> 'val stream"

text {*
  Expressions. For now an expression is either a variable x, a value v or a shallowly
  embedded binary or unary operation.
*}
datatype 'val exp = Var var 
  | Val 'val
  | UnOp "'val \<Rightarrow> 'val" "'val exp"
  | BinOp "('val \<Rightarrow> 'val \<Rightarrow> 'val)" "'val exp" "'val exp"

text {* Events are either output events, input event, or declassification events.

Output and declassification events also carry the expression that generated the
corresponding value to express relationships between different events in the trace
in the security policies later on.
*}
datatype ('level, 'val) event = Output 'level 'val "'val exp"
  | Input 'level 'val
  | Declassification 'level 'val "'val exp" (* Declassification l v e represents declassifying
                                       expression e that has value v to level l. e's level
                                       can be inferred from the variables in it, since
                                       variable levels don't change for now. *)

type_synonym 'val mem = "var \<Rightarrow> 'val"

type_synonym ('level, 'val) trace = "('level, 'val) event list"

text {* To avoid the proliferation of type variables everywhere, we
model locks as natural numbers for now. *}
type_synonym lock = nat

text {* Similarly, a thread is identified by a natural number,
acting as an index into the thread pool. *}
type_synonym thread_id = nat

text {* Each lock is held by at most one thread, so we represent what locks are held by what thread
as a mapping to @{typ "thread_id option"}. *}

type_synonym "lock_state" = "lock \<Rightarrow> thread_id option"

record ('level, 'val) global_state =
  mem :: "'val mem"
  env :: "('level, 'val) env"
  trace :: "('level, 'val) trace"
  lock_state :: lock_state

type_synonym ('level, 'val) pred = "('level, 'val) global_state \<Rightarrow> bool"

record ('level, 'val) ann = 
  preds :: "('level, 'val) pred set"

definition ann\<^sub>0 :: "('level, 'val) ann" where
  "ann\<^sub>0 = \<lparr>preds = {}\<rparr>"

text {* We adopt in the language a form of copying declassification. The idea is to
   prevent having to declassify @{term H} \emph{variables} and instead to allow us to
   declassify (only) @{term H} \emph{data}. Then we can impose a global invariant that
   all such declassifications must assign to a @{term L} variable. 

Additionally, each command contains an annotation on the global state that is asserted
to hold whenever the command is reached.
*}
datatype ('level, 'val) com = Skip
  | Assign "('level, 'val) ann" var "'val exp" ("{_} _ ::= _" [0, 90, 90] 80)
  | DAssign "('level, 'val) ann" var "'val exp" ("{_} _ :D= _" [0, 90, 90] 80)
  | Out "('level, 'val) ann" 'level "'val exp"
  | DOut "('level, 'val) ann" 'level "'val exp"
  | In "('level, 'val) ann" var 'level ("{_} _ <- _" [0, 90, 90] 80)
  | If "('level, 'val) ann" "'val exp" "('level, 'val) com" "('level, 'val) com"
  | While "('level, 'val) ann" "'val exp" "('level, 'val) ann" "('level, 'val) com"
  | Seq "('level, 'val) com" "('level, 'val) com" (infixr ";;" 70)
  | Acquire "('level, 'val) ann" "lock"
  | Release "('level, 'val) ann" "lock"

record ('level, 'val) local_state =
  com :: "('level, 'val) com"
  tid :: "thread_id"

type_synonym ('level, 'val) conf = "('level, 'val) local_state * ('level, 'val) global_state"

text {*
  A scheduler is an infinite list of indices of threads to schedule, modeled as a function
  from naturals to naturals. Note that currently the thread pool is constant, so there's
  no need to pass the number of threads to the scheduler in the beginning.
*}
type_synonym sched = "nat \<Rightarrow> nat"

text {*
  A global configuration consists of a list of commands, a shared memory,
  and a scheduler.
*}
type_synonym ('level, 'val) global_conf = "('level, 'val) local_state list * ('level, 'val) global_state * sched"


(* For now we use Isabelle predicates to model declassification predicates;
   once we know what kind of predicates we want and for what we can
   compositionally enforce security, we can pick some restricted
   syntactic representation. The first level argument to a predicate
   denotes the level the information is declassified to.

   To accommodate delimited release, we also pass the command that triggered
   the declassification to the predicate, so that it can require the
   annotation on it is strong enough. *)
type_synonym ('level, 'val) decl_pred = 
  "'level \<Rightarrow> ('level, 'val) global_state \<Rightarrow> 'val \<Rightarrow> ('level, 'val) com \<Rightarrow> bool"

(* To simplify things, we assume that each level is associated with a fixed
   trace predicate governing allowed kinds of declassification *from* that level
   are allowed. *)
primrec
  eval_exp :: "'val exp \<Rightarrow> 'val mem \<Rightarrow> 'val"
where
  "eval_exp (Var x) m = (m x)" |
  "eval_exp (Val v) m = v" |
  "eval_exp (UnOp f e) m = f (eval_exp e m)" |
  "eval_exp (BinOp opf e f) m = (opf (eval_exp e m) (eval_exp f m))"

(* This is a helper function for extracting the relevant part of a command to pass
   to the declassification predicate. *)
fun head_com :: "('level, 'val) com \<Rightarrow> ('level, 'val) com" where
  "head_com Skip = Skip" |
  "head_com ({A} x ::= e) = {A} x ::= e" |
  "head_com ({A} x :D= e) = {A} x :D= e" |
  "head_com ({A} x <- l') = {A} x <- l'" |
  "head_com (Out A l' e) = Out A l' e" |
  "head_com (DOut A l' e) = DOut A l' e" |
  "head_com (Acquire A loc) = Acquire A loc" |
  "head_com (Release A loc) = Release A loc" |
  "head_com (If A e c\<^sub>1 c\<^sub>2) = If A e Skip Skip" |
  "head_com (c\<^sub>1 ;; c\<^sub>2) = head_com c\<^sub>1" |
  "head_com (While A e I c) = While A e I Skip"

(* We keep the type of values abstract here to remain general enough to handle
  arrays and higher-order functions for example. *)
locale lang =
  fixes \<L> :: "var \<Rightarrow> 'level::complete_lattice option"
  fixes \<D> :: "'level \<Rightarrow> ('level, 'val) decl_pred"
  fixes level_rename_param :: "'level" (* Instantiating or combining locales seems to weirdly rename
  types and renaming them back using "for" seems to make Isabelle forget that the instantiation
  of a parameter may be a constant. We add an unused fake parameter here to
  allow renaming the type variable more easily. Reader, if you have a better understanding
  of the type variable renaming mechanism behind locales feel free to change this to something
  more sensible. *)
  fixes val_rename_param :: "'val"
  fixes val_true :: "'val \<Rightarrow> bool" (* For conditionals *)
  fixes tt ff :: "'val"
  assumes tt_true: "val_true tt"
      and ff_false: "\<not> val_true ff"

context lang
begin

inductive_set eval :: "('level, 'val) conf rel" and
  eval_abv :: "('level, 'val) conf \<Rightarrow> ('level, 'val) conf \<Rightarrow> bool" (infix "\<leadsto>" 60)
  where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval" |
  eval_assign: "\<lbrakk> com l = {A} x ::= e \<rbrakk> \<Longrightarrow> 
   (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g \<lparr>mem := (mem g)(x := eval_exp e (mem g))\<rparr>)" |
  eval_dassign: "\<lbrakk> com l = {A} x :D= e ; \<L> x = Some l' \<rbrakk> \<Longrightarrow>
   (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g \<lparr>mem := (mem g)(x := eval_exp e (mem g)),
                           trace := trace g @ [Declassification l' (eval_exp e (mem g)) e]\<rparr>)" |
  eval_out: "\<lbrakk> com l = Out A lev e \<rbrakk> \<Longrightarrow>
   (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g \<lparr>trace := trace g @ [Output lev (eval_exp e (mem g)) e ] \<rparr>)" |
  eval_dout: "\<lbrakk> com l = DOut A lev e \<rbrakk> \<Longrightarrow>
   (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g \<lparr>trace := trace g @ [Declassification lev (eval_exp e (mem g)) e ] \<rparr>)" |
  eval_in: "\<lbrakk> com l = {A} x <- lev; v = lhd (env g lev) \<rbrakk> \<Longrightarrow>
   (l, g) \<leadsto> 
   (l \<lparr>com := Skip\<rparr>, g \<lparr>env := (env g)(lev := ltl (env g lev)), 
                       mem := (mem g)(x := v),
                       trace := trace g @ [Input lev v] \<rparr>)" |
  eval_iftrue: "\<lbrakk> com l = If A e c\<^sub>1 c\<^sub>2 ; eval_exp e (mem g) = n ; val_true n \<rbrakk> \<Longrightarrow> 
    (l, g) \<leadsto> (l \<lparr>com := c\<^sub>1\<rparr>, g)" |
  eval_iffalse: "\<lbrakk> com l = If A e c\<^sub>1 c\<^sub>2 ; \<not> (val_true (eval_exp e (mem g))) \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l \<lparr>com := c\<^sub>2\<rparr>, g)" |
  eval_seq: "\<lbrakk> com l = c\<^sub>1 ;; c\<^sub>2 ; (l \<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g') ; com l' \<noteq> Skip \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l' \<lparr>com := com l' ;; c\<^sub>2\<rparr>, g')" |
  eval_seqskip: "\<lbrakk> com l = c\<^sub>1 ;; c\<^sub>2 ; (l \<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l', g') ; com l' = Skip \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l' \<lparr>com := c\<^sub>2\<rparr>, g')" | 
  eval_whiletrue: "\<lbrakk> com l = While A e I c ; val_true (eval_exp e (mem g)) \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l \<lparr>com := c ;; While I e I c\<rparr>, g)" |
  eval_whilefalse: "\<lbrakk> com l = While A e I c ; \<not> (val_true (eval_exp e (mem g))) \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g)" |
  eval_acquire: "\<lbrakk> com l = Acquire A loc ; lock_state g loc = None \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g\<lparr>lock_state := (lock_state g)(loc := Some (tid l))\<rparr>)" |
  eval_release: "\<lbrakk> com l = Release A loc ; lock_state g loc = Some (tid l) \<rbrakk> \<Longrightarrow>
    (l, g) \<leadsto> (l \<lparr>com := Skip\<rparr>, g\<lparr>lock_state := (lock_state g)(loc := None)\<rparr>)"

text {* Locking semantics rely on threads having a consistent idea of their own thread id, so we
need a well-formedness condition on global configurations; we use global configurations even though
just a list of threads would be enough at this point, because we may need more well-formedness
constraints on other parts as well later on and I don't want to change the types everywhere when
that happens.

Another way to avoid this would have been to pass thread-specific information,
such as the thread id to the local @{term eval} relation, though that
seems messier to me. *}

lemma eval_skip_elim:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = Skip \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_assign_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = {A} x ::= e ;
     (l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow> g' = g \<lparr>mem := (mem g)(x := eval_exp e (mem g))\<rparr> \<Longrightarrow> P) \<rbrakk> \<Longrightarrow>
   P"
  by (induction rule: eval.induct, auto)

lemma eval_dassign_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = {A} x :D= e ;
     (\<And> lev. l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow>
      \<L> x = Some lev \<Longrightarrow>
      g' = g \<lparr>mem := (mem g)(x := eval_exp e (mem g)),
               trace := trace g @ [Declassification lev (eval_exp e (mem g)) e]\<rparr> \<Longrightarrow> P) \<rbrakk> \<Longrightarrow>
   P"
  by (induction rule: eval.induct, auto)

lemma eval_out_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = Out A lev e ;
     l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow> 
     g' = g \<lparr>trace := trace g @ [Output lev (eval_exp e (mem g)) e]\<rparr> \<Longrightarrow> P  \<rbrakk> \<Longrightarrow>
 P"
  by (induction rule: eval.induct, auto)

lemma eval_dout_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = DOut A lev e ;
     l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow> 
     g' = g \<lparr>trace := trace g @ [Declassification lev (eval_exp e (mem g)) e]\<rparr> \<Longrightarrow> P  \<rbrakk> \<Longrightarrow>
 P"
  by (induction rule: eval.induct, auto)

lemma eval_in_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = {A} x <- lev ;
     l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow>
     g' = g \<lparr>env := (env g)(lev := ltl (env g lev)),
             mem := (mem g)(x := lhd (env g lev)),
             trace := trace g @ [Input lev (lhd (env g lev))]\<rparr> \<Longrightarrow>
     P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_if_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = If A e c\<^sub>1 c\<^sub>2 ;
     l' = l \<lparr>com := c\<^sub>1\<rparr> \<Longrightarrow>
       val_true (eval_exp e (mem g)) \<Longrightarrow>
       g' = g \<Longrightarrow>
       P ;
       l' = l \<lparr>com := c\<^sub>2\<rparr> \<Longrightarrow>
       \<not> val_true (eval_exp e (mem g)) \<Longrightarrow>
       g' = g \<Longrightarrow>
       P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_seq_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = c\<^sub>1 ;; c\<^sub>2 ;
     \<And> g'' l''.
         (l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l'', g'') \<Longrightarrow>
         com l'' = Skip \<Longrightarrow>
         l' = l'' \<lparr>com := c\<^sub>2\<rparr> \<Longrightarrow>
         g' = g'' \<Longrightarrow>
         P ;
     \<And> g'' l''.
         (l\<lparr>com := c\<^sub>1\<rparr>, g) \<leadsto> (l'', g'') \<Longrightarrow>
         com l'' \<noteq> Skip \<Longrightarrow>
         l' = l'' \<lparr>com := com l'' ;; c\<^sub>2\<rparr> \<Longrightarrow>
         g' = g'' \<Longrightarrow>
         P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_while_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = While A e I c ;
     l' = l\<lparr>com := c ;; While I e I c\<rparr> \<Longrightarrow>
       g' = g \<Longrightarrow>
       val_true (eval_exp e (mem g)) \<Longrightarrow>
       P ;
     l' = l\<lparr>com := Skip\<rparr> \<Longrightarrow>
       g' = g \<Longrightarrow>
       \<not> val_true (eval_exp e (mem g)) \<Longrightarrow>
       P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_whiletrue_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = While A e I c ;
     val_true (eval_exp e (mem g)) ;
     l' = l \<lparr>com := c ;; While I e I c\<rparr> \<Longrightarrow>
       g' = g \<Longrightarrow> P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_whilefalse_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = While A e I c ;
     \<not> val_true (eval_exp e (mem g)) ;
     l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow>
       g' = g \<Longrightarrow> P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_acquire_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = Acquire A loc ;
     lock_state g loc = None \<Longrightarrow>
       l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow>
       g' = g \<lparr>lock_state := (lock_state g)(loc := Some (tid l))\<rparr> \<Longrightarrow>
       P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)

lemma eval_release_elim[elim]:
  "\<lbrakk> (l, g) \<leadsto> (l', g') ; com l = Release A loc ;
     lock_state g loc = Some (tid l) \<Longrightarrow>
       l' = l \<lparr>com := Skip\<rparr> \<Longrightarrow>
       g' = g \<lparr>lock_state := (lock_state g)(loc := None)\<rparr> \<Longrightarrow>
       P \<rbrakk> \<Longrightarrow> P"
  by (induction rule: eval.induct, auto)


lemmas eval_elims = eval_skip_elim eval_dassign_elim eval_assign_elim eval_if_elim
                    eval_seq_elim eval_while_elim eval_release_elim eval_acquire_elim
                    eval_out_elim eval_dout_elim eval_in_elim

text {* This picks the right elimination rule for evaluation
and inserts it into the context, which makes auto a lot faster
in the following rules. This is probably not good style though, since
it messes with randomly applies auto in an unconstrained way. *}
method auto_eval_elim =
  (simp ;
   (match premises in
        P: "com l = c" for l c \<Rightarrow> 
        \<open>match eval_elims in
           Q: "_ \<Longrightarrow> R \<Longrightarrow> _" for R \<Rightarrow>
              \<open>match P in
                  X: R \<Rightarrow> 
                      \<open>insert Q, auto\<close>\<close>\<close>) | auto | fail)

lemma eval_deterministic:
  assumes ev\<^sub>1: "(l, g) \<leadsto> (l\<^sub>1, g\<^sub>1)"
  assumes ev\<^sub>2: "(l, g) \<leadsto> (l\<^sub>1', g\<^sub>1')"
  shows "l\<^sub>1 = l\<^sub>1' \<and> g\<^sub>1 = g\<^sub>1'"
  using assms
proof (induction arbitrary: l\<^sub>1' rule: eval.induct)
  case (eval_assign l A x e g)
  then show ?case
    by blast
next
  case (eval_dassign l A x e g)
  then show ?case by auto
next
  case (eval_out l A lev e g)
  then show ?case by blast
next
  case (eval_dout l A lev e g)
  then show ?case by blast
next
  case (eval_in l A x lev v g)
  then show ?case by blast
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then show ?case
    using lang.eval_if_elim by auto
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then show ?case by blast
next
  case (eval_seq l c\<^sub>1 c\<^sub>2 g l' g')
  then show ?case
    apply (cases "c\<^sub>1 = Skip")
    apply simp_all
    apply auto[1]
    by auto
next
  case (eval_seqskip l c\<^sub>1 c\<^sub>2 g l' g')
  show ?case
    apply (rule eval_seq_elim[where c\<^sub>1 = c\<^sub>1 and c\<^sub>2 = c\<^sub>2 and l' = l\<^sub>1' and g' = g\<^sub>1'])
    using eval_seqskip by auto
next
  case (eval_whiletrue l A e c g)
  then show ?case by blast
next
  case (eval_whilefalse l A e c g)
  then show ?case by blast
next
  case (eval_acquire l A loc g)
  then show ?case by blast
next
  case (eval_release l A loc g)
  then show ?case by blast
qed

definition stuck :: "('level, 'val) conf \<Rightarrow> bool"
  where "stuck cnf \<equiv> \<nexists> cnf'. cnf \<leadsto> cnf'"

text {* We always follow the schedule to model a deterministic scheduler that is
  willing to spend idle time on blocked threads to prevent information leakage
  via the scheduler *}
fun next_thread :: "('level, 'val) global_conf \<Rightarrow> nat"
  where "next_thread (ls, g, sched) = lhd sched mod length ls"

declare next_thread.simps[simp del]

definition next_sched :: "sched \<Rightarrow> sched"
  where "next_sched sched = ltl sched"

text {*
  when scheduling a stuck thread, we idle for an execution step to avoid information
  leakage via the scheduler. Since the schedule is infinite, the system will always run
  forever. This makes some reasoning more straightforward so we can focus on details of
  compositionality and declassification without having to worry about extraneous
  complications.
*}

text {* Since @{text "\<Rightarrow>"} now no longer has a trace as an argument, the parsing became ambiguous, so
I changed it to @{text "\<rightarrow>"} and use @{text "\<leadsto>"} for @{term local_eval} (since @{term local_eval} is
probably gonna be used less often in lemmas, I picked the easier to type arrow for @{term
global_eval} *}
inductive_set global_eval :: "(('level, 'val) global_conf * ('level, 'val) global_conf) set"
  and global_eval_abv :: "('level, 'val) global_conf \<Rightarrow> ('level, 'val) global_conf \<Rightarrow> bool"
      ("_ \<rightarrow> _" [60, 60] 60) where
  "x \<rightarrow> y \<equiv> (x, y) \<in> global_eval" |
  global_step[intro]: "\<lbrakk> length ls > 0; n = next_thread (ls, g, sched) ;
                         (ls ! n, g) \<leadsto> (l', g') ;
                         ls' = ls[n := l'] \<rbrakk> \<Longrightarrow>
                (ls, g, sched) \<rightarrow> (ls', g', next_sched sched)" |
  global_wait[intro]: "\<lbrakk> length ls > 0; n = next_thread (ls, g, sched) ;
                         stuck (ls ! n, g) \<rbrakk> \<Longrightarrow>
                (ls, g, sched) \<rightarrow> (ls, g, next_sched sched)"

inductive_cases global_eval_elim [elim]: "(ls, g, sched) \<rightarrow> (ls', g', sched')"

abbreviation global_meval_abv :: "('level, 'val) global_conf \<Rightarrow> ('level, 'val) global_conf \<Rightarrow> bool" (infix "\<rightarrow>\<^sup>*" 50)
  where "x \<rightarrow>\<^sup>* y \<equiv> (x, y) \<in> global_eval\<^sup>*"

section "Language Properties"

lemma if_not_stuck:
  assumes "com l = If A e c\<^sub>1 c\<^sub>2"
  shows "\<exists> l' g'. (l, g) \<leadsto> (l', g')"
  using assms 
  apply (cases "val_true (eval_exp e (mem g))")
  using eval_iftrue apply blast
  using eval_iffalse by blast

lemma eval_independent_of_memory:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
      and "lock_state g\<^sub>1 = lock_state g\<^sub>1'"
  shows "\<exists> l\<^sub>2' g\<^sub>2'. (l\<^sub>1, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
  using assms
proof (induction rule: eval.induct)
  case (eval_assign l A x e g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_dassign l A x e g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_out l A lev e g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_dout l A lev e g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_in l A x lev v g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_iftrue l A e c\<^sub>1 c\<^sub>2 g n)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_iffalse l A e c\<^sub>1 c\<^sub>2 g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_seq l c\<^sub>1 c\<^sub>2 g l' g')
  then obtain l\<^sub>2' g\<^sub>2' where IH: "(l\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
    by auto
  show ?case (is "\<exists> l\<^sub>2' g\<^sub>2'. ?P l\<^sub>2' g\<^sub>2'")
  proof (cases "com l\<^sub>2' = Skip")
    case True
    hence "?P (l\<^sub>2'\<lparr>com := c\<^sub>2\<rparr>) g\<^sub>2'"
      using IH eval_seq eval.eval_seqskip
      by force
    then show ?thesis by auto
  next
    case False
    hence "?P (l\<^sub>2'\<lparr>com := com l\<^sub>2' ;; c\<^sub>2\<rparr>) g\<^sub>2'"
      using  IH eval_seq eval.eval_seq
      by force
    then show ?thesis by auto
  qed
next
  case (eval_seqskip l c\<^sub>1 c\<^sub>2 g l' g')
  then obtain l\<^sub>2' g\<^sub>2' where IH: "(l\<lparr>com := c\<^sub>1\<rparr>, g\<^sub>1') \<leadsto> (l\<^sub>2', g\<^sub>2')"
    by auto
  show ?case (is "\<exists> l g. ?P l g")
  proof (cases "com l\<^sub>2' = Skip")
    case True
    hence "?P (l\<^sub>2'\<lparr>com := c\<^sub>2\<rparr>) g\<^sub>2'"
      using IH eval_seqskip eval.eval_seqskip
      by force
    then show ?thesis by auto
  next
    case False
    hence "?P (l\<^sub>2'\<lparr>com := com l\<^sub>2' ;; c\<^sub>2\<rparr>) g\<^sub>2'"
      using  IH eval_seqskip eval.eval_seq
      by force
    then show ?thesis by auto
  qed
next
  case (eval_whiletrue l A e I c g)
  then show ?case
    apply (cases "val_true (eval_exp e (mem g\<^sub>1'))")
    using eval.eval_whiletrue eval.eval_whilefalse by blast+
next
  case (eval_whilefalse l A e I c g)
  then show ?case
    by (simp add: if_not_stuck | blast intro: eval.intros)
next
  case (eval_acquire l A loc g)
  then show ?case
    using eval.eval_acquire[where g = g\<^sub>1' and loc = loc and l = l]
    by metis
next
  case (eval_release l A loc g)
  then show ?case
    using eval.eval_release[where g = g\<^sub>1' and loc = loc and l = l]
    by metis
qed

lemma stuck_independent_of_memory:
  "lock_state g = lock_state g' \<Longrightarrow> stuck (l, g) = stuck (l, g')"
  unfolding stuck_def using eval_independent_of_memory
  by (metis old.prod.exhaust)

lemma next_sched_independent_of_memory:
  "next_thread (ls, g, sched) = next_thread (ls, g', sched)"
  by (simp add: next_thread.simps)

lemma global_eval_deterministic:
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2', g\<^sub>2', sched\<^sub>2')"
  shows "ls\<^sub>2' = ls\<^sub>2 \<and> g\<^sub>2' = g\<^sub>2 \<and> sched\<^sub>2' = sched\<^sub>2"
  using assms
proof (induction rule: global_eval.induct)
  case (global_step ls\<^sub>1 n g\<^sub>1 sched\<^sub>1 l\<^sub>2 g\<^sub>2 ls\<^sub>2)
  then show ?case
    by (metis eval_deterministic global_eval.cases stuck_def) 
next
  case (global_wait ls n g sched)
  then show ?case 
    using stuck_def stuck_independent_of_memory
    by (meson lang.global_eval.simps lang_axioms)
qed

lemma tid_constant:
  "(l, g) \<leadsto> (l', g') \<Longrightarrow> tid l' = tid l"
  by (induction rule: eval.induct, auto)

lemma tid_constant_global:
  "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<Longrightarrow> map tid ls\<^sub>1 = map tid ls\<^sub>2"
  apply (cases rule: global_eval.cases, simp_all)
  apply (metis list_update_id map_update tid_constant)
  by auto

lemma tid_constant_multi:
  "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<Longrightarrow> map tid ls\<^sub>1 = map tid ls\<^sub>2"
  apply (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  using tid_constant_global by auto

lemma lift_property_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and inv: "\<And> l g l' g'. P l \<Longrightarrow> (l, g) \<leadsto> (l', g') \<Longrightarrow> P l'"
      and init: "list_all (\<lambda> l. P l) ls"
    shows "list_all (\<lambda> l. P l) ls'"
  using assms
proof (cases rule: global_eval.cases)
  case (global_step n l')
  hence n_len: "n < length ls" using next_thread.simps by auto
  have "\<And> i. i < length ls' \<Longrightarrow> P (ls' ! i)"
  proof -
    fix i
    assume i_len: "i < length ls'"
    with global_step have i_len': "i < length ls"
      by simp
    show "?thesis i"
    proof (cases "i = n")
      case True
      with global_step have "P l'"
        using inv init
        by (metis i_len length_list_update list_all_length)
      then show ?thesis 
        using global_step True init n_len
        by simp
    next
      case False
      hence "ls' ! i = ls ! i"
        using local.global_step(5) by auto
      with i_len' init show ?thesis 
        using list_all_length 
        by auto
    qed
  qed
  then show ?thesis
    using list_all_length by auto
next
  case (global_wait n)
  then show ?thesis 
    using init list_all_length
    by auto
qed

lemma lift_property_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and inv: "\<And> l g l' g'. P l \<and> (l, g) \<leadsto> (l', g') \<Longrightarrow> P l'"
      and init: "list_all P ls"
    shows "list_all P ls'"
  using assms
  apply (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  by (auto simp: lift_property_global)

lemma lift_idx_property_global:
  assumes ev: "(ls, g, sched) \<rightarrow> (ls', g', sched')"
      and inv: "\<And> l g l' g' x. P x l \<Longrightarrow> (l, g) \<leadsto> (l', g') \<Longrightarrow> P x l'"
      and init: "\<And> i. i < length ls \<Longrightarrow> P (f i) (ls ! i)"
      and i_len: "i < length ls'"
    shows "P (f i) (ls' ! i)"
  using assms
proof (induction rule: global_eval.induct)
  case (global_step ls n g sched l' g' ls')
  then show ?case
    by (metis length_list_update nth_list_update_eq nth_list_update_neq)
next
  case (global_wait ls n g sched)
  then show ?case
    by blast
qed

lemma lift_idx_property_multi:
  assumes ev: "(ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched')"
      and inv: "\<And> l g l' g' x. P x l \<Longrightarrow> (l, g) \<leadsto> (l', g') \<Longrightarrow> P x l'"
      and init: "\<And> i. i < length ls \<Longrightarrow> P (f i) (ls ! i)"
      and i_len: "i < length ls'"
    shows "P (f i) (ls' ! i)"
  using ev init i_len
proof (induction rule: rtrancl.induct[where r = global_eval, split_format(complete)])
  case (rtrancl_refl a a b)
  then show ?case by blast
next
  case (rtrancl_into_rtrancl ls g sched ls' g' sched' ls'' g'' sched'')
  moreover hence [simp]: "length ls = length ls'"
    by (metis map_eq_imp_length_eq tid_constant_multi)
  moreover from rtrancl_into_rtrancl have [simp]: "length ls = length ls''"
    by auto
  from rtrancl_into_rtrancl(2) show ?case
    apply (cases rule: global_eval.cases)
    apply (metis \<open>length ls = length ls''\<close> calculation(6) inv nth_list_update_eq nth_list_update_neq rtrancl_into_rtrancl.IH rtrancl_into_rtrancl.prems(1) rtrancl_into_rtrancl.prems(2))
    using rtrancl_into_rtrancl.IH rtrancl_into_rtrancl.prems(1) rtrancl_into_rtrancl.prems(2) by blast
qed


(* Helper lemma for the scheduling *)
lemma plus_mod_surj:
  fixes n :: nat
  assumes lt: "x < n"
  shows "\<exists> j. (j + k) mod n = x"
  oops
text {* Sanity check: we can always perform a step, regardless of the scheduler:
*}

lemma global_step_not_stuck:
  fixes ls :: "('level, 'val) local_state list" 
  shows "length ls > 0 \<Longrightarrow> \<exists> ls' g' sched'. (ls, g, sched) \<rightarrow> (ls', g', sched')"
proof -
  assume not_empty: "length ls > 0"
  let ?i = "next_thread (ls, g, sched)"
  show ?thesis
  proof(cases "stuck (ls ! ?i, g)")
    case True
    with not_empty show ?thesis 
      by blast
  next
    case False
    with not_empty show ?thesis
      using global_step stuck_def
      by fastforce
  qed
qed


lemma eval_at_most_one_event:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  shows "trace g\<^sub>2 = trace g\<^sub>1 \<or> (\<exists> ev. trace g\<^sub>2 = trace g\<^sub>1 @ [ev])"
  using assms
  by (induction rule: eval.induct, auto)

lemma eval_at_most_one_event_global:
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  shows "trace g\<^sub>2 = trace g\<^sub>1 \<or> (\<exists> ev. trace g\<^sub>2 = trace g\<^sub>1 @ [ev])"
  using assms eval_at_most_one_event
  by (induction rule: global_eval.induct, auto)

lemma eval_at_most_one_event_len:
  assumes "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2)"
  shows "length (trace g\<^sub>2) = length (trace g\<^sub>1) \<or> length (trace g\<^sub>2) = length (trace g\<^sub>1) + 1"
  using assms
  by (induction rule: eval.induct, auto)

lemma eval_at_most_one_event_global_len:
  assumes "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  shows "length (trace g\<^sub>2) = length (trace g\<^sub>1) \<or> length (trace g\<^sub>2) = length (trace g\<^sub>1) + 1"
  using assms eval_at_most_one_event_len
  by (induction rule: global_eval.induct, auto)

lemma trace_monotonic:
  "(l\<^sub>1, g\<^sub>1) \<leadsto> (l\<^sub>2, g\<^sub>2) \<Longrightarrow> trace g\<^sub>1 \<le> trace g\<^sub>2"
  by (induction rule: eval.induct, auto)

lemma trace_monotonic_global:
  "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow> (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<Longrightarrow> trace g\<^sub>1 \<le> trace g\<^sub>2"
  by (induction rule: global_eval.induct, auto simp: trace_monotonic)

lemma sublist_index:
  "t \<le> t' \<Longrightarrow> i < length t \<Longrightarrow> t ! i = t' ! i"
  by (metis Prefix_Order.prefixE nth_append)

end

section {* Annotation on commands *}

definition satisfies_ann_preds :: "('level, 'val) global_state \<Rightarrow> ('level, 'val) ann \<Rightarrow> bool" (infix "\<Turnstile>\<^sub>P" 60) where
  "g \<Turnstile>\<^sub>P A \<equiv> \<forall> p \<in> preds A. p g"

text {* It remains to define what it means for annotations to hold throughout the
execution of a program, which consists of two parts:
- At each point, the global state has to satisfy all the predicates in each
  current annotation in each thread.
- At each point, for each thread all the other threads have to satisfy the assumptions of the
  thread under consideration.
*}
fun active_ann_com :: "('level, 'val) com \<Rightarrow> ('level, 'val) ann" where
  "active_ann_com Skip = ann\<^sub>0" |
  "active_ann_com ({A} x ::= e) = A" |
  "active_ann_com ({A} x :D= e) = A" |
  "active_ann_com ({A} x <- l) = A" |
  "active_ann_com (Out A l e) = A" |
  "active_ann_com (DOut A l e) = A" |
  "active_ann_com (If A e c\<^sub>1 c\<^sub>2) = A" |
  "active_ann_com (While A e I c) = A" |
  "active_ann_com (Acquire A loc) = A" |
  "active_ann_com (Release A loc) = A" |
  "active_ann_com (c\<^sub>1 ;; c\<^sub>2) = active_ann_com c\<^sub>1"

definition active_ann :: "('level, 'val) local_state \<Rightarrow> ('level, 'val) ann" where
  "active_ann l = active_ann_com (com l)"

lemma active_ann_head_com:
  "active_ann_com c = active_ann_com (head_com c)"
  by (induction c, auto)

fun anns_satisfied_now :: "('level, 'val) global_conf \<Rightarrow> bool" where
  "anns_satisfied_now (ls, g, sched) = (list_all (\<lambda> l. g \<Turnstile>\<^sub>P active_ann l) ls)"

context lang
begin

fun anns_satisfied ("\<Turnstile> _") where
  "anns_satisfied (ls, g, sched) = 
    (\<forall> ls' g' sched'. (ls, g, sched) \<rightarrow>\<^sup>* (ls', g', sched') \<longrightarrow> anns_satisfied_now (ls', g', sched'))"

end

section "Global executions"

text {* This is a variant of the evaluation relation that keeps all intermediate states in the
execution around, which is needed to state various intermediate lemmas. *}

type_synonym ('level, 'val) execution = "('level, 'val) global_conf list"

context lang
begin

text {* Yet another variant of the above definition that keeps track of the entire
  execution. Hopefully this will help with stating required
  intermediate lemmas. *}
inductive global_exec :: "('level, 'val) global_conf \<Rightarrow> ('level, 'val) execution \<Rightarrow> bool"
where
  global_meval_full_refl: "global_exec (ls, g, sched) []" |
  global_meval_full_step: "\<lbrakk> (ls, g, sched) \<rightarrow> (ls', g', sched');
                          global_exec (ls', g', sched') exec \<rbrakk> \<Longrightarrow>
    global_exec (ls, g, sched) ((ls',g',sched')#exec)"

lemma global_exec_appI:
  assumes fst: "global_exec (ls, g, sched) U"
  assumes U: "U = exec@[(ls',g',sched')]"
  assumes snd: "global_exec (ls',g',sched') exec'"
  shows "global_exec (ls, g, sched) (exec@[(ls',g',sched')]@exec')"
using assms proof(induct arbitrary: exec ls' g' sched' rule: global_exec.induct)
  case (global_meval_full_refl ls g sched)
  then show ?case by simp
next
  case (global_meval_full_step ls g sched ls'' g'' sched'' exec')
  then show ?case
    apply (cases exec)
    apply (simp add: global_exec.global_meval_full_step)
    using global_exec.global_meval_full_step by auto
qed

lemma global_exec_appD:
  "global_exec (ls, g, sched) U \<Longrightarrow> U = (exec@[(ls',g',sched')]@exec') \<Longrightarrow>
   global_exec (ls, g, sched) (exec@[(ls', g', sched')]) \<and> global_exec (ls', g', sched') exec'"
proof(induct arbitrary: exec ls' g' sched' exec' rule: global_exec.induct)
  case (global_meval_full_refl cs mem sched)
then show ?case by simp
next
  case (global_meval_full_step ls g sched ls'' g'' sched'' exec'')
  then show ?case
    apply (induction exec)
    apply (simp add: global_exec.global_meval_full_step global_meval_full_refl)
    using global_exec.global_meval_full_step by auto
qed

lemma global_exec_deterministic:
  "global_exec (ls, g, sched) exec \<Longrightarrow>
   global_exec (ls, g, sched) exec' \<Longrightarrow>
   exec \<le> exec' \<or> exec' \<le> exec"
proof(induct arbitrary: exec' rule: global_exec.induct)
  case (global_meval_full_refl cs mem sched)
  then show ?case by simp
next
  case (global_meval_full_step ls g sched ls' g' sched' exec)
  note gmfstep = global_meval_full_step
  from `global_exec (ls, g, sched) exec'` show ?case
  proof(cases)
    case global_meval_full_refl
    then show ?thesis
      by blast
  next
    case (global_meval_full_step ls'' g'' sched'' exec'')
    from `((ls, g, sched), ls'', g'', sched'') \<in> global_eval`
         ` ((ls, g, sched), ls', g', sched') \<in> global_eval` have
    "ls'' = ls'" "g'' = g'" "sched'' = sched'"
      by (metis (no_types, lifting) eval_deterministic global_eval_elim stuck_def)+
    with gmfstep global_meval_full_step
    show ?thesis
      by simp
  qed
qed


fun exec_events :: "('level, 'val) trace \<Rightarrow> ('level, 'val) execution \<Rightarrow> ('level, 'val) trace" where
  "exec_events t [] = t" |
  "exec_events _ ((ls, g, sched) # exec) = exec_events (trace g) exec"


lemma exec_events_cons:
  assumes ex: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
      and exec\<^sub>1_cons: "exec\<^sub>1 = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) # exec\<^sub>2"
    shows "exec_events (trace g\<^sub>1) exec\<^sub>1 = exec_events (trace g\<^sub>2) exec\<^sub>2"
  using assms
  apply (induction arbitrary: ls\<^sub>2 g\<^sub>2 sched\<^sub>2 exec\<^sub>2 rule: global_exec.induct)
  by auto

lemmas global_exec_induct = global_exec.induct[split_format(complete), OF lang_axioms(1)]

lemma exec_trace_monotonic:
  assumes ex: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
  shows "trace g\<^sub>1 \<le> exec_events (trace g\<^sub>1) exec\<^sub>1"
  using assms
  apply (induction rule: global_exec_induct[OF ex])
  using trace_monotonic_global by fastforce+

fun last_config :: "('level, 'val) global_conf \<Rightarrow> ('level, 'val) execution \<Rightarrow> ('level, 'val) global_conf" where
  "last_config cnf [] = cnf" |
  "last_config _ (cnf # exec) = last_config cnf exec"

abbreviation global_meval_rev_abv :: "('level, 'val) global_conf \<Rightarrow> ('level, 'val) global_conf \<Rightarrow> bool"
  (infix "\<rightarrow>\<^sup>*\<^sup>r" 50) where
  "a \<rightarrow>\<^sup>*\<^sup>r b \<equiv> (a, b) \<in> global_eval\<^sup>*\<^sup>r"

lemma global_meval_to_exec:
  assumes ev: "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
  shows "\<exists> exec. global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec \<and> 
                 last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
proof -
  from ev have "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>*\<^sup>r (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    using rtranclr_rtrancl_same by blast
  thus ?thesis 
  proof (induction rule: rtranclr.induct[where r = global_eval, split_format(complete)])
    case (rtranclr_refl ls g sched)
    then show ?case
      apply (rule exI [where x="[]"])
      using global_exec.intros by auto
  next
    case (rtranclr_into_rtrancl ls\<^sub>1 g\<^sub>1 sched\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 ls\<^sub>3 g\<^sub>3 sched\<^sub>3)
    then show ?case
      using lang.global_meval_full_step lang_axioms
      by fastforce
  qed
qed

lemma global_exec_to_meval:
  assumes ex: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
  shows "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>* last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
proof -
  from ex have "(ls\<^sub>1, g\<^sub>1, sched\<^sub>1) \<rightarrow>\<^sup>*\<^sup>r last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
  proof (induction rule: global_exec_induct[OF ex])
    case (1 ls g sched)
    then show ?case 
      by auto
  next
    case (2 ls g sched ls' g' sched' exec)
    then show ?case
      using rtranclr.rtranclr_into_rtrancl 
      by (metis (no_types) \<open>(ls, g, sched) \<rightarrow> (ls', g', sched')\<close> lang_axioms lang.last_config.simps(2))
  qed
  thus ?thesis
    using rtranclr_rtrancl_same by blast
qed

lemma exec_events_last_trace:
  assumes ex: "global_exec (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1"
      and "last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) exec\<^sub>1 = (ls\<^sub>2, g\<^sub>2, sched\<^sub>2)"
    shows "trace g\<^sub>2 = exec_events (trace g\<^sub>1) exec\<^sub>1"
  using assms
  by (induction rule: global_exec_induct[OF ex], auto)

lemma global_exec_split:
  assumes ex: "global_exec (ls, g, sched) exec\<^sub>1"
  assumes exec_eq: "exec\<^sub>1 = exec @ exec'"
  shows "global_exec (last_config (ls, g, sched) exec) exec'"
using assms proof (induction arbitrary: exec exec' rule: global_exec.induct)
  case (global_meval_full_refl cs mem sched)
  then show ?case 
    apply simp
    using global_exec.global_meval_full_refl
    by blast
next
  case (global_meval_full_step cs mem sched cs' mem' sched' exec\<^sub>I)
  then show ?case
    apply (cases "exec")
    using global_exec.global_meval_full_step
    by auto
qed

lemma last_config_last_eq:
  "last_config x exec = (if exec = [] then x else last exec)"
  by (induction exec arbitrary: x, auto)

lemma last_config_split:
  assumes "exec = exec\<^sub>1 @ [(ls\<^sub>2, g\<^sub>2, sched\<^sub>2)] @ exec\<^sub>2"
  shows "last_config (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) exec\<^sub>2 = last_config (ls\<^sub>1, g\<^sub>1, sched\<^sub>1) (exec)"
  using assms
  unfolding last_config_last_eq
  by auto

end
subsection "Utility commands"


fun exp_vars :: "'val exp \<Rightarrow> var set" where
  "exp_vars (Val v) = {}" |
  "exp_vars (Var x) = {x}" |
  "exp_vars (UnOp f e) = exp_vars e" |
  "exp_vars (BinOp f e e') = exp_vars e \<union> exp_vars e'"

fun com_vars :: "('level, 'val) com \<Rightarrow> var set" where
  "com_vars Skip = {}" |
  "com_vars ({A} x ::= e) = {x} \<union> exp_vars e" |
  "com_vars ({A} x :D= e) = {x} \<union> exp_vars e" |
  "com_vars ({A} x <- l') = {x}" |
  "com_vars (Out A l' e) = exp_vars e" |
  "com_vars (DOut A l' e) = exp_vars e" |
  "com_vars (Acquire A loc) = {}" |
  "com_vars (Release A loc) = {}" |
  "com_vars (If A e c\<^sub>1 c\<^sub>2) = exp_vars e \<union> com_vars c\<^sub>1 \<union> com_vars c\<^sub>2" |
  "com_vars (c\<^sub>1 ;; c\<^sub>2) = com_vars c\<^sub>1 \<union> com_vars c\<^sub>2" |
  "com_vars (While A e I c) = com_vars c \<union> exp_vars e"

context lang
begin

lemma com_vars_no_write:
  assumes notin: "x \<notin> com_vars c" 
      and c_eq: "com l = c"
      and ev: "(l, g) \<leadsto> (l', g')"
    shows "mem g x = mem g' x"
  using ev c_eq notin
  apply (induction arbitrary: c rule: eval.induct)
  by auto

end

end