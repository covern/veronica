theory VDBuffer_OG
  imports "../Security" "../OGTranslation" VDBuffer
begin

context parity'
begin

lemma satisfies_ann_preds_helper:
  "translate_initial_anns ls = S \<Longrightarrow> g \<in> S \<Longrightarrow>
   list_all (\<lambda>l. g \<Turnstile>\<^sub>P active_ann' l) ls"
  by auto

lemma mode_input_lock_state_upd [simp]: "mode_input g \<Longrightarrow> mode_input (g\<lparr>lock_state := blah\<rparr>)"
  by(clarsimp simp: mode_input_def split: if_splits simp: level_inputs_def last_input_def)

lemma mode_input_lbuf_upd [simp]: "mode_input g \<Longrightarrow> mode_input (g\<lparr>mem := (mem g)(''lbuf'' := blah)\<rparr>)"
  by(clarsimp simp: mode_input_def split: if_splits simp: level_inputs_def last_input_def)

lemma mode_input_hbuf_upd [simp]: "mode_input g \<Longrightarrow> mode_input (g\<lparr>mem := (mem g)(''hbuf'' := blah)\<rparr>)"
  by(clarsimp simp: mode_input_def split: if_splits simp: level_inputs_def last_input_def)

lemma modeeq_hbuf_upd [simp]: "modeeq g \<Longrightarrow> modeeq (g\<lparr>mem := (mem g)(''hbuf'' := blah)\<rparr>)"
  by(clarsimp simp: modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma lock_mode_input_to_mode_input [simp]: "lock_mode_input g \<Longrightarrow> lock_state g vd_lock = None \<Longrightarrow> mode_input (g\<lparr>lock_state := (lock_state g)(vd_lock := Some t)\<rparr>)"
  by(auto simp: mode_input_def split: if_splits simp: level_inputs_def last_input_def lock_mode_input_def)

lemma modeeq_lbuf_upd [simp]: "modeeq g \<Longrightarrow> modeeq (g\<lparr>mem := (mem g)(''lbuf'' := blah)\<rparr>)"
  by(clarsimp simp: modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma modeeq_env_upd [simp]: "modeeq g \<Longrightarrow> modeeq (g\<lparr>env := blah\<rparr>)"
  by(clarsimp simp: modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma modeeq_trace_upd [simp]: "modeeq g \<Longrightarrow> modeeq (g\<lparr>trace := blah\<rparr>)"
  by(clarsimp simp: modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma modeeq_buf_upd [simp]: "modeeq g \<Longrightarrow> modeeq (g\<lparr>mem := (mem g)(''buf'' := blah g)\<rparr>)"
  by(clarsimp simp: modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma lock_modeeq_to_modeeq [simp]: "lock_modeeq g \<Longrightarrow> lock_state g vd_lock = None \<Longrightarrow> modeeq (g\<lparr>lock_state := (lock_state g)(vd_lock := Some t)\<rparr>)"
  by(auto simp: modeeq_def split: if_splits simp: level_inputs_def last_input_def lock_modeeq_def)

lemma modeeq_env_buf_upd [simp]:
  "modeeq g \<Longrightarrow> modeeq (g\<lparr>env := blah,
               mem := (mem g)(''buf'' := meh)\<rparr>)"
  by(auto simp: modeeq_def)

lemma mode_input_to_mode_input_weak_low [simp]:
  "mode_input g \<Longrightarrow>
   mem g ''mode'' = 0 \<Longrightarrow>
         mode_input_weak
          (g\<lparr>env := (env g)(\<bottom> := ltl (env g \<bottom>)),
               mem := (mem g)(''buf'' := lhd (env g \<bottom>)),
               trace := trace g @ [Input \<bottom> (lhd (env g \<bottom>))]\<rparr>)"
  apply(auto simp: mode_input_def mode_input_weak_def split: if_splits simp: last_input_def level_inputs_def)
  done

lemma mode_input_to_mode_input_weak_high [simp]:
  "mode_input g \<Longrightarrow>
   mem g ''mode'' \<noteq> 0 \<Longrightarrow>
         mode_input_weak
          (g\<lparr>env := (env g)(\<top> := ltl (env g \<top>)),
               mem := (mem g)(''buf'' := lhd (env g \<top>)),
               trace := trace g @ [Input \<top> (lhd (env g \<top>))]\<rparr>)"
  apply(auto simp: mode_input_def mode_input_weak_def split: if_splits simp: last_input_def level_inputs_def)
  done

lemma mode_input_to_mode_input_weak [simp]:
  " mode_input_weak x \<Longrightarrow>
         mode_input (x\<lparr>mem := (mem x)(''GHOST_running'' := Suc 0)\<rparr>)"
  by(auto simp: mode_input_weak_def mode_input_def last_input_def level_inputs_def)

lemma modeeq_mem_upd [simp]:
  "modeeq x \<Longrightarrow> var \<notin> {''mode'', ''mode2''} \<Longrightarrow>
         modeeq (x\<lparr>mem := (mem x)(var := blah)\<rparr>)"
  by(auto simp: modeeq_def mode_input_def last_input_def level_inputs_def)

lemma lock_mode_input_other_acq [simp]:
  "lock_mode_input g \<Longrightarrow> lock_state g vd_lock = None \<Longrightarrow>
   (lock_mode_input (g\<lparr>lock_state := (lock_state g)(vd_lock := Some t)\<rparr>))"
  by(auto simp: lock_mode_input_def)

lemma lock_modeeq [simp]:
  "lock_modeeq g \<Longrightarrow> lock_state g vd_lock = None \<Longrightarrow>
   (lock_modeeq (g\<lparr>lock_state := (lock_state g)(vd_lock := Some t)\<rparr>))"
  by(auto simp: lock_modeeq_def)

lemma lock_modeeq_lbuf_upd [simp]: "lock_modeeq g \<Longrightarrow> lock_modeeq (g\<lparr>mem := (mem g)(''lbuf'' := blah)\<rparr>)"
  by(clarsimp simp: lock_modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma lock_modeeq_hbuf_upd [simp]: "lock_modeeq g \<Longrightarrow> lock_modeeq (g\<lparr>mem := (mem g)(''hbuf'' := blah)\<rparr>)"
  by(clarsimp simp: lock_modeeq_def split: if_splits simp: level_inputs_def last_input_def)

lemma lock_mode_input_lbuf_upd [simp]: "lock_mode_input g \<Longrightarrow> lock_mode_input (g\<lparr>mem := (mem g)(''lbuf'' := blah)\<rparr>)"
  by(clarsimp simp: lock_mode_input_def split: if_splits simp: level_inputs_def last_input_def)

lemma lock_mode_input_hbuf_upd [simp]: "lock_mode_input g \<Longrightarrow> lock_mode_input (g\<lparr>mem := (mem g)(''hbuf'' := blah)\<rparr>)"
  by(clarsimp simp: lock_mode_input_def split: if_splits simp: level_inputs_def last_input_def)

lemma modeeq_to_lock_modeeq [simp]:
  "modeeq g \<Longrightarrow> lock_modeeq (g\<lparr>lock_state := (lock_state g)(vd_lock := None)\<rparr>)"
  by(auto simp: modeeq_def lock_modeeq_def)

lemma mode_input_to_lock_mode_input [simp]:
  "mode_input g \<Longrightarrow> lock_mode_input (g\<lparr>lock_state := (lock_state g)(vd_lock := None)\<rparr>)"
  by(auto simp: mode_input_def lock_mode_input_def last_input_def level_inputs_def)

lemma mode_input_weak_lbuf_upd [simp]:
  "mode_input_weak g \<Longrightarrow> mode_input_weak (g\<lparr>mem := (mem g)(''lbuf'' := blah)\<rparr>)"
  by(auto simp: mode_input_weak_def level_inputs_def last_input_def)

lemma mode_input_weak_hbuf_upd [simp]:
  "mode_input_weak g \<Longrightarrow> mode_input_weak (g\<lparr>mem := (mem g)(''hbuf'' := blah)\<rparr>)"
  by(auto simp: mode_input_weak_def level_inputs_def last_input_def)

(* Sanity checks that assertions are true in isolation: *)
lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes init_sat[simp]: "lock_mode_input g \<and> lock_modeeq g"
  shows "anns_satisfied ([mk_loc_state t vd_fetch'], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t vd_fetch']"
  have init_anns: "translate_initial_anns ls = {g. lock_mode_input g \<and>  lock_modeeq g}"
    by (clarsimp simp: ls_def vd_fetch'_def active_ann'_def split: option.splits simp:Let_def ann\<^sub>0_def satisfies_ann_preds_def label_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def  Let_def
    apply (clarsimp)
    apply oghoare
              apply (simp_all add: active_ann'_def ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def)
          apply (auto simp: last_input_def level_inputs_def label_def)
    done
    hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    apply (rule satisfies_ann_preds_helper)
     apply (rule init_anns)
    using init_sat by blast
  thus ?thesis using ls_def by auto
qed

  

lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes init_sat[simp]: "lock_mode_input g \<and> lock_modeeq g"
  shows "anns_satisfied ([mk_loc_state t (vd_forward')], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (vd_forward')]"
  have init_anns: "translate_initial_anns ls = {g. lock_mode_input g \<and>  lock_modeeq g}"
    apply (clarsimp simp: label_def ls_def vd_fetch'_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
    done
    then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all  add: active_ann'_def ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def)
         apply(clarsimp simp: label_def)+
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    apply (rule satisfies_ann_preds_helper)
     apply (rule init_anns)
    using init_sat by blast
  thus ?thesis using ls_def by auto
qed

lemma Collect_label[simp]:
  "Collect (label blah) = UNIV"
  using label_def by auto

lemma lock_modeeq_triv [simp]:
  "lock_state g vd_lock = Some t \<Longrightarrow> lock_state g' = lock_state g \<Longrightarrow> lock_modeeq g'"
  by(auto simp: lock_modeeq_def)

lemma lock_mode_input_triv [simp]:
  "lock_state g vd_lock = Some t \<Longrightarrow> lock_state g' = lock_state g \<Longrightarrow> lock_mode_input g'"
  by(auto simp: lock_mode_input_def)



(* Simpler case without toggle thread: *)
lemma fetch_forward_sat:
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neqs[simp]: "t \<noteq> t'" "t'' \<noteq> t" "t'' \<noteq> t'" "(\<bottom>::'level) \<noteq> \<top>"
  assumes init_sat[simp]: "lock_mode_input g \<and> lock_modeeq g"
  shows "anns_satisfied ([mk_loc_state t vd_fetch', 
                          mk_loc_state t' (vd_forward')], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward')]"
  note neqs[trace_lems]
  have [intro]: "t = t' \<Longrightarrow> False"
                "t'' = t \<Longrightarrow> False"
                "t'' = t' \<Longrightarrow> False"
                "(\<bottom>::'level) = \<top> \<Longrightarrow> False"
    using neqs apply blast+
    done
  have init_anns: "translate_initial_anns ls = {g. lock_mode_input g \<and>  lock_modeeq g}"
    apply (clarsimp simp: label_def ls_def vd_fetch'_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
    done
    then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: neqs active_ann'_def ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def)
                         apply((clarsimp simp: label_def | (auto intro: Int_emptyI)[1])+)
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    apply (rule satisfies_ann_preds_helper)
     apply (rule init_anns)
    using init_sat by blast
  thus ?thesis using ls_def by auto
qed

lemma INV_simp [simp]:
  "INV g = (mode_input g \<and> modeeq g)"
  apply(auto simp add: INV_def mode_input_def modeeq_def)
  done

lemma lock_INV_simp [simp]:
  "lock_INV g = (lock_mode_input g \<and> lock_modeeq g)"
  apply(auto simp add: lock_INV_def lock_mode_input_def lock_modeeq_def mode_input_def modeeq_def)
  done

lemma exists_mem_upd_triv [intro]:
  "P g \<Longrightarrow> \<exists>v. P (g\<lparr>mem := (mem g)(var := v)\<rparr>)"
  apply(rule_tac x="mem g var" in exI)
  by auto

lemma toggle_helper [simp]:
  "mode_input (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       modeeq (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       mem x ''buf'' = 0 \<Longrightarrow>
       mem x ''mode'' = Suc v' \<Longrightarrow>
       mode_input
        (x\<lparr>mem := (mem x)(''mode2'' := Suc v', ''GHOST_running'' := 0)\<rparr>)"
  by(auto simp: mode_input_def modeeq_def)

lemma toggle_helper2 [simp]:
  "mode_input (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       modeeq (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       mem x ''buf'' = 0 \<Longrightarrow>
       mem x ''mode'' = Suc v' \<Longrightarrow>
       modeeq
        (x\<lparr>mem := (mem x)(''mode2'' := Suc v', ''GHOST_running'' := 0)\<rparr>)"
  by(auto simp: mode_input_def modeeq_def)

lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neq: "distinct [t, t', t'']"
  assumes levels: "(\<top>::'level) \<noteq> \<bottom>"
  assumes init_sat[simp]: "lock_mode_input g \<and> lock_modeeq g"
  shows "anns_satisfied ([mk_loc_state t vd_fetch', 
                          mk_loc_state t' (vd_forward'), 
                          mk_loc_state t'' vd_toggle'], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward'),
                         mk_loc_state t'' vd_toggle']"
  note neqs[trace_lems,simp] = neq[simplified] levels
  have [intro,simp,dest!]: "t = t' \<Longrightarrow> False" 
                           "t' = t \<Longrightarrow> False"
                     "t = t'' \<Longrightarrow> False"
                     "t'' = t \<Longrightarrow> False"
                     "t' = t'' \<Longrightarrow> False"
                     "t'' = t' \<Longrightarrow> False"
    using neqs apply blast+
    done
  from levels have [intro]: "(\<top>::'level) = \<bottom> \<Longrightarrow> False" by blast

  have init_anns: "translate_initial_anns ls = {g. lock_mode_input g \<and>  lock_modeeq  g}"
    apply (clarsimp simp: label_def ls_def vd_fetch'_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
    done
    then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: neqs active_ann'_def ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def)
                        apply((clarsimp simp: label_def | (auto intro: Int_emptyI)[1])+)
  done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    apply (rule satisfies_ann_preds_helper)
     apply (rule init_anns)
    using init_sat by blast
  thus ?thesis using ls_def by auto
qed

lemma lock_mode_input_out [simp]:
  " lock_mode_input x \<Longrightarrow>
         lock_mode_input
          (x\<lparr>trace := trace x @ [Output blah bleh meh]\<rparr>)"
  apply(auto simp: lock_mode_input_def mode_input_def level_inputs_def last_input_def)
  done

lemma mode_input_out [simp]:
  " mode_input x \<Longrightarrow>
         mode_input
          (x\<lparr>trace := trace x @ [Output blah bleh meh]\<rparr>)"
  apply(auto simp: lock_mode_input_def mode_input_def level_inputs_def last_input_def)
  done

lemma mode_input_weak_out [simp]:
  " mode_input_weak x \<Longrightarrow>
         mode_input_weak
          (x\<lparr>trace := trace x @ [Output blah bleh meh]\<rparr>)"
  apply(auto simp: mode_input_weak_def lock_mode_input_def mode_input_def level_inputs_def last_input_def)
  done

lemma lock_modeeq_trace_upd [simp]:
  " lock_modeeq x \<Longrightarrow>
         lock_modeeq
          (x\<lparr>trace := blah\<rparr>)"
  apply(auto simp: lock_modeeq_def mode_input_def level_inputs_def last_input_def)
  done

lemma toggle_helper3 [simp]:
  "         mode_input (x\<lparr>mem := (mem x)(''buf'' := v)\<rparr>) \<Longrightarrow>
           modeeq (x\<lparr>mem := (mem x)(''buf'' := v)\<rparr>) \<Longrightarrow>
           mem x ''buf'' = 0 \<Longrightarrow>
           \<exists>v. mode_input
                (x\<lparr>trace :=
                     trace x @ [Output \<top> (mem x ''hbuf'') (Var ''hbuf'')],
                     mem := (mem x)(''buf'' := v)\<rparr>) \<and>
               modeeq
                (x\<lparr>trace :=
                     trace x @ [Output \<top> (mem x ''hbuf'') (Var ''hbuf'')],
                     mem := (mem x)(''buf'' := v)\<rparr>)"
  apply(rule_tac x=v in exI)
  apply(rule conjI)
  apply(auto simp: mode_input_def level_inputs_def last_input_def)[1]
  apply(auto simp: modeeq_def)
  done

lemma toggle_helper4 [simp]:
       "mode_input (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       modeeq (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       mem x ''buf'' = 0 \<Longrightarrow>
       mem x ''mode'' = Suc v' \<Longrightarrow>
       \<exists>v. mode_input
            (x\<lparr>trace := trace x @ [Output \<top> (mem x ''hbuf'') (Var ''hbuf'')],
                 mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<and>
           modeeq
            (x\<lparr>trace := trace x @ [Output \<top> (mem x ''hbuf'') (Var ''hbuf'')],
                 mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>)"
  apply(rule_tac x=v in exI)
  apply(rule conjI)
  apply(auto simp: mode_input_def level_inputs_def last_input_def)[1]
  apply(auto simp: modeeq_def)
  done

lemma toggle_helper5 [simp]:
  "        mode_input (x\<lparr>mem := (mem x)(''GHOST_running'' := 0)\<rparr>) \<Longrightarrow>
         modeeq (x\<lparr>mem := (mem x)(''GHOST_running'' := 0)\<rparr>) \<Longrightarrow>
         mode_input
          (x\<lparr>trace := trace x @ [Output \<top> (mem x ''hbuf'') (Var ''hbuf'')],
               mem := (mem x)(''GHOST_running'' := 0)\<rparr>) \<and>
         modeeq
          (x\<lparr>trace := trace x @ [Output \<top> (mem x ''hbuf'') (Var ''hbuf'')],
               mem := (mem x)(''GHOST_running'' := 0)\<rparr>)"
  apply(auto simp: mode_input_def level_inputs_def last_input_def modeeq_def)
  done

lemma toggle_helper6 [simp]:
   "       mode_input (x\<lparr>mem := (mem x)(''buf'' := v)\<rparr>) \<Longrightarrow>
           modeeq (x\<lparr>mem := (mem x)(''buf'' := v)\<rparr>) \<Longrightarrow>
           \<exists>v. mode_input
                (x\<lparr>trace :=
                     trace x @ [Output blah meh rah],
                     mem := (mem x)(''buf'' := v)\<rparr>) \<and>
               modeeq
                (x\<lparr>trace :=
                     trace x @ [Output blah meh rah],
                     mem := (mem x)(''buf'' := v)\<rparr>)"
  apply(rule_tac x=v in exI)
  apply(auto simp: mode_input_def level_inputs_def last_input_def modeeq_def)
  done

lemma toggle_helper7 [simp]:
  "       mode_input (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       modeeq (x\<lparr>mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<Longrightarrow>
       mem x ''buf'' = 0 \<Longrightarrow>
       mem x ''mode'' = Suc v' \<Longrightarrow>
       \<exists>v. mode_input
            (x\<lparr>trace := trace x @ [Output blah meh rah],
                 mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>) \<and>
           modeeq
            (x\<lparr>trace := trace x @ [Output blah meh rah],
                 mem := (mem x)(''mode'' := v', ''buf'' := v)\<rparr>)"
  apply(rule_tac x=v in exI)
  apply(auto simp: mode_input_def level_inputs_def last_input_def modeeq_def)
  done

lemma toggle_helper8 [simp]:
  "         mode_input (x\<lparr>mem := (mem x)(''GHOST_running'' := 0)\<rparr>) \<Longrightarrow>
         modeeq (x\<lparr>mem := (mem x)(''GHOST_running'' := 0)\<rparr>) \<Longrightarrow>
         mode_input
          (x\<lparr>trace := trace x @ [Output \<bottom> (mem x ''lbuf'') (Var ''lbuf'')],
               mem := (mem x)(''GHOST_running'' := 0)\<rparr>) \<and>
         modeeq
          (x\<lparr>trace := trace x @ [Output \<bottom> (mem x ''lbuf'') (Var ''lbuf'')],
               mem := (mem x)(''GHOST_running'' := 0)\<rparr>)"
  apply(auto simp: mode_input_def level_inputs_def last_input_def modeeq_def)
  done


lemma system_sat:
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neq: "distinct [t, t', t'',t''',t'''']"
  assumes levels: "(\<top>::'level) \<noteq> \<bottom>"
  assumes init_sat[simp]: "lock_mode_input g \<and> lock_modeeq g"
  shows "anns_satisfied ([mk_loc_state t vd_fetch', 
                          mk_loc_state t' (vd_forward'), 
                          mk_loc_state t'' vd_toggle',
                          \<lparr>local_state.com = vd_hout', tid = t'''\<rparr>,
                          \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward'),
                         mk_loc_state t'' vd_toggle',
                          \<lparr>local_state.com = vd_hout', tid = t'''\<rparr>,
                          \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
  note neqs[trace_lems,simp] = neq[simplified] levels
  have [intro,simp,dest!]: "t = t' \<Longrightarrow> False" 
                           "t' = t \<Longrightarrow> False"
                     "t = t'' \<Longrightarrow> False"
                     "t'' = t \<Longrightarrow> False"
                     "t' = t'' \<Longrightarrow> False"
                     "t'' = t' \<Longrightarrow> False"
    using neqs apply blast+
    done
  from levels have [intro]: "(\<top>::'level) = \<bottom> \<Longrightarrow> False" by blast

  have init_anns: "translate_initial_anns ls = {g. lock_mode_input g \<and>  lock_modeeq  g}"
    apply (clarsimp simp: label_def ls_def vd_fetch'_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def vd_hout'_def vd_lout'_def)
    done
    then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_hout'_def vd_lout'_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: neqs active_ann'_def ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def)
                        apply((clarsimp simp: label_def | (auto intro: Int_emptyI)[1])+)
  done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    apply (rule satisfies_ann_preds_helper)
     apply (rule init_anns)
    using init_sat by blast
  thus ?thesis using ls_def by auto
qed

lemma system_secure:
  assumes ls_def: "ls = [mk_loc_state t vd_fetch', 
                          mk_loc_state t' (vd_forward'), 
                          mk_loc_state t'' vd_toggle',
                          \<lparr>local_state.com = vd_hout', tid = t'''\<rparr>,
                          \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
      and Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''hbuf'' = Some \<top>" "\<L> ''lbuf'' = Some \<bottom>"
              "\<L> ''GHOST_running'' = Some \<bottom>"
  assumes neq: "distinct [t, t', t'',t''',t'''']"
  assumes levels: "(\<top>::'level) \<noteq> \<bottom>"
  assumes init_sat[simp]: "lock_mode_input g \<and> lock_modeeq g"
  shows "system_secure \<bottom> (\<lambda>g. lock_mode_input g \<and> lock_modeeq g) sched ls"
  apply (rule soundness)
   apply(subst ls_def)
   apply(rule system_sat)
  using assms apply blast+
  apply(simp add: ls_def)
  using fetch'_typed Ls forward'_typed toggle'_typed lout'_typed hout'_typed by metis

end
end
