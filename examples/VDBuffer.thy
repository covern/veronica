theory VDBuffer
  imports Main "../TypeSystem" "../Automation" "../OGTranslation" "~~/src/HOL/Hoare_Parallel/OG_Tactics"
begin

text {*
  This theory is the VERONICA analogue of the VDBuffer example that is
  now in COVERN, as of COVERN revision: 
  c12bee01897c27e89ac737f5c1e7b97300bbb569.

  To verify the example in VERONICA, we need to add a Low ghost variable,
  and also specify the invariant with a little more functional detail.
  Essentially the invariant in VERONICA is naturally a single-state
  Hoare logic property, rather than a COVERN style relational invariant.
  We use a ghost variable to help us recover (a slightly stronger form
  of) the relational invariant from the Hoare logic invariant, which now
  also talks about some details of functionality that COVERN can abstract
  from: e.g. when the buffer is cleared.
*}

context parity'
begin

definition nat_eq :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "nat_eq x y = (if x = y then 1 else 0)"

definition set_to_ann :: "('level, nat) pred set \<Rightarrow> ('level, nat) ann" where
  "set_to_ann Ps = \<lparr>preds = Ps\<rparr>"

declare
  [[coercion_enabled]]
  [[coercion "Var :: var \<Rightarrow> nat exp", coercion "Val :: nat \<Rightarrow> nat exp", coercion set_to_ann]]

(* TODO: move this stuff elsewhere *)
definition vd_lock :: lock where  "vd_lock = 0"


fun mk_loc_state :: "thread_id \<Rightarrow> (thread_id \<Rightarrow> ('level, nat) lcom) \<Rightarrow> ('level, nat) local_state" where
  "mk_loc_state t cf = \<lparr>local_state.com = cf t, tid = t\<rparr>"


text {* We use a (Low) ghost variable to distinguish which case we are in. 
        This variable @{term "''GHOST_running''"} must never be allowed to
        be read by any program text.
*}
definition INV :: "('level, nat) pred" where
   "INV = (\<lambda> g.  (if mem g ''GHOST_running'' = 0 then mem g ''buf'' = 0 else
                                (if mem g ''mode'' = 0 
                                then (mem g ''buf'' = last_input \<bottom> g \<and> level_inputs \<bottom> g \<noteq> [])
                                else (mem g ''buf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> []))) \<and>
                  mem g ''mode'' = mem g ''mode2'')"


definition mode_input_weak :: "('level, nat) pred" where
   "mode_input_weak = (\<lambda> g. if mem g ''mode'' = 0 
                                   then mem g ''buf'' = last_input \<bottom> g \<and> level_inputs \<bottom> g \<noteq> []
                                   else mem g ''buf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> [])" 

definition modeeq :: "('level, nat) pred" where
   "modeeq = (\<lambda> g. mem g ''mode'' = mem g ''mode2'')" 

definition mode_input :: "('level, nat) pred" where
   "mode_input = (\<lambda> g.  (if mem g ''GHOST_running'' = 0 then mem g ''buf'' = 0 else
                                (if mem g ''mode'' = 0 
                                then (mem g ''buf'' = last_input \<bottom> g \<and> level_inputs \<bottom> g \<noteq> [])
                                else (mem g ''buf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> []))))"

definition lock_mode_input :: "('level, nat) pred" where
   "lock_mode_input = (\<lambda> g.  lock_state g vd_lock = None \<longrightarrow> mode_input g)"

definition lock_INV :: "('level, nat) pred" where
   "lock_INV = (\<lambda> g.  lock_state g vd_lock = None \<longrightarrow> INV g)"


definition lock_modeeq :: "('level, nat) pred" where
   "lock_modeeq g = ((lock_state g vd_lock = None) \<longrightarrow> mem g ''mode'' = mem g ''mode2'')"

definition label :: "string \<Rightarrow> ('level, nat) pred" where
  "label x g = True"

definition mode_lock :: "lock" where  "mode_lock = 1"



definition vd_fetch' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_fetch' t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in let mode0 = (\<lambda> g. mem g ''mode'' = (0::nat))
     in let moden0 = Not \<circ> mode0
     in
     Acquire \<lparr>preds = {lock_mode_input, lock_modeeq,  label ''fetch_acquire''}\<rparr> vd_lock ;;
     (If \<lparr>preds = {loc, mode_input, modeeq, label ''fetch_if''}\<rparr> (BinOp nat_eq ''mode'' (0::nat))
          ({\<lparr>preds = {loc, mode_input, modeeq, mode0}\<rparr>} ''buf'' <- \<bottom>)
          ({\<lparr>preds = {loc, mode_input, modeeq, moden0}\<rparr>} ''buf'' <- \<top>)) ;;
     ((Assign \<lparr>preds = {loc, modeeq, mode_input_weak}\<rparr> ''GHOST_running''  (Val 1)) ;;
     Release \<lparr>preds = {loc, mode_input, modeeq, label ''fetch_release''}\<rparr> vd_lock))"

definition vd_forward' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_forward' t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in let mode0 = (\<lambda> g. mem g ''mode2'' = (0::nat))
     in let moden0 = Not \<circ> mode0
     in
     (Acquire \<lparr>preds = {lock_mode_input, lock_modeeq,  label ''forward_acquire2''}\<rparr> vd_lock ;;
      ((If \<lparr>preds = {loc, modeeq, mode_input}\<rparr> (BinOp nat_eq ''mode2'' (0::nat))
           (Assign \<lparr>preds = {loc, modeeq, mode0, mode_input}\<rparr> ''lbuf'' ''buf'')
           (Assign \<lparr>preds = {loc, modeeq, moden0, mode_input}\<rparr> ''hbuf'' ''buf'') 
         ))
         ;;
      Release \<lparr>preds = {loc, modeeq, mode_input}\<rparr> vd_lock))"

definition vd_lout' :: "('level, nat) lcom" where
  "vd_lout' = Out \<lparr>preds = {lock_mode_input, lock_modeeq}\<rparr> \<bottom> ''lbuf''"

definition vd_hout' :: "('level, nat) lcom" where
  "vd_hout' = Out \<lparr>preds = {lock_mode_input, lock_modeeq}\<rparr> \<top> ''hbuf''"



definition vd_toggle' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_toggle' t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in
      (Acquire \<lparr>preds = {lock_mode_input, lock_modeeq }\<rparr> vd_lock ;;
      {\<lparr>preds = {loc, INV}\<rparr>} ''buf'' ::= Val 0 ;;
      {\<lparr>preds = {loc, (\<lambda>g. \<exists>v. INV (g\<lparr>mem := ((mem g)(''buf'' := v))\<rparr>) \<and> mem g ''buf'' = 0 )}\<rparr>} ''mode'' ::= UnOp (\<lambda> n. n + 1) ''mode'' ;;
      {\<lparr>preds = {loc, (\<lambda>g. \<exists>v v'. INV (g\<lparr>mem := ((mem g)(''mode'' := v', ''buf'' := v))\<rparr>) \<and> mem g ''buf'' = 0 \<and> mem g ''mode'' = (v' + 1)) }\<rparr>} ''mode2'' ::= ''mode'' ;;
      {\<lparr>preds = {loc, (\<lambda>g. INV (g\<lparr>mem := ((mem g)(''GHOST_running'' := 0))\<rparr>)) }\<rparr>} ''GHOST_running'' ::= Val 0 ;;
      Release \<lparr>preds = {loc, modeeq, INV}\<rparr> vd_lock))"

lemma trace_eq_level_inputs_eq [simp]:
  "trace g =\<^sup>t\<^bsub>l\<^esub> trace g' \<Longrightarrow> l' \<le> l \<Longrightarrow> level_inputs l' g = level_inputs l' g'"
  by (metis level_inputs_def project_trace_upto_eq_project_trace_eq trace_equiv_def trace_equiv_le_closed)

lemma forward'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              
               "\<L> ''hbuf'' = Some \<top>" "\<L> ''lbuf'' = Some \<bottom>"
              "\<L> ''GHOST_running'' = Some \<bottom>"
            shows "has_type ah \<bottom> (vd_forward' t)"
  unfolding vd_forward'_def Let_def
  apply typing 
    apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
      apply (auto simp: Ls)[1]
     apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def mode_input_def modeeq_def)[2]     
   apply(rule LAssign_type[where lev=\<top> and l\<^sub>e=\<top>])
     apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def modeeq_def mode_input_def)[3]
  done


lemma fetch'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''GHOST_running'' = Some \<bottom>"
  shows "has_type ah \<bottom> (vd_fetch' t)"
  unfolding vd_fetch'_def Let_def
  apply typing
     apply (rule UIn_type)
     apply (auto simp: Ls)[1]
    apply (rule UIn_type)
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
  apply (rule_tac lev=\<bottom> and l\<^sub>e=\<bottom> in LAssign_type)
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  done

lemma toggle'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''GHOST_running'' = Some \<bottom>"
shows "has_type ah \<bottom> (vd_toggle' t)"
  unfolding vd_toggle'_def Let_def
  apply typing
     apply (rule UAssign_type)
     apply (auto simp: Ls)[1]
    apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
      apply (auto simp: Ls)[1]
     apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
   apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
     apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def global_state_equiv_def mem_loweq_def Ls)[2]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
    apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def global_state_equiv_def mem_loweq_def Ls)[2]
  done

lemma lout'_typed:
  assumes Ls: "\<L> ''lbuf'' = Some \<bottom>"
            shows "has_type ah \<bottom> (vd_lout')"
  unfolding vd_lout'_def Let_def
  apply typing
  by(auto simp: ann_implies_low_def global_equiv_defs Ls)

lemma hout'_typed:
  assumes Ls: "\<L> ''hbuf'' = Some \<top>" 
            shows "has_type ah \<bottom> (vd_hout')"
  unfolding vd_hout'_def Let_def
  apply typing
  by(auto simp: ann_implies_low_def global_equiv_defs Ls)

end

end
