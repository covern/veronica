theory RunningAvgExtensional
  imports Main "../TypeSystem" "../Automation" "../OGTranslation" "~~/src/HOL/Hoare_Parallel/OG_Tactics" HOL.Rat HOL.Int
begin

datatype Val = Rat rat | List "rat list"

definition avg :: "rat list \<Rightarrow> rat" where
  "avg vals = (sum_list vals) / (of_nat (max (length vals) 1))"

lemma "avg [] = 0" unfolding avg_def by auto

lemma "nat \<lfloor>Fract 5 1\<rfloor> = 5"
  by (simp add: rat_number_collapse(3))

lemma "avg [5, 6, 7] = 6"
  by (auto simp: avg_def)

lemma "avg [1, 2] = 3 / 2"
  apply (auto simp: avg_def)
  done

lemma "avg [3, 1, 1, 1] = 6 / 4"
  apply (auto simp: avg_def)
  done

lemma "avg [3, 1, 1, 1] = 3/2"
  apply (auto simp: avg_def)
  done

definition nat_of_rat :: "rat \<Rightarrow> nat" where [simp]:
  "nat_of_rat r = nat \<lfloor>r\<rfloor>"


definition round_inp :: "rat \<Rightarrow> rat" where [simp]:
  "round_inp x = of_nat (nat_of_rat x)"

(* Sanity check *)
lemma "of_nat (nat_of_rat x + nat_of_rat y) = of_nat (nat_of_rat x) + of_nat (nat_of_rat y)"
  apply auto
  done

lemma "nat_of_rat (2/3) = 0"
  apply auto
  done 
term sum_list

definition \<D>\<^sub>p :: "('level::complete_lattice) \<Rightarrow> ('level::complete_lattice, rat) decl_pred" where
  "\<D>\<^sub>p from to g v e \<equiv> let inps = (level_inputs \<top> g)
                       in ((if length inps \<ge> sum_list (map nat_of_rat (level_inputs \<bottom> g))
                            then v = avg inps
                            else False))"

locale running_avg = lang 
  where level_rename_param = lr and \<D> = "\<D>\<^sub>p" and val_rename_param = "0::rat"
    and val_true = "(<) 0" and tt = "1::rat" and ff = "0::rat"
  for lr :: "'level::complete_lattice" + assumes [simp, trace_lems]: "(\<top>::'level) \<noteq> \<bottom>"

context running_avg begin

definition rat_eq :: "rat \<Rightarrow> rat \<Rightarrow> rat" where
  "rat_eq x y = (if x = y then 1 else 0)"

definition set_to_ann :: "('level, nat) pred set \<Rightarrow> ('level, nat) ann" where
  "set_to_ann Ps = \<lparr>preds = Ps\<rparr>"

declare
  [[coercion_enabled]]
  [[coercion "Var :: var \<Rightarrow> rat exp", coercion "Val :: rat \<Rightarrow> rat exp", coercion set_to_ann]]

term "\<top>::'level"
term "{ann\<^sub>0} ''x'' ::= Fract 5 1"

(*
abbreviation bot_abv :: "'level" ("\<bottom>'") where "\<bottom>' \<equiv> bot"

abbreviation top_abv :: "'level" ("\<top>'") where "\<top>' \<equiv> bot"
*)
term "\<bottom>::'level"

term tt
term top

definition top_alias  :: "'level" where [simp]: "top_alias = \<top>"
(* TODO: move this stuff elsewhere *)
definition vd_lock :: lock where [simp]: "vd_lock = 0"
definition bot_alias  :: "'level" where [simp]: "bot_alias = \<bottom>"


fun mk_loc_state :: "thread_id \<Rightarrow> (thread_id \<Rightarrow> ('level, rat) lcom) \<Rightarrow> ('level, rat) local_state" where
  "mk_loc_state t cf = \<lparr>local_state.com = cf t, tid = t\<rparr>"

definition label :: "string \<Rightarrow> ('level, rat) pred" where
  "label x g = True"

definition avg_lock :: "lock" where [simp]: "avg_lock = 1"
definition min_lock :: "lock" where [simp]: "min_lock = 2"

definition input_invs :: "('level, rat) global_state \<Rightarrow> bool" where [simp]:
  "input_invs g \<equiv> (mem g ''inp_sum'' = sum_list (level_inputs \<top> g)) \<and>
                  (mem g ''inp_cnt'' = of_nat (length (level_inputs \<top> g)))"

definition lock_implies_inv where [simp]:
  "lock_implies_inv t_read g = (lock_state g avg_lock \<noteq> Some t_read \<longrightarrow>
     input_invs g)"

definition min_lock_implies_min_inv where [simp]:
  "min_lock_implies_min_inv t_min g = (lock_state g min_lock \<noteq> Some t_min \<longrightarrow>
     (mem g ''min_inps'' = of_nat (sum_list (map nat_of_rat (level_inputs \<bottom> g)))))"

definition read_inp :: "thread_id \<Rightarrow> ('level, rat) Language.com" where
  "read_inp t =
   (let avg_lockA = (\<lambda> g. lock_state g avg_lock = Some t)
    in let inp_sumA = (\<lambda> g. mem g ''inp_sum'' = sum_list (level_inputs \<top> g))
    in let inp_cntA = (\<lambda> g. mem g ''inp_cnt'' = of_nat (length (level_inputs \<top> g)))
    in let inp_cntM1 = (\<lambda> g. 1 + mem g ''inp_cnt'' = of_nat (length (level_inputs \<top> g)))
    in let inp_sumM1 = (\<lambda> g. mem g ''read_buf'' + mem g ''inp_sum'' = sum_list (level_inputs \<top> g))
    in let read_bufA = (\<lambda> g. mem g ''read_buf'' = last (level_inputs \<top> g))
    in let inps_nonemptyA = (\<lambda> g. level_inputs \<top> g \<noteq> [])
    in (Acquire (\<lparr>preds = {lock_implies_inv t}\<rparr>) avg_lock ;;
        {\<lparr>preds = {avg_lockA, inp_sumA, inp_cntA}\<rparr>} ''read_buf'' <- \<top> ;;
        {\<lparr>preds = {avg_lockA, inp_sumM1, inp_cntM1, read_bufA, inps_nonemptyA}\<rparr>} 
            ''inp_cnt'' ::= BinOp (+) (Val 1) (Var ''inp_cnt'') ;;
        {\<lparr>preds = {avg_lockA, inp_sumM1, inp_cntA, read_bufA, inps_nonemptyA}\<rparr>}
            ''inp_sum'' ::= BinOp (+) (Var ''inp_sum'') (Var ''read_buf'')) ;;
        Release (\<lparr>preds = {avg_lockA, inp_sumA, inp_cntA, read_bufA, inps_nonemptyA}\<rparr>) avg_lock)"


definition inc_window :: "thread_id \<Rightarrow> thread_id \<Rightarrow> ('level, rat) Language.com" where
  "inc_window t_read t =
    (let min_inpsA = (\<lambda> g. mem g ''min_inps'' = of_nat (sum_list (map nat_of_rat (level_inputs \<bottom> g))))
     in
    (Acquire \<lparr>preds={min_lock_implies_min_inv t}\<rparr> min_lock ;;
     {\<lparr>preds = {(\<lambda> g. lock_state g min_lock = Some t), min_inpsA}\<rparr>} 
     ''increment'' <- \<bottom> ;;
     {\<lparr>preds = {(\<lambda> g. lock_state g min_lock = Some t),
                (\<lambda> g. mem g ''min_inps'' + round_inp (last_input \<bottom> g) = 
                        of_nat (sum_list (map nat_of_rat (level_inputs \<bottom> g)))),
                (\<lambda> g. mem g ''increment'' = last_input \<bottom> g)}\<rparr>}
     ''increment'' ::= UnOp round_inp (Var ''increment'') ;;
     {\<lparr>preds = {(\<lambda> g. lock_state g min_lock = Some t),
                (\<lambda> g. mem g ''min_inps'' + round_inp (last_input \<bottom> g) = 
                        of_nat (sum_list (map nat_of_rat (level_inputs \<bottom> g)))),
                (\<lambda> g. mem g ''increment'' = round_inp (last_input \<bottom> g))}\<rparr>} 
     ''min_inps'' ::= BinOp (+) (Var ''increment'') (Var ''min_inps'') ;;
     Release \<lparr>preds = {min_inpsA}\<rparr>  min_lock))"

definition rat_ge :: "rat \<Rightarrow> rat \<Rightarrow> rat" (infix "\<ge>\<^sub>r" 90) where
  "rat_ge r s = (if r \<ge> s then 1 else 0)"

lemma [simp]: "((r \<ge>\<^sub>r s) > 0) = (r \<ge> s)"
  by (auto simp: rat_ge_def)


definition rat_gt :: "rat \<Rightarrow> rat \<Rightarrow> rat" (infix ">\<^sub>r" 90) where
  "rat_gt r s = (if r > s then 1 else 0)"

lemma [simp]: "((r >\<^sub>r s) > 0) = (r > s)"
  by (auto simp: rat_gt_def)

definition decl_avg :: "thread_id \<Rightarrow> thread_id \<Rightarrow> thread_id \<Rightarrow> ('level, rat) Language.com" where
  "decl_avg t_read t_min t =
   (let inp_sumA = (\<lambda> g. mem g ''inp_sum'' = sum_list (level_inputs \<top> g))
    in let inp_cntA = (\<lambda> g. mem g ''inp_cnt'' = of_nat (length (level_inputs \<top> g)))
    in let avg_lockA = (\<lambda> g. lock_state g avg_lock = Some t)
    in let min_lockA = (\<lambda> g. lock_state g min_lock = Some t)
    in let min_inpsA = (\<lambda> g. mem g ''min_inps'' = of_nat (sum_list (map nat_of_rat (level_inputs \<bottom> g))))
    in let enough_inps = (\<lambda> g. mem g ''inp_cnt'' \<ge> mem g ''min_inps'')
    in let nonzero = (\<lambda> g. mem g ''inp_cnt'' > 0)
    in
    Acquire (\<lparr>preds = {lock_implies_inv t_read, min_lock_implies_min_inv t_min}\<rparr>) avg_lock ;;
    Acquire (\<lparr>preds = {min_lock_implies_min_inv t_min, avg_lockA, inp_sumA, inp_cntA}\<rparr>) min_lock ;;    
    If (\<lparr>preds = {avg_lockA, min_lockA, inp_sumA, inp_cntA, min_inpsA}\<rparr>) 
     (BinOp (\<ge>\<^sub>r) (Var ''inp_cnt'') (Var ''min_inps''))
    (If (\<lparr>preds = {avg_lockA, min_lockA, enough_inps, inp_sumA, inp_cntA, min_inpsA}\<rparr>) 
      (BinOp (>\<^sub>r) (Var ''inp_cnt'') (Val 0))
        (DOut (\<lparr>preds = {avg_lockA, min_lockA, inp_sumA, inp_cntA, enough_inps, nonzero, min_inpsA}\<rparr>) 
              \<bottom>
              (BinOp (/) (Var ''inp_sum'') (Var ''inp_cnt'')))
        ({\<lparr>preds = {min_lockA, avg_lockA, inp_sumA, inp_cntA, min_inpsA}\<rparr>} ''x'' ::= Var ''x''))
    ({\<lparr>preds = {min_lockA, avg_lockA, inp_sumA, inp_cntA, min_inpsA}\<rparr>} ''x'' ::= Var ''x'') ;;
    Release (\<lparr>preds = {min_lockA, avg_lockA, inp_sumA, inp_cntA, min_inpsA}\<rparr>) min_lock ;;
    Release (\<lparr>preds = {avg_lockA, inp_sumA, inp_cntA, min_lock_implies_min_inv t_min}\<rparr>) avg_lock)"

lemma decl_avg_typed:
  assumes Ls: "\<L> ''inp_sum'' = Some \<top>" "\<L> ''inp_cnt'' = Some \<bottom>" "\<L> ''read_buf'' = Some \<top>"
              "\<L> ''x'' = None"  "\<L> ''min_inps'' = Some \<bottom>"
            shows "has_type False \<bottom> (decl_avg t_read t_min t)"
  unfolding decl_avg_def Let_def
  apply typing
     apply (simp_all add: avg_def ann\<^sub>0_def \<D>\<^sub>p_def ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def Let_def)
    apply linarith
   apply (rule UAssign_type)
   apply (auto simp: Ls)
  apply (rule UAssign_type)
  apply (auto simp: Ls)
  done

lemma read_inp_typed:
  assumes Ls: "\<L> ''inp_sum'' = Some \<top>" "\<L> ''inp_cnt'' = Some \<bottom>" "\<L> ''read_buf'' = Some \<top>"
    "\<L> ''x'' = None"  "\<L> ''min_inps'' = Some \<bottom>"
  shows "has_type False \<bottom> (read_inp t)"
  unfolding read_inp_def Let_def
  apply typing
    apply (simp_all add: avg_def ann\<^sub>0_def \<D>\<^sub>p_def ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def Let_def)
    apply (rule LIn_type[where lev = \<top>])
     apply (auto simp: Ls)[2]
   apply (rule LAssign_type)
     apply (auto simp: Ls)[3]
   apply (simp_all add: avg_def ann\<^sub>0_def \<D>\<^sub>p_def ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def Let_def)
  apply (rule LAssign_type)
    apply (auto simp: Ls)[3]
  apply (simp_all add: avg_def ann\<^sub>0_def \<D>\<^sub>p_def ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def Let_def)
  using assms by auto

lemma inc_window_typed:
  assumes Ls: "\<L> ''inp_sum'' = Some \<top>" "\<L> ''inp_cnt'' = Some \<bottom>" "\<L> ''read_buf'' = Some \<top>"
    "\<L> ''x'' = None"  "\<L> ''min_inps'' = Some \<bottom>" "\<L> ''increment'' = Some \<bottom>"
  shows "has_type False \<bottom> (inc_window t_read t)"
  unfolding inc_window_def Let_def
  apply typing
    apply (rule LIn_type[where lev=\<bottom>])
     apply (auto simp: Ls)[2]
   apply (rule LAssign_type[where lev=\<bottom>])
     apply (auto simp: Ls)[3]
  apply (smt ann_implies_low_def assms(6) eval_exp.simps(3) exp_level_syn.simps(1) global_state_equiv_def mem_loweq_eval_same order_refl)
   apply (rule LAssign_type)
     apply (auto simp: Ls)[3]
  apply (simp_all add: avg_def ann\<^sub>0_def \<D>\<^sub>p_def ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def Let_def)
  using trace_equiv_bot_last
  using assms(5) by auto 


(* check that decl_avg satisfies its annotations on its own first *)
lemma
  fixes g :: "('level, rat) global_state"
  fixes sched :: "sched"
  assumes init_sat[simp]: "lock_implies_inv t_read g" "min_lock_implies_min_inv t_min g"
  assumes [simp]: "distinct [t, t_read, t_min]"
  shows "anns_satisfied ([mk_loc_state t (decl_avg t_read t_min)], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (decl_avg t_read t_min)]"
  note label_def[trace_lems]
  have init_anns: "translate_initial_anns ls = {g. lock_implies_inv t_read g \<and> 
                                                   min_lock_implies_min_inv t_min g}"
    by (clarsimp simp: ls_def decl_avg_def active_ann'_def split: option.splits simp: Let_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def decl_avg_def Let_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def rat_eq_def        
                               satisfies_ann_preds_def label_def last_input_upds level_input_upds
                               level_inputs_def)
          apply(crush | force_crush)+
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns
    apply (simp add: active_ann'_def satisfies_ann_preds_def ann\<^sub>0_def)
    using init_sat by auto
  thus ?thesis using ls_def by auto
qed

lemma sum_list_last_butlast[simp]:
  "ls \<noteq> [] \<Longrightarrow> sum_list (butlast ls) + last ls = sum_list ls"
  apply (induction ls, auto)
  by (simp add: add.assoc)

(* check that read_inp satisfies its annotations on its own first *)
lemma
  fixes g :: "('level, rat) global_state"
  fixes sched :: "sched"
  assumes init_sat[simp]: "input_invs g"
  shows "anns_satisfied ([mk_loc_state t read_inp], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t read_inp]"
  note label_def[trace_lems]
  have init_anns: "translate_initial_anns ls = {g. lock_implies_inv t g}"
    by (clarsimp simp: ls_def read_inp_def active_ann'_def split: option.splits simp: Let_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def read_inp_def Let_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def rat_eq_def        
                               satisfies_ann_preds_def label_def last_input_upds level_input_upds
                               level_inputs_def)
     apply(crush | force_crush)+
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns
    apply (simp add: active_ann'_def satisfies_ann_preds_def ann\<^sub>0_def)
    using init_sat by auto
  thus ?thesis using ls_def by auto
qed
(* check that inc_window satisfies its annotations on its own first *)
lemma
  fixes g :: "('level, rat) global_state"
  fixes sched :: "sched"
  assumes [simp]: "t \<noteq> t_read"

  assumes init_sat[simp]: "min_lock_implies_min_inv t g"
  shows "anns_satisfied ([mk_loc_state t (inc_window t_read)], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (inc_window t_read)]"
  note label_def[trace_lems]
  have init_anns: "translate_initial_anns ls = {g. min_lock_implies_min_inv t g}"
    by (clarsimp simp: ls_def inc_window_def active_ann'_def split: option.splits simp: Let_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def inc_window_def Let_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def rat_eq_def        
                               satisfies_ann_preds_def label_def last_input_upds level_input_upds
                               level_inputs_def)
    by auto
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns
    apply (simp add: active_ann'_def satisfies_ann_preds_def ann\<^sub>0_def)
    using init_sat by auto
  thus ?thesis using ls_def
    by auto
qed

(* check that decl_avg and read_inp satisfy their annotations on their own first *)
lemma
  fixes g :: "('level, rat) global_state"
  fixes sched :: "sched"
  assumes dist: "distinct [t_read, t_avg, t_min]"
  assumes init_sat[simp]: "input_invs g" "min_lock_implies_min_inv t_min g"
  shows "anns_satisfied ([mk_loc_state t_read read_inp, mk_loc_state t_avg (decl_avg t_read t_min)], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t_read read_inp, mk_loc_state t_avg (decl_avg t_read t_min)]"
  from dist have [simp, trace_lems]: "t_read \<noteq> t_avg" "t_min \<noteq> t_avg"
    by auto
  note label_def[trace_lems]
  have init_anns: "translate_initial_anns ls = {g. lock_implies_inv t_read g \<and> min_lock_implies_min_inv t_min g}"
    by (clarsimp simp: ls_def read_inp_def decl_avg_def active_ann'_def split: option.splits simp: Let_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def read_inp_def Let_def decl_avg_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def rat_eq_def    
        satisfies_ann_preds_def label_def last_input_upds level_input_upds
        level_inputs_def)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[1]
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[1]
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[5]
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[1]
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[1]
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[1]
                        apply(crush | force_crush)
                        apply(crush | force_crush)
                        apply auto[1]
                        apply(crush | force_crush)
                       apply(crush | force_crush)
                      apply auto[1]
                       apply(crush | force_crush)
                      apply(crush | force_crush)
                     apply(crush | force_crush)
                    apply(crush | force_crush)
                   apply auto[1]
                   apply(crush | force_crush)
                  apply(crush | force_crush)
                 apply auto[1]
                  apply(crush | force_crush)
                 apply(crush | force_crush)
                apply(crush | force_crush)
               apply(crush | force_crush)
              apply auto[1]
              apply(crush | force_crush)
             apply(crush | force_crush)
            apply auto[1]
             apply(crush | force_crush)
            apply(crush | force_crush)
           apply(crush | force_crush)
          apply(crush | force_crush)
         apply auto[1]
         apply(crush | force_crush)
        apply(crush | force_crush)
       apply auto[1]
      apply(crush | force_crush)
     apply(crush | force_crush)
    apply auto[1]
     apply(crush | force_crush)
    apply(crush | force_crush)
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns
    apply (simp add: active_ann'_def satisfies_ann_preds_def ann\<^sub>0_def)
    using init_sat 
    by auto
  thus ?thesis using ls_def by auto
qed

lemma running_avg_sats_anns:
  fixes g :: "('level, rat) global_state"
  fixes sched :: "sched"
  assumes dist: "distinct [t_read, t_avg, t_min]"
  assumes init_sat[simp]: "input_invs g" "min_lock_implies_min_inv t_min g"

  shows "anns_satisfied ([mk_loc_state t_read read_inp, mk_loc_state t_avg (decl_avg t_read t_min),
                          mk_loc_state t_min (inc_window t_read)], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t_read read_inp, mk_loc_state t_avg (decl_avg t_read t_min),
                         mk_loc_state t_min (inc_window t_read)]"
  from dist have [simp, trace_lems]: "t_read \<noteq> t_avg"
    by auto
  from dist have [simp, trace_lems]: "t_read \<noteq> t_min"
    by auto
  from dist have [simp, trace_lems]: "t_avg \<noteq> t_min"
    by auto
  note label_def[trace_lems]
  have init_anns: "translate_initial_anns ls = {g. lock_implies_inv t_read g \<and> 
                                                   min_lock_implies_min_inv t_min g}"
    by (clarsimp simp: ls_def read_inp_def decl_avg_def inc_window_def active_ann'_def split: option.splits simp: Let_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def read_inp_def Let_def decl_avg_def inc_window_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def rat_eq_def    
        satisfies_ann_preds_def label_def last_input_upds level_input_upds
        level_inputs_def)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]

                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]

                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]

                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]

                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]

                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[2]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (smt IntE \<open>t_read \<noteq> t_avg\<close> mem_Collect_eq option.inject subsetI)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply auto[1]
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                        apply (crush | force_crush)
                       apply auto[1]
                      apply (crush | force_crush)
                     apply (crush | force_crush)
                    apply auto[1]
                       apply (crush | force_crush)
                      apply (crush | force_crush)
                     apply (crush | force_crush)
                    apply (crush | force_crush)
                   apply auto[1]
                  apply (crush | force_crush)
                 apply (crush | force_crush)
                apply (crush | force_crush)
               apply (crush | force_crush)
              apply auto[1]
             apply (crush | force_crush)
            apply (crush | force_crush)
           apply (crush | force_crush)
          apply auto[1]
          apply (crush | force_crush)
         apply (crush | force_crush)
        apply (crush | force_crush)
       apply auto[1]
       apply (crush | force_crush)
      apply (crush | force_crush)
     apply (crush | force_crush)
    apply auto[1]
    apply (crush | force_crush)
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns
    apply (simp add: active_ann'_def satisfies_ann_preds_def ann\<^sub>0_def)
    using init_sat
  by auto
  thus ?thesis using ls_def by auto
qed

lemma running_avg_secure:
  assumes ls_def[simp]: "ls = [mk_loc_state t_read read_inp, mk_loc_state t_avg (decl_avg t_read t_min),
                 mk_loc_state t_min (inc_window t_read)]"
      and [simp]: "distinct [t_read, t_avg, t_min]"
      and Ls: "\<L> ''inp_sum'' = Some \<top>" "\<L> ''inp_cnt'' = Some \<bottom>" "\<L> ''read_buf'' = Some \<top>"
    "\<L> ''x'' = None"  "\<L> ''min_inps'' = Some \<bottom>" "\<L> ''increment'' = Some \<bottom>"
    shows "system_secure \<bottom> (\<lambda> g. input_invs g \<and> min_lock_implies_min_inv t_min g) sched ls"
  apply (rule soundness[where ah=False])
proof -
  fix g
  assume P[simp]: "input_invs g \<and> min_lock_implies_min_inv t_min g" 
  thus "anns_satisfied (ls, g, sched)"
    apply (insert running_avg_sats_anns)
    apply (auto simp del: anns_satisfied.simps)
    using P assms by force
next
  show "list_all (has_type False \<bottom> \<circ> local_state.com) ls"
    unfolding ls_def
    using read_inp_typed decl_avg_typed inc_window_typed
    by (auto simp: Ls)
qed

end

end
