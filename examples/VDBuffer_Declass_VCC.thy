(* This is a copy of VDBuffer_Declass with minimal annotations
  used to show that we can verify these annotations with VCC instead
  of Isabelle. See vdbuffer.c for a C translation of this program. *)
theory VDBuffer_Declass_VCC
  imports Main "../TypeSystem" "../Automation" "../OGTranslation" "~~/src/HOL/Hoare_Parallel/OG_Tactics" "../DelimitedRelease"
begin

consts
  CHECK_SIG :: "'a \<Rightarrow> 'a"

definition vdbuffer_dr_pol :: "('level::complete_lattice, nat) dr_policy" where
  "vdbuffer_dr_pol from to =
    (if from = \<top>
     then {\<lambda> t. if t = [] then 0 else CHECK_SIG (last t)} \<union>
          {\<lambda> t. (if t = [] then 0 else (if CHECK_SIG (last t) = 0 then last t else 0))}
     else {})"

locale delimited_vdbuffer = delimited_release where \<E> = vdbuffer_dr_pol 
  and level_rename = lr and val_rename = 0 and val_true = "(<) 0"
  for lr :: "'level::complete_lattice"
  and vr :: nat

context delimited_vdbuffer begin


definition nat_eq :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "nat_eq x y = (if x = y then 1 else 0)"

definition set_to_ann :: "('level, nat) pred set \<Rightarrow> ('level, nat) ann" where
  "set_to_ann Ps = \<lparr>preds = Ps\<rparr>"

declare
  [[coercion_enabled]]
  [[coercion "Var :: var \<Rightarrow> nat exp", coercion "Val :: nat \<Rightarrow> nat exp", coercion set_to_ann]]

(* TODO: move this stuff elsewhere *)
definition vd_lock :: lock where [simp]: "vd_lock = 0"

fun mk_loc_state :: "thread_id \<Rightarrow> (thread_id \<Rightarrow> ('level, nat) lcom) \<Rightarrow> ('level, nat) local_state" where
  "mk_loc_state t cf = \<lparr>local_state.com = cf t, tid = t\<rparr>"

definition mode_lock :: "lock" where [simp]: "mode_lock = 1"

definition vd_fetch' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_fetch' t =
    (Acquire ann\<^sub>0 vd_lock ;;
     (If ann\<^sub>0 (BinOp nat_eq ''mode'' (0::nat))
          ({ann\<^sub>0} ''buf'' <- \<bottom>)
          ({ann\<^sub>0} ''buf'' <- \<top>)) ;;
     {ann\<^sub>0} ''valid'' ::= Val 1 ;;
     Release ann\<^sub>0 vd_lock)"

definition vd_forward' :: "thread_id \<Rightarrow> thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_forward' t_toggle t =
    ((Acquire ann\<^sub>0 vd_lock ;;
      (If ann\<^sub>0 (BinOp nat_eq ''valid'' (1::nat))
         (If ann\<^sub>0 (BinOp nat_eq ''mode2'' (0::nat))
             (Assign \<lparr>preds = { \<lambda> g. mem g ''buf'' = last_input \<bottom> g }\<rparr> ''lbuf'' ''buf'')
         ((Assign \<lparr>preds = { \<lambda> g. mem g ''buf'' = last_input \<top> g}\<rparr> ''hbuf'' ''buf'') ;;
          {\<lparr>preds = { \<lambda> g. mem g ''hbuf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> []}\<rparr>} ''ldeclas'' :D= (UnOp CHECK_SIG (Var ''hbuf'')) ;;
          (If \<lparr>preds = {}\<rparr> (BinOp nat_eq ''ldeclas'' (0::nat))
             (DAssign \<lparr>preds = { \<lambda> g. mem g ''hbuf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> [], \<lambda> g. CHECK_SIG (mem g ''hbuf'') = 0}\<rparr> ''lbuf'' ''hbuf'')
             ({\<lparr>preds = {}\<rparr>} ''x'' ::= Var ''x''))
         ))
         ({\<lparr>preds = {}\<rparr>} ''x'' ::= ''x'')) ;;
      Release \<lparr>preds = {}\<rparr> vd_lock))"

definition vd_lout' :: "('level, nat) lcom" where
  "vd_lout' = Out ann\<^sub>0 \<bottom> ''lbuf''"

definition vd_hout' :: "('level, nat) lcom" where
  "vd_hout' = Out ann\<^sub>0 \<top> ''hbuf''"

definition vd_toggle' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_toggle' t =
    (
     (Acquire ann\<^sub>0 vd_lock ;;
      {\<lparr>preds = {}\<rparr>} ''valid'' ::= Val 0 ;;
      {\<lparr>preds = {}\<rparr>} ''mode'' ::= UnOp (\<lambda> n. n + 1) ''mode'' ;;
      {\<lparr>preds = {}\<rparr>} ''mode2'' ::= ''mode'' ;;
      Release \<lparr>preds = {}\<rparr> vd_lock))"

lemma forward'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>"
              "\<L> ''ldeclas'' = Some \<bottom>"  "\<L> ''hbuf'' = Some \<top>" "\<L> ''lbuf'' = Some \<bottom>"
            shows "has_type False \<bottom> (vd_forward' t_toggle t)"
  unfolding vd_forward'_def Let_def
  apply typing
          apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
            apply (auto simp: Ls)[1]
           apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
         apply(rule LAssign_type[where lev=\<top> and l\<^sub>e=\<top>])
           apply (auto simp: Ls)[1]
          apply (auto simp: ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
        apply(rule DAssign_type[where l\<^sub>e=\<top>])
          apply(auto simp: Ls)[2]
        apply(simp add: embed_dr_def)
        apply(rule_tac x="\<lambda> t. if t = [] then 0 else CHECK_SIG (last t)" in bexI)
         apply (auto simp: embed_dr_def vdbuffer_dr_pol_def eval_hatch_def satisfies_ann_preds_def last_input_def)[1]
        apply(fastforce simp: vdbuffer_dr_pol_def)
       apply(rule DAssign_type)
         apply((fastforce simp: Ls)+)[2]
       apply(simp add: embed_dr_def)
       apply(rule_tac x="\<lambda> t. (if t = [] then 0 else (if CHECK_SIG (last t) = 0 then last t else 0))" in bexI)
        apply (auto simp: embed_dr_def vdbuffer_dr_pol_def eval_hatch_def satisfies_ann_preds_def last_input_def)[1]
       apply(fastforce simp: vdbuffer_dr_pol_def)
      apply(rule UAssign_type)
      apply(simp add: Ls)
     apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
   apply (rule UAssign_type)
   apply(simp add: Ls)
  apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)
  done


lemma fetch'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
    "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>"
  shows "has_type False \<bottom> (vd_fetch' t)"
  unfolding vd_fetch'_def Let_def
  apply typing
     apply (rule UIn_type)
     apply (auto simp: Ls)[1]
    apply (rule UIn_type)
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
  by (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]

lemma toggle'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
    "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>"
  shows "has_type False \<bottom> (vd_toggle' t)"
  unfolding vd_toggle'_def Let_def
  apply typing
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
      apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
      apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
    apply (auto simp: Ls)[1]
  by (auto simp: ann_implies_low_def global_state_equiv_def mem_loweq_def Ls)

lemma lout'_typed:
  assumes Ls: "\<L> ''lbuf'' = Some \<bottom>"
            shows "has_type False \<bottom> (vd_lout')"
  unfolding vd_lout'_def Let_def
  apply typing
  by(auto simp: ann_implies_low_def global_equiv_defs Ls)

lemma hout'_typed:
  assumes Ls: "\<L> ''hbuf'' = Some \<top>" 
            shows "has_type False \<bottom> (vd_hout')"
  unfolding vd_hout'_def Let_def
  apply typing
  by(auto simp: ann_implies_low_def global_equiv_defs Ls)

text {* To demonstrate how we are verifying an extensional property
for this example, we explicitly expand the definition of delimited release,
and show that this is implied by proving security with respect to @{term system_secure}.
*}

lemma system_secure_implies_dr:
  assumes ls_def: "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward' t''), 
                         mk_loc_state t'' vd_toggle',
                         \<lparr>local_state.com = vd_hout', tid = t'''\<rparr>,
                         \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
      and Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>" "\<L> ''ldeclas'' = Some \<bottom>"
              "\<L> ''hbuf'' = Some \<top>"
              "\<L> ''lbuf'' = Some \<bottom>"
      and P_def: "P = (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g)"
      and neqs: "distinct [t, t', t'',t''',t'''']"
      and br: "no_high_br \<bottom> P ls"
      and sec: "system_secure \<bottom> P sched ls"
      and anns: "(\<And>g. P g \<Longrightarrow> anns_satisfied (ls, g, sched))"
    shows "dr_secure \<bottom> P sched ls"
  using embedding_sound
  using anns br sec by blast

lemma extensional_dr_equiv:
  "dr_equiv \<bottom> e e' =
   (let signature_result = (\<lambda> t. if t = [] then 0 else CHECK_SIG (last t))
    in let valid_sig = (\<lambda> t. (if t = [] then 0 else (if CHECK_SIG (last t) = 0 then last t else 0)))
    in (\<forall> n. \<forall> h \<in> {valid_sig, signature_result}.
      eval_hatch h (stream_take n (e \<top>)) = eval_hatch h (stream_take n (e' \<top>))))"
  unfolding dr_equiv_def dr_equiv'_def vdbuffer_dr_pol_def Let_def
  by (simp add: bot.extremum_unique )

lemma extensional_dr_sec:
  "dr_secure \<bottom> P sched ls =
   (\<forall> g\<^sub>1 ls\<^sub>2 g\<^sub>2 sched\<^sub>2 g\<^sub>1'. 
     P g\<^sub>1 \<and> P g\<^sub>1' \<and> mem g\<^sub>1 = mem g\<^sub>1' \<and> g\<^sub>1 =\<^sup>g\<^bsub>\<bottom>\<^esub> g\<^sub>1' \<and>
     dr_equiv \<bottom> (all_inputs g\<^sub>1) (all_inputs g\<^sub>1') \<and>
     same_input_count g\<^sub>1 g\<^sub>1' \<and>
     global_meval_abv (ls, g\<^sub>1, sched) (ls\<^sub>2, g\<^sub>2, sched\<^sub>2) \<longrightarrow>
       (\<exists> ls\<^sub>2' g\<^sub>2' sched\<^sub>2'. global_meval_abv (ls, g\<^sub>1', sched) (ls\<^sub>2', g\<^sub>2', sched\<^sub>2') \<and> trace g\<^sub>2 =\<^sup>t\<^bsub>\<bottom>\<^esub> trace g\<^sub>2'))"
  unfolding dr_secure_def
  by blast

end

end
