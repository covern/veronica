theory VDBuffer_Declass_Confirm_OG
  imports "../Security" "../OGTranslation" VDBuffer_Declass_Confirm
begin


context vdbuffer_confirm
begin


(* check that vd_forward' satisfies its annotations on its own first *)
lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes init: "mode_inputA g \<and> lock_modeeq t' g"
  assumes neqs[trace_lems]: "t \<noteq> t'"
    and foo: "(\<bottom>::'level) \<noteq> \<top>"
  shows "anns_satisfied ([mk_loc_state t (vd_forward' t')], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (vd_forward' t')]"
  note neqs[trace_lems]
  note label_def[trace_lems]
 have init_anns: "translate_initial_anns ls = {g. mode_inputA g \<and> lock_modeeq t' g}"
    by (clarsimp simp: ls_def vd_fetch'_def vd_modeeq_simpl_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def label_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_modeeq_simpl_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def nat_eq_def  last_output_def      
                               satisfies_ann_preds_def label_def last_input_upds level_input_upds
                               level_inputs_def)
    apply (auto simp: trace_lems)[6]
    using foo
         apply force
    apply (auto simp: trace_lems)[5]
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns init by auto
  thus ?thesis using ls_def by auto
qed

(* check that vd_fetch' satisfies its annotations on its own first *)
lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neqs[trace_lems]: "t \<noteq> t'" 
  shows "anns_satisfied ([mk_loc_state t (vd_fetch')], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (vd_fetch')]"
  note neqs[trace_lems]
  note label_def[trace_lems]
 have init_anns: "translate_initial_anns ls = UNIV"
    by (clarsimp simp: ls_def vd_fetch'_def vd_modeeq_simpl_def active_ann'_def split: option.splits simp: Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_modeeq_simpl_def
    apply (clarsimp)
    apply oghoare
              apply (simp_all add: active_ann'_def  ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def last_input_upds)
    apply (crush | force_crush)+
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns by auto
  thus ?thesis using ls_def by auto
qed

(* check that vd_hout' satisfies its annotations on its own first *)
lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neqs[trace_lems]: "t \<noteq> t'" 
  shows "anns_satisfied ([mk_loc_state t (vd_hout')], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (vd_hout')]"
  note neqs[trace_lems]
  note label_def[trace_lems]
 have init_anns: "translate_initial_anns ls = UNIV"
   by (clarsimp simp: ls_def vd_hout'_def vd_modeeq_simpl_def active_ann'_def split: option.splits simp: Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_hout'_def vd_forward'_def vd_toggle'_def Let_def vd_modeeq_simpl_def
    apply (clarsimp)
    apply oghoare
              apply (simp_all add: active_ann'_def  ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def last_input_upds)
    done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns by auto
  thus ?thesis using ls_def by auto
qed

(*
lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neqs[trace_lems]: "t \<noteq> t'" "t' \<noteq> t''" "t \<noteq> t''"
  shows "anns_satisfied ([mk_loc_state t (vd_forward' t''),
                          mk_loc_state t' vd_fetch'], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (vd_forward' t''), 
                         mk_loc_state t' vd_fetch']"
  note neqs[trace_lems]
  note label_def[trace_lems]
 have init_anns: "translate_initial_anns ls = UNIV"
    by (clarsimp simp: ls_def vd_fetch'_def vd_modeeq_simpl_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_modeeq_simpl_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def  ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def last_input_upds level_input_upds level_inputs_def) 
    apply ((crush | force_crush)+)
  done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns by auto
  thus ?thesis using ls_def by auto
qed



lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neqs[trace_lems]: "t \<noteq> t'" "t \<noteq> t''" "t' \<noteq> t''"
  shows "anns_satisfied ([mk_loc_state t (vd_forward' t'),
                          mk_loc_state t' vd_toggle'], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t (vd_forward' t'), 
                         mk_loc_state t' vd_toggle']"
  note neqs[trace_lems]
 have init_anns: "translate_initial_anns ls = UNIV"
    by (clarsimp simp: ls_def vd_fetch'_def vd_modeeq_simpl_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_modeeq_simpl_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def nat_eq_def        
                               satisfies_ann_preds_def label_def last_input_upds level_input_upds
                               level_inputs_def)
    apply(crush | force_crush)+
  done

  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns by auto
  thus ?thesis using ls_def by auto
qed

lemma
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes neqs[trace_lems]: "t \<noteq> t'"
  shows "anns_satisfied ([mk_loc_state t vd_fetch', 
                          mk_loc_state t' vd_toggle'], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' vd_toggle']"
  note neqs[trace_lems]
 have init_anns: "translate_initial_anns ls = UNIV"
    by (clarsimp simp: ls_def vd_fetch'_def vd_modeeq_simpl_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_modeeq_simpl_def
    apply (clarsimp)
    apply oghoare


                        apply (simp_all add: active_ann'_def ann\<^sub>0_def nat_eq_def satisfies_ann_preds_def label_def)
    by (crush | force_crush)+
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns by auto
  thus ?thesis using ls_def by auto
qed
*)


lemma fetch_forward_toggle_sat:
  fixes g :: "('level, nat) global_state"
  fixes sched :: "sched"
  assumes init: "mode_inputA g \<and> lock_modeeq t'' g"
  assumes neq: "distinct [t, t', t'', t''', t'''']"
    and foo [simp]: "(\<bottom>::'level) \<noteq> \<top>"
  shows "anns_satisfied ([mk_loc_state t vd_fetch', 
                          mk_loc_state t' (vd_forward' t''), 
                          mk_loc_state t'' vd_toggle',
                          \<lparr>local_state.com = vd_hout' t''', tid = t'''\<rparr>,
                          \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>], g, sched)"
proof -
  define ls where "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward' t''), 
                         mk_loc_state t'' vd_toggle',
                         \<lparr>local_state.com = vd_hout' t''', tid = t'''\<rparr>,
                         \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
  let ?A = "{t, t', t'',t''',t''''}"
  have "finite ?A" by auto
  note neqs[trace_lems] = neq[simplified]
  have init_anns: "translate_initial_anns ls = {g. mode_inputA g \<and> lock_modeeq t'' g}"
    by (clarsimp simp: ls_def vd_fetch'_def active_ann'_def split: option.splits simp: vd_forward'_def Let_def vd_toggle'_def ann\<^sub>0_def satisfies_ann_preds_def vd_lout'_def vd_hout'_def label_def)
  then have "\<parallel>- (translate_initial_anns ls) (translate_coms ls) UNIV"
    unfolding ls_def vd_fetch'_def vd_forward'_def vd_toggle'_def Let_def vd_lout'_def vd_hout'_def
    apply (clarsimp)
    apply oghoare
                        apply (simp_all add: active_ann'_def ann\<^sub>0_def nat_eq_def        
                               satisfies_ann_preds_def label_def last_input_upds level_input_upds
                               level_inputs_def last_input_def level_outputs_def last_output_def)    (* very slow, be patient *)   
    apply((crush | (clarsimp simp: level_inputs_def last_input_def last_output_def level_outputs_def) | force_crush)+) (* very slow, be patient, it works *)
  done
  hence "anns_satisfied (ls, g, sched)"
    apply (rule og_hoare_par_soundness)
    apply simp
    using init_anns init by auto
  thus ?thesis using ls_def by auto
qed

lemma fetch_forward_toggle_hout_lout_secure:
  assumes ls_def: "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward' t''), 
                         mk_loc_state t'' vd_toggle',
                         \<lparr>local_state.com = vd_hout' t''', tid = t'''\<rparr>,
                         \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
      and Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>" "\<L> ''ldeclas'' = Some \<bottom>"
              "\<L> ''hbuf'' = Some \<top>"
              "\<L> ''lbuf'' = Some \<bottom>" "\<L> ''user_answer'' = Some \<bottom>"
      and neqs: "distinct [t, t', t'',t''',t'''']"
      and sane: "(\<top>::'level) \<noteq> \<bottom>"
    shows "system_secure \<bottom> (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g) sched ls"
  apply (rule soundness)
  using fetch_forward_toggle_sat assms apply force
  unfolding ls_def  apply clarsimp
  using fetch'_typed forward'_typed toggle'_typed hout'_typed lout'_typed assms by blast 

end
end
