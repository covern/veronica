#include <vcc.h>

// Definitions for spinlocks in VCC
_(claimable, volatile_owns) struct Lock {
  volatile int locked;
  _(ghost \object protected_obj;)
  _(invariant locked == 0 ==> \mine(protected_obj))
};

void InitializeLock(struct Lock *l _(ghost \object obj))
  _(writes \span(l), obj)
  _(requires \wrapped(obj))
  _(ensures \wrapped(l) && l->protected_obj == obj)
{
  l->locked = 0;
  _(ghost {
    l->protected_obj = obj;
    l->\owns = {obj};
    _(wrap l)
  })
}

_(atomic_inline) int InterlockedCompareExchange(volatile int *Destination, int Exchange, int Comparand) {
  if (*Destination == Comparand) {
    *Destination = Exchange;
    return Comparand;
  } else {
    return *Destination;
  }
}

void Acquire(struct Lock *l _(ghost \claim c))
  _(always c, l->\closed)
  _(ensures \wrapped(l->protected_obj) && \fresh(l->protected_obj))
{
  int stop = 0;

  do {
    _(atomic c, l) {
      stop = InterlockedCompareExchange(&l->locked, 1, 0) == 0;
      _(ghost if (stop) l->\owns -= l->protected_obj)
    }
  } while (!stop);
}

void Release(struct Lock *l _(ghost \claim c))
  _(always c, l->\closed)
  _(requires l->protected_obj != c)
  _(writes l->protected_obj)
  _(requires \wrapped(l->protected_obj))
{
  _(atomic c, l) {
    l->locked = 0;
    _(ghost l->\owns += l->protected_obj)
  }
}

// Model for input and output functions
extern int low_input();
extern int high_input();
extern void low_output(int);
extern void high_output(int);
extern void declass_output(int);
_(pure)
extern int check_sig(int);

_(ghost 
_(claimable) struct _DATA_CONTAINER {
  int dummy;
  _(invariant \mine(&DataLock))
  _(invariant DataLock.protected_obj == &Data)
} DataContainer;)

struct Lock DataLock;

// Model for shared state
typedef struct _DATA {
  int buf;
  int mode;
  int mode2;
  int lbuf;
  int hbuf;
  int valid;
  // We keep track of the last low input and output
  // It's sufficient for this example to just track low
  // input. Since this is purely for modelling purposes,
  // we leave this as ghost data.
  _(ghost int low_in, low_out, high_in)
  _(invariant valid ? ((mode == 0) ? buf == low_in : buf == high_in) : \true)
  _(invariant mode == mode2)
} DATA;

DATA Data;

void vd_fetch(_(ghost \claim c)) 
  _(always c, (&DataContainer)->\closed)
{
	Acquire(&DataLock _(ghost c));
	_(unwrapping &Data) {
		if (Data.mode == 0) {
			Data.buf = low_input();
			_(ghost {Data.low_in = Data.buf;})
		} else {
			Data.buf = high_input();
			_(ghost {Data.high_in = Data.buf;})
		}
		Data.valid = 1;
	}
	Release(&DataLock _(ghost c));
}


void vd_forward(_(ghost \claim c)) 
  _(always c, (&DataContainer)->\closed)
{
	Acquire(&DataLock _(ghost c));
	_(unwrapping &Data) {
		if (Data.valid) {
			if (Data.mode2 == 0) {
				_(assert Data.buf == Data.low_in)
				Data.lbuf = Data.buf;
			} else {
				_(assert Data.buf == Data.high_in)
				Data.hbuf = Data.buf;
				if (check_sig(Data.hbuf)) {
					_(assert Data.hbuf == Data.high_in)
					_(assert check_sig(Data.hbuf))
					Data.lbuf = Data.hbuf;
				}
			}
		}
	}
	Release(&DataLock _(ghost c));
}

void vd_lout(_(ghost \claim c)) 
	_(always c, (&DataContainer)->\closed)
{
	Acquire(&DataLock _(ghost c));
	low_output(Data.lbuf);
	Release(&DataLock _(ghost c));
}

void vd_hout(_(ghost \claim c))
	_(always c, (&DataContainer)->\closed)

{
	Acquire(&DataLock _(ghost c));
	high_output(Data.hbuf);
	Release(&DataLock _(ghost c));
}

void vd_toggle(_(ghost \claim c))
	_(always c, (&DataContainer)->\closed)
{
	Acquire(&DataLock _(ghost c));
	_(unwrapping &Data) {
		Data.mode = !Data.mode;
		Data.mode2 = !Data.mode2;
		Data.valid = 0;
	}
	Release(&DataLock _(ghost c));
}


// Initialize system to consistent state:
void boot()
  _(writes \universe())1
  _(requires \program_entry_point())
{
  _(ghost \claim c;)
    _(ghost { Data.low_in = 0; })
	  Data.buf = 0;
	  Data.mode = 0;
	  Data.mode2 = 0;
	  Data.valid = 0;
  _(wrap &Data)
  InitializeLock(&DataLock _(ghost &Data));
  _(ghost (&DataContainer)->\owns = {&DataLock, &DataContainer})
  _(wrap &DataContainer)

  _(ghost c = \make_claim({&DataContainer}, (&DataContainer)->\closed);)
  vd_forward(_(ghost c));
  vd_fetch(_(ghost c));
  vd_toggle(_(ghost c));
  vd_lout(_(ghost c));
  vd_hout(_(ghost c));
}
