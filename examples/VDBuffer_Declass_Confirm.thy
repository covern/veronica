theory VDBuffer_Declass_Confirm
  imports Main "../TypeSystem" "../Automation" "../OGTranslation" "~~/src/HOL/Hoare_Parallel/OG_Tactics"
begin

definition \<D>\<^sub>p :: "('level::complete_lattice) \<Rightarrow> ('level::complete_lattice, nat) decl_pred" where
  "\<D>\<^sub>p from to g v e \<equiv> (v = last_output \<top> g \<and> last_input \<bottom> g = 1)"


locale vdbuffer_confirm = lang 
  where level_rename_param = lr and \<D> = "\<D>\<^sub>p" and val_rename_param = "0::nat"
    and val_true = "(<) 0"
  for lr :: "'level::complete_lattice" + assumes [simp]: "(\<top>::'level) \<noteq> \<bottom>"

context vdbuffer_confirm begin

definition nat_eq :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "nat_eq x y = (if x = y then 1 else 0)"

definition set_to_ann :: "('level, nat) pred set \<Rightarrow> ('level, nat) ann" where
  "set_to_ann Ps = \<lparr>preds = Ps\<rparr>"


declare
  [[coercion_enabled]]
  [[coercion "Var :: var \<Rightarrow> nat exp", coercion "Val :: nat \<Rightarrow> nat exp", coercion set_to_ann]]

(*
abbreviation bot_abv :: "'level" ("\<bottom>'") where "\<bottom>' \<equiv> bot"

abbreviation top_abv :: "'level" ("\<top>'") where "\<top>' \<equiv> bot"
*)
term "\<bottom>::'level"

term tt
term top

definition top_alias  :: "'level" where [simp]: "top_alias = \<top>"
(* TODO: move this stuff elsewhere *)
definition vd_lock :: lock where [simp]: "vd_lock = 0"
definition bot_alias  :: "'level" where [simp]: "bot_alias = \<bottom>"


fun mk_loc_state :: "thread_id \<Rightarrow> (thread_id \<Rightarrow> ('level, nat) lcom) \<Rightarrow> ('level, nat) local_state" where
  "mk_loc_state t cf = \<lparr>local_state.com = cf t, tid = t\<rparr>"

definition mode_inputA :: "('level, nat) pred" where
  [simp]: "mode_inputA = (\<lambda> g. mem g ''valid'' = 1 \<longrightarrow> 
                               (if mem g ''mode'' = 0 
                                then mem g ''buf'' = last_input \<bottom> g \<and> level_inputs \<bottom> g \<noteq> []
                                else mem g ''buf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> []))"

definition mode_input_weak :: "('level, nat) pred" where
  [simp]: "mode_input_weak = (\<lambda> g. if mem g ''mode'' = 0             
                                   then mem g ''buf'' = last_input \<bottom> g \<and> level_inputs \<bottom> g \<noteq> []
                                   else mem g ''buf'' = last_input \<top> g \<and> level_inputs \<top> g \<noteq> [])" 

definition is_validA :: "('level, nat) pred" where
  [simp]: "is_validA = (\<lambda> g. mem g ''valid'' = 1)"

definition lock_modeeq :: "thread_id \<Rightarrow> ('level, nat) pred" where
  [simp]: "lock_modeeq t g = ((lock_state g vd_lock \<noteq> Some t) \<longrightarrow> mem g ''mode'' = mem g ''mode2'')"

definition label :: "string \<Rightarrow> ('level, nat) pred" where
  "label x g = True"

definition mode_lock :: "lock" where [simp]: "mode_lock = 1"

definition vd_fetch' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_fetch' t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in let mode0 = (\<lambda> g. mem g ''mode'' = (0::nat))
     in let moden0 = Not \<circ> mode0
     in
     Acquire ann\<^sub>0 vd_lock ;;
     (If \<lparr>preds = {loc, label ''fetch_if''}\<rparr> (BinOp nat_eq ''mode'' (0::nat))
          ({\<lparr>preds = {loc, mode0}\<rparr>} ''buf'' <- \<bottom>)
          ({\<lparr>preds = {loc, moden0}\<rparr>} ''buf'' <- \<top>)) ;;
     {\<lparr>preds = {loc, mode_input_weak, label ''set_valid''}\<rparr>} ''valid'' ::= Val 1 ;;
     Release \<lparr>preds = {loc, mode_inputA, is_validA, label ''fetch_release''}\<rparr> vd_lock)"

definition vd_forward' :: "thread_id \<Rightarrow> thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_forward' t_toggle t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in let modeeq = (\<lambda> g. mem g ''mode'' = mem g ''mode2'')
     in let mode0 = (\<lambda> g. mem g ''mode2'' = (0::nat))
     in let moden0 = Not \<circ> mode0
     in let hbuf_buf = (\<lambda> g. mem g ''hbuf'' = mem g ''buf'')
     in let lanswer = (\<lambda> g. mem g ''user_answer'' = last_input \<bottom> g)
     in let lout = (\<lambda> g. mem g ''buf'' = last_output \<top> g)
     in let lanswer1 = (\<lambda> g. mem g ''user_answer'' = 1)
     in
     (Acquire \<lparr>preds = {mode_inputA, lock_modeeq t_toggle, label ''forward_acquire2''}\<rparr> vd_lock ;;
      (If \<lparr>preds = {loc, modeeq, mode_inputA}\<rparr> (BinOp nat_eq ''valid'' (1::nat))
         (If \<lparr>preds = {loc, modeeq, mode_inputA, is_validA}\<rparr> (BinOp nat_eq ''mode2'' (0::nat))
             (Assign \<lparr>preds = {loc, modeeq, mode0, mode_inputA, is_validA}\<rparr> ''lbuf'' ''buf'')
         ((Assign \<lparr>preds = {loc, modeeq, moden0, mode_inputA, is_validA}\<rparr> ''hbuf'' ''buf'') ;;
          Out (\<lparr>preds = {loc, modeeq, moden0, mode_inputA, is_validA, hbuf_buf}\<rparr>) \<top> (Var ''buf'') ;;
          {\<lparr>preds = {loc, modeeq, moden0, mode_inputA, is_validA, hbuf_buf, lout}\<rparr>} ''user_answer'' <- \<bottom> ;;
          (If \<lparr>preds = {loc, modeeq, moden0, mode_inputA, is_validA, hbuf_buf, lout, lanswer}\<rparr> (BinOp nat_eq ''user_answer'' (1::nat))
             (DAssign \<lparr>preds = {loc, modeeq, moden0, mode_inputA, is_validA, hbuf_buf, lanswer, lanswer1, lout}\<rparr> ''lbuf'' ''hbuf'')
             ({\<lparr>preds = {loc, modeeq, moden0, mode_inputA, is_validA}\<rparr>} ''x'' ::= Var ''x''))
         ))
         ({\<lparr>preds = {loc, modeeq}\<rparr>} ''x'' ::= ''x'')) ;;
      Release \<lparr>preds = {loc, modeeq}\<rparr> vd_lock))"

definition vd_lout' :: "('level, nat) lcom" where
  "vd_lout' = Out ann\<^sub>0 \<bottom> ''lbuf''"

(* Not sure if the locking here is necessary *)
definition vd_hout' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_hout' t = (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
                 in Acquire \<lparr>preds = {}\<rparr> vd_lock ;;
                    Out \<lparr>preds = {loc}\<rparr> \<top> ''hbuf'';;
                    Release \<lparr>preds = {}\<rparr> vd_lock)"


definition vd_modeeq_simpl :: "thread_id \<Rightarrow> thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_modeeq_simpl t_toggle t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in let modeeq = (\<lambda> g. mem g ''mode'' = mem g ''mode2'')
     in
     {ann\<^sub>0} ''mode2'' ::= ''mode'' ;;
     Acquire \<lparr>preds = {label ''acquire_mid'', lock_modeeq t_toggle}\<rparr> vd_lock ;;
     {\<lparr>preds = {loc, modeeq, label ''x_assign''}\<rparr>} ''x'' ::= ''x'' ;;
     Release \<lparr>preds = {loc, modeeq}\<rparr> vd_lock)"

definition vd_toggle' :: "thread_id \<Rightarrow> ('level, nat) lcom" where
  "vd_toggle' t =
    (let loc = (\<lambda> g. lock_state g vd_lock = Some t)
     in let modeeq = (\<lambda> g. mem g ''mode'' = mem g ''mode2'')
     in let nvalid = (Not \<circ> is_validA)
     in
     (Acquire ann\<^sub>0 vd_lock ;;
      {\<lparr>preds = {loc}\<rparr>} ''valid'' ::= Val 0 ;;
      {\<lparr>preds = {loc, nvalid}\<rparr>} ''mode'' ::= UnOp (\<lambda> n. n + 1) ''mode'' ;;
      {\<lparr>preds = {loc, nvalid}\<rparr>} ''mode2'' ::= ''mode'' ;;
      Release \<lparr>preds = {loc, modeeq, nvalid}\<rparr> vd_lock))"

lemma forward'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>"
              "\<L> ''user_answer'' = Some \<bottom>"  "\<L> ''hbuf'' = Some \<top>" "\<L> ''lbuf'' = Some \<bottom>"
            shows "has_type False \<bottom> (vd_forward' t_toggle t)"
  unfolding vd_forward'_def Let_def
  apply typing
          apply(rule LAssign_type[where lev=\<bottom> and l\<^sub>e=\<bottom>])
            apply (auto simp: Ls)[1]
           apply (auto simp: ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
         apply(rule LAssign_type[where lev=\<top> and l\<^sub>e=\<top>])
           apply (auto simp: Ls)[1]
           apply (auto simp: ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
           apply (auto simp: ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
        apply (rule LIn_type[where lev=\<bottom>])
           apply (auto simp: Ls)[2]
        apply(rule DAssign_type[where l\<^sub>e=\<top>])
         apply(auto simp: Ls)[2]
           apply (auto simp: \<D>\<^sub>p_def ann_implies_low_def trace_equiv_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
      apply(rule UAssign_type)
      apply(simp add: Ls)
     apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[2]
   apply (rule UAssign_type)
   apply(simp add: Ls)
  apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)
  done

lemma fetch'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
    "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>"
  shows "has_type False \<bottom> (vd_fetch' t)"
  unfolding vd_fetch'_def Let_def
  apply typing
     apply (rule UIn_type)
     apply (auto simp: Ls)[1]
    apply (rule UIn_type)
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
  by (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]

lemma toggle'_typed:
  assumes Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
    "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>"
  shows "has_type False \<bottom> (vd_toggle' t)"
  unfolding vd_toggle'_def Let_def
  apply typing
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
      apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
      apply (auto simp: Ls)[1]
    apply (auto simp: ann_implies_low_def trace_equiv_bot_last global_state_equiv_def assms mem_loweq_def satisfies_ann_preds_def)[3]
  apply (rule LAssign_type[where lev = \<bottom> and l\<^sub>e = \<bottom>])
    apply (auto simp: Ls)[1]
  by (auto simp: ann_implies_low_def global_state_equiv_def mem_loweq_def Ls)

lemma lout'_typed:
  assumes Ls: "\<L> ''lbuf'' = Some \<bottom>"
            shows "has_type False \<bottom> (vd_lout')"
  unfolding vd_lout'_def Let_def
  apply typing
  by(auto simp: ann_implies_low_def global_equiv_defs Ls)

lemma hout'_typed:
  assumes Ls: "\<L> ''hbuf'' = Some \<top>" 
            shows "has_type False \<bottom> (vd_hout' t)"
  unfolding vd_hout'_def Let_def
  apply typing
  by(auto simp: ann_implies_low_def global_equiv_defs Ls)

lemma extensional:
  assumes ls_def: "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward' t''), 
                         mk_loc_state t'' vd_toggle',
                         \<lparr>local_state.com = vd_hout' t''', tid = t'''\<rparr>,
                         \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
      and Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>" "\<L> ''ldeclas'' = Some \<bottom>"
              "\<L> ''hbuf'' = Some \<top>"
              "\<L> ''lbuf'' = Some \<bottom>" "\<L> ''user_answer'' = Some \<bottom>"
      and neqs: "distinct [t, t', t'',t''',t'''']"
      and sane: "(\<top>::'level) \<noteq> \<bottom>"
    shows "system_secure \<bottom> (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g) sched ls =
           (\<forall> g ls' g' sched' ls'' g'' sched'' ev.  mode_inputA g \<and> lock_modeeq t'' g \<and>
       global_meval_abv (ls, g, sched) (ls', g', sched') \<and>
       global_eval_abv (ls', g', sched') (ls'', g'', sched'') \<and>
       trace g'' = trace g' @ [ev] \<longrightarrow>
         event_secure \<bottom> (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g) ls g sched ls' g' sched' ev)"
  unfolding sys_secure_def2
  by simp

lemma decl_pred_from_unused:
  "\<D>\<^sub>p from to g' v c = \<D>\<^sub>p from' to g' v c"
  unfolding \<D>\<^sub>p_def
  by auto

text {* Like @{thm extensional} but with the predicate expanded *}
lemma extensional':
  assumes ls_def: "ls = [mk_loc_state t vd_fetch', 
                         mk_loc_state t' (vd_forward' t''), 
                         mk_loc_state t'' vd_toggle',
                         \<lparr>local_state.com = vd_hout' t''', tid = t'''\<rparr>,
                         \<lparr>local_state.com = vd_lout', tid = t''''\<rparr>]"
      and Ls: "\<L> ''mode'' = Some \<bottom>" "\<L> ''buf'' = None" "\<L> ''mode2'' = Some \<bottom>"
              "\<L> ''x'' = None" "\<L> ''valid'' = Some \<bottom>" "\<L> ''ldeclas'' = Some \<bottom>"
              "\<L> ''hbuf'' = Some \<top>"
              "\<L> ''lbuf'' = Some \<bottom>" "\<L> ''user_answer'' = Some \<bottom>"
      and neqs: "distinct [t, t', t'',t''',t'''']"
      and sane: "(\<top>::'level) \<noteq> \<bottom>"
      and sec: "system_secure \<bottom> (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g) sched ls"
    shows "(\<forall> g ls' g' sched' ls'' g'' sched'' ev.  mode_inputA g \<and> lock_modeeq t'' g \<and>
       global_meval_abv (ls, g, sched) (ls', g', sched') \<and>
       global_eval_abv (ls', g', sched') (ls'', g'', sched'') \<and>
       trace g'' = trace g' @ [ev] \<longrightarrow>
         (((\<forall> v e.
            (ev = Declassification \<bottom> v e \<longrightarrow>
              (v = last_output \<top> g' \<and> last_input \<bottom> g' = 1))) \<and>
     (not_declass_to \<bottom> ev \<longrightarrow>  
              uncertainty \<bottom> (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g) ls g sched (trace g') \<subseteq> 
              uncertainty \<bottom> (\<lambda>g. mode_inputA g \<and> lock_modeeq t'' g) ls g sched ((trace g')@[ev])))))"
  using sec
  unfolding sys_secure_def2 
  unfolding event_secure_no_lift[OF decl_pred_from_unused]
  apply clarify
  by (meson VDBuffer_Declass_Confirm.\<D>\<^sub>p_def \<open>\<And>sched' sched ls' ls l g' g ev P. event_secure l P ls g sched ls' g' sched' ev \<Longrightarrow> (\<forall>to v e. ev = Declassification to v e \<and> to \<le> l \<longrightarrow> VDBuffer_Declass_Confirm.\<D>\<^sub>p undefined to g' v (head_com (local_state.com (ls' ! next_thread (ls', g', sched'))))) \<and> (not_declass_to l ev \<longrightarrow> uncertainty l P ls g sched (trace g') \<subseteq> uncertainty l P ls g sched (trace g' @ [ev]))\<close> order_refl)

end

end
