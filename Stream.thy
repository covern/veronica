theory Stream
  imports Main
begin

type_synonym 'a stream = "nat \<Rightarrow> 'a"

definition lhd :: "'a stream \<Rightarrow> 'a" where
  "lhd S = S 0"

definition ltl :: "'a stream \<Rightarrow> 'a stream" where
  "ltl S n = S (Suc n)"

fun stream_cons :: "'a \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "stream_cons x s 0 = x" |
  "stream_cons x s (Suc n) = s n"


fun stream_append :: "'a list \<Rightarrow> 'a stream \<Rightarrow> 'a stream" where
  "stream_append [] s i = s i" |
  "stream_append (x # xs) s i = stream_cons x (stream_append xs s) i"

lemma stream_append_nonrec:
  "stream_append xs s n = (if n < length xs then xs ! n else s (n - length xs))"
  apply (induction xs arbitrary: s n)
   apply auto
  using less_Suc_eq_0_disj apply auto[1]
  by (metis (mono_tags, lifting) Suc_pred' diff_Suc_eq_diff_pred less_Suc_eq_0_disj linorder_neqE_nat not_less0 stream_cons.simps(2))

fun stream_take :: "nat \<Rightarrow> 'a stream \<Rightarrow> 'a list" where
  "stream_take 0 s = []" |
  "stream_take (Suc n) s = s 0 # stream_take n (s \<circ> Suc)"

lemma stream_take_nonrec:
  "stream_take n f = map f [0..<n]"
  apply (induction n arbitrary: f)
   apply auto
  by (smt le_less less_Suc_eq list.simps(8) list.simps(9) map_Suc_upt map_append map_map upt_Suc upt_rec zero_less_Suc)

lemma stream_cons_tail:
  "s = stream_cons (lhd s) (ltl s)"
  unfolding lhd_def ltl_def
  apply (rule ext)
  by (rename_tac i, induct_tac i, auto)

lemma stream_append_singleton:
  "stream_append (xs @ [v]) ys = stream_append xs (stream_cons v ys)"
  apply (induction xs, simp_all)
  apply (rule ext)
  by (rename_tac i, induct_tac i, auto)

lemma stream_take_len:
  "length (stream_take n s) = n"
  unfolding stream_take_nonrec by auto

lemma stream_take_append':
  "n \<le> length xs \<Longrightarrow> stream_take n (stream_append xs s) = take n xs"
proof -
  assume A: "n \<le> length xs"
  have "length (stream_take n (stream_append xs s)) = n"
    using stream_take_len by auto
  moreover have "length (take n xs) = n"
    using A length_take
    by auto
  ultimately have "length (stream_take n (stream_append xs s)) = length (take n xs)"
    by auto
  moreover have "\<And> i. i < n \<Longrightarrow> stream_take n (stream_append xs s) ! i = take n xs ! i"
    unfolding stream_take_nonrec stream_append_nonrec
    using A by auto
  ultimately show ?thesis
    by (simp add: nth_equalityI)
qed

lemma stream_take_append:
  "stream_take (length xs) (stream_append xs s) = xs"
  by (simp add: stream_take_append') 

end